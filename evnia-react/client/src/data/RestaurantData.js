// Import images
import americanCuisine from '../../src/assets/images/american-cuisine.jpg';
import israeliCuisine from '../../src/assets/images/israeli-cuisine.jpg';
import vietnameseCuisine from '../../src/assets/images/vietnamese-cuisine.jpg';
import greekCuisine from '../../src/assets/images/greek-cuisine.jpg';
import canadianCuisine from '../../src/assets/images/canadian-cuisine.jpg';
import iranianCuisine from '../../src/assets/images/iranian-cuisine.jpg';

// Restaurant data
const restaurantData = [
  {
    image: americanCuisine,
    restaurant: "Johnny G's Cafe",
    alpha_2_code: 'US',
    nationality: 'American',
    food_items: 'Fish and Chips, French Fries',
    frequency: 'Every Monday to Thursday',
    start_time: '8 AM',
    end_time: '4 PM',
    address: '478 Parliament St, Toronto, ON M4X 2P1',
  },
  {
    image: israeliCuisine,
    restaurant: 'A Yiddishe Mame',
    alpha_2_code: 'IL',
    nationality: 'Israeli',
    food_items: 'Pelmeni, Bourkeas, Mititei',
    frequency: 'Every Monday to Friday',
    start_time: '2 PM',
    end_time: '5 PM',
    address: '1416 Centre St, Thornhill, ON L4J 8A1',
  },
  {
    image: vietnameseCuisine,
    restaurant: 'Phosome',
    alpha_2_code: 'VN',
    nationality: 'Vietnamese',
    food_items: 'Banh Mi Bites, Sample Palm Caramel Bubble Tea, Root Salad',
    frequency: 'Daily',
    start_time: '2 PM',
    end_time: '8 PM',
    address: '482 Parliament St, Toronto, ON M4X 1P2',
  },
  {
    image: greekCuisine,
    restaurant: 'Anestis Taverna',
    alpha_2_code: 'GR',
    nationality: 'Greek',
    food_items: 'Beef and Lamb Gyro Sandwich, Chicken Stick, Mousaka',
    frequency: 'Every Monday to Friday',
    start_time: '2 PM',
    end_time: '5 PM',
    address: '526 Danforth Ave, Toronto, ON M4K 1P8',
  },
  {
    image: canadianCuisine,
    restaurant: 'Maple Leaf Sports Bar',
    alpha_2_code: 'CA',
    nationality: 'Canadian',
    food_items: 'Dumplings, Spring Roll, Piece of Fish, BBQ Chicken Steak',
    frequency: 'Every Monday and Thursday',
    start_time: '2 PM',
    end_time: '5 PM',
    address: '828 Danforth Ave, Toronto, ON M4J 1L6',
  },
  {
    image: iranianCuisine,
    restaurant: 'Herby',
    alpha_2_code: 'IR',
    nationality: 'Iranian',
    food_items: 'Kashe e Bademjan, Shirazi Salad, Koobideh, Loobia Polo',
    frequency: 'Every Monday',
    start_time: '3 PM',
    end_time: '6 PM',
    address: '397 Danforth Ave, Toronto, ON M4K 1P1',
  },
];

export default restaurantData;
