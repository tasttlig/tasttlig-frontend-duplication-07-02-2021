// Libraries
import React, { useContext } from 'react';
import { Route } from 'react-router-dom';
import { AppContext } from '../ContextProvider/AppProvider';

// Components
import Passport from '../components/Passport/Passport';

import PassportConfirmation from '../components/Passport/PassportConfirmationPage/PassportConfirmationPage';

const PassportRoutes = () => {
  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const userRole = appContext.state.user.role;

  return (
    <div>
      {userRole && (
        <div>
          <Route exact path={'/passport'} render={(props) => <Passport {...props} />} />
        </div>
      )}
    </div>
  );
};

export default PassportRoutes;
