// Libraries
import React from 'react';
import { Route } from 'react-router-dom';

// Components
import Apply from '../components/Apply/Apply';

const MemberUpgradeRoutes = () => {
  return (
    <div>
      <Route exact path={'/become-a-food-provider'} render={(props) => <Apply {...props} />} />
    </div>
  );
};

export default MemberUpgradeRoutes;
