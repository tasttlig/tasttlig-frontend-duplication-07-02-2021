// Libraries
import React from 'react';
import { Route } from 'react-router-dom';

// Components
import CompleteProfile from '../components/CompleteProfile/CompleteProfile';
import Profile from '../components/Profile/Profile';
import UserInfo from '../components/UserInfo/UserInfo';
import HostButtonApply from '../components/Apply/HostButtonApply';
import VendButtonApply from '../components/Apply/VendButtonApply';
import VendButtonFestival from '../components/Apply/VendButtonFestival';
import VendButtonFestivalTwo from '../components/Apply/VendButtonFestivalTwo';
import PassportConfirmation from '../components/UserInfo/PassportConfirmation';
import UserPassportPreference from '../components/Apply/UserPassportPreference';
import BusinessPassport from '../components/BusinessPassport/BusinessPassport';
import BusinessPassport2 from '../components/BusinessPassport/BusinessPassport2';
import BusinessPassportConfirmation from '../components/BusinessPassport/BusinessPassportConfirmation';
import CreateNewUser from '../components/Apply/CreateNewUser';
import Festivals from '../components/Festivals/Festivals';
// import EditAccount from "../components/Profile/EditAccount/EditAccount";
// import EditProfile from "../components/Profile/EditProfile/EditProfile";
//import UserPreferences from "../components/UserPreferences/UserPreferences";

const ProfileRoutes = () => {
  return (
    <div>
      <Route exact path={'/complete-profile'} render={(props) => <CompleteProfile {...props} />} />
      <Route exact path={'/user-info'} render={(props) => <UserInfo {...props} />} />
      <Route
        exact
        path={'/user-info/passport-confirmation'}
        render={(props) => <PassportConfirmation {...props} />}
      />
      <Route
        exact
        path={'/business-passport'}
        render={(props) => <BusinessPassport {...props} />}
      />
      <Route
        exact
        path={'/business-passport2'}
        render={(props) => <BusinessPassport2 {...props} />}
      />
      <Route
        exact
        path={'/business-passport-confirmation'}
        render={(props) => <BusinessPassportConfirmation {...props} />}
      />
      <Route
        exact
        path={'/complete-profile/business'}
        render={(props) => <HostButtonApply {...props} />}
      />
      <Route
        exact
        path={'/complete-profile/preference'}
        render={(props) => <UserPassportPreference {...props} />}
      />
      <Route
        exact
        path={'/complete-profile/welcome-form'}
        render={(props) => <VendButtonApply {...props} />}
      />
      <Route
        exact
        path={'/complete-profile/festival-vendor'}
        render={(props) => <VendButtonFestival {...props} />}
      />
      <Route
        exact
        path={'/complete-profile/festival-vendor-2'}
        render={(props) => <VendButtonFestivalTwo {...props} />}
      />
      <Route
        exact
        path={'/complete-profile/new-user'}
        render={(props) => <CreateNewUser {...props} />}
      />
      <Route exact path={'/passport/:id'} render={(props) => <Profile {...props} />} />

      {/* <Route
        exact
        path={"/account"}
        render={props => <EditAccount {...props} />}
      />
      <Route
        exact
        path={"/account/profile"}
        render={props => <EditProfile {...props} />}
      /> 
      <Route
        exact
        path={"/user-preferences"}
        render={(props) => <UserPreferences {...props} />}
      />*/}
    </div>
  );
};

export default ProfileRoutes;
