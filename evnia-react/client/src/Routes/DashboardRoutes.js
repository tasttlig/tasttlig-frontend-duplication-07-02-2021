// Libraries
import React, { useContext } from 'react';
import { Route } from 'react-router-dom';
import { AppContext } from '../ContextProvider/AppProvider';

// Components
import Dashboard from '../components/Dashboard/Dashboard';
import Order from '../components/Dashboard/Order/Order';
import Ticket from '../components/Dashboard/Ticket/Ticket';
import MemberClaimedFood from '../components/Dashboard/MemberClaimedFood/MemberClaimedFood';
import FestivalReservation from '../components/Dashboard/FestivalReservation/FestivalReservations';
import FestivalDashboard from '../components/Dashboard/Festival/FestivalDashboard';
import HostRedeem from '../components/Dashboard/HostRedeemFood/HostRedeem';
import BusinessDashboard from '../components/Dashboard/BusinessDashboard/BusinessDashboard';
// import UserSpecials from "../components/Specials/UserSpecials";

const DashboardRoutes = () => {
  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const userRole = appContext.state.user.role;

  return (
    <div>
      <Route exact path={'/dashboard'} render={(props) => <Dashboard {...props} />} />
      {/* {userRole &&
        (userRole.includes("ADMIN") ||
          userRole.includes("RESTAURANT") ||
          userRole.includes("RESTAURANT_PENDING")) && (
          <div>
            <Route
              exact
              path={"/dashboard/festivals"}
              render={(props) => <FestivalDashboard {...props} />}
            />
            <Route
              exact
              path={"/dashboard/specials"}
              render={(props) => <UserSpecials {...props} />}
            />
          </div>
        )} */}

      {userRole &&
        userRole.includes('HOST') &&
        !userRole.includes('HOST_PENDING') &&
        !userRole.includes('ADMIN') &&
        !userRole.includes('RESTAURANT') &&
        !userRole.includes('RESTAURANT_PENDING') &&
        !userRole.includes('SPONSOR') &&
        !userRole.includes('SPONSOR_PENDING') &&
        !userRole.includes('VENDOR') &&
        !userRole.includes('VENDOR_PENDING') && (
          <div>
            <Route
              exact
              path={'/dashboard/my-redeem-food'}
              render={(props) => <HostRedeem {...props} />}
            />
          </div>
        )}

      {userRole &&
        !userRole.includes('HOST') &&
        !userRole.includes('HOST_PENDING') &&
        !userRole.includes('ADMIN') &&
        !userRole.includes('RESTAURANT') &&
        !userRole.includes('RESTAURANT_PENDING') &&
        !userRole.includes('SPONSOR') &&
        !userRole.includes('SPONSOR_PENDING') &&
        !userRole.includes('VENDOR') &&
        !userRole.includes('VENDOR_PENDING') && (
          <div>
            {/* {/* <Route
              exact
              path={"/dashboard/orders"}
              render={(props) => <Order {...props} />}
            /> */}
            <Route exact path={'/dashboard/tickets'} render={(props) => <Ticket {...props} />} />
            <Route
              exact
              path={'/dashboard/reservations'}
              render={(props) => <FestivalReservation {...props} />}
            />
            <Route
              exact
              path={'/dashboard/my-claimed-food'}
              render={(props) => <MemberClaimedFood {...props} />}
            />
          </div>
        )}
      {/*sponsor dashboard route*/}
      {userRole && userRole.includes('SPONSOR') && (
        <>
          <div>
            <Route
              exact
              path={'/dashboard/business'}
              render={(props) => <BusinessDashboard {...props} />}
            />
          </div>
        </>
      )}
    </div>
  );
};

export default DashboardRoutes;
