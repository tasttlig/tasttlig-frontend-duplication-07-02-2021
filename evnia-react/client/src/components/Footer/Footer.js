// Libraries
import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import { AppContext } from '../../ContextProvider/AppProvider';
// Styling
import './Footer.scss';

const Footer = (props) => {
  const appContext = useContext(AppContext);
  const userRole = appContext.state.user.role;
  return (
    <footer>
      <div className="row">
        <div className="container footer-content">
          <div className="col footer-contact">
            <h3 className="footer-heading">Contact</h3>
            <a
              target="_blank"
              href="https://www.google.com/maps/place/Tasttlig/@43.6599131,-79.3641913,17z/data=!3m1!4b1!4m5!3m4!1s0x89d4cbb168337463:0xc48ec33e02ee9ed0!8m2!3d43.6599092!4d-79.3620026"
            >
              <div className="footer-address">585 Dundas St. East, 3rd Floor.</div>
              <div className="footer-address">Toronto Ontario. M5A 2B7</div>
            </a>
            <div className="footer-phone">
              <a target="_blank" href="tel:4166397499">
                ‭+1 (416) 639-7499
              </a>
            </div>
            <div className="footer-email">
              <a target="_blank" href="mailto:support@tasttlig.com">
                support@tasttlig.com
              </a>
            </div>
          </div>
          <div className="col footer-follow">
            <div className="row footer__right charity">
              {userRole && !userRole.includes("BUSINESS_MEMBER_PENDING") && !userRole.includes("BUSINESS_MEMBER")? 
              (<Link exact="true" to="/business-passport"> 
              <button className="apply-button">Add Your Restaurant</button>
            </Link>) : null}
            {/* {!userRole ? ( 
            <>
            <h3 className="footer-heading">Do Business with us</h3>
            <Link exact="true" to="/sign-up" onClick={() => localStorage.setItem('signUp', 'Guest')} >
                <button className="sponsor-button-charity">Add your business</button>
              </Link>
               <Link exact="true" to="/host-landing"  >
               <button className="sponsor-button-charity">Add your Restaurant</button>
             </Link>
             </>
              ): userRole && (!userRole.includes('BUSINESS_MEMBER_PENDING') && !userRole.includes('BUSINESS_MEMBER') && !userRole.includes('HOST') && !userRole.includes('ADMIN') ) ? ( 
                <>
                <h3 className="footer-heading">Do Business with us</h3>
              <Link exact="true" to="/business-passport">
                <button className="sponsor-button-charity">Add your business</button>
              </Link>
              <Link exact="true" to="/business-passport" onClick={() => localStorage.setItem('dataFromFestival', 'Host')}>
                <button className="sponsor-button-charity">Add your Restaurant</button>
              </Link>
              </>
              ) : userRole && ( !userRole.includes('HOST') && !userRole.includes('ADMIN') && (userRole.includes('BUSINESS_MEMBER_PENDING') || userRole.includes('BUSINESS_MEMBER')) ) ? ( 
                <>
                 <h3 className="footer-heading">Do Business with us</h3>
            <Link exact="true" to="/business-passport" onClick={() => localStorage.setItem('dataFromFestival', 'Host')}>
                <button className="sponsor-button-charity">Add your Restaurant</button>
              </Link>
              </>
              ) : null } */}
             
              {/* {!userRole ? ( ): userRole && ( !userRole.includes('HOST') && !userRole.includes('ADMIN') ) ? ( 
            <Link exact="true" to="/business-passport">
                <button className="sponsor-button-charity">Add your Restaurant</button>
              </Link>) : null } */}

            </div>
            <div className="row footer__right follow">
              <h3 className="footer-heading">Follow Us</h3>
              <div className="social-icons">
                <a target="_blank" href="https://www.instagram.com/tasttlig">
                  <i className="fab fa-instagram"></i>
                </a>
                <a target="_blank" href="https://www.facebook.com/tasttlig">
                  <i className="fab fa-facebook"></i>
                </a>
                <a target="_blank" href="https://www.twitter.com/tasttlig1">
                  <i className="fab fa-twitter"></i>
                </a>
                <a target="_blank" href="https://www.linkedin.com/company/tasttlig/about">
                  <i className="fab fa-linkedin-in"></i>
                </a>
                <a target="_blank" href="https://www.youtube.com/channel/UCMlJcL7dEwAkGBbMvgbjYVQ">
                  <i className="fab fa-youtube"></i>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="row footer-copyright">
        <strong>&copy; Tasttlig, 2021.</strong> &nbsp; All Rights Reserved.
      </div>
    </footer>
  );
};

export default Footer;
