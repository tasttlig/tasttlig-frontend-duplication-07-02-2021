// Libraries
import React, { useState } from 'react';
import axios from 'axios';
// import { Progress } from "react-sweet-progress";

// Components
import { CheckboxGroup, Form, Input, Select } from '../../../EasyForm';

// Styling
// import "react-sweet-progress/lib/style.css";

const PromotionsForm = (props) => {
  const { nextStep, prevStep, update, values, readMode } = props;

  const [festivalParticipant, setFestivalParticipant] = useState(
    values.festival_participant === 'yes',
  );
  const [samplesAvailable, setSamplesAvailable] = useState(values.samples_available === 'yes');

  const updateMenuItem = async (data) => {
    const response = await axios({
      method: 'PUT',
      url: '/user/updateMenuItem',
      data: {
        ...data,
        email: values.email,
      },
    });

    return response.data;
  };

  const onSubmit = async (data) => {
    const menuList = values.menu_list;

    menuList.forEach((m) => {
      m.include_in_festival = false;
      m.samples_per_day = 0;

      if (data.samples && data.samples.includes(m.id.toString())) {
        m.include_in_festival = true;
        m.samples_per_day = data.samples_per_day;
      }
    });

    update({
      ...data,
      menu_list: menuList,
    });
    await updateMenuItem(menuList);
    nextStep();
  };

  return (
    <div>
      {/* {!readMode && <Progress />} */}
      <div className={!readMode ? 'apply-to-host' : ''}>
        {!readMode ? (
          <h1 className="apply-to-host-step-name">Promotions</h1>
        ) : (
          <>
            <h6>Promotions</h6>
            <hr />
          </>
        )}

        <Form data={values} onSubmit={onSubmit} readMode={readMode}>
          <Select
            name="festival_participant"
            label="Do you want to be part of the festival?"
            onChange={(e) => setFestivalParticipant(e.target.value === 'yes')}
          >
            <option value="yes">Yes</option>
            <option value="no">No</option>
          </Select>

          {festivalParticipant && (
            <>
              <Select
                name="samples_available"
                label="Are you prepared to give free samples?"
                onChange={(e) => setSamplesAvailable(e.target.value === 'yes')}
                required
              >
                <option value="yes">Yes</option>
                <option value="no">
                  No (Answering no, means you are not able to participate in the festival)
                </option>
              </Select>

              {samplesAvailable && (
                <>
                  <Input
                    type="number"
                    step="1"
                    name="samples_per_day"
                    label="How many can you give per day?"
                    required
                  />

                  <CheckboxGroup
                    name="samples"
                    label="Which of the following menu items will you like to promote?"
                    options={values.menu_list.map((m) => [m.menuName, m.id.toString()])}
                    required
                  />
                </>
              )}
            </>
          )}

          {!readMode && (
            <div className="apply-to-host-navigation">
              <span onClick={prevStep} disabled={values.submitAuthDisabled} className="back-btn">
                Back
              </span>
              <input
                type="submit"
                value="Continue"
                disabled={values.submitAuthDisabled}
                className=" continue-btn"
              />
            </div>
          )}
        </Form>
      </div>
    </div>
  );
};

export default PromotionsForm;
