// Libraries
import React from 'react';
import axios from 'axios';
// import { Progress } from "react-sweet-progress";

// Components
import { Form, CheckboxGroup } from '../../../EasyForm';

// Styling
// import "react-sweet-progress/lib/style.css";

const ServicesForm = (props) => {
  const { nextStep, prevStep, update, values, readMode } = props;

  const updateServices = async (data) => {
    const response = await axios({
      method: 'PUT',
      url: '/user/updateServices',
      data: {
        ...data,
        email: values.email,
      },
    });

    return response.data;
  };

  const onSubmit = async (data) => {
    update(data);
    await updateServices(data);
    nextStep();
  };

  const serviceOptions = () => {
    if (values.business_category === 'Food') {
      return [
        ['Catering', 'catering'],
        ['Event Planning', 'eventPlanning'],
        ['Delivery', 'delivery'],
      ];
    } else if (values.business_category === 'Entertainment') {
      return [
        ['Party Services', 'partyServices'],
        ['In-home Entertainment', 'inHomeEntertainment'],
      ];
    } else if (values.business_category === 'Venues') {
      return [
        ['Full Service', 'fullService'],
        ['Hosting Services', 'hostingServices'],
        ['Wait Staff', 'waitStaff'],
        ['Cleaning Services', 'cleaningServices'],
      ];
    } else if (values.business_category === 'Transportation') {
      return [
        ['Pickup Services', 'pickupServices'],
        ['Drop-off Services', 'dropoffServices'],
        ['Delivery Services', 'deliveryServices'],
        ['Scheduled-pickup Services', 'scheduledPickupServices'],
      ];
    } else if (values.business_category === 'Party Suppliers') {
      return [
        ['Pickup Services', 'pickupServices'],
        ['Delivery Services', 'deliveryServices'],
        ['Inflating Services', 'inflatingServices'],
        ['Decoration Services', 'decorationServices'],
        ['Event Organizer', 'eventOrganizer'],
      ];
    }
  };

  return (
    <div>
      {/* {!readMode && <Progress />} */}
      <div className={!readMode ? 'apply-to-host' : ''}>
        {!readMode ? (
          <h1 className="apply-to-host-step-name">Services</h1>
        ) : (
          <>
            <h6>Services</h6>
            <hr />
          </>
        )}

        <Form data={values} onSubmit={onSubmit} readMode={readMode}>
          <CheckboxGroup
            name="services"
            required={true}
            label="What are the services you provide"
            options={serviceOptions()}
          />

          {!readMode && (
            <div className="apply-to-host-navigation">
              <span onClick={prevStep} disabled={values.submitAuthDisabled} className="back-btn">
                Back
              </span>
              <input
                type="submit"
                value="Continue"
                disabled={values.submitAuthDisabled}
                className=" continue-btn"
              />
            </div>
          )}
        </Form>
      </div>
    </div>
  );
};

export default ServicesForm;
