// Libraries
import React, { useState, useEffect } from 'react';
// import { Progress } from "react-sweet-progress";

// Components
import { Form, Input } from '../../../EasyForm';

// Styling
// import "react-sweet-progress/lib/style.css";

const EntertainersProductForm = (props) => {
  const { values, prevStep, updateSampleLinks, readMode } = props;

  const [links] = useState(
    values.sample_links
      .map((l, i) => ({ [`link_${i + 1}`]: l }))
      .reduce(
        (a, b) => ({
          ...a,
          ...b,
        }),
        {},
      ),
  );

  const [link1, setLink1] = useState('');
  const [link2, setLink2] = useState('');
  const [link3, setLink3] = useState('');

  const onSubmit = (data) => {
    const samples = [link1, link2, link3].filter((l) => l && l.trim());

    updateSampleLinks(samples);
  };

  useEffect(() => {
    if (values.sample_links.length > 0) {
      setLink1(values.sample_links[0]);
    }

    if (values.sample_links.length > 1) {
      setLink2(values.sample_links[1]);
    }

    if (values.sample_links.length > 2) {
      setLink3(values.sample_links[2]);
    }
  }, []);

  return (
    <div>
      {/* {!readMode && <Progress />} */}
      {!readMode ? (
        <h1 className="apply-to-host-step-name">Add up to 3 sample links to your music/video</h1>
      ) : (
        <>
          <h6>Sample music/video links</h6>
          <hr />
        </>
      )}

      <Form data={links} onSubmit={onSubmit} readMode={readMode}>
        <Input
          name="link_1"
          label="Sample Link 1"
          onChange={(e) => setLink1(e.target.value)}
          required
        />

        {link1 && link1.trim() ? (
          <Input name="link_2" label="Sample Link 2" onChange={(e) => setLink2(e.target.value)} />
        ) : null}

        {link2 && link2.trim() ? (
          <Input name="link_3" label="Sample Link 3" onChange={(e) => setLink3(e.target.value)} />
        ) : null}

        {!readMode && (
          <div className="apply-to-host-navigation">
            <span onClick={prevStep} disabled={values.submitAuthDisabled} className="back-btn">
              Back
            </span>
            <button type="submit" disabled={values.submitAuthDisabled} className="continue-btn">
              Continue
            </button>
          </div>
        )}
      </Form>
    </div>
  );
};

export default EntertainersProductForm;
