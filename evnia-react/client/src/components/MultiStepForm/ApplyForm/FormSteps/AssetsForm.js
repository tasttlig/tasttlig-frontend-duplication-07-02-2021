// Libraries
import React, { useState, useEffect } from 'react';
import axios from 'axios';
// import { Progress } from "react-sweet-progress";

// Components
import {
  CheckboxGroup,
  DateInput,
  Form,
  Input,
  MultiImageInput,
  Select,
  Textarea,
} from '../../../EasyForm';

// Styling
// import "react-sweet-progress/lib/style.css";

const AssetsForm = (props) => {
  const { nextStep, prevStep, update, values, readMode } = props;

  const [hasAssets, setHasAssets] = useState(values.has_assets === 'yes');
  const [assets, setAssets] = useState(
    values.assets.map((a) => ({
      ...a,
      has_assets: values.has_assets,
    })),
  );
  const [selectedAsset, setSelectedAsset] = useState({
    id: assets.length + 1,
    has_assets: values.has_assets,
  });

  const updateAssets = async (data) => {
    const response = await axios({
      method: 'PUT',
      url: '/user/updateAssets',
      data: {
        ...data,
        email: values.email,
      },
    });

    return response.data;
  };

  useEffect(() => {
    if (!hasAssets) {
      setAssets([]);
    } else {
      values.has_assets = 'yes';
    }
  }, [hasAssets]);

  const onSubmit = (data) => {
    window.scroll(0, 0);

    let list = assets;

    if (data.id) {
      list = [...list.filter((m) => parseInt(m.id) !== parseInt(data.id)), data];
    } else {
      data.id = assets.length + 1;
      list.push(data);
    }

    setAssets(list);
    setSelectedAsset({
      has_assets: values.has_assets,
    });
  };

  const next = async () => {
    update({
      has_assets: hasAssets ? 'yes' : 'no',
      assets,
    });
    await updateAssets(assets);
    nextStep();
  };

  const removeAsset = (asset) => {
    const list = assets.filter((a) => a.id !== asset.id);

    setAssets(list);
  };

  const editAsset = (a) => {
    setSelectedAsset(a);
  };

  return (
    <div>
      {/* {!readMode && <Progress />} */}
      <div className={!readMode ? 'apply-to-host' : ''}>
        {!readMode ? (
          <h1 className="apply-to-host-step-name">Monetize Your Assets</h1>
        ) : (
          <>
            <h6>Monetize Your Assets</h6>
            <hr />
          </>
        )}

        {!readMode ? (
          <Form data={selectedAsset} onSubmit={onSubmit} readMode={readMode}>
            <div className="row">
              <div className="col-12 px-0">
                {/* <Input name="id" type="hidden" /> */}

                <Select
                  name="has_assets"
                  label="Do you have any assets you want to monetize?"
                  onChange={() => setHasAssets(!hasAssets)}
                >
                  <option value="yes">Yes</option>
                  <option value="no">No</option>
                </Select>

                {hasAssets && (
                  <>
                    <Input name="name" label="Name" required />
                    <MultiImageInput
                      dropbox_label="Click or drag-and-drop to upload one or more images"
                      name="images"
                      label="Images"
                      required
                    />

                    <Select name="asset_type" label="Type" required>
                      <option value="">--Select--</option>
                      <option value="Dining Room">Dining Room</option>
                      <option value="Patio">Patio</option>
                      <option value="Commercial Kitchen">Commercial Kitchen</option>
                      <option value="Rooftop">Rooftop</option>
                      <option value="Venue">Venue</option>
                    </Select>

                    <CheckboxGroup
                      name="asset_availability"
                      label="Availability"
                      required
                      options={[
                        ['Monday', 'monday'],
                        ['Tuesday', 'tuesday'],
                        ['Wednesday', 'wednesday'],
                        ['Thursday', 'thursday'],
                        ['Friday', 'friday'],
                        ['Saturday', 'saturday'],
                        ['Sunday', 'sunday'],
                      ]}
                    />

                    <div className="row">
                      <div className="col-md-6 col-sm-12 asset-start-time">
                        <DateInput
                          name="asset_start_time"
                          label="From"
                          showTimeSelect
                          showTimeSelectOnly
                          dateFormat="h:mm aa"
                          required
                        />
                      </div>
                      <div className="col-md-6 col-sm-12 asset-end-time">
                        <DateInput
                          name="asset_end_time"
                          label="To"
                          showTimeSelect
                          showTimeSelectOnly
                          dateFormat="h:mm aa"
                          required
                        />
                      </div>
                    </div>

                    <Input name="asset_price" label="Price" type="number" step="0.01" required />
                    <Textarea name="description" label="Description" required />
                    <Input name="asset_contact" label="Contact Name" />
                    <Input name="asset_contact_phone" label="Contact Phone Number" />

                    {hasAssets && (
                      <button type="submit" className="add-asset-btn">
                        Add Asset
                      </button>
                    )}
                  </>
                )}
              </div>
              {assets.length ? (
                <div className="col-12 px-0">
                  {assets.map((a) => (
                    <div className="menu-items-form__item" key={a.name}>
                      <img className="menu-items-form__img" src={a.images[0]} alt={a.name} />
                      <div className="menu-items-form__ctrl">
                        <div className="menu-items-form__ctrl__title">{a.name}</div>
                        <div className="menu-items-form__ctrl__actions">
                          <i className="fa fa-edit" title="edit" onClick={() => editAsset(a)} />
                          <span> | </span>
                          <i
                            className="fa fa-times"
                            title="remove"
                            onClick={() => removeAsset(a)}
                          />
                        </div>
                      </div>
                    </div>
                  ))}
                </div>
              ) : null}
            </div>

            {!readMode && (
              <div className="apply-to-host-navigation">
                <span onClick={prevStep} disabled={values.submitAuthDisabled} className="back-btn">
                  Back
                </span>
                <div>
                  <input
                    type="submit"
                    value="Continue"
                    onClick={next}
                    disabled={values.submitAuthDisabled}
                    className="continue-btn"
                  />
                </div>
              </div>
            )}
          </Form>
        ) : (
          <div className="menu-items-read-mode">
            {assets.map((a) => (
              <div className="menu-items-form__item" key={a.name}>
                <img className="menu-items-form__img" src={a.images[0]} alt={a.name} />
                <div className="menu-items-form__ctrl">
                  <div className="menu-items-form__ctrl__title">{a.name}</div>
                </div>
              </div>
            ))}
          </div>
        )}
      </div>
    </div>
  );
};

export default AssetsForm;
