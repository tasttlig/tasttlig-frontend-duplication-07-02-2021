// Libraries
import React, { useEffect } from 'react';
import styled from 'styled-components';

const Start = (props) => {
  const { nextStep } = props;

  const lmOptions = [
    {
      name: 'Homes',
      image: 'https://s3.us-east-2.amazonaws.com/content.tasttlig.com/apply-images/homes.jpg',
      link: '/homes',
    },
    {
      name: 'Restaurants',
      image: 'https://s3.us-east-2.amazonaws.com/content.tasttlig.com/apply-images/restaurants.jpg',
      link: '/restaurants',
    },
    {
      name: 'Offices',
      image: 'https://s3.us-east-2.amazonaws.com/content.tasttlig.com/apply-images/offices.jpg',
      link: '/offices',
    },
    {
      name: 'Schools',
      image: 'https://s3.us-east-2.amazonaws.com/content.tasttlig.com/apply-images/schools.jpg',
      link: '/schools',
    },
    {
      name: 'Outdoors',
      image: 'https://s3.us-east-2.amazonaws.com/content.tasttlig.com/apply-images/outdoors.jpg',
      link: '/outdoors',
    },
    {
      name: 'Online',
      image: 'https://s3.us-east-2.amazonaws.com/content.tasttlig.com/apply-images/online.jpg',
      link: '/online',
    },
  ];

  const Container = styled.div`
    margin-top: 80px;
  `;

  const BoxImage = styled.img`
    height: 317px;
    width: 100%;
    display: block;
    background-position: center;
    background-size: cover;
  `;

  const Label = styled.h4`
    height: 56px;
    overflow: hidden;
  `;

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <>
      <section className="jumbotron text-center mb-0">
        <Container className="container p-0">
          <h1 className="jumbotron-heading">Apply To Host</h1>
          <p className="lead text-muted">
            Tasttlig Host makes guaranteed $2,000/month hosting nationally indexed culinary
            experiences.
          </p>
          <button onClick={nextStep} className="get-started-btn">
            Get Started
          </button>
        </Container>
      </section>
      <div className="album py-5 bg-light">
        <div className="container p-0">
          <div className="row">
            {lmOptions.map((lmo) => (
              <div key={lmo.name} className="col-lg-4">
                <div className="card mb-4 box-shadow">
                  <BoxImage
                    className="card-img-top"
                    style={{ backgroundImage: `url(${lmo.image})` }}
                  />
                  <div className="card-body">
                    <Label className="text-center" title={lmo.name}>
                      {lmo.name}
                    </Label>
                    {/* <div className="d-flex justify-content-between align-items-center">
                      <div className="btn-group btn-block">
                        <Link to={lmo.link} className="btn btn-sm btn-secondary">Learn More</Link>
                      </div>
                    </div> */}
                  </div>
                </div>
              </div>
            ))}
          </div>
          <div className="row justify-content-center">
            <div className="col-12 col-md-10 col-lg-8 text-center">
              <p className="lead text-gray-700 mt-6">
                Tasttlig Host makes money by showcasing their talent, products and services hosting
                experiences for their guests.
              </p>
              <h1 className="mt-6 mb-3">How It Works</h1>
            </div>
          </div>
          <div className="row mb-3 text-center">
            <div className="col-lg-4">
              <div className="card mb-4 box-shadow">
                <div className="card-body">
                  <h1 className="card-title pricing-card-title">Apply To Host</h1>
                </div>
              </div>
            </div>
            <div className="col-lg-4">
              <div className="card mb-4 box-shadow">
                <div className="card-body">
                  <h1 className="card-title pricing-card-title">Create Experiences</h1>
                </div>
              </div>
            </div>
            <div className="col-lg-4">
              <div className="card mb-4 box-shadow">
                <div className="card-body">
                  <h1 className="card-title pricing-card-title">Host Experiences</h1>
                </div>
              </div>
            </div>
          </div>
          <div className="row justify-content-center">
            <div className="col-12 col-md-10 col-lg-8 text-center mt-6">
              <button onClick={nextStep} className="get-started-btn">
                Get Started
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Start;
