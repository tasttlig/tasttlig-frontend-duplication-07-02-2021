// Libraries
import React, { Component } from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import { AppContext } from '../../../ContextProvider/AppProvider';

// Components
import PersonalInfoForm from './FormSteps/PersonalInfoForm';
import BusinessInfoForm from './FormSteps/BusinessInfoForm';
import DocumentForm from './FormSteps/DocumentForm';
import PaymentInfoForm from './FormSteps/PaymentInfoForm';
import SocialProofForm from './FormSteps/SocialProofForm';
import ApplyToHostForm from './FormSteps/ApplyToHostForm';
import FormReview from './FormSteps/FormReview';
import ServicesForm from './FormSteps/ServicesForm';
import ProductForm from './FormSteps/ProductForm';
import Start from './FormSteps/Start';
import ResidentialAddressForm from './FormSteps/ResidentialAddressForm';
import ApplyToCookForm from './FormSteps/ApplyToCook';
import AssetsForm from './FormSteps/AssetsForm';
import PromotionsForm from './FormSteps/PromotionsForm';

toast.configure();

export default class ApplyForm extends Component {
  static contextType = AppContext;

  constructor(props) {
    super(props);

    const currentTime = new Date();
    currentTime.setHours(currentTime.getHours() + 2);

    // Set initial state
    this.state = {
      step: 0,

      // personal information form fields
      first_name: '',
      last_name: '',
      email: '',
      phone_number: '',
      residential_address_line_1: '',
      residential_address_line_2: '',
      residential_city: '',
      residential_state: '',
      residential_postal_code: '',
      residential_country: 'CANADA',

      // business information form fields
      has_business: 'no',
      business_category: '',
      service_provider: '',
      use_residential: false,
      business_name: '',
      business_type: '',
      culture: '',
      address_line_1: '',
      address_line_2: '',
      business_city: '',
      state: '',
      postal_code: '',
      country: '',
      registration_number: '',
      facebook: '',
      instagram: '',
      transportation_rate: 0,

      // services form fields
      services: [],

      // documents form fields
      food_handler_certificate: '',
      food_handler_certificate_date_of_issue: '',
      food_handler_certificate_date_of_expired: '',
      dine_safe_certificate: '',
      dine_safe_certificate_date_of_issue: '',
      dine_safe_certificate_date_of_expired: '',
      health_safety_certificate: '',
      health_safety_certificate_date_of_issue: '',
      health_safety_certificate_date_of_expired: '',
      insurance: '',
      insurance_date_of_issue: '',
      insurance_date_of_expired: '',
      government_id: '',
      government_id_date_of_issue: '',
      government_id_date_of_expired: '',

      // assets form fields
      has_assets: 'no',
      assets: [],

      // promotions form fields
      festival_participant: 'no',
      samples_available: 'yes',
      samples_per_day: 1,
      samples: [],

      // payment information form fields
      banking: '',
      bank_number: '',
      account_number: '',
      institution_number: '',
      void_cheque: '',
      online_email: '',
      paypal_email: '',
      stripe_account: '',

      // social proof form fields
      yelp_review: '',
      google_review: '',
      tripadvisor_review: '',
      instagram_review: '',
      facebook_review: '',
      youtube_review: '',
      personal_review: '',
      media_recognition: '',

      // host form fields
      is_host: 'no',
      host_selection: '',
      host_selection_resume: '',
      host_selection_video: '',
      host_youtube_link: '',

      // cook form fields
      is_cook: 'no',
      cook_selection: '',
      cook_selection_resume: '',
      cook_selection_video: '',
      cook_youtube_link: '',

      // product form fields
      menu_list: [],
      sample_links: [],
      venue_name: '',
      venue_description: '',
      venue_photos: [],

      submitAuthDisabled: false,
    };
  }

  UNSAFE_componentWillMount() {
    const hostFormData = JSON.parse(localStorage.getItem('hostFormData'));

    if (hostFormData) {
      Object.keys(this.state).forEach((key) => {
        const value = hostFormData[key];

        if (value) {
          this.setState({
            [key]: value,
          });
        }
      });
    }
  }

  // Proceed to next step
  nextStep = () => {
    window.scrollTo(0, 0);

    this.setState({
      step: this.state.step + 1,
    });
  };

  // Go back to prev step
  prevStep = () => {
    window.scrollTo(0, 0);

    this.setState({ step: this.state.step - 1 });
  };

  update = (data) => {
    const hostFormData = JSON.parse(localStorage.getItem('hostFormData')) || {};

    localStorage.setItem('hostFormData', JSON.stringify({ ...hostFormData, ...data }));

    Object.keys(data).forEach((key) => {
      this.setState({ [key]: data[key] });
    });
  };

  // Submit Become a Food Provider form helper function
  handleSubmitApplication = async (event) => {
    event.preventDefault();

    const url = '/user/host';

    const { submitAuthDisabled, use_residential, ...data } = this.state;

    try {
      const response = await axios({ method: 'POST', url, data });

      if (response && response.data && response.data.success) {
        localStorage.removeItem('hostFormData');

        setTimeout(() => {
          window.location.href = '/dashboard';
        }, 2000);

        toast(`Success! Thank you for submitting your application!`, {
          type: 'success',
          autoClose: 2000,
        });

        this.setState({ submitAuthDisabled: true });
      }
    } catch (error) {
      toast('Error! Something went wrong!', {
        type: 'error',
        autoClose: 2000,
      });
    }
  };

  getSteps = () => {
    let steps = [Start];

    if (!this.context.state.signedInStatus) {
      steps.push(PersonalInfoForm);
    }

    steps = [...steps, ResidentialAddressForm, BusinessInfoForm];

    if (
      this.state.has_business === 'yes' &&
      this.state.business_category !== 'Party Suppliers' &&
      this.state.business_category !== 'Transportation'
    ) {
      steps.push(ProductForm);

      if (this.state.service_provider === 'Restaurant') {
        steps.push(PromotionsForm);
        steps.push(AssetsForm);
      }

      if (this.state.business_category !== 'MC') {
        steps.push(ServicesForm);
      }
    }

    steps = [...steps, ApplyToHostForm, ApplyToCookForm, PaymentInfoForm];

    if (
      this.state.business_category === 'Food' ||
      this.state.business_category === 'Transportation'
    ) {
      steps.push(DocumentForm);
    }

    steps = [...steps, SocialProofForm, FormReview];

    return steps;
  };

  componentDidMount = () => {
    window.scrollTo(0, 0);
  };

  render = () => {
    const { step, ...values } = this.state;
    const Step = this.getSteps()[step];

    return (
      <Step
        nextStep={this.nextStep}
        prevStep={this.prevStep}
        update={this.update}
        values={values}
        handleSubmitApplication={this.handleSubmitApplication}
      />
    );
  };
}
