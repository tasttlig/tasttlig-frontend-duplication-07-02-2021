// Libraries
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

// Styling
import './EmailVerification.scss';
import logo from '../../assets/images/tasttlig-logo-black.png';

const EmailVerification = (props) => {
  console.log("props coming from email verification:", props)
  // Set initial state
  const [load, setLoad] = useState(false);
  const [confirmed, setConfirmed] = useState(false);

  // Verify email helper function
  const getVerified = async () => {
    try {
      const token = props.match.params.token;
      const response = await axios.get(`/user/confirmation/${token}`);
      console.log('TOKEN', token);
      setTimeout(() => {
        window.location.href = '/';
      }, 2000);

      response.data.success && setConfirmed(true);
      console.log('EMAIL VERIFICATION RESPONSE', response);
    } catch (error) {
      console.log(error);
    }
  };

  // Mount Email Verification page
  useEffect(() => {
    window.scrollTo(0, 0);

    getVerified();
  }, []);

  // Render Email Verification page
  return (
    <div className="email-verification">
      {confirmed ? (
        <div>
          <div>
            <Link to="/">
              <img
                src={logo}
                alt="Tasttlig"
                onLoad={() => setLoad(true)}
                className={load ? 'email-verification-tasttlig-logo' : 'loading-image'}
              />
            </Link>
          </div>
          <div className="email-verification-statement">
            Congratulations! Your account is confirmed.
          </div>
        </div>
      ) : (
        <div>
          <div>
            <Link to="/">
              <img
                src={logo}
                alt="Tasttlig"
                onLoad={() => setLoad(true)}
                className={load ? 'email-verification-tasttlig-logo' : 'loading-image'}
              />
            </Link>
          </div>
          <div className="email-verification-statement">
            Sorry! Your account could not be confirmed.
          </div>
          <div className="email-verification-statement">Please try again.</div>
        </div>
      )}
    </div>
  );
};

export default EmailVerification;
