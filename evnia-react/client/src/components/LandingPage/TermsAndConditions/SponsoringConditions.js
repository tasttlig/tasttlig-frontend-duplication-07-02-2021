import React, { useEffect, useContext, useState } from 'react';
import { Fragment } from 'react';
import { Modal, Container, Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { AuthModalContext } from '../../../ContextProvider/AuthModalProvider';
import { AppContext } from '../../../ContextProvider/AppProvider';
import { useHistory } from 'react-router-dom';
import './TermsAndConditions.scss';
import { toast } from 'react-toastify';
import axios from 'axios';

const SponsoringConditions = (props) => {
  console.log("props coming from business conditions:", props)
  const appContext = useContext(AppContext);
  const authModalContext = useContext(AuthModalContext);
  const user = appContext.state.user;
  const userRole = appContext.state.user.role;
  const history = useHistory();
  const user_id = appContext.state.user.id;

  // console.log('USER', user);

  const [businessPreference, setBusinessPreference] = useState([]);

  const handleContinue = (e) => {
    console.log("im coming from handle conitnue")
    props.changeSubmissionConditions(true);
  };

  const handleDisagree = () => {
    props.changeBusinessConditions(false);
  };

  console.log(businessPreference);

  return (
    <div className="termsAndConditions1">
      <div className="bg-image" />

      <div className="row proceed-button">
        <div>
          <div className="conditions-title-card"> Sponsoring - Terms and Conditions </div>
          <div className="conditions-content-card">
            If you have chosen $0 for the item price you will be sponsoring this item:
            <ul>
              <li>Your Product will be uploaded as sponsor because the price is 0.</li>
              <li>You can change the price if you dont wish to sponsor this item.</li>
              <li>You can see Sponsor packages on your dashboard for more sponsoring options.</li>
            </ul>
          </div>
        </div>
        <row className="align-vend-button">
          <button
            type="submit" 
            className="agree-disagree-button"
            onClick={(e) => {
              handleContinue();
            }}
          >
            Agree
          </button>
          <button
            type="button"
            className="agree-disagree-button"
            onClick={() => {
              handleDisagree(false)
            }}
          >
            Disagree
          </button>
        </row>
      </div>
    </div>
  );
};

export default SponsoringConditions;
