// Libraries
import React, { useState } from 'react';

// Components
import Guest from './Guest/Guest';
import Ambassador from './Ambassador/Ambassador';
import Business from './Business/Business';
import Pricing from './Pricing/Pricing';

// Styling
import './CreateYourPassport.scss';
import { Fragment } from 'react';

const CreateYourPassport = (props) => {
  const [toggleState, setToggleState] = useState('Guest');
  const [toggleStateBusiness, setToggleStateBusiness] = useState('Host');
  // const [userSubscription, setUserSubscription] = useState(toggleState.toLowerCase());
  const [userSubscription, setUserSubscription] = useState(toggleState.toLowerCase());

  const toggleTab = (index) => {
    setToggleState(index);
  };

  return (
    <section className="landing-page-section create-your-passport">
      <div className="container">
        <div className="row">
          <h1 className="landing-page__heading">Celebrate Our Festivals With Your Own Tasttlig Passport</h1>
        </div>
        <div className="tab-row">
          <div className="tab-switcher">
            <button
              onClick={() => toggleTab('Guest')}
              className={toggleState === 'Guest' ? 'tab active-tab' : 'tab'}
            >
              Guest
            </button>
            <button
              onClick={() => toggleTab('Ambassador')}
              className={toggleState === 'Ambassador' ? 'tab active-tab' : 'tab'}
            >
              Ambassador
            </button>
            <button
              onClick={() => toggleTab('Host')}
              className={toggleState === 'Host' ? 'tab active-tab' : 'tab'}
            >
              Business
            </button>
          </div>
        </div>
        {toggleState === 'Guest' ? (
          <Fragment>
            <Guest />
            {/* <Pricing
              key={'guest'}
              userSubscription={'guest'}
              setUserSubscription={setUserSubscription}
            /> */}
          </Fragment>
        ) : toggleState === 'Ambassador' ? (
          <Fragment>
            <Ambassador />
            {/* <Pricing
              key={'ambassador'}
              userSubscription={'ambassador'}
              setUserSubscription={setUserSubscription}
            /> */}
          </Fragment>
        ) : toggleState === 'Host' ? (
          <Fragment>
            <Business toggleState={toggleStateBusiness} setToggleState={setToggleStateBusiness} />
            {/* <Pricing
              key={'host'}
              userSubscription={'host'}
              setUserSubscription={setUserSubscription}
            /> */}
          </Fragment>
        ) : null}
      </div>
    </section>
  );
};

export default CreateYourPassport;
