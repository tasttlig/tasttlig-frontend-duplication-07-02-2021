
import React, { useState, useContext, useEffect } from 'react';
import { Link } from 'react-router-dom';
import TermsAndConditions from '../../TermsAndConditions/TermsAndConditions';
import VendingConditions from '../../TermsAndConditions/VendingConditions';
import Modal from 'react-modal';

import './Business.scss';

// Components
// import Host from './Host/Host';
// import Vend from './Vend/Vend';

const Business = (props) => {
  const { toggleState, setToggleState } = props;

  const [viewTermsAndConditions, setViewTermsAndConditions] = useState(false);
  const [vendingConditions, setVendingConditions] = useState(false);

  const toggleTab = (index) => {
    setToggleState(index);
  };

  return (
    <div className="row create-your-passport__layout">
      <div className="col create-your-passport-content">
        <h1 className="create-your-passport__heading">Tasttlig Business Booklet</h1>
        <p className="create-your-passport__body">
          Tasttlig Business Booklet allows you to gain access to our business services.
          Your business booklet allows you to <span className="red-text">vend</span> Tasttlig
          festivals in your neighbourhood and around the world.
        </p>
        {/* <Link
          exact="true"
          to="/host-landing"
          onClick={() => localStorage.setItem('dataFromFestival', 'Host')}
        > */}
          <Link exact="true" to="/complete-profile/festival-vendor">
            <button className="landing-page__button-primary-black-solid create-your-passport__call-to-action host-button-red"
            onClick={(e) => localStorage.setItem('VendFromSignOut', 'true')}>
              Vend
            </button>
          </Link>

          {vendingConditions ? (
                <Modal className="terms-conditions-modal" 
                 isOpen ={vendingConditions} onRequestClose={() => setVendingConditions(false) }>
                  <div className = "terms-conditions-background-image"/>                  
                  <VendingConditions
                    user_id={props.user_id}
                    viewVendingConditionsState={vendingConditions}
                    changeVendingConditions={(e) => setVendingConditions(e)}
                    />
                </Modal>
              ) : null}
        {/* </Link> */}
      </div>
      <div className="col">
        <img className="create-your-passport__picture" src="/img/business-booklet.jpg" />
      </div>
    </div>
  );
};

export default Business;
