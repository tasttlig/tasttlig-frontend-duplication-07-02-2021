import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import { AppContext } from '../../../../../ContextProvider/AppProvider';
import './Vend.scss';

const Vend = (props) => {
  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const userRole = appContext.state.user.role;

  return (
    <div className="row create-your-passport__layout business-tab-content">
      <p className="create-your-passport__body">
        Become a vendor at <strong>Tasttlig</strong> for free and{' '}
        <span className="red-text">earn money</span>.
      </p>
      <button className="landing-page__button-primary-black-solid create-your-passport__call-to-action">
        {!appContext.state.user.id ? (
          <Link exact="true" to="/sign-up">
            Apply to Vend
          </Link>
        ) : (
          <Link exact="true" to="/">
            Apply to Vend
          </Link>
        )}
      </button>
    </div>
  );
};

export default Vend;
