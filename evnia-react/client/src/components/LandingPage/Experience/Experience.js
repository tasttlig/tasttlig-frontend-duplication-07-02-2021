import React from 'react';
import { Link } from 'react-router-dom';
import './Experience.scss';

const Experience = (props) => {
  return (
    <div className="container">
      <section className="landing-page-section experience">
        <div className="experience-content">
          {/* <h1 className="landing-page__heading-light experience-heading">
            Experience Tasttlig
          </h1> */}
          <h2 className="experience-message">
            Attend our festivals, claim food samples from each restaurant that is vending and get
            your passport stamped for a chance to win a prize.
          </h2>

          <button className="landing-page__button-white-primary experience__call-to-action">
            <Link exact="true" to="/sign-up">
              Join
            </Link>
          </button>
        </div>
      </section>
    </div>
  );
};

export default Experience;
