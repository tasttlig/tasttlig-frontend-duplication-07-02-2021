import React, { useState, useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import useInfiniteScroll from 'react-infinite-scroll-hook';
import { AppContext } from '../../../ContextProvider/AppProvider';

// import FestivalCard from './FestivalCard/FestivalCard';
import FestivalCard from '../../Festivals/FestivalCard/FestivalCard';
import './Explore.scss';

const Explore = (props) => {
  // Set initial state
  const [festivalItems, setFestivalItems] = useState([]);
  const [selectedNationalities, setSelectedNationalities] = useState([]);
  const [startDate, setFilterStartDate] = useState('');
  const [startTime, setFilterStartTime] = useState('');
  const [cityLocation, setCityLocation] = useState('');
  const [filterRadius, setFilterRadius] = useState(25000000);
  const [hasNextPage, setHasNextPage] = useState(true);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(0);
  const [selectedItem, setSelectedItem] = useState('');
  const [selectMultipleFestivals, setSelectMultipleFestivals] = useState(false);
  const [hostFestivalList, setHostFestivalList] = useState([]);
  const [festivalSponsorList, setFestivalSponsorList] = useState([]);

  // To use the JWT credentials
  const appContext = useContext(AppContext);
  //const { latitude, longitude, geoError } = usePosition();

  // Host multiple festivals helper function
  const handleHostMultipleFestivals = () => {
    setSelectMultipleFestivals(true);
  };

  // Render festival cards helper function
  const renderFestivalCards = (arr) => {
    if (arr.length > 3) {
      arr.slice(2);
    }
    return arr.map((card, index) => (
      <FestivalCard
        key={index}
        festivalId={card.festival_id}
        images={card.image_urls}
        title={card.festival_name}
        type={card.festival_type}
        price={card.festival_price}
        city={card.festival_city}
        startDate={card.festival_start_date}
        endDate={card.festival_end_date}
        startTime={card.festival_start_time}
        endTime={card.festival_end_time}
        description={card.festival_description}
        selectMultipleFestivals={selectMultipleFestivals}
        hostFestivalList={card.festival_restaurant_host_id}
        festivalSponsorList={card.festival_business_sponsor_id}
        history={props.history}
      />
    ));
  };

  // Toggle nationality helper function
  const toggleNationality = (nationalities) => {
    let nationalities_list = [];

    nationalities.map((nationality) => {
      nationalities_list.push(nationality.value.nationality);
    });

    setSelectedNationalities(nationalities_list);
  };

  // Load next set of festivals helper functions
  const loadNextPage = async (page) => {
    const url = '/festival/landing-page?';

    return axios({
      method: 'GET',
      url,
      params: {
        page: page + 1,
        nationalities: selectedNationalities,
        radius: filterRadius,
        startDate,
        startTime,
        cityLocation,
      },
    });
  };

  const handleLoadMore = (page = currentPage, festivals = festivalItems) => {
    setLoading(true);
    loadNextPage(page).then((newPage) => {
      setLoading(false);

      if (!newPage) {
        return false;
      }

      const pagination = newPage.data.details.pagination;

      if (page < pagination.lastPage) {
        setCurrentPage(page + 1);
      }

      setHasNextPage(currentPage < pagination.lastPage);
      if (festivalItems.length < 3) {
        setFestivalItems(festivals.concat(newPage.data.details.data));
      }
    });
  };

  const infiniteRef = useInfiniteScroll({
    loading,
    hasNextPage,
    onLoadMore: handleLoadMore,
    scrollContainer: 'window',
    threshold: 50,
  });

  // Mount Festivals page
  useEffect(() => {
    window.scrollTo(0, 0);

    handleLoadMore();
  }, []);

  useEffect(() => {
    handleLoadMore(0, []);
  }, [selectedNationalities, startDate, startTime, cityLocation, filterRadius]);

  return (
    <section className="landing-page-section explore-tasttlig">
      <div className="container">
        <div className="row">
          <h1 className="landing-page__heading">Explore our Festivals</h1>
        </div>
        <div className="explore-cards" ref={infiniteRef}>
          {festivalItems.length !== 0 ? renderFestivalCards(festivalItems) : null}
        </div>
        <div className="explore-see-more">
          <button className="landing-page__button-primary-black-solid see-more-festivals-button">
            <Link exact="true" to="/festivals">
              See More
            </Link>
          </button>
        </div>
      </div>
    </section>
  );
};

export default Explore;
