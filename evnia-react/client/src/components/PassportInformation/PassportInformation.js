// Libraries
import React, { useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import Scroll, { Element } from 'react-scroll';
import { AuthModalContext } from '../../ContextProvider/AuthModalProvider';
import { AppContext } from '../../ContextProvider/AppProvider';

// Components
import Nav from '../Navbar/Nav';
import BannerFooter from '../Home/BannerFooter/BannerFooter';

// Styling
import './PassportInformation.scss';

const ScrollLink = Scroll.Link;

const PassportInformation = (props) => {
  // Source tracking on new sign ups
  const source = new URLSearchParams(props.location.search).get('source');
  if (source) {
    if (!localStorage.getItem('source')) {
      localStorage.setItem('source', source);
    }
  }

  // To use the JWT credentials
  const authModalContext = useContext(AuthModalContext);
  const appContext = useContext(AppContext);

  // Mount Passport Information page
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  // Render Passport Information page
  return (
    <div>
      <Nav />

      <div>
        <section className="jumbotron passport-banner">
          <h1 className="mb-4 passport-banner-statement">
            Every Member is Provided with a Free Tasttlig Passport
          </h1>
          {!appContext.state.user.id && (
            <div>
              <div onClick={authModalContext.openModal('sign-up')} className="join-btn">
                Join
              </div>
            </div>
          )}
          <ScrollLink to="exclusivePassports" offset={-80} className="more-passport-options">
            Business Passports
          </ScrollLink>
        </section>
      </div>

      <div className="bg-light passport-benefits">
        <div className="container p-0">
          <div className="row justify-content-center">
            <div className="col-12 col-md-10 col-lg-8 px-3 text-center">
              <div className="mb-5 lead text-gray-700">Tasttlig Passport provides access to</div>
            </div>
          </div>
          <div className="row text-center">
            <div className="col-lg-4 px-3">
              <div className="card mb-4 rounded-0">
                <div className="card-body p-4">
                  <h1 className="card-title mb-0">
                    Free Food at <br></br>Festivals
                  </h1>
                </div>
              </div>
            </div>
            <div className="col-lg-4 px-3">
              <div className="card mb-4 rounded-0">
                <div className="card-body p-4">
                  <h1 className="card-title mb-0">
                    Great Food Deals <br></br>and Specials
                  </h1>
                </div>
              </div>
            </div>
            <div className="col-lg-4 px-3">
              <div className="card mb-4 rounded-0">
                <div className="card-body p-4">
                  <h1 className="card-title mb-0">
                    Rewards, Points <br></br>and Stamps
                  </h1>
                </div>
              </div>
            </div>
          </div>
          {!appContext.state.user.id && (
            <div className="row justify-content-center">
              <div className="col-12 col-md-10 col-lg-8 px-3 text-center">
                <div className="mt-4 mb-3 lead text-gray-700">All Free</div>
                <Link to="#" onClick={authModalContext.openModal('sign-up')} className="join-btn">
                  Join
                </Link>
              </div>
            </div>
          )}
        </div>
      </div>

      <Element id="exclusivePassports">
        <div className="passport-subscription-options">
          <div className="container p-0">
            <div className="row justify-content-center">
              <div className="col-12 col-md-10 col-lg-8 px-3 text-center">
                <div className="mb-5 lead text-gray-700">
                  Access more of what Tasttlig has to offer with Business Passports
                </div>
              </div>
            </div>

            <div className="row text-center">
              <div className="col-lg-4 px-3">
                <div className="mb-5 border">
                  <div className="passport-subscription-name">Business Basic</div>
                  <div className="card rounded-0">
                    <div className="card-body p-4 text-left passport-subscription-text">
                      <ul className="fa-ul mb-0 ml-4">
                        <li className="mb-2">
                          <span className="fa-li fas fa-check passport-check"></span>
                          Showcase your brand at festivals
                        </li>
                        <li className="mb-2">
                          <span className="fa-li fas fa-check passport-check"></span>
                          Promote your brand on Kodidi
                        </li>
                        <li className="mb-2">
                          <span className="fa-li fas fa-check passport-check"></span>
                          Sell experiences
                        </li>
                        <li className="mb-2">
                          <span className="fa-li fas fa-check passport-check"></span>
                          Sell food
                        </li>
                        <li className="mb-2">
                          <span className="fa-li fas fa-check passport-check"></span>
                          Sell services
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div className="card rounded-0">
                    <div className="card-body p-4 bg-light">
                      <div>Free 30 Day Trial</div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-lg-6 p-0">
                      <div className="card rounded-0">
                        <div className="card-body p-3">$50/month</div>
                      </div>
                    </div>
                    <div className="col-lg-6 p-0">
                      <div className="card rounded-0">
                        <div className="card-body p-3">$500/year</div>
                      </div>
                    </div>
                  </div>
                  <div>
                    <Link exact="true" to="/apply" className="buy-passport-btn">
                      Learn More
                    </Link>
                  </div>
                </div>
              </div>

              <div className="col-lg-4 px-3">
                <div className="mb-5 border">
                  <div className="passport-subscription-name">Business Intermediate</div>
                  <div className="card rounded-0">
                    <div className="card-body p-4 text-left passport-subscription-text">
                      <ul className="fa-ul mb-0 ml-4">
                        <li className="mb-2">
                          <span className="fa-li fas fa-check passport-check"></span>
                          Business Basic Passport
                        </li>
                        <li className="mb-2">
                          <span className="fa-li fas fa-check passport-check"></span>
                          Advertise their brands by hosting food at Tasttlig Festivals
                        </li>
                        <li className="mb-2">
                          <span className="fa-li fas fa-check passport-check"></span>
                          Advertise their brands through experiences on Tasttlig
                        </li>
                        <li className="mb-2">
                          <span className="fa-li fas fa-check passport-check"></span>
                          Advertise their brands through specials on Kodidi
                        </li>
                        <li className="mb-2">
                          <span className="fa-li fas fa-check passport-check"></span>
                          Gains contact access to Tasttlig financial, marketing and technology team
                        </li>
                        <li className="mb-2">
                          <span className="fa-li fas fa-check passport-check"></span>
                          Make money by renting their kitchens, dining rooms, venues, patios and
                          rooftops to other members
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div className="card rounded-0">
                    <div className="card-body p-4 bg-light">
                      <div>Free 30 Day Trial</div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-lg-6 p-0">
                      <div className="card rounded-0">
                        <div className="card-body p-3">$200/month</div>
                      </div>
                    </div>
                    <div className="col-lg-6 p-0">
                      <div className="card rounded-0">
                        <div className="card-body p-3">$2,000/year</div>
                      </div>
                    </div>
                  </div>
                  <div>
                    <Link exact="true" to="/apply" className="buy-passport-btn">
                      Learn More
                    </Link>
                  </div>
                </div>
              </div>

              <div className="col-lg-4 px-3">
                <div className="mb-5 border">
                  <div className="passport-subscription-name">Business Advanced</div>
                  <div className="card rounded-0">
                    <div className="card-body p-4 text-left passport-subscription-text">
                      <ul className="fa-ul mb-0 ml-4">
                        <li className="mb-2">
                          <span className="fa-li fas fa-check passport-check"></span>
                          Business Basic Passport
                        </li>
                        <li className="mb-2">
                          <span className="fa-li fas fa-check passport-check"></span>
                          Business Intermediate Passport
                        </li>
                        <li className="mb-2">
                          <span className="fa-li fas fa-check passport-check"></span>
                          Social media management
                        </li>
                        <li className="mb-2">
                          <span className="fa-li fas fa-check passport-check"></span>
                          Videography management
                        </li>
                        <li className="mb-2">
                          <span className="fa-li fas fa-check passport-check"></span>
                          Photography management
                        </li>
                        <li className="mb-2">
                          <span className="fa-li fas fa-check passport-check"></span>
                          Public Relations management
                        </li>
                      </ul>
                    </div>
                    <div className="card rounded-0">
                      <div className="card-body p-4 bg-light">
                        <div>Free 30 Day Trial</div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-6 p-0">
                        <div className="card rounded-0">
                          <div className="card-body p-3">$500/month</div>
                        </div>
                      </div>
                      <div className="col-lg-6 p-0">
                        <div className="card rounded-0">
                          <div className="card-body p-3">$5,000/year</div>
                        </div>
                      </div>
                    </div>
                    <div>
                      <Link exact="true" to="/apply" className="buy-passport-btn">
                        Learn More
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Element>

      {/* Responsive Design */}
      <div>
        <ScrollLink
          to="exclusivePassports"
          offset={-80}
          className="more-passport-options-responsive"
        >
          Business Passports
        </ScrollLink>
      </div>

      <BannerFooter />
    </div>
  );
};

export default PassportInformation;
