// Libraries
import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import moment from 'moment';

// Components
import Nav from '../../Navbar/Nav';
import AllHostApplicantsCard from './AllHostApplicantsCard/AllHostApplicantsCard';
// Styling
import './AllHostApplicants.scss';

export default class AllHostApplicants extends Component {
  // Set initial state
  state = {
    allHostApplicantItems: [],
  };

  // Render All Host Applicants helper function
  renderAllHostApplicants = (arr) => {
    return arr.map((card, index) => (
      <AllHostApplicantsCard
        key={index}
        id={card.id}
        userId={card.user_id}
        profileImage={card.profile_img_url}
        firstName={card.first_name}
        lastName={card.last_name}
        email={card.email}
        phoneNumber={card.phone_number}
        businessName={card.business_name}
        businessType={card.business_type}
        CRA-BusinessNumber={card.CRA_business_number}
        businessRegistered={card.business_phone_number}
        foodHandlerCertificate={card.food_handler_certificate}
        voidCheque={card.void_cheque}
        onlineEmail={card.online_email}
        paypalEmail={card.paypal_email}
        stripeAccount={card.stripe_account}
        hostSelectionVideo={card.host_selection_video}
        verified={card.verified}
        isHost={card.is_host}
        createdAt={card.created_at}
      />
    ));
  };

  // Mount All Host Applicants page
  componentDidMount = () => {
    window.scrollTo(0, 0);

    const url = '/hosts/all-host-applications';

    axios({ method: 'GET', url })
      .then((response) => {
        this.setState({
          allHostApplicantItems: [
            ...this.state.allHostApplicantItems,
            ...response.data.applications.reverse(),
          ],
        });
        console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };


  // Render All Host Applicants page
  render = () => {
    const { allHostApplicantItems } = this.state;
    console.log("allHostApplicantItems", allHostApplicantItems)
    return (
      <div>
        <Nav />

        <div className="all-host-applicants">
          <div className="all-host-applicants-navigation">
            <Link exact="true" to="/admin" className="all-host-applicants-navigation-content">
              Admin
            </Link>
            <span className="arrow">&nbsp;&gt;&nbsp;</span>All Host Applicants
          </div>

          {allHostApplicantItems && allHostApplicantItems.length ? (
            <div className="table-responsive mt-5">
              <table className="table table-bordered">
                <thead>
                  <tr>
                    <th>Email</th>
                    <th>CRA Business Number</th>
                    <th>Date Submitted</th>
                  </tr>
                </thead>
                <tbody>
                  {allHostApplicantItems.map((a) => (
                    <tr key={a.tasttlig_user_id}>
                      <td>
                        <Link exact="true" to={`/admin/host-application/${a.tasttlig_user_id}`}>
                          {`${a.email}`}
                        </Link>
                      </td>
                      <td className="all-host-applicants-business-type">{a.CRA_business_number}</td>
                      <td>{moment(a.updated_at_datetime).toLocaleString()}</td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          ) : (
            <div className="mt-5">
              <strong>No applications available.</strong>
            </div>
          )}
        </div>
      </div>
    );
  };
}
