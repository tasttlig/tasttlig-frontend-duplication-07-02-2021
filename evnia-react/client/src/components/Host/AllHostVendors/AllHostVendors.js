// Libraries
import React, { useState, useEffect, useContext, Fragment, useRef } from 'react';
import { useForm, Controller } from 'react-hook-form';
import axios from 'axios';
import DatePicker from 'react-datepicker';
import useInfiniteScroll from 'react-infinite-scroll-hook';
import { AppContext } from '../../../ContextProvider/AppProvider';
import Modal from 'react-modal';
import LoadingBar from 'react-top-loading-bar';
import { Link } from 'react-router-dom';

// Components
import Nav from '../../Navbar/Nav';
import Footer from '../../Footer/Footer';
// import ExpiredFestivalCard from '../Festivals/FestivalCard/ExpiredFestivalCard.js';

// Styling
import './AllHostVendors.scss';

const AllHostVendors = (props) => {
 
  // Set initial state
  const [festivalVendorList, setFestivalVendorList] = useState([]);


  // To use the JWT credentials
  const appContext = useContext(AppContext);

  if (!props.location.state) {
    props.location.state = { keyword: '' };
  }

  const fetchHostFestivals = async () => {
    try {
      const response = await axios({
        method: 'GET',
        url: `hostvendors/festival/${appContext.state.user.id}`,
        params: {
          user_id: appContext.state.user.id,
        },
      });

      setFestivalVendorList(response.data.filter((v,i,a)=>a.findIndex(t=>(t.tasttlig_user_id === v.tasttlig_user_id))===i));
    } catch (error) {
      console.log(error);
      return error.response;
    }
  };

  // Mount HostProfile page
  useEffect(() => {
    window.scrollTo(0, 0);
    fetchHostFestivals();

  }, []);

  // Render Host page
  return (
    <div className="sponsor-container">
      <Nav />

          <div className="all-host-applicants-navigation">
          <Link exact="true" to="/host-admin" className="all-host-applicants-navigation-content">
            <span>
              Host
            </span>
            </Link>
            <span className="arrow">&nbsp;&gt;&nbsp;</span>All Vendors
          </div>

          {festivalVendorList && festivalVendorList.length ? (
                <div className="table-responsive mt-5">
                  <table className="table table-bordered">
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>Phone Number</th>
                        <th>Email</th>
                        <th>CRA Business Number</th>
                      </tr>
                    </thead>
                    <tbody>
                      {festivalVendorList.map((a) => (
                        <tr key={a.tasttlig_user_id}>
                          <td className="all-host-applicants-business-type">{a.first_name} {a.last_name}</td>
                          <td className="all-host-applicants-business-type">{a.phone_number}</td>
                          <td className="all-host-applicants-business-type">{a.email}</td>
                          <td className="all-host-applicants-business-type">{a.CRA_business_number}</td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>
              ) : (
                <div className="mt-5">
                  <strong>No applications available.</strong>
                </div>
              )}
       <Footer />  
    </div>
  );
};

export default AllHostVendors;
