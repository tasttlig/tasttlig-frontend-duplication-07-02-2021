// Libraries
import React from 'react';

// Styling
import './PassportBanner.scss';

const PassportBanner = () => {
  return <div className="passport-banner">Get 3 Free Food Samples for All Passport Holders</div>;
};

export default PassportBanner;
