// Libraries
import React, { useEffect, useContext } from 'react';
// import { Redirect } from "react-router-dom";
import { AppContext } from '../../ContextProvider/AppProvider';

// Components
import Nav from '../Navbar/Nav';
import ProfileForm from '../Profile/EditProfile/ProfileForm/ProfileForm';

// Styling
import './CompleteProfile.scss';

const CompleteProfile = () => {
  // To use the JWT credentials
  const appContext = useContext(AppContext);

  // Mount Complete Profile page
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  // Redirect to Dashboard page if accepted
  // if (appContext.state.user.profile_status === "accepted") {
  //   return <Redirect to="/dashboard" />;
  // }

  // Render Complete Profile Page
  return (
    <div>
      <Nav />
      <div className="complete-profile">
        <div className="complete-profile-title">Complete Your Profile</div>
        <ProfileForm user={appContext.state.user} update={true} />
      </div>
    </div>
  );
};

export default CompleteProfile;
