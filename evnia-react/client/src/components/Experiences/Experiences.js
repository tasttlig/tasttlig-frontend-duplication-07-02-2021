// Libraries
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import useInfiniteScroll from 'react-infinite-scroll-hook';
import { usePosition } from '../../hooks';

// Components
import Nav from '../Navbar/Nav';
import SearchBar from '../Navbar/SearchBar';
import ExperiencesCard from './ExperiencesCard/ExperiencesCard';
import BannerFooter from '../Home/BannerFooter/BannerFooter';
import GoTop from '../Shared/GoTop';
// import ShoppingCartHoverList from "../ShoppingCart/ShoppingCartHoverList";

// Styling
import './Experiences.scss';

const Experiences = (props) => {
  // Source tracking on new sign ups
  const source = new URLSearchParams(props.location.search).get('source');
  if (source) {
    if (!localStorage.getItem('source')) {
      localStorage.setItem('source', source);
    }
  }

  // Set initial state
  const [experienceItems, setExperienceItems] = useState([]);
  const [availableNationalities, setAvailableNationalities] = useState([]);
  const [selectedNationalities, setSelectedNationalities] = useState([]);
  const [filterRadius, setFilterRadius] = useState(25000000);
  const [hasNextPage, setHasNextPage] = useState(true);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(0);
  const { latitude, longitude, geoError } = usePosition();

  // Set search bar empty
  if (!props.location.state) {
    props.location.state = { keyword: '' };
  }

  // Render Experience Cards helper function
  const renderExperienceCards = (arr) => {
    return arr.map((card, index) => (
      <ExperiencesCard key={card.experience_id} experiencesCard={card} />
    ));
  };

  // Toggle nationality helper function
  const toggleNationality = (event) => {
    const nationality = event.target.value;
    if (selectedNationalities.includes(nationality)) {
      setSelectedNationalities(selectedNationalities.filter((c) => c !== nationality));
    } else {
      setSelectedNationalities([nationality].concat(selectedNationalities));
    }
  };

  // Fetch experiences helper function
  const fetchExperiences = async () => {
    try {
      const response = await axios({
        method: 'GET',
        url: '/experience/all',
      });

      return response;
    } catch (error) {
      return error.response;
    }
  };

  // Load next set of user experiences helper functions
  const loadNextPage = async (page) => {
    const url = '/experience/all?';

    return axios({
      method: 'GET',
      url: url,
      params: {
        keyword: props.location.state.keyword,
        page: page + 1,
        nationalities: selectedNationalities,
        radius: filterRadius,
        latitude,
        longitude,
      },
    });
  };

  const handleLoadMore = (page = currentPage, experiences = experienceItems) => {
    if (latitude || geoError) {
      setLoading(true);
      loadNextPage(page).then((newPage) => {
        setLoading(false);

        const pagination = newPage.data.details.pagination;

        if (page < pagination.lastPage) {
          setCurrentPage(page + 1);
        }

        setHasNextPage(currentPage < pagination.lastPage);
        setExperienceItems(experiences.concat(newPage.data.details.data));
      });
    }
  };

  const infiniteRef = useInfiniteScroll({
    loading,
    hasNextPage,
    onLoadMore: handleLoadMore,
    scrollContainer: 'window',
    threshold: 50,
  });

  // Mount Experiences page
  useEffect(() => {
    window.scrollTo(0, 0);

    fetchExperiences().then(({ data }) => {
      setExperienceItems(data.details.data);
    });
  }, []);

  useEffect(() => {
    window.scrollTo(0, 0);

    const fetchDistinctNationalities = async () => {
      const response = await axios({
        method: 'GET',
        url: '/experience/nationalities',
      });

      setAvailableNationalities(response.data.nationalities);
    };

    fetchDistinctNationalities();
    handleLoadMore();
  }, []);

  useEffect(() => {
    handleLoadMore(0, []);
  }, [props.location.state.keyword, selectedNationalities, filterRadius, latitude, geoError]);

  // Render empty page
  const Empty = () => {
    return <strong className="no-experiences-found">No experiences found.</strong>;
  };

  // Render filters
  const Filters = () => (
    <div className="page-filters">
      <div className="filter-section-categories">
        <ul>
          {availableNationalities &&
            availableNationalities.map((c) => (
              <li key={c}>
                <input
                  type="checkbox"
                  defaultChecked={selectedNationalities.includes(c)}
                  value={c || ''}
                  onClick={toggleNationality}
                />
                {c}
              </li>
            ))}
        </ul>
      </div>
      {latitude && (
        <div className="filter-section-categories">
          <select
            onChange={(e) => setFilterRadius(parseInt(e.target.value))}
            value={filterRadius}
            className="custom-select"
          >
            <option value="25000000">--Select---</option>
            <option value="5000">5km</option>
            <option value="10000">10km</option>
            <option value="25000">25km</option>
            <option value="50000">50km</option>
          </select>
        </div>
      )}
    </div>
  );

  // Render Experiences page
  return (
    <div>
      <Nav />
      {/*<ShoppingCartHoverList/>*/}

      <div className="experiences">
        <div className="row">
          <div className="col-lg-3 experiences-title-content">
            <div className="experiences-title">Experiences</div>
          </div>
          <div className="col-lg-9 search-bar-content">
            <SearchBar keyword={props.location.state.keyword} url="/experiences" />
          </div>
        </div>

        <div className="row">
          <div className="col-lg-3 px-0">
            <Filters />
          </div>

          <div className="col-lg-9 px-0">
            <div className="experience-cards" ref={infiniteRef}>
              {experienceItems.length !== 0 ? renderExperienceCards(experienceItems) : <Empty />}
            </div>
          </div>
        </div>
      </div>

      {/* Footer Area */}
      <BannerFooter />

      {/* Back to Top */}
      <GoTop scrollStepInPx="50" delayInMs="16.66" />
    </div>
  );
};

export default Experiences;
