// Libraries
import React, { useState, useEffect } from 'react';
import ReactModal from 'react-modal';
import axios from 'axios';

// Styling
import './ImagesModal.scss';

export default ({ ExperienceId, showModal, handleCloseModal }) => {
  const [ExperienceImages, setExperienceImages] = useState([]);

  // GET Experience images
  const fetchImages = async (ExperienceId) => {
    const url = `/api/ExperienceImages/${ExperienceId}`;

    const acc_token = localStorage.getItem('access_token');
    const headers = {
      Authorization: `Bearer ${acc_token}`,
    };

    // Make the API request
    try {
      const response = await axios({
        method: 'GET',
        url,
        headers,
      });

      if (response.status === 200) {
        return Object.values(response.data.images[0]).filter((value) => value !== null);
      } else {
        return [];
      }
    } catch (error) {
      console.log(error);
      return [];
    }
  };

  useEffect(() => {
    // Set the response to the state
    (async function () {
      const imagesResponse = await fetchImages(ExperienceId);
      setExperienceImages(imagesResponse);
    })();
  }, []);

  return (
    <>
      <ReactModal
        isOpen={showModal}
        contentLabel="onRequestClose Example"
        onRequestClose={handleCloseModal}
        className="images-modal"
        ariaHideApp={false}
      >
        {ExperienceImages.map((image, index) => (
          <div className="image-container">
            <img
              key={index}
              src={image}
              alt="Experience Images"
              className="img-thumbnail modal-images"
            />
          </div>
        ))}
      </ReactModal>
    </>
  );
};
