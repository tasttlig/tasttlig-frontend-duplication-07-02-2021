// Libraries
import React from 'react';

// Components
import Card from './Card';

// Styling
import './ExperienceList.scss';

export default (props) => {
  const {
    field,
    form: { setFieldValue },
    ExperienceList,
  } = props;

  const items = field.value;
  const setItem = (item) => {
    setFieldValue('menuItems', [...items, item]);
  };

  return (
    <div id="ExperienceList">
      <div className="container-fluid scroll">
        {ExperienceList.map((item) => (
          <div className="image-container">
            <Card key={item.id} item={item} cardType={'add'} handleClick={setItem} />
          </div>
        ))}
      </div>
    </div>
  );
};
