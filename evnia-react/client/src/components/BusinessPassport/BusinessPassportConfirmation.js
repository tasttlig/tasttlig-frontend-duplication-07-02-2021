// Libraries
import axios from 'axios';
import React, { useEffect, useContext, useState } from 'react';
import { Link } from 'react-router-dom';
import { AppContext } from '../../ContextProvider/AppProvider';
import { AuthModalContext } from '../../ContextProvider/AuthModalProvider';
import { toast } from 'react-toastify';
import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router-dom';

//components

import AsyncSelect from 'react-select/async';
import Select from '../EasyForm/Controls/Select';

// import { canadaProvincesTerritories } from "../../../Functions/Functions";

// Styling
import './BusinessPassportConfirmation.scss';
import './BusinessPassport.scss';
import tasttligLogoBlack from '../../assets/images/tasttlig-logo-black.png';
import confirmationImage from '../../assets/images/businessPassportConfirmation.png';

const UserInfo = (props) => {
  const history = useHistory();

  const userData = {};

  //set initial state
  const { update, handleSubmit } = useForm({ defaultValues: userData });
  const [selectedItem, setSelectedItem] = useState('');
  const [submitAuthDisabled, setSubmitAuthDisabled] = useState(false);
  const [initialFestivalSelection, setInitialFestivalSelection] = useState('');
  const [initialFestivalTitle, setInitialFestivalTitle] = useState('');

  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const userRole = appContext.state.user.role;
  const authModalContext = useContext(AuthModalContext);

  const onClickVendor = async () => {
    try {
      setTimeout(1000);
      {
        setTimeout(() => {
          window.location.href = '/';
        }, 2000);
        // toast("Success! Your details have been updated!",
        //     {
        //     type: "success",
        //     autoClose: 2000,
        //     }
        // );
      }
    } catch (error) {
      toast('Error! Something went wrong!', {
        type: 'error',
        autoClose: 2000,
      });
    }
  };

  const handleExplore = async () => {
    try {
      setTimeout(1000);
      {
        setTimeout(() => {
          window.location.href = '/';
        }, 2000);
      }
    } catch (error) {
      toast('Error! Something went wrong!', {
        type: 'error',
        autoClose: 2000,
      });
    }
  };

  const handleVending = async () => {
    try {
      setTimeout(1000);
      {
        setTimeout(() => {
          window.location.href = '/complete-profile/festival-vendor';
        }, 2000);
      }
    } catch (error) {
      toast('Error! Something went wrong!', {
        type: 'error',
        autoClose: 2000,
      });
    }
  };

  const handleSponsoring = async () => {
    if (
      !userRole.includes('SPONSOR_PENDING') &&
      !userRole.includes('BUSINESS') &&
      !userRole.includes('VENDOR') &&
      !userRole.includes('HOST')
    ) {
      window.location.href = '/get-business-passport';
    } else if (userRole.includes('SPONSOR_PENDING')) {
      toast(`You have already applied to become a sponsor`, {
        type: 'info',
        autoClose: 2000,
      });
      return;
    }
    const url = '/become-in-kind-sponsor';
    const data = { is_sponsor: true };
    const response = await axios({ method: 'POST', url, data });
    if (response && response.data && response.data.success) {
      await history.push({
        pathname: '/sponsor-in-kind-confirmation',
        festivalId: initialFestivalSelection ? initialFestivalSelection : null,
        title: initialFestivalTitle ? initialFestivalTitle : null,
      });
    } else {
      toast(`Something went wrong`, {
        type: 'error',
        autoClose: 2000,
      });
    }
    /* try {
            setTimeout(1000);
            {
                setTimeout(() => {
                    window.location.href = "/sponsor-plan";
                }, 2000);
            }
        } catch (error) {
            toast("Error! Something went wrong!", {
                type: "error",
                autoClose: 2000,
            });
        } */
  };

  const handleHosting = async () => {
    try {
      setTimeout(1000);
      {
        setTimeout(() => {
          window.location.href = '/become-a-host';
        }, 2000);
      }
    } catch (error) {
      toast('Error! Something went wrong!', {
        type: 'error',
        autoClose: 2000,
      });
    }
  };

  // Mount Login page
  useEffect(() => {
    window.scrollTo(0, 0);
    const initialFestival = /* JSON.parse */ localStorage.getItem('initialFestivalSelection');
    if (initialFestival) {
      setInitialFestivalSelection(initialFestival);
      console.log(
        'json console log',
        /* JSON.parse */ localStorage.getItem('initialFestivalTitle'),
      );
      setInitialFestivalTitle(/* JSON.parse */ localStorage.getItem('initialFestivalTitle'));
    }
  }, []);

  // Render  page
  return (
    <div className="row">
      <div className="col-lg-1 col-xl-4 px-0 user-background-image-one" />
      <div className="col-lg-8 col-xl-5 px-0 login-background">
        <div className="login-content">
          <div className="passport-confirmation-image">
            <img src={confirmationImage} width="660" height="221" alt="Tasttlig" />
          </div>

          <div>
            <row>
              <div className="submit-button-vending">
                <button
                  onClick={handleHosting}
                  type="submit"
                  disabled={authModalContext.state.submitAuthDisabled}
                  className="log-in-btn"
                >
                  Host Tasttlig
                </button>
              </div>
              <div className="submit-button-vending">
                <button
                  onClick={handleVending}
                  type="submit"
                  disabled={authModalContext.state.submitAuthDisabled}
                  className="log-in-btn"
                >
                  Vend Tasttlig
                </button>
              </div>

              <div className="submit-button-sponsor">
                <button
                  onClick={handleSponsoring}
                  type="submit"
                  disabled={authModalContext.state.submitAuthDisabled}
                  className="log-in-btn"
                >
                  Sponsor Tasttlig
                </button>
              </div>
              {/* <Link
                    exact="true"
                    to={{
                      pathname: "/sponsor-plan",
                      festivalId: initialFestivalSelection ? initialFestivalSelection
                      :null,
                      title: initialFestivalTitle ? initialFestivalTitle
                      :null,
                    }}
                    className="submit-button-sponsor text-white">
                        <div className=" log-in-btn ">
                     Sponsor Tasttlig
                        </div>
                  </Link> */}
              {/* </div> */}

              <div className="submit-button-explore">
                <button
                  onClick={handleExplore}
                  type="submit"
                  disabled={authModalContext.state.submitAuthDisabled}
                  className="log-in-btn"
                >
                  Explore Tasttlig
                </button>
              </div>
            </row>
          </div>
        </div>
      </div>
      <div className="col-lg-1 col-xl-3 px-0 user-background-image-two" />
    </div>
  );
};

export default UserInfo;
