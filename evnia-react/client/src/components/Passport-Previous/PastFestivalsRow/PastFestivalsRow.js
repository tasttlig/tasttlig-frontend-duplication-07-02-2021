// Libraries
import React, { useState, useContext } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import LazyLoad from 'react-lazyload';
import moment from 'moment';
import { toast } from 'react-toastify';
import { AppContext } from '../../../ContextProvider/AppProvider';

// Components
import ImageSlider from '../../ImageSlider/ImageSlider';
import { formatDate } from '../../Functions/Functions';

// Styling
import './PastFestivalsRow.scss';
import 'react-toastify/dist/ReactToastify.css';
import liveFestival from '../../../assets/images/live.png';

toast.configure();

const PastFestivalsRow = (props) => {
  const [load, setLoad] = useState(false);

  // Calculate the number of days between start/end and current date
  const d1 = new Date(props.startDate);
  const d2 = new Date(props.endDate);
  const d3 = new Date();
  const millisecondsInDay = 86400000;
  const upcomingDays = Math.floor((d1 - d3) / millisecondsInDay) + 1;
  const endingDays = Math.ceil((d2 - d3) / millisecondsInDay) + 1;

  // Set date and time
  // const startDateTime = moment(
  //   new Date(props.startDate).toISOString().split("T")[0] +
  //     "T" +
  //     props.startTime +
  //     ".000Z"
  // ).add(new Date().getTimezoneOffset(), "m");
  // const endDateTime = moment(
  //   new Date(props.startDate).toISOString().split("T")[0] +
  //     "T" +
  //     props.endTime +
  //     ".000Z"
  // ).add(new Date().getTimezoneOffset(), "m");

  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const userRole = appContext.state.user.role;

  // Render Past Festival Table Row
  return (
    <tr>
      <th scope="row">{props.title}</th>
      <td>{formatDate(props.startDate)}</td>
      <td>{props.city}</td>
      <td>${props.price}</td>
      <td>
        <Link exact="true" to={`/ticket/${props.ticketId}`} className="festival-card-link">
          <button type="button" className="profile-button-primary view-ticket-button">
            View
          </button>
        </Link>
      </td>
    </tr>
  );
};

export default PastFestivalsRow;
