// Libraries
import React, { useState, useEffect, useContext } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

import { toast } from 'react-toastify';

// Components
import Nav from '../../Navbar/Nav';

import ImageSlider from '../../ImageSlider/ImageSlider';
import BannerFooter from '../../Home/BannerFooter/BannerFooter';
import GoToTop from '../../Shared/GoTop';
import { formatDate, formatMilitaryToStandardTime } from '../../Functions/Functions';

// Styling
import './PassportConfirmationPage.scss';
import 'react-toastify/dist/ReactToastify.css';

toast.configure();

const PassportConfirmation = (props) => {
  const [passportDetails, setPassportDetails] = useState([]);

  const [loading, setLoading] = useState(true);

  // Fetch ticket details helper function
  const fetchPassportDetails = async () => {
    try {
      const response = await axios({
        method: 'GET',
        url: `/passport/${props.match.params.passportId}`,
      });

      setPassportDetails(response.data.details);

      return response;
    } catch (error) {
      return error.response;
    }
  };

  // Get all fetches helper function
  const getAllFetches = async () => {
    await fetchPassportDetails();

    setLoading(false);
  };

  // Mount ticket Details page
  useEffect(() => {
    window.scrollTo(0, 0);

    getAllFetches();
  }, []);

  // Render Ticket Details page
  return (
    <div>
      <Nav />

      {!loading && (
        <div className="festival-details">
          <div className="festival-details-information-filters">
            <div className="row">
              <div className="col-lg-4 festival-details-text-section">
                <div className="d-flex flex-column h-100">
                  <div className="p-4">
                    <div className="mb-4">
                      <div className="festival-details-title">
                        {passportDetails[0].festival_name}
                      </div>
                      <div>
                        <span>by </span>
                        {passportDetails[0].business_name ? (
                          <span>{passportDetails[0].business_name}</span>
                        ) : (
                          <span>Tasttlig</span>
                        )}
                      </div>
                    </div>
                    <div className="mb-4">
                      <div className="festival-details-sub-title">Ticket Confirmation Number</div>
                      <div> {passportDetails[0].ticket_booking_confirmation_id}</div>
                    </div>
                    <div className="mb-4">
                      <div className="festival-details-sub-title">Name of Attendant</div>
                      <div>{`${passportDetails[0].first_name} ${passportDetails[0].last_name}`}</div>
                    </div>

                    <div className="mb-4">
                      <div className="festival-details-sub-title">Attendant Contact</div>
                      <div>{`${passportDetails[0].phone_number}`}</div>
                    </div>
                    <div className="mb-4">
                      <div className="festival-details-sub-title">Date</div>
                      <div>{`${formatDate(passportDetails[0].festival_start_date)} to ${formatDate(
                        passportDetails[0].festival_end_date,
                      )}`}</div>
                    </div>
                    <div className="mb-4">
                      <div className="festival-details-sub-title">Time</div>
                      {passportDetails[0].festival_start_time &&
                        passportDetails[0].festival_end_time && (
                          <div>
                            {`${formatMilitaryToStandardTime(
                              passportDetails[0].festival_start_time,
                            )} to ${formatMilitaryToStandardTime(
                              passportDetails[0].festival_end_time,
                            )}`}
                          </div>
                        )}
                    </div>
                    <div className="mb-4">
                      <div className="festival-details-sub-title">City</div>
                      <div>{`${passportDetails[0].festival_city}`}</div>
                    </div>
                    <div className="mb-4">
                      <div className="festival-details-sub-title">Price</div>
                      <div>${`${passportDetails[0].festival_price}`}</div>
                    </div>
                    {passportDetails[0].sponsor_name && (
                      <div className="mb-4">
                        <div className="festival-details-sub-title">Sponsored by</div>
                        <div>{passportDetails[0].sponsor_name}</div>
                      </div>
                    )}
                  </div>
                </div>
              </div>
              <div className="col-lg-8 festival-details-images-section">
                {[passportDetails[0].image_urls].length > 1 ? (
                  <ImageSlider images={passportDetails[0].image_urls} />
                ) : (
                  <div>
                    <img
                      src={[passportDetails[0].image_urls][0]}
                      alt="Festival Details"
                      className="festival-details-images"
                    />
                    <div className="festival-details-type">{passportDetails[0].festival_type}</div>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
      )}

      <BannerFooter />

      <GoToTop />
    </div>
  );
};

export default PassportConfirmation;
