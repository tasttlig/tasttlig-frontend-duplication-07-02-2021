// Libraries
import React, { useState } from 'react';
import PlacesAutocomplete, { geocodeByAddress, getLatLng } from 'react-places-autocomplete';
import styled from 'styled-components';

const Input = styled.input`
  border: 1px solid #ccc;
  font-family: 'Poppins', Arial, sans-serif !important;
  padding: 12px 40px 12px 12px !important;
  width: 100%;
`;

const DropdownContainer = styled.div`
  background-color: #fff;
  border: 1px solid #ccc;
  padding: 10px;
  position: absolute;
`;

const Suggestion = styled.div`
  font-size: 13px;
  border-bottom: 1px solid #ccc;
  margin-bottom: 15px;
  padding-bottom: 8px;
`;

const searchOptions = {
  types: ['address'],
  componentRestrictions: { country: 'ca' },
};

const AddressLookup = ({ value, onChange, onSelect }) => {
  const [address, setAddress] = useState(value ?? '');

  const handleChange = (value) => {
    setAddress(value);
    if (onChange) {
      onChange(value);
    }
  };

  const handleSelect = async (value) => {
    setAddress(value);
    const latlng = await geocodeByAddress(value)
      .then((results) => getLatLng(results[0]))
      .catch((error) => console.error('Error', error));
    if (onSelect) {
      onSelect(value, latlng);
    }
  };

  return (
    <PlacesAutocomplete
      value={address}
      onChange={handleChange}
      onSelect={handleSelect}
      searchOptions={searchOptions}
    >
      {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
        <div>
          <Input
            {...getInputProps({
              placeholder: 'Search Places ...',
              className: 'location-search-input',
            })}
          />
          {suggestions.length ? (
            <DropdownContainer className="autocomplete-dropdown-container">
              {loading && <div>Loading...</div>}
              {suggestions.map((suggestion) => {
                const className = suggestion.active ? 'suggestion-item--active' : 'suggestion-item';

                const style = suggestion.active
                  ? { backgroundColor: '#fafafa', cursor: 'pointer' }
                  : { backgroundColor: '#ffffff', cursor: 'pointer' };

                return (
                  <Suggestion
                    {...getSuggestionItemProps(suggestion, {
                      className,
                      style,
                    })}
                    key={suggestion.placeId}
                  >
                    <span>{suggestion.description}</span>
                  </Suggestion>
                );
              })}
            </DropdownContainer>
          ) : null}
        </div>
      )}
    </PlacesAutocomplete>
  );
};

export default AddressLookup;
