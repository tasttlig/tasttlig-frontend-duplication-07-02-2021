// Libraries
import React, { useState, useEffect, useContext } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { v4 as uuidv4 } from 'uuid';
import { toast } from 'react-toastify';
import { AppContext } from '../../ContextProvider/AppProvider';
// import ProductForm from "./Apply/ApplyForm/FormSteps/ProductForm";
// Components
import Nav from '../Navbar/Nav';
import { listHostedEvents, countriesOnPassport } from '../../../src/components/Functions/Functions';
import { formatPhoneNumber } from '../Functions/Functions';
import HostButtonApply from '../../components/Apply/HostButtonApply';
// Styling
import './BecomeAHost.scss';
import 'react-toastify/dist/ReactToastify.css';
toast.configure();

const BecomeAHost = (props) => {
  console.log('props from become a host:', props);
  // Set initial state
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');
  const [video, setVideo] = useState([]);
  const [description, setDescription] = useState('');
  const [governmentId, setGovernmentId] = useState([]);
  const [firstNameError, setFirstNameError] = useState('');
  const [lastNameError, setLastNameError] = useState('');
  const [emailError, setEmailError] = useState('');
  const [phoneNumberError, setPhoneNumberError] = useState('');
  const [videoError, setVideoError] = useState('');
  const [descriptionError, setDescriptionError] = useState('');
  const [governmentIdError, setGovernmentIdError] = useState('');
  const [errorMessage, setErrorMessage] = useState('');
  const [submitAuthDisabled, setSubmitAuthDisabled] = useState(false);
  const [openCompleteProfile, setOpenCompleteProfile] = useState(false);
  const [openCompleteOther, setOpenCompleteOther] = useState(false);

  const [hostedEvents, setHostedEvents] = useState([]);
  const [hostedCountry, setHostedCountry] = useState([]);

  const [haveYouHostedCountry, setHaveYouHostedCountry] = useState('');
  const [noHaveYouHostedCountryButton, setNohaveYouHostedCountryButton] = useState(
    'do-you-have-a-restaurant-no1-btn',
  );
  const [haveYouHostedCountryButton, setHaveYouHostedCountryButton] = useState(
    'do-you-have-a-restaurant-no1-btn',
  );

  const [discoverCuisine, setDiscoverCuisine] = useState('');
  const [NodiscoverCuisineButton, setNodiscoverCuisineButton] = useState(
    'do-you-have-a-restaurant-no1-btn',
  );
  const [discoverCuisineButton, setdiscoverCuisineButton] = useState(
    'do-you-have-a-restaurant-no1-btn',
  );

  const [provideSamples, setProvideSamples] = useState('');
  const [NoprovideSamplesButton, setNoprovideSamplesButton] = useState(
    'do-you-have-a-restaurant-no1-btn',
  );
  const [provideSamplesButton, setprovideSamplesButton] = useState(
    'do-you-have-a-restaurant-no1-btn',
  );

  const [nationalities, setNationalities] = useState([]);
  const [otherHostedEvents, setOtherHostedEvents] = useState([]);

  const [hasSeating, setHasSeating] = useState('');
  const [noSelectedSeatingButton, setNoSelectedSeatingButton] = useState(
    'do-you-have-a-restaurant-no1-btn',
  );
  const [selectedSeatingButton, setSelectedSeatingButton] = useState(
    'do-you-have-a-restaurant-no1-btn',
  );

  // To use the JWT credentials
  const appContext = useContext(AppContext);
  // Handle file upload helper function

  const data = {
    first_name: appContext.state.signedInStatus ? appContext.state.user.first_name : firstName,
    last_name: appContext.state.signedInStatus ? appContext.state.user.last_name : lastName,
    email: appContext.state.signedInStatus ? appContext.state.user.email : email,
    host_user_id: appContext.state.user.id,

    has_hosted_anything_before: hostedEvents,
    has_hosted_other_things_before: otherHostedEvents,
    have_a_restaurant: haveYouHostedCountry,
    cuisine_type: hostedCountry,
    seating_option: hasSeating,
    want_people_to_discover_your_cuisine: discoverCuisine,
    able_to_provide_food_samples: provideSamples,
    is_host: 'yes',
  };
  console.log('data from host submit', data);

  // Mount Become a Host page
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  const onChangeHostedEvents = function (event) {
    //takes the file and reads the file from buffer of array

    if (event.target.value === 'No' || hostedEvents.includes('No')) {
      hostedEvents.length = 0;

      setHostedEvents(hostedEvents.concat(event.target.value));
    }

    // console.log("hosted events", hostedEvents)
    else if (hostedEvents.includes(event.target.value)) {
      console.log('error');
    } else if (event.target.value === 'Other' || hostedEvents.includes('Other')) {
      hostedEvents.length = 0;

      setOpenCompleteOther(!openCompleteOther);
    } else {
      setOpenCompleteOther(false);
      setHostedEvents(hostedEvents.concat(event.target.value));
    }
  };

  //fetch nationalities to display
  useEffect(() => {
    window.scrollTo(0, 0);

    const fetchNationalities = async () => {
      const response = await axios({
        method: 'GET',
        url: '/nationalities',
      });

      setNationalities(response.data.nationalities);
    };

    fetchNationalities();
  }, []);

  const onChangeHostedCountry = function (event) {
    //takes the file and reads the file from buffer of array

    if (hostedCountry.includes(event.target.value)) {
      console.log('error');
    } else {
      setHostedCountry(hostedCountry.concat(event.target.value));
    }
  };

  //function to render over the selected options from dropdown and print them
  const getOptions = function (arr) {
    return arr.map((item) => <button className="festival-card-attend-btn">{item}</button>);
  };

  const changeButton = function (button) {
    return <button className="festival-card-attend-btn">{button}</button>;
  };

  // Render do you have a restaurant question
  const DoYouHaveARestaurant = () => (
    <div className="text-center">
      <div className="row">
        <div className="col-md-6 do-you-have-a-restaurant-yes1">
          <div
            onClick={() => {
              setHaveYouHostedCountry('');
              setHaveYouHostedCountry('Yes');
              changeButton('Yes');
              setNohaveYouHostedCountryButton('do-you-have-a-restaurant-no1-btn');
              setHaveYouHostedCountryButton('do-you-have-a-restaurant-yes1-btn');
              setOpenCompleteProfile(!openCompleteProfile);
              // setSelectedButton("do-you-have-a-restaurant-1-btn")
            }}
            className={haveYouHostedCountryButton}
          >
            Yes
          </div>
        </div>
        <div className="col-md-6 do-you-have-a-restaurant-yes1">
          <div
            onClick={() => {
              setHostedCountry([]);
              setHaveYouHostedCountry('No');
              setOpenCompleteProfile(false);
              setHaveYouHostedCountryButton('do-you-have-a-restaurant-no1-btn');
              setNohaveYouHostedCountryButton('do-you-have-a-restaurant-yes1-btn');
            }}
            className={noHaveYouHostedCountryButton}
          >
            No
          </div>
        </div>
      </div>
    </div>
  );
  // Render Become a Host page
  return (
    <div className="row">
      <Nav />
      <div className="col-lg-8 col-xl-6 px-0 sign-up-background-image-one" />
      <div className="col-lg-4 col-xl-4 px-0 become-a-host">
        <div className="become-a-host-title">Become a Host</div>

        <form noValidate>
          {!appContext.state.signedInStatus && (
            <div>
              <div className="mb-3">
                <div className="input-title">First Name*</div>
                <input
                  type="text"
                  value={firstName}
                  onChange={(e) => setFirstName(e.target.value)}
                  disabled={submitAuthDisabled}
                  className="form-control"
                  required
                />
                {firstNameError && <div className="error-message">{firstNameError}</div>}
              </div>
              <div className="mb-3">
                <div className="input-title">Last Name*</div>
                <input
                  type="text"
                  value={lastName}
                  onChange={(e) => setLastName(e.target.value)}
                  disabled={submitAuthDisabled}
                  className="form-control"
                  required
                />
                {lastNameError && <div className="error-message">{lastNameError}</div>}
              </div>
              <div className="mb-3">
                <div className="input-title">Email*</div>
                <input
                  type="email"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                  disabled={submitAuthDisabled}
                  className="form-control"
                  required
                />
                {emailError ? (
                  <div className="error-message">{emailError}</div>
                ) : errorMessage ? (
                  <div className="error-message">{errorMessage}</div>
                ) : null}
              </div>
            </div>
          )}
          <div className="mb-3">
            <h6>Have You Hosted Anything Before?</h6>
            <div className="input-title">Select all that apply</div>
            <select
              // ref={update}
              name="EventsHosted"
              // defaultValue={profileData.provinceTerritory}
              onChange={onChangeHostedEvents}
              disabled={submitAuthDisabled}
              className="custom-select"
            >
              {listHostedEvents()}
            </select>
            {hostedEvents.length === 0 ? (
              <div className="col-md-4 p-2">
                <p></p>
              </div>
            ) : (
              <div className="col-md-4">{getOptions(hostedEvents)}</div>
            )}
            <div>
              {openCompleteOther ? (
                <div className="mb-3">
                  <div className="input-title">Please list all that you have hosted</div>
                  <textarea
                    value={otherHostedEvents}
                    onChange={(e) => setOtherHostedEvents(e.target.value)}
                    disabled={submitAuthDisabled}
                    className="become-a-host-description"
                    required
                  />
                  {/* {descriptionError && (
                       <div className="error-message">{descriptionError}</div>
                     )} */}
                </div>
              ) : (
                <div className="col-md-4 p-2">
                  <p></p>
                </div>
              )}
            </div>
            <h6>Do you have a restaurant or work for a restaurant with physical location?</h6>
            <DoYouHaveARestaurant />
            {openCompleteProfile ? (
              <div>
                <h6>What kind of cuisines do you serve to your customers?</h6>
                <div className="input-title">Select all that apply</div>

                {nationalities.length && (
                  <select
                    name="nationality_id"
                    label="Nationality"
                    onChange={onChangeHostedCountry}
                    disabled={submitAuthDisabled}
                    className="custom-select"
                    required
                  >
                    <option value="">--Select--</option>
                    {nationalities.map((n) => (
                      <option key={n.id} value={n.nationality}>
                        {n.nationality}
                      </option>
                    ))}
                  </select>
                )}
                {hostedCountry.length === 0 ? (
                  <div className="col-md-4 p-2">
                    <p></p>
                  </div>
                ) : (
                  <div className="col-md-4">{getOptions(hostedCountry)}</div>
                )}
              </div>
            ) : (
              <div className="col-md-4 p-2">
                <p></p>
              </div>
            )}
            <h6>Do you have seating in your restaurant to host guests?</h6>
            <div className="text-center">
              <div className="row">
                <div className="col-md-6 do-you-have-a-restaurant-yes1">
                  <div
                    onClick={() => {
                      setHasSeating('');
                      setHasSeating('Yes');
                      setNoSelectedSeatingButton('do-you-have-a-restaurant-no1-btn');
                      setSelectedSeatingButton('do-you-have-a-restaurant-yes1-btn');
                    }}
                    className={selectedSeatingButton}
                  >
                    Yes
                  </div>
                </div>
                <div className="col-md-6 do-you-have-a-restaurant-yes1">
                  <div
                    onClick={() => {
                      setHasSeating('');
                      setHasSeating('No');
                      setSelectedSeatingButton('do-you-have-a-restaurant-no1-btn');
                      setNoSelectedSeatingButton('do-you-have-a-restaurant-yes1-btn');
                    }}
                    className={noSelectedSeatingButton}
                  >
                    No
                  </div>
                </div>
              </div>
            </div>
            <h6>Would you like more people to discover and experience your National cuisine?</h6>
            <div className="text-center">
              <div className="row">
                <div className="col-md-6 do-you-have-a-restaurant-yes1">
                  <div
                    onClick={() => {
                      setDiscoverCuisine('');
                      setDiscoverCuisine('Yes');
                      setNodiscoverCuisineButton('do-you-have-a-restaurant-no1-btn');
                      setdiscoverCuisineButton('do-you-have-a-restaurant-yes1-btn');
                    }}
                    className={discoverCuisineButton}
                  >
                    Yes
                  </div>
                </div>
                <div className="col-md-6 do-you-have-a-restaurant-yes1">
                  <div
                    onClick={() => {
                      setDiscoverCuisine('');
                      setDiscoverCuisine('No');
                      setdiscoverCuisineButton('do-you-have-a-restaurant-no1-btn');
                      setNodiscoverCuisineButton('do-you-have-a-restaurant-yes1-btn');
                    }}
                    className={NodiscoverCuisineButton}
                  >
                    No
                  </div>
                </div>
              </div>
            </div>
            <h6>Are you able to provide food tasting samples of your menu to our guests?</h6>
            <div className="text-center">
              <div className="row">
                <div className="col-md-6 do-you-have-a-restaurant-yes1">
                  <div
                    onClick={() => {
                      setProvideSamples('');
                      setProvideSamples('Yes');
                      setNoprovideSamplesButton('do-you-have-a-restaurant-no1-btn');
                      setprovideSamplesButton('do-you-have-a-restaurant-yes1-btn');
                    }}
                    className={provideSamplesButton}
                  >
                    Yes
                  </div>
                </div>
                <div className="col-md-6 do-you-have-a-restaurant-yes1">
                  <div
                    onClick={() => {
                      setProvideSamples('');
                      setProvideSamples('No');
                      setprovideSamplesButton('do-you-have-a-restaurant-no1-btn');
                      setNoprovideSamplesButton('do-you-have-a-restaurant-yes1-btn');
                    }}
                    className={NoprovideSamplesButton}
                  >
                    No
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div>
            <Link
              to={{
                pathname: '/become-a-host/food-sample-inquiries',
              }}
            >
              {localStorage.setItem('dataFromBecomeHost', JSON.stringify(data))}
              <button type="submit" disabled={submitAuthDisabled} className="submit-button">
                Next
              </button>
            </Link>
          </div>
        </form>
      </div>
      <div className="col-xl-2 px-0 sign-up-background-image-two" />
    </div>
  );
};

export default BecomeAHost;
