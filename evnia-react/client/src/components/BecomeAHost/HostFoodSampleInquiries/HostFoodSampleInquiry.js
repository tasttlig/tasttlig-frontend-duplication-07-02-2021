// Libraries
import React, { useState, useEffect, useContext } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { v4 as uuidv4 } from 'uuid';
import { toast } from 'react-toastify';
import { AppContext } from '../../../ContextProvider/AppProvider';
// import ProductForm from "./Apply/ApplyForm/FormSteps/ProductForm";
// Components
import Nav from '../../Navbar/Nav';
import {
  listHostedEvents,
  countriesOnPassport,
} from '../../../../src/components/Functions/Functions';

// Styling
import './HostFoodSampleInquiry.scss';
import 'react-toastify/dist/ReactToastify.css';
toast.configure();

const HostFoodSampleInquiry = (props) => {
  // Set initial state

  const [submitAuthDisabled, setSubmitAuthDisabled] = useState(false);

  const [explainSampleOrigins, setExplainSampleOrigins] = useState('');
  const [noexplainSampleOriginsButton, setNoexplainSampleOriginsButton] = useState(
    'do-you-have-a-restaurant-no1-btn',
  );
  const [explainSampleOriginsButton, setexplainSampleOriginsButton] = useState(
    'do-you-have-a-restaurant-no1-btn',
  );

  const [providePrivateDining, setProvidePrivateDining] = useState('');
  const [noprovidePrivateDiningButton, setNoprovidePrivateDiningButton] = useState(
    'do-you-have-a-restaurant-no1-btn',
  );
  const [providePrivateDiningButton, setprovidePrivateDiningButton] = useState(
    'do-you-have-a-restaurant-no1-btn',
  );

  const [moreCourseMeals, setMoreCourseMeals] = useState('');
  const [nomoreCourseMealsButton, setNomoreCourseMealsButton] = useState(
    'do-you-have-a-restaurant-no1-btn',
  );
  const [moreCourseMealsButton, setmoreCourseMealsButton] = useState(
    'do-you-have-a-restaurant-no1-btn',
  );

  const [showcaseCulture, setShowcaseCulture] = useState('');
  const [noshowcaseCultureButton, setNoshowcaseCultureButton] = useState(
    'do-you-have-a-restaurant-no1-btn',
  );
  const [showcaseCultureButton, setshowcaseCultureButton] = useState(
    'do-you-have-a-restaurant-no1-btn',
  );

  const [provideLiveEntertainment, setProvideLiveEntertainment] = useState('');
  const [noprovideLiveEntertainmentButton, setNoprovideLiveEntertainmentButton] = useState(
    'do-you-have-a-restaurant-no1-btn',
  );
  const [provideLiveEntertainmentButton, setprovideLiveEntertainmentButton] = useState(
    'do-you-have-a-restaurant-no1-btn',
  );

  const [otherEntertainment, setOtherEntertainment] = useState('');
  const [nootherEntertainmentButton, setNootherEntertainmentButton] = useState(
    'do-you-have-a-restaurant-no1-btn',
  );
  const [otherEntertainmentButton, setotherEntertainmentButton] = useState(
    'do-you-have-a-restaurant-no1-btn',
  );

  // To use the JWT credentials
  const appContext = useContext(AppContext);

  //retreiving data from the become a host page
  const dataFromBecomeHost = JSON.parse(localStorage.getItem('dataFromBecomeHost'));

  const data = {
    has_hosted_anything_before: dataFromBecomeHost.has_hosted_anything_before,
    have_a_restaurant: dataFromBecomeHost.have_a_restaurant,
    cuisine_type: dataFromBecomeHost.cuisine_type,
    seating_option: dataFromBecomeHost.seating_option,
    want_people_to_discover_your_cuisine: dataFromBecomeHost.want_people_to_discover_your_cuisine,
    able_to_provide_food_samples: dataFromBecomeHost.able_to_provide_food_samples,
    is_host: dataFromBecomeHost.is_host,
    has_hosted_other_things_before: dataFromBecomeHost.has_hosted_other_things_before,

    able_to_explain_the_origins_of_tasting_samples: explainSampleOrigins,
    able_to_proudly_showcase_your_culture: showcaseCulture,
    able_to_provie_private_dining_experience: providePrivateDining,
    able_to_provide_3_or_more_course_meals_to_guests: moreCourseMeals,
    able_to_provide_live_entertainment: provideLiveEntertainment,
    able_to_provide_other_form_of_entertainment: otherEntertainment,
  };

  console.log('data after everything', data);

  // Mount Become a Host page
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  // Render Become a Host page
  return (
    <div className="row">
      <Nav />
      <div className="col-lg-8 col-xl-6 px-0 sign-up-background-image-one" />
      <div className="col-lg-4 col-xl-4 px-0 become-a-host">
        <div className="become-a-host-title">Become a Host</div>

        <form noValidate>
          <div className="mb-3">
            <h6>Are you able to explain the origins of your tasttlig samples to our guests?</h6>
            <div className="text-center">
              <div className="row">
                <div className="col-md-6 do-you-have-a-restaurant-yes1">
                  <div
                    onClick={() => {
                      setExplainSampleOrigins('');
                      setExplainSampleOrigins('Yes');
                      setNoexplainSampleOriginsButton('do-you-have-a-restaurant-no1-btn');
                      setexplainSampleOriginsButton('do-you-have-a-restaurant-yes1-btn');
                    }}
                    className={explainSampleOriginsButton}
                  >
                    Yes
                  </div>
                </div>
                <div className="col-md-6 do-you-have-a-restaurant-yes1">
                  <div
                    onClick={() => {
                      setExplainSampleOrigins('');
                      setExplainSampleOrigins('No');
                      setexplainSampleOriginsButton('do-you-have-a-restaurant-no1-btn');
                      setNoexplainSampleOriginsButton('do-you-have-a-restaurant-yes1-btn');
                    }}
                    className={noexplainSampleOriginsButton}
                  >
                    No
                  </div>
                </div>
              </div>
            </div>
            <h6>Are you able to proudly showcase your culture with food and entertainment?</h6>
            <div className="text-center">
              <div className="row">
                <div className="col-md-6 do-you-have-a-restaurant-yes1">
                  <div
                    onClick={() => {
                      setShowcaseCulture('');
                      setShowcaseCulture('Yes');
                      setNoshowcaseCultureButton('do-you-have-a-restaurant-no1-btn');
                      setshowcaseCultureButton('do-you-have-a-restaurant-yes1-btn');
                    }}
                    className={showcaseCultureButton}
                  >
                    Yes
                  </div>
                </div>
                <div className="col-md-6 do-you-have-a-restaurant-yes1">
                  <div
                    onClick={() => {
                      setShowcaseCulture('');
                      setShowcaseCulture('No');
                      setshowcaseCultureButton('do-you-have-a-restaurant-no1-btn');
                      setNoshowcaseCultureButton('do-you-have-a-restaurant-yes1-btn');
                    }}
                    className={noshowcaseCultureButton}
                  >
                    No
                  </div>
                </div>
              </div>
            </div>
            <h6>Are you able to provide a private dining experience for our guests?</h6>
            <div className="text-center">
              <div className="row">
                <div className="col-md-6 do-you-have-a-restaurant-yes1">
                  <div
                    onClick={() => {
                      setProvidePrivateDining('');
                      setProvidePrivateDining('Yes');
                      setNoprovidePrivateDiningButton('do-you-have-a-restaurant-no1-btn');
                      setprovidePrivateDiningButton('do-you-have-a-restaurant-yes1-btn');
                    }}
                    className={providePrivateDiningButton}
                  >
                    Yes
                  </div>
                </div>
                <div className="col-md-6 do-you-have-a-restaurant-yes1">
                  <div
                    onClick={() => {
                      setProvidePrivateDining('');
                      setProvidePrivateDining('No');
                      setprovidePrivateDiningButton('do-you-have-a-restaurant-no1-btn');
                      setNoprovidePrivateDiningButton('do-you-have-a-restaurant-yes1-btn');
                    }}
                    className={noprovidePrivateDiningButton}
                  >
                    No
                  </div>
                </div>
              </div>
            </div>
            <h6>Are you able to provide 3 or more course meals to our guests if necessary?</h6>
            <div className="text-center">
              <div className="row">
                <div className="col-md-6 do-you-have-a-restaurant-yes1">
                  <div
                    onClick={() => {
                      setMoreCourseMeals('');
                      setMoreCourseMeals('Yes');
                      setNomoreCourseMealsButton('do-you-have-a-restaurant-no1-btn');
                      setmoreCourseMealsButton('do-you-have-a-restaurant-yes1-btn');
                    }}
                    className={moreCourseMealsButton}
                  >
                    Yes
                  </div>
                </div>
                <div className="col-md-6 do-you-have-a-restaurant-yes1">
                  <div
                    onClick={() => {
                      setMoreCourseMeals('');
                      setMoreCourseMeals('No');
                      setmoreCourseMealsButton('do-you-have-a-restaurant-no1-btn');
                      setNomoreCourseMealsButton('do-you-have-a-restaurant-yes1-btn');
                    }}
                    className={nomoreCourseMealsButton}
                  >
                    No
                  </div>
                </div>
              </div>
            </div>
            <h6>Are you able to provide live entertainment to our guests?</h6>
            <div className="text-center">
              <div className="row">
                <div className="col-md-6 do-you-have-a-restaurant-yes1">
                  <div
                    onClick={() => {
                      setProvideLiveEntertainment('');
                      setProvideLiveEntertainment('Yes');
                      setNoprovideLiveEntertainmentButton('do-you-have-a-restaurant-no1-btn');
                      setprovideLiveEntertainmentButton('do-you-have-a-restaurant-yes1-btn');
                    }}
                    className={provideLiveEntertainmentButton}
                  >
                    Yes
                  </div>
                </div>
                <div className="col-md-6 do-you-have-a-restaurant-yes1">
                  <div
                    onClick={() => {
                      setProvideLiveEntertainment('');
                      setProvideLiveEntertainment('No');
                      setprovideLiveEntertainmentButton('do-you-have-a-restaurant-no1-btn');
                      setNoprovideLiveEntertainmentButton('do-you-have-a-restaurant-yes1-btn');
                    }}
                    className={noprovideLiveEntertainmentButton}
                  >
                    No
                  </div>
                </div>
              </div>
            </div>
            <h6>Are you able to provide any other form of entertainment to our guests?</h6>
            <div className="text-center">
              <div className="row">
                <div className="col-md-6 do-you-have-a-restaurant-yes1">
                  <div
                    onClick={() => {
                      setOtherEntertainment('');
                      setOtherEntertainment('Yes');
                      setNootherEntertainmentButton('do-you-have-a-restaurant-no1-btn');
                      setotherEntertainmentButton('do-you-have-a-restaurant-yes1-btn');
                    }}
                    className={otherEntertainmentButton}
                  >
                    Yes
                  </div>
                </div>
                <div className="col-md-6 do-you-have-a-restaurant-yes1">
                  <div
                    onClick={() => {
                      setOtherEntertainment('');
                      setOtherEntertainment('No');
                      setotherEntertainmentButton('do-you-have-a-restaurant-no1-btn');
                      setNootherEntertainmentButton('do-you-have-a-restaurant-yes1-btn');
                    }}
                    className={nootherEntertainmentButton}
                  >
                    No
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div>
            <Link
              to={{
                pathname: '/become-a-host/customer-service-inquiries',
              }}
            >
              {localStorage.setItem('dataFromHostFoodSampleInquiry', JSON.stringify(data))}
              <button type="submit" disabled={submitAuthDisabled} className="submit-button">
                Next
              </button>
            </Link>
          </div>
        </form>
      </div>
      <div className="col-xl-2 px-0 sign-up-background-image-two" />
    </div>
  );
};

export default HostFoodSampleInquiry;
