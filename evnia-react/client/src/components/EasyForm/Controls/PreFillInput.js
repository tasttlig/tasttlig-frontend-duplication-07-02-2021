// Libraries
import React, { useState, useEffect, useContext } from 'react';

// Components
import { FormContext } from '../Form';

const PreFillInput = (props) => {
  const { register, setValue, formData, readMode } = useContext(FormContext);
  const { name, label, preFill, required, className, children, ...rest } = props;

  const onChange = props.onChange ? props.onChange : () => {};

  const [fieldValue, setFieldValue] = useState(formData[name] || '');

  useEffect(() => {
    setFieldValue(formData[name] || '');
    setValue(name, name === 'email' ? '' : formData[name] || '');
  }, [formData]);

  return (
    <div className="mb-3">
      {!readMode ? (
        <>
          <div className="input-title">
            {label || children}
            {required ? '*' : ''}
          </div>
          <input
            type="text"
            name={name}
            placeholder={preFill}
            value={preFill}
            onChange={onChange}
            disabled={true}
            className={`${className || ''} form-control`}
            ref={register({ required })}
            {...rest}
          />
        </>
      ) : (
        <div className="pb-3">
          <div>
            <h6 className="ez-form__readMode-title">
              {label || children}
              {required ? '*' : ''}
            </h6>
            <span>{fieldValue}</span>
          </div>
        </div>
      )}
    </div>
  );
};

export default PreFillInput;
