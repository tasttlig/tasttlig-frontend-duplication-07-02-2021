// Libraries
import React, { useState, useEffect, useContext } from 'react';

// Components
import { FormContext } from '../Form';

const Input = (props) => {
  const { register, errors, setValue, formData, readMode } = useContext(FormContext);
  const { name, label, disabled, required, className, children, ...rest } = props;

  const fieldError = errors[name];
  const [fieldValue, setFieldValue] = useState(formData[name] || '');

  useEffect(() => {
    setFieldValue(formData[name] || '');
    setValue(name, name === 'email' ? '' : formData[name] || '');
  }, [formData]);

  return (
    <div className="mb-3">
      {!readMode ? (
        <>
          <div className="input-title">
            {label || children}
            {/* {required ? "*" : ""} */}
          </div>
          <input
            type="text"
            name={name}
            defaultValue={fieldValue}
            className={`${className || ''} form-control`}
            disabled={disabled}
            ref={register({ required })}
            {...rest}
          />
          {fieldError && <div className="error-message">This field is required.</div>}
        </>
      ) : (
        <div className="pb-3">
          <div>
            <h6 className="ez-form__readMode-title">
              {label || children}
              {required ? '*' : ''}
            </h6>
            <span>{fieldValue}</span>
          </div>
        </div>
      )}
    </div>
  );
};

export default Input;
