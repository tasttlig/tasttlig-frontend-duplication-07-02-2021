// Libraries
import React, { useState, useEffect, useContext } from 'react';
import axios from 'axios';
import AsyncSelect from 'react-select/async';

// Components
import { FormContext } from '../Form';
import { Select } from '../index';

const RestaurantSelector = (props) => {
  const { formData, setValue } = useContext(FormContext);
  const {
    name,
    label,
    disabled,
    required,
    className,
    children,
    is_restaurant_location,
    ...rest
  } = props;

  const [isRestaurantLocation, setIsRestaurantLocation] = useState(
    props.is_restaurant_location ? props.is_restaurant_location : '',
  );
  const [selectedItem, setSelectedItem] = useState('');

  const onChange = (selectedItem) => {
    setValue('addressLine1', selectedItem.value.business_address_1 || '');
    setValue('addressLine2', selectedItem.value.business_address_2 || '');
    setValue('city', selectedItem.value.city || '');
    setValue('provinceTerritory', selectedItem.value.state || '');
    setValue('postal_code', selectedItem.value.postal_code || '');
    setSelectedItem(selectedItem);
  };

  const loadOptions = async (inputText, callback) => {
    const response = await axios({
      method: 'GET',
      url: `/business/${inputText}`,
    });

    callback(
      response.data.details.data.map((item) => ({
        label: `${item.business_name} ${item.business_address_1}`,
        value: item,
      })),
    );
  };

  useEffect(() => {
    setValue('addressLine1', formData['addressLine1'] || '');
    setValue('addressLine2', formData['addressLine2'] || '');
    setValue('city', formData['city'] || '');
    setValue('provinceTerritory', formData['provinceTerritory'] || '');
    setValue('postal_code', formData['postal_code'] || '');
  }, [formData]);

  return (
    <>
      <Select
        name="is_restaurant_location"
        label="Is this a restaurant location?"
        value={isRestaurantLocation}
        onChange={(e) => {
          setIsRestaurantLocation(e.target.value);
        }}
        disabled={props.disabled}
        required
      >
        <option value="">Select</option>
        <option value="Yes">Yes</option>
        <option value="No">No</option>
      </Select>
      {isRestaurantLocation === 'Yes' && (
        <div className="mb-3">
          <div className="input-title">Restaurant Finder</div>
          <AsyncSelect
            loadOptions={loadOptions}
            value={selectedItem}
            placeholder={'Type Restaurant Name, Location'}
            onChange={onChange}
          />
        </div>
      )}
    </>
  );
};

export default RestaurantSelector;
