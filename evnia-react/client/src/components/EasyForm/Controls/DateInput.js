// Libraries
import React, { useState, useEffect, useContext } from 'react';
import DatePicker from 'react-datepicker';
import moment from 'moment';

// Components
import { FormContext } from '../Form';

const DateInput = (props) => {
  console.log("props from date in put:", props)
  const { register, unregister, errors, setValue, formData, readMode } = useContext(FormContext);
  const {
    name,
    placeholder,
    label,
    disabled,
    required,
    className,
    children,
    format,
    ...rest
  } = props;

  const fieldError = errors[name];
  const [dateValue, setDateValue] = useState(formData[name] ? new Date(formData[name]) : null);
  // const [dateValue, setDateValue] = useState(new Date());
  // console.log('DATE VALUE',dateValue );

  const handleChange = (date) => {
    setValue(name, date);
    setDateValue(date);
  };

  useEffect(() => {
    register({ name }, { required });
    return () => unregister(name);
  }, [register]);

  useEffect(() => {
    const value = formData[name] ? new Date(formData[name]) : null;
    setDateValue(value);
    setValue(name, value);
  }, [formData]);

  return (
    <div className="mb-3">
      {!readMode ? (
        <>
          <div className="input-title">
            {label || children}
            {/* {required ? "*" : ""} */}
          </div>
          <DatePicker
            name={name}
            placeholderText={placeholder}
            dateFormat={format || 'yyyy/MM/dd'}
            selected={dateValue}
            // selected= {new Date()}
            className={`${className || ''} form-control`}
            dropdownMode="select"
            onChange={handleChange}
            peekNextMonth
            showMonthDropdown
            showYearDropdown
            {...rest}
          />
          {fieldError && <div className="error-message">This field is required.</div>}
        </>
      ) : (
        <div className="pb-3">
          <div>
            <h6 className="ez-form__readMode-title">
              {label}
              {required ? '*' : ''}
            </h6>
            <span>{moment(dateValue).format('dddd, MMMM Do YYYY, h:mm:ss a')}</span>
          </div>
        </div>
      )}
    </div>
  );
};

export default DateInput;
