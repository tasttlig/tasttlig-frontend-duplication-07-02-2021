// Libraries
import React, { useState, useEffect, useContext } from 'react';

// Components
import { FormContext } from '../Form';

const PreFillSelect = (props) => {
  const { register, formData, setValue, readMode } = useContext(FormContext);
  const { name, label, preFill, required, className, options, children, ...rest } = props;

  const onChange = props.onChange ? props.onChange : () => {};

  const [fieldValue, setFieldValue] = useState(formData[name] || '');

  useEffect(() => {
    setFieldValue(formData[name] || '');
    setValue(name, formData[name] || '');
  }, [formData]);

  return (
    <div className="mb-3">
      {!readMode ? (
        <>
          <div className="input-title">
            {label ? label : options ? children : ''}
            {required ? '*' : ''}
          </div>
          <select
            name={name}
            placeholder={preFill}
            value={preFill}
            onChange={onChange}
            disabled={true}
            className={`${className || ''} custom-select`}
            ref={register({ required })}
            {...rest}
          >
            {options ? (
              <>
                {options.map((o) => (
                  <option key={o[1]} value={o[1]}>
                    {o[0]}
                  </option>
                ))}
              </>
            ) : (
              <>{children}</>
            )}
          </select>
        </>
      ) : (
        <div>
          <h6 className="ez-form__readMode-title">
            {label ? label : options ? children : ''}
            {required ? '*' : ''}
          </h6>
          <span>{fieldValue || 'N/A'}</span>
        </div>
      )}
    </div>
  );
};

export default PreFillSelect;
