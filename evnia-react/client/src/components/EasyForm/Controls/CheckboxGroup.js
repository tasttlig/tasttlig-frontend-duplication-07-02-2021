// Libraries
import React, { useState, useEffect, useContext } from 'react';

// Components
import { FormContext } from '../Form';

const CheckboxGroup = (props) => {
  const { register, unregister, errors, setValue, formData, readMode } = useContext(FormContext);
  const { name, label, disabled, required, className, children, options, ...rest } = props;

  const getInitialState = (initialValues) => {
    if (options) {
      return options
        .map((option) => ({
          [option[1]]: initialValues.includes(option[1]),
        }))
        .reduce((a, b) => ({ ...a, ...b }));
    }
  };

  const getSelectedValues = () => {
    if (options) {
      return Object.keys(fieldValues).filter((fv) => fieldValues[fv]);
    }
  };

  const fieldError = errors[name];
  const initialValues = formData[name] || [];
  const [fieldValues, setFieldValues] = useState(getInitialState(initialValues));

  useEffect(() => {
    register(
      { name },
      {
        required,
        validate: (value) => {
          return !required || (value && value.length > 0);
        },
      },
    );
    setValue(name, getSelectedValues());
    return () => unregister(name);
  }, [register]);

  useEffect(() => {
    const iv = formData[name] || [];
    setFieldValues(getInitialState(iv));
    setValue(name, getSelectedValues());
  }, [formData]);

  const handleClick = (e) => {
    fieldValues[e.target.value] = e.target.checked;
    setFieldValues(fieldValues);
    setValue(name, getSelectedValues());
  };

  const getReadModeValues = () => {
    if (options) {
      return options
        .filter((option) => fieldValues[option[1]])
        .map((option) => option[0])
        .join(', ');
    }
  };

  return (
    <div className="mb-3">
      {!readMode ? (
        <>
          <div className="input-title">
            {label || children}
            {required ? '*' : ''}
          </div>
          {options &&
            options.map((option) => (
              <div key={option[1]}>
                <input
                  key={`${option[1]}_${fieldValues[option[1]]}`}
                  type="checkbox"
                  name={name}
                  defaultChecked={fieldValues[option[1]]}
                  value={option[1]}
                  className={`${className || ''}`}
                  disabled={disabled}
                  onClick={handleClick}
                  {...rest}
                />{' '}
                <span>{option[0]}</span>
              </div>
            ))}
          {fieldError && <div className="error-message">This field is required.</div>}
        </>
      ) : (
        <div className="pb-3">
          <div>
            <h6 className="ez-form__readMode-title">
              {label}
              {required ? '*' : ''}
            </h6>
            <span>{getReadModeValues()}</span>
          </div>
        </div>
      )}
    </div>
  );
};

export default CheckboxGroup;
