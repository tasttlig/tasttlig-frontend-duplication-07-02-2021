// Libraries
import React from 'react';
import moment from 'moment';

// Date formatting helper function
export const formatDate = (event) => {
  const utcDate = new Date(event);
  const options = { month: 'short', day: '2-digit', year: 'numeric' };
  const standardDate = new Date(
    utcDate.getTime() + Math.abs(utcDate.getTimezoneOffset() * 60000),
  ).toLocaleDateString([], options);
  return standardDate;
};

// Time formatting helper function
export const formatTime = (event) => {
  const options = { hour: '2-digit', minute: '2-digit' };
  return new Date(event).toLocaleTimeString([], options);
};

// Format military to standard time helper function
export const formatMilitaryToStandardTime = (event) => {
  const militaryHours = parseInt(event.substring(0, 2));
  const standardHours = ((militaryHours + 11) % 12) + 1;
  const amPm = militaryHours > 11 ? 'PM' : 'AM';
  const minutes = event.substring(2, 5);
  const standardTime = `${standardHours}${minutes} ${amPm}`;
  return standardTime;
};

// Format time to date time helper function
export const formattedTimeToDateTime = (formattedTime) => {
  const fullDate = new Date();
  const d = moment(fullDate).format('L');
  return moment(`${d} ${formattedTime}`).format();
};

// Format to Canadian phone number helper function
export const formatPhoneNumber = (event) => {
  let phoneNumber = event.replace(/\D/g, '');
  let match = phoneNumber.match(/^(\d{1,3})(\d{0,3})(\d{0,4})$/);
  if (match) {
    phoneNumber = `(${match[1]}${match[2] ? ') ' : ''}${match[2]}${match[3] ? '-' : ''}${match[3]}`;
  }
  return phoneNumber;
};

// Format price helper function
export const formatPrice = (event) => {
  let price = event.replace(/([^\d]*)(\d*(\.\d{0,2})?)(.*)/, '$2');
  return price;
};

// Canadian Provinces and Territories helper function
export const canadaProvincesTerritories = () => (
  <>
    <option value="">--Select--</option>
    <option value="AB">Alberta</option>
    <option value="BC">British Columbia</option>
    <option value="MB">Manitoba</option>
    <option value="NB">New Brunswick</option>
    <option value="NL">Newfoundland and Labrador</option>
    <option value="NT">Northwest Territories</option>
    <option value="NS">Nova Scotia</option>
    <option value="NU">Nunavut</option>
    <option value="ON">Ontario</option>
    <option value="PE">Prince Edward Island</option>
    <option value="QC">Quebec</option>
    <option value="SK">Saskatchewan</option>
    <option value="YK">Yukon</option>
  </>
);

export const countriesOnPassport = () => (
  <>
    <option value="">--Select--</option>
    <option value="African">African</option>
    <option value="Afghan">Afghan</option>
    <option value="American">American</option>
    <option value="Belgian">Belgian</option>
    <option value="Brazilian">Brazilian</option>
    <option value="British">British</option>
    <option value="Canadian">Canadian</option>
    <option value="Chinese">Chinese</option>
    <option value="Cajun">Cajun</option>
    <option value="Cuban">Cuban</option>
    <option value="Egyptian">Egyptian</option>
    <option value="French">French</option>
    <option value="German">German</option>
    <option value="Greek">Greek</option>
    <option value="Hawaiian">Hawaiian</option>
    <option value="Irish">Irish</option>
    <option value="Italian">Italian</option>
    <option value="Indian">Indian</option>
    <option value="Japanese">Japanese</option>
    <option value="Jewish">Jewish</option>
    <option value="Korean">Korean</option>
    <option value="Latvian">Latvian</option>
    <option value="Libyan">Libyan</option>
    <option value="Mexican">Mexican</option>
    <option value="Mormon">Mormon</option>
    <option value="Nigerian">Nigerian</option>
    <option value="Peruvian">Peruvian</option>
    <option value="Portugese">Portugese</option>
    <option value="Philippine">Philippine</option>
    <option value="Polish">Polish</option>
    <option value="Russian">Russian</option>
    <option value="Swedish">Swedish</option>
    <option value="Spanish">Spanish</option>
    <option value="Scottish">Scottish</option>
    <option value="Tahitian">Tahitian</option>
    <option value="Thai">Thai</option>
    <option value="Turkish">Turkish</option>
    <option value="Welsh">Welsh</option>
  </>
);

export const countriesOnPassportPreferences = () => (
  [
    { value: "African",  label: 'African'},
    { value: "Afghan",  label: 'Afghan'},
    { value: "American",  label: 'American'},
    { value: "Belgian",  label: 'Belgian'},
    { value: "Brazilian",  label: 'Brazilian'},
    { value: "British",  label: 'British'},
    { value: "Canadian",  label: 'Canadian'},
    { value: "Chinese",  label: 'Chinese'},
    { value: "Cajun",  label: 'Cajun'},
    { value: "Cuban",  label: 'Cuban'},
    { value: "Egyptian",  label: 'Egyptian'},
    { value: "French",  label: 'French'},
    { value: "German",  label: 'German'},
    { value: "Greek",  label: 'Greek'},
    { value: "Hawaiian",  label: 'Hawaiian'},
    { value: "Irish",  label: 'Irish'},
    { value: "Italian",  label: 'Italian'},
    { value: "Indian",  label: 'Indian'},
    { value: "Japanese",  label: 'Japanese'},
    { value: "Jewish",  label: 'Jewish'},
    { value: "Korean",  label: 'Korean'},
    { value: "Latvian",  label: 'Latvian'},
    { value: "Libyan",  label: 'Libyan'},
    { value: "Mexican",  label: 'Mexican'},
    { value: "Mormon",  label: 'Mormon'},
    { value: "Nigerian",  label: 'Nigerian'},
    { value: "Peruvian",  label: 'Peruvian'},
    { value: "Portugese",  label: 'Portugese'},
    { value: "Philippine",  label: 'Philippine'},
    { value: "Polish",  label: 'Polish'},
    { value: "Russian",  label: 'Russian'},
    { value: "Swedish",  label: 'Swedish'},
    { value: "Spanish",  label: 'Spanish'},
    { value: "Scottish",  label: 'Scottish'},
    { value: "Tahitian",  label: 'Tahitian'},
    { value: "Thai", label: 'Thai'},
    { value: "Turkish", label: 'Turkish'},
    { value: "Welsh",  label: 'Welsh'},
  ]
);

export const foodsOnPassport = () => (
  <>
    <option value="">--Select--</option>
    <option value="Rice">Rice</option>
    <option value="Spaghetti">Spaghetti/Noodles</option>
    <option value="Bread">Bread</option>
    <option value="Vegetarian">Vegetarian</option>
    <option value="Vegan">Vegan</option>
    <option value="Meat">Meat</option>
    <option value="SeaFood">Sea food</option>
    <option value="Salads">Salads</option>
    <option value="Desserts">Desserts</option>
    <option value="Halal">Halal</option>
    <option value="Kosher">Kosher</option>
  </>
);

export const allergiesOnPassport = () => (
  <>
    <option value="">--Select--</option>
    <option value="No allergies">No allergies</option>
    <option value="Milk">Milk</option>
    <option value="Eggs">Eggs</option>
    <option value="Peanuts">Peanuts</option>
    <option value="Tree Nuts">Tree Nuts</option>
    <option value="Soy">Soy</option>
    <option value="Wheat">Wheat</option>
    <option value="Fish">Fish</option>
    <option value="Shellfish">Shellfish</option>
    <option value="Gelatin">Gelatin</option>
    <option value="Gluten">Gluten</option>
    <option value="Gluten">Gluten</option>
    <option value="Meat">Meat</option>
    <option value="Seeds">Seeds</option>
    <option value="Spices">Spices</option>
  </>
);

export const listHostedEvents = () => (
  <>
    <option value="">--Select--</option>
    <option value="No">No</option>
    <option value="Party">Party</option>
    <option value="Celebration">Celebration</option>
    <option value="Festivals">Festivals</option>
    <option value="Other">Other</option>
  </>
);

// Generate string of 4 random alphanumeric characters helper function
export const generateRandomString = () => {
  let text = '';
  let str = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
  for (let i = 0; i < 4; i++) {
    text += str.charAt(Math.floor(Math.random() * str.length));
  }
  return text;
};
