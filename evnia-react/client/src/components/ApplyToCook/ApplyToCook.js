// Libraries
import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';

// Components
import Footer from '../Home/BannerFooter/BannerFooter';

// Styling
import './ApplyToCook.scss';
import '../Home/HowItWorks/HowItWorks.scss';
import tasttligLogoBlack from '../../assets/images/tasttlig-logo-black.png';
import applyToCookBanner from '../../assets/images/apply-to-cook-banner.jpg';
import certificate from '../../assets/images/certificate.png';
import profile from '../../assets/images/profile.png';
import kitchen from '../../assets/images/kitchen.png';
import receipt from '../../assets/images/receipt.png';
import cooking from '../../assets/images/cooking.png';
import foodPackage from '../../assets/images/food-package.png';
import foodDelivery from '../../assets/images/food-delivery.png';
import connect from '../../assets/images/connect.png';
import money from '../../assets/images/money.png';

const ApplyToCook = () => {
  // Mount Apply to Cook page
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  // Render Apply to Cook page
  return (
    <div>
      <div className="navbar">
        <Link exact="true" to="/">
          <img src={tasttligLogoBlack} alt="Tasttlig" className="main-navbar-logo" />
        </Link>
        <Link to="/apply" className="apply-to-cook-navbar-btn">
          Get Started
        </Link>
      </div>

      <div className="row apply-to-cook-header">
        <div className="col-lg-8 apply-to-cook-header-image-content">
          <img src={applyToCookBanner} alt="Apply to Cook" className="apply-to-cook-header-image" />
        </div>
        <div className="col-lg-4 apply-to-cook-header-text-content">
          <div className="apply-to-cook-header-text">
            Tasttlig Cooks are professionals who make money cooking and delivering food to our
            customers.
          </div>
          <Link to="/apply" className="apply-to-cook-header-btn">
            Get Started
          </Link>
        </div>
      </div>

      <div className="text-center apply-to-cook-body">
        <div className="apply-to-cook-body-title">Tasttlig Cook</div>

        <div className="apply-to-cook-body-section">
          <div className="apply-to-cook-body-timeline"></div>

          <div className="apply-to-cook-body-sub-title">Apply to Cook</div>

          <div className="row">
            <div className="col text-right apply-to-cook-body-text">
              <img src={certificate} alt="Food Handler Certificate" />
              <span className="apply-to-cook-body-timeline-circle-right"></span>
            </div>
            <div className="col text-left apply-to-cook-body-text">
              <div>Submit your Food Handler Certificate</div>
            </div>
          </div>

          <div className="row">
            <div className="col text-right apply-to-cook-body-text">
              <div>Become a Tasttlig Cook</div>
            </div>
            <div className="col text-left apply-to-cook-body-text">
              <span className="apply-to-cook-body-timeline-circle-left"></span>
              <img src={profile} alt="Profile" />
            </div>
          </div>

          <div className="row">
            <div className="col text-right apply-to-cook-body-text">
              <img src={receipt} alt="Receipt" />
              <span className="apply-to-cook-body-timeline-circle-right"></span>
            </div>
            <div className="col text-left apply-to-cook-body-text">
              <div>Accept your first cooking order</div>
            </div>
          </div>

          <div className="apply-to-cook-body-sub-title">Cook the Food</div>

          <div className="row">
            <div className="col text-right apply-to-cook-body-text">
              <div>Book a commercial kitchen</div>
            </div>
            <div className="col text-left apply-to-cook-body-text">
              <span className="apply-to-cook-body-timeline-circle-left"></span>
              <img src={kitchen} alt="Commercial Kitchen" />
            </div>
          </div>

          <div className="row">
            <div className="col text-right apply-to-cook-body-text">
              <img src={cooking} alt="Cooking" />
              <span className="apply-to-cook-body-timeline-circle-right"></span>
            </div>
            <div className="col text-left apply-to-cook-body-text">
              <div>Cook the food</div>
            </div>
          </div>

          <div className="row">
            <div className="col text-right apply-to-cook-body-text">
              <div>Package the food for delivery</div>
            </div>
            <div className="col text-left apply-to-cook-body-text">
              <span className="apply-to-cook-body-timeline-circle-left"></span>
              <img src={foodPackage} alt="Food Package" />
            </div>
          </div>

          <div className="apply-to-cook-body-sub-title">Deliver the Food</div>

          <div className="row">
            <div className="col text-right apply-to-cook-body-text">
              <img src={foodDelivery} alt="Food Delivery" />
              <span className="apply-to-cook-body-timeline-circle-right"></span>
            </div>
            <div className="col text-left apply-to-cook-body-text">
              <div>Book a delivery service</div>
            </div>
          </div>

          <div className="row">
            <div className="col text-right apply-to-cook-body-text">
              <div>Connect with the customer</div>
            </div>
            <div className="col text-left apply-to-cook-body-text">
              <span className="apply-to-cook-body-timeline-circle-left"></span>
              <img src={connect} alt="Client and Customer" />
            </div>
          </div>

          <div className="row">
            <div className="col text-right apply-to-cook-body-text">
              <img src={money} alt="Money" />
              <span className="apply-to-cook-body-timeline-circle-right"></span>
            </div>
            <div className="col text-left apply-to-cook-body-text">
              <div>Deliver the food to the customer</div>
            </div>
          </div>

          <Link exact="true" to="/apply" className="apply-to-cook-body-btn">
            Get Started
          </Link>
        </div>

        {/* Responsive Design */}
        <div className="apply-to-cook-body-section-responsive">
          <div className="apply-to-cook-body-sub-title-responsive">Apply to Cook</div>

          <div className="mb-4">
            <img src={certificate} alt="Food Handler Certificate" />
            <div className="apply-to-cook-body-text-responsive">
              Submit your Food Handler Certificate
            </div>
          </div>

          <div className="mb-4">
            <img src={profile} alt="Profile" />
            <div className="apply-to-cook-body-text-responsive">Become a Tasttlig Cook</div>
          </div>

          <div className="mb-4">
            <img src={receipt} alt="Receipt" />
            <div className="apply-to-cook-body-text-responsive">
              Accept your first cooking order
            </div>
          </div>

          <div className="apply-to-cook-body-sub-title-responsive">Cook the Food</div>

          <div className="mb-4">
            <img src={kitchen} alt="Commercial Kitchen" />
            <div className="apply-to-cook-body-text-responsive">Book a commercial kitchen</div>
          </div>

          <div className="mb-4">
            <img src={cooking} alt="Cooking" />
            <div className="apply-to-cook-body-text-responsive">Cook the food</div>
          </div>

          <div className="mb-4">
            <img src={foodPackage} alt="Food Package" />
            <div className="apply-to-cook-body-text-responsive">Package the food for delivery</div>
          </div>

          <div className="apply-to-cook-body-sub-title-responsive">Deliver the Food</div>

          <div className="mb-4">
            <img src={foodDelivery} alt="Food Delivery" />
            <div className="apply-to-cook-body-text-responsive">Book a delivery service</div>
          </div>

          <div className="mb-4">
            <img src={connect} alt="Client and Customer" />
            <div className="apply-to-cook-body-text-responsive">Connect with the customer</div>
          </div>

          <div className="mb-4">
            <img src={money} alt="Money" />
            <div className="apply-to-cook-body-text-responsive">
              Deliver the food to the customer
            </div>
          </div>

          <Link exact="true" to="/apply" className="apply-to-cook-body-btn-responsive">
            Get Started
          </Link>
        </div>
      </div>

      <Footer />
    </div>
  );
};

export default ApplyToCook;
