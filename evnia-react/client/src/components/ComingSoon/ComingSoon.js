// Libraries
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

// Components
import Nav from '../Navbar/Nav';
import BusinessServices from './BusinessServices/BusinessServices';
import Welcome from './Welcome/Welcome';
import Visitors from './Visitors/Visitors';
import Footer from '../Home/BannerFooter/BannerFooter';
import ShoppingCartHoverList from '../ShoppingCart/ShoppingCartHoverList';

// Styling
import './ComingSoon.scss';

const ComingSoon = () => {
  const [festivalDetails, setFestivalDetails] = useState([]);

  const fetchFestivalDetails = async () => {
    const url = '/get-festivals';

    try {
      const response = await axios({ method: 'GET', url });

      return response;
    } catch (error) {
      return error.response;
    }
  };

  // Mount Coming Soon Landing page
  useEffect(() => {
    window.scrollTo(0, 0);

    fetchFestivalDetails().then((response) => {
      setFestivalDetails(response.data.details);
    });
  }, []);

  // Render Coming Soon Landing page
  return (
    <div>
      <Nav />

      <div className="coming-soon-page">
        <BusinessServices />
        <Welcome />
        <Visitors />
      </div>

      <div>
        <Link
          exact="true"
          to={{
            pathname: `/sponsor`,
            state: {
              festival_id: festivalDetails[0],
            },
          }}
        >
          Sponsor
        </Link>
        <Link
          exact="true"
          to={{
            pathname: `/host`,
            state: {
              festival_id: festivalDetails[1],
            },
          }}
        >
          Host
        </Link>
        <button>Attend</button>
      </div>

      <Footer />
    </div>
  );
};

export default ComingSoon;
