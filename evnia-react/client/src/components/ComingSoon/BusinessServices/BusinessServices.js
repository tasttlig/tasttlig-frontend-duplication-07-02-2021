// Libraries
import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import { AppContext } from '../../../ContextProvider/AppProvider';

// Styling
import comingSoonBusinessServices from '../../../assets/images/coming-soon-business-services.png';

const BusinessServices = () => {
  // To use the JWT credentials
  const appContext = useContext(AppContext);

  return (
    <div className="coming-soon-page-business-services">
      <div className="coming-soon-page-sub-title">Business Services, Coming Soon.</div>
      <div>
        <img
          src={comingSoonBusinessServices}
          alt="Coming Soon Business Services"
          className="coming-soon-page-image"
        />
      </div>
      {!appContext.state.user.id && (
        <div>
          <Link exact="true" to="/sign-up" className="coming-soon-page-sign-up-btn">
            Sign Up
          </Link>
        </div>
      )}
    </div>
  );
};

export default BusinessServices;
