// Libraries
import React, { Component } from 'react';
import axios from 'axios';
import LazyLoad from 'react-lazyload';
import Modal from 'react-modal';
import uuid from 'uuid';
import { toast } from 'react-toastify';
import { AppContext } from '../../../ContextProvider/AppProvider';

// Components
import Comment from './Comment/Comment';
import { formatDate } from '../../Functions/Functions';

// Styling
import './ResourcesCard.scss';
import 'react-toastify/dist/ReactToastify.css';

toast.configure();

export default class ResourcesCard extends Component {
  // Set initial state
  state = {
    load: false,
    reportOpened: false,
    report: '',
    submitAuthDisabled: false,
  };

  // To use the JWT credentials
  static contextType = AppContext;

  // Load image helper function
  onLoad = () => {
    this.setState({ load: true });
  };

  // Open modal type helper function
  openModal = (modalType) => () => {
    if (modalType === 'report') {
      this.setState({ reportOpened: true });
    }
  };

  // Close modal type helper function
  closeModal = (modalType) => () => {
    if (modalType === 'report') {
      this.setState({ reportOpened: false });
    }
  };

  // See resources publisher profile helper function
  handleResourcesPublisher = () => {
    this.props.history.push({
      pathname: `/resources/publisher/${uuid.v4(this.props.publisher)}`,
      state: {
        profileImage: this.props.profileImage,
        verified: this.props.verified,
        firstName: this.props.firstName,
        lastName: this.props.lastName,
        phoneNumber: this.props.phoneNumber,
        email: this.props.email,
      },
    });
  };

  // User input change helper function
  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  // Validate user input for reporting resources post helper function
  validateReport = () => {
    let reportError = '';

    // Render report error message
    if (!this.state.report) {
      reportError = 'Description is required.';
    }

    // Set validation error state
    if (reportError) {
      this.setState({ reportError });
      return false;
    }

    return true;
  };

  // Flag resources post helper function
  handleSubmitFlagResourcesPost = async (event) => {
    event.preventDefault();

    const isValid = this.validateReport();

    if (isValid) {
      const url = '/flagged-forums';

      const acc_token = localStorage.getItem('access_token');

      const headers = {
        Authorization: `Bearer ${acc_token}`,
      };

      const data = {
        post_id: this.props.id.toString(),
        flagged_email: this.context.state.user.email,
        flagged_profile_img_url: this.context.state.user.profile_img_url,
        flagged_first_name: this.context.state.user.first_name,
        flagged_body: this.state.report,
        post_profile_img_url: this.props.profileImage,
        post_first_name: this.props.firstName,
        post_title: this.props.resourcesPostTitle,
        post_body: this.props.resourcesPostBody,
        post_img_url: this.props.resourcesPostImage,
      };

      try {
        const response = await axios({ method: 'POST', url, headers, data });

        if (response) {
          setTimeout(() => {
            window.location.reload();
          }, 2000);

          toast(`Success! Thank you for flagging ${this.props.firstName}'s resources post!`, {
            type: 'success',
            autoClose: 2000,
          });

          this.setState({
            reportError: '',
            submitAuthDisabled: true,
          });
        }
      } catch (error) {
        console.log(error);

        toast('Error! Something went wrong!', {
          type: 'error',
          autoClose: 2000,
        });
      }
    }
  };

  // Render Resources card
  render = () => {
    const { load, reportOpened, report, reportError, submitAuthDisabled } = this.state;

    const {
      id,
      profileImage,
      firstName,
      lastName,
      resourcesPostCategory,
      resourcesPostMethodOfTransportation,
      resourcesPostTitle,
      resourcesPostBody,
      resourcesPostImage,
      createdAt,
    } = this.props;

    return (
      <div className="pt-5 resources-card">
        <LazyLoad once>
          <div className="resources-card-content">
            <div className="row text-left">
              {profileImage ? (
                <span onClick={this.handleResourcesPublisher}>
                  <img
                    src={profileImage}
                    alt={firstName}
                    onLoad={this.onLoad}
                    className={load ? 'resources-card-user-picture' : 'loading-image'}
                  />
                </span>
              ) : (
                <span
                  onClick={this.handleResourcesPublisher}
                  className="fas fa-user-circle fa-3x resources-card-default-picture"
                ></span>
              )}
              <span>
                <div onClick={this.handleResourcesPublisher} className="resources-card-user-name">
                  {`${firstName} ${lastName}`}
                </div>
                <div className="resources-card-user-date">{`${formatDate(createdAt)}`}</div>
              </span>
            </div>

            <div>
              {/* Report Modal */}
              <Modal
                isOpen={reportOpened}
                onRequestClose={this.closeModal('report')}
                ariaHideApp={false}
                className="report-resources-post-modal"
              >
                <div className="form-group">
                  <span
                    onClick={this.closeModal('report')}
                    className="fas fa-times fa-2x close-modal"
                  ></span>
                </div>

                <div className="modal-title">Report this Resources Post</div>

                {this.context.state.user.verified ? (
                  <form onSubmit={this.handleSubmitFlagResourcesPost} noValidate>
                    <div className="form-group">
                      <textarea
                        name="report"
                        placeholder="Why are your reporting this resources post?"
                        value={report}
                        onChange={this.handleChange}
                        disabled={submitAuthDisabled}
                        className="report-resources-post-input"
                        required
                      />
                      <span className="fas fa-pencil-alt report-resources-post-input-icon"></span>
                      {reportError ? <div className="error-message">{reportError}</div> : null}
                    </div>

                    <div>
                      <button type="submit" disabled={submitAuthDisabled} className="modal-btn">
                        Report
                      </button>
                    </div>
                  </form>
                ) : (
                  <div>Please verify your email.</div>
                )}
              </Modal>

              {this.context.state.signedInStatus ? (
                <div onClick={this.openModal('report')} className="resources-card-flag">
                  <span className="fas fa-flag"></span>
                </div>
              ) : null}
            </div>

            <div className="resources-card-description">
              <div className="form-group">
                <div className="text-left">{`Category: ${resourcesPostCategory}`}</div>
                {resourcesPostCategory ? (
                  <div className="text-left">
                    {`Transportation: ${resourcesPostMethodOfTransportation}`}
                  </div>
                ) : null}
              </div>
              <div className="row resources-card-title">{resourcesPostTitle}</div>
              <div className="row resources-card-body">{resourcesPostBody}</div>
              {resourcesPostImage ? (
                <div className="text-center">
                  <img
                    src={resourcesPostImage}
                    alt={resourcesPostTitle}
                    onLoad={this.onLoad}
                    className={load ? 'resources-post-image' : 'loading-image'}
                  />
                </div>
              ) : null}
            </div>

            <Comment postId={id} firstName={firstName} />
          </div>
        </LazyLoad>
      </div>
    );
  };
}
