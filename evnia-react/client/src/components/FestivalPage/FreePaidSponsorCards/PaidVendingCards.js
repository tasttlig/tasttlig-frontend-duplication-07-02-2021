// Libraries
import React, { useState, useContext, useEffect, Fragment } from 'react';
import { useHistory } from 'react-router-dom';
import axios from 'axios';
import LazyLoad from 'react-lazyload';
import Modal from 'react-modal';
import Flag from 'react-world-flags';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import { AppContext } from '../../../ContextProvider/AppProvider';
// import {
//   Select,
//   Input
// } from "../../../EasyForm";

// Components
import ImageSlider from '../../ImageSlider/ImageSlider';
import {
  formatDate,
  formatMilitaryToStandardTime,
  formattedTimeToDateTime,
} from '../../Functions/Functions';

// Styling
import '../../Passport-Old/PassportCard/PassportCard.scss';
import './FreeSampleCard.scss';
import 'react-toastify/dist/ReactToastify.css';

toast.configure();

const PaidVendCard = (props) => {
  const appContext = useContext(AppContext);
  const userRole = appContext.state.user.role;
  //

  //console.log('appcontext from food sample created: ', appContext);
  // Set initial state
  const [load, setLoad] = useState(false);
  const [passportDetailsOpened, setPassportDetailsOpened] = useState(false);
  const [submitAuthDisabled, setSubmitAuthDisabled] = useState(false);
  const history = useHistory();
  const [errorMessage, setErrorMessage] = useState('');
  const [canClaim, setCanClaim] = useState(true);
  const [claimQuantity, setClaimQuantity] = useState(0);
  const [userSubscriptions, setUserSubscriptions] = useState([]);
  const [claimReservationList, setClaimReservationList] = useState([]);
  const [festivalDetails, setFestivalDetails] = useState([]);
  const valueOfTicketFromFestivalDetails = localStorage.getItem('valueFromFestivalTicket');

    // console.log("valueOfTicketFromFestivalDetails", typeof valueOfTicketFromFestivalDetails )

  const idOfCard =
    props.foodSampleProps.product_id ||
    props.foodSampleProps.service_id ||
    props.foodSampleProps.experience_id;

  // Set dietary restrictions
  let dietaryRestrictions = [];
  props.foodSampleProps.is_vegetarian && dietaryRestrictions.push('vegetarian');
  props.foodSampleProps.is_vegan && dietaryRestrictions.push('vegan');
  props.foodSampleProps.is_gluten_free && dietaryRestrictions.push('glutenFree');
  props.foodSampleProps.is_halal && dietaryRestrictions.push('halal');

  // Set days available
  let daysAvailable = [];
  props.foodSampleProps.is_available_on_monday && daysAvailable.push('available_on_monday');
  props.foodSampleProps.is_available_on_tuesday && daysAvailable.push('available_on_tuesday');
  props.foodSampleProps.is_available_on_wednesday && daysAvailable.push('available_on_wednesday');
  props.foodSampleProps.is_available_on_thursday && daysAvailable.push('available_on_thursday');
  props.foodSampleProps.is_available_on_friday && daysAvailable.push('available_on_friday');
  props.foodSampleProps.is_available_on_saturday && daysAvailable.push('available_on_saturday');
  props.foodSampleProps.is_available_on_sunday && daysAvailable.push('available_on_sunday');

  const getUserSubscriptions = async () => {
    const response = await axios({
      method: 'GET',
      url: `/valid-user-subscriptions/${appContext.state.user.id}`,
    });
    console.log('r u', response.data.user);
    setUserSubscriptions(response.data.user);
    //return response;
  };
  
  // Fetch festival details helper function
  const fetchFestivalDetails = async () => {
    try {
      const response = await axios({
        method: 'GET',
        url: `/festival/${props.festivalId}`,
      });

      setFestivalDetails(response.data.details);
     
      return response;
    } catch (error) {
      return error.response;
    }
  };
  console.log("festival Details from businessownerItems:", festivalDetails)

  useEffect(() => {
    getUserSubscriptions();
     fetchFestivalDetails();

  }, []);

  const getSelectedServices = (ids) => {
    if (ids.length > 0) {
      return ids && ids.map((i) => '' + i);
    }
    return [];
  };

  // Fetch user festival reservations helper function
  const fetchUserClaimFestivalReservations = async () => {
    try {
      const url = '/all-product-claim/user/reservations';
      const acc_token = localStorage.getItem('access_token');
      const headers = { Authorization: `Bearer ${acc_token}` };

      return await axios({ method: 'GET', url, headers });
    } catch (error) {
      return error.response;
    }
  };

  // Mount Festival Reservations page
  useEffect(() => {
    window.scrollTo(0, 0);
    setLoad(true);

    fetchUserClaimFestivalReservations().then(({ data }) => {
      console.log(data.details);
      setClaimReservationList(data.details);
    });
    setLoad(false);
  }, []);

  const hasSubscriptionAndValid = () => {
    let result = false;
    userSubscriptions.length > 0 &&
      userSubscriptions.map((sub) => {
        if (sub.subscription_code.startsWith('G')) {
          if (
            sub.user_subscription_status === 'ACTIVE' &&
            (sub.suscribed_festivals == null ||
              getSelectedServices(sub.suscribed_festivals).includes(props.festivalId))
          ) {
            console.log('sub has fest or null', props.festivalId);
            result = true;
          }
        } else if (sub.subscription_code.startsWith('H')) {
          result = true;
        }
      });
    if (!result) {
      toast(`You have no valid subscription to attend this festival!`, {
        type: 'info',
        autoClose: 2000,
      });
    }
    console.log('result', result);
    return result;
  };

  // Open modal type helper function
  const openModal = (modalType) => () => {
    if (modalType === 'passport-details') {
      setPassportDetailsOpened(true);
    }
  };
  
  // Close modal type helper function
  const closeModal = (modalType) => () => {
    if (modalType === 'passport-details') {
      setPassportDetailsOpened(false);
    }
  };

  //check if the ticket has been bought already
  const valueFromTicket = localStorage.getItem('valueFromTicketDashboard');

  // Edit current food sample helper function
  const handleEditFoodSample = () => {
    console.log("im inside the handle foddsample functn,", props)
    const propState = {
      foodSampleId: props.foodSampleProps.product_id,
      images: props.foodSampleProps.image_urls,
      title: props.foodSampleProps.title,
      nationality_id: props.foodSampleProps.nationality_id,
      start_date: props.foodSampleProps.start_date,
      end_date: props.foodSampleProps.end_date,
      festival_selected: props.foodSampleProps.festival_selected,
      start_time: formattedTimeToDateTime(props.foodSampleProps.start_time),
      end_time: formattedTimeToDateTime(props.foodSampleProps.end_time),
      product_size: props.foodSampleProps.product_size,
      quantity: props.foodSampleProps.quantity,
      dietaryRestrictions,
      daysAvailable,
      spice_level: props.foodSampleProps.spice_level,
      city: props.foodSampleProps.city,
      provinceTerritory: props.foodSampleProps.state,
      postal_code: props.foodSampleProps.postal_code,
      description: props.foodSampleProps.description,
    }
    
    console.log("propState handle foddsample functn,", propState)
    history.push({
      pathname: '/edit-food-sample',
      state: propState
    });
  };

   // Edit current food sample helper function
   const handleEditExperience = () => {
    history.push({
      pathname: '/edit-experience',
      state: {
        experienceId: props.foodSampleProps.experience_id,
        experience_images: props.foodSampleProps.image_urls,
        experience_name: props.foodSampleProps.experience_name,
        nationality_id: props.foodSampleProps.nationality_id,
        start_date: props.foodSampleProps.start_date,
        end_date: props.foodSampleProps.end_date,
        festival_selected: props.foodSampleProps.festival_selected,
        start_time: formattedTimeToDateTime(props.foodSampleProps.start_time),
        end_time: formattedTimeToDateTime(props.foodSampleProps.end_time),
        experience_size_scope: props.foodSampleProps.experience_size_scope,
        experience_price: props.foodSampleProps.experience_price,
        experience_capacity: props.foodSampleProps.experience_capacity,
        spice_level: props.foodSampleProps.spice_level,
        experience_description: props.foodSampleProps.experience_description,
        // actual_price: props.foodSampleProps.price,
        // promotional_price: props.foodSampleProps.discounted_price
      },
    });
  };

   // Edit current food sample helper function
   const handleEditServices = () => {
    history.push({
      pathname: '/edit-service',
      state: {
        service_id: props.foodSampleProps.service_id,
        service_images: props.foodSampleProps.image_urls,
        service_name: props.foodSampleProps.service_name,
        service_nationality_id: props.foodSampleProps.service_nationality_id,
        start_date: props.foodSampleProps.start_date,
        end_date: props.foodSampleProps.end_date,
        festival_selected: props.foodSampleProps.festival_selected,
        start_time: formattedTimeToDateTime(props.foodSampleProps.start_time),
        end_time: formattedTimeToDateTime(props.foodSampleProps.end_time),
        service_size_scope: props.foodSampleProps.service_size_scope,
        service_capacity: props.foodSampleProps.service_capacity,
        service_price: props.foodSampleProps.service_price,
        // spice_level: props.foodSampleProps.spice_level,
        service_description: props.foodSampleProps.service_description,

      },
    });
  };

  let passportIdOrEmail = null;

  const validatePassportIdOrEmail = () => {
    passportIdOrEmail = appContext.state.user.passport_id;

    if (!passportIdOrEmail) {
      setErrorMessage('Passport Id or Email is required.');

      return false;
    } else {
      setErrorMessage('');

      return true;
    }
  };
  let isSelected;
  if (props.selected) {
    isSelected = props.selected.includes(idOfCard);
  }

  const handleCheckboxChange = (event) => {
    //setIsSelected((prev) => !prev);
    //props.setSelected(value);
    //console.log(idOfCard);
    if (isSelected) {
      props.setSelected((prev) => prev.filter((id) => id !== idOfCard));
    } else {
      props.setSelected((prev) => [...prev, idOfCard]);
    }
  };

  // Claim food sample helper function
  const handleClaimProducts = async (event) => {
    // if (hasSubscriptionAndValid()) {
      if (!event) {
        event = window.event;
      }
      event.preventDefault();
      const isValid = validatePassportIdOrEmail();
      if (props.passportId || isValid) {
        const url = '/all-products-claim';

        const acc_token = await localStorage.getItem('access_token');

        const headers = { Authorization: `Bearer ${acc_token}` };
        
        if(appContext.state.user.verified === false) {
          toast('Verify your account to Enjoy festivals!', {
            type: 'error',
            autoClose: 3000,
          });
        } else {
        
        if(parseInt(claimQuantity) === 0) {
          toast('Error! You have to select a quantity!', {
            type: 'error',
            autoClose: 3000,
          });
        } else {

          if ((parseInt(claimQuantity) > 0) && (parseInt(claimQuantity) <= props.foodSampleProps.quantity)) {
            const totalClaimQuantity =
              props.foodSampleProps.claimed_total_quantity + parseInt(claimQuantity);
              
            const data = {
              food_sample_claim_user: props.passportId ? props.passportId : passportIdOrEmail,
              food_sample_id: props.foodSampleProps.product_id,
              claimed_quantity: parseInt(claimQuantity),
              claimed_total_quantity: totalClaimQuantity,
              claim_viewable_id:
                appContext.state.user.id + props.festivalId + props.foodSampleProps.product_id,
              foodsample_festival_name: festivalDetails[0].festival_name,
              festival_id: props.festivalId,
            };
            try {
              const response = await axios({ method: 'POST', url, headers, data });
              console.log(response);
              if (response && response.data && response.data.success) {
                setTimeout(() => {
                  window.location.reload();
                }, 2000);

                toast(`Success! Thank you for claiming ${props.foodSampleProps.title}!`, {
                  type: 'success',
                  autoClose: 2000,
                });

                setSubmitAuthDisabled(true);
                setErrorMessage('');
              } else if (
                response &&
                response.data &&
                !response.data.success &&
                appContext.state.user.passport_id
              ) {
                toast(response.data.message, {
                  type: 'error',
                  autoClose: 2000,
                });
              } else {
                setErrorMessage(response.data.message);
                setCanClaim(response.data.canClaim);
              }
            } catch (error) {
              toast('Error! Something went wrong!', {
                type: 'error',
                autoClose: 2000,
              });
            }
          } else {
            toast('Exceeded the Quantity available!', {
              type: 'error',
              autoClose: 2000,
            });
          }
      }
      }

    }
    // }
  };

  // Archive current food sample helper function
  const handleSubmitArchiveFoodSample = async (event) => {
    event.preventDefault();

    try {
      const url = `/food-sample/update/${props.foodSampleProps.food_sample_id}`;
      const acc_token = localStorage.getItem('access_token');
      const headers = { Authorization: `Bearer ${acc_token}` };
      const data = {
        food_sample_update_data: {
          status: 'ARCHIVED',
        },
      };
      const response = await axios({ method: 'PUT', url, headers, data });

      if (response && response.data && response.data.success) {
        setTimeout(() => {
          window.location.reload();
        }, 2000);

        toast(`Success! Thank you for archiving ${props.foodSampleProps.title}!`, {
          type: 'success',
          autoClose: 2000,
        });

        setSubmitAuthDisabled(true);
      }
    } catch (error) {
      toast('Error! Something went wrong!', {
        type: 'error',
        autoClose: 2000,
      });
    }
  };

  const truncateDescription = (description) => {
    if (description.length > 130) {
      if (description[description.length - 1] === '.') {
        description = description.substr(0, 130) + '..';
      } else {
        description = description.substr(0, 130) + '...';
      }
    }
    return description;
  };

  const getClaimReservationstatus = function (claimReservationListItem) {

    let status = false;
    for (let claimedItem of claimReservationListItem ) {
      if(claimedItem.claimed_product_id === props.foodSampleProps.product_id || claimedItem.claimed_experience_id === props.foodSampleProps.experience_id || claimedItem.claimed_service_id === props.foodSampleProps.service_id) {
        status = true
       } 
    }
 
    return status;
  };

 
  console.log('props from food samples:', props.foodSampleProps);
  console.log('appContext.state.user.id:', appContext.state.user.id);

  const productPaymentFirst = async () => {
    try{
    await history.push({
      pathname: `/payment/product/${props.foodSampleProps.product_id}`,
      /* title: props.title,
      festivalId: props.festivalId, */
      quantityClaimed: parseInt(claimQuantity),
    });
    localStorage.setItem("discount", props.discount);
  } catch (error) {
    return error.response;
  }
  };

  const experiencePaymentFirst = async () => {
    await history.push({
      pathname: `/payment/experience/${props.foodSampleProps.experience_id}`,
      quantityClaimed: parseInt(claimQuantity),
      /* title: props.title,
      festivalId: props.festivalId, */
    });
    localStorage.setItem("discount", props.discount);
  };

  const servicePaymentFirst = async () => {
    await history.push({
      pathname: `/payment/service/${props.foodSampleProps.service_id}`,
      quantityClaimed: parseInt(claimQuantity),
      /* title: props.title,
      festivalId: props.festivalId, */
    });
    localStorage.setItem("discount", props.discount);
  };

  // Claim experience helper function
  const handleClaimExperiences = async (event) => {
    console.log("im props from handle claim experience:", props)
    // if (hasSubscriptionAndValid()) {
      if (!event) {
        event = window.event;
      }
      event.preventDefault();
      const isValid = validatePassportIdOrEmail();
      if (props.passportId || isValid) {
        const url = '/all-experiences-claim';
        const acc_token = await localStorage.getItem('access_token');
        const headers = { Authorization: `Bearer ${acc_token}` };

      if(parseInt(claimQuantity) === 0) {
          toast('Error! You have to select a quantity!', {
            type: 'error',
            autoClose: 2000,
          });
      } else {

        if ((parseInt(claimQuantity) > 0) && (parseInt(claimQuantity) <= props.foodSampleProps.experience_capacity)) {
          const totalClaimQuantity =
            props.foodSampleProps.claimed_total_quantity + parseInt(claimQuantity);

          const data = {
            food_sample_claim_user: props.passportId ? props.passportId : passportIdOrEmail,
            food_sample_id: props.foodSampleProps.experience_id,
            claimed_quantity: parseInt(claimQuantity),
            claimed_total_quantity: totalClaimQuantity,
            claim_viewable_id:
              appContext.state.user.id + props.festivalId + props.foodSampleProps.experience_id,
            foodsample_festival_name: festivalDetails[0].festival_name,
            festival_id: props.festivalId,
          };
          try {
            const response = await axios({ method: 'POST', url, headers, data });
            console.log(response);
            if (response && response.data && response.data.success) {
              setTimeout(() => {
                window.location.reload();
              }, 2000);

              toast(`Success! Thank you for claiming ${props.foodSampleProps.experience_name}!`, {
                type: 'success',
                autoClose: 2000,
              });

              setSubmitAuthDisabled(true);
              setErrorMessage('');
            } else if (
              response &&
              response.data &&
              !response.data.success &&
              appContext.state.user.passport_id
            ) {
              toast(response.data.message, {
                type: 'error',
                autoClose: 2000,
              });
            } else {
              setErrorMessage(response.data.message);
              setCanClaim(response.data.canClaim);
            }
          } catch (error) {
            console.log("error coming from experiences:", error)
            toast('Error! Something went wrong!', {
              type: 'error',
              autoClose: 2000,
            });
          }
        } else {
          toast('Exceeded the Quantity available!', {
            type: 'error',
            autoClose: 2000,
          });
        }
      }
      }
    // }
  };


  // Claim service helper function
  const handleClaimServices = async (event) => {
    console.log("im props from handle claim service:", props)
    // if (hasSubscriptionAndValid()) {
      if (!event) {
        event = window.event;
      }
      event.preventDefault();
      const isValid = validatePassportIdOrEmail();
      if (props.passportId || isValid) {
        const url = '/all-services-claim';

        const acc_token = await localStorage.getItem('access_token');

        const headers = { Authorization: `Bearer ${acc_token}` };

      if(parseInt(claimQuantity) === 0) {
        toast('Error! You have to select a quantity!', {
          type: 'error',
          autoClose: 2000,
        });
      } else {
        if ( (parseInt(claimQuantity) > 0) && (parseInt(claimQuantity) <= props.foodSampleProps.service_capacity)) {
          const totalClaimQuantity =
            props.foodSampleProps.claimed_total_quantity + parseInt(claimQuantity);

          const data = {
            food_sample_claim_user: props.passportId ? props.passportId : passportIdOrEmail,
            food_sample_id: props.foodSampleProps.service_id,
            claimed_quantity: parseInt(claimQuantity),
            claimed_total_quantity: totalClaimQuantity,
            claim_viewable_id:
              appContext.state.user.id + props.festivalId + props.foodSampleProps.service_id,
            foodsample_festival_name: festivalDetails[0].festival_name,
            festival_id: props.festivalId,
          };
          try {
            const response = await axios({ method: 'POST', url, headers, data });
            console.log(response);
            if (response && response.data && response.data.success) {
              setTimeout(() => {
                window.location.reload();
              }, 2000);

              toast(`Success! Thank you for claiming ${props.foodSampleProps.service_name}!`, {
                type: 'success',
                autoClose: 2000,
              });

              setSubmitAuthDisabled(true);
              setErrorMessage('');
            } else if (
              response &&
              response.data &&
              !response.data.success &&
              appContext.state.user.passport_id
            ) {
              toast(response.data.message, {
                type: 'error',
                autoClose: 2000,
              });
            } else {
              setErrorMessage(response.data.message);
              setCanClaim(response.data.canClaim);
            }
          } catch (error) {
            console.log("error coming from services:", error)
            toast('Error! Something went wrong!', {
              type: 'error',
              autoClose: 2000,
            });
          }
        } else {
          toast('Exceeded the Quantity available!', {
            type: 'error',
            autoClose: 2000,
          });
        }
      }
      }
    // }
  };

  // Render Current Food Samples Card
  return (
    <div className="col-lg-6 col-md-12">
      {props.isSelectable && (
        <input type="checkbox" value={isSelected} onChange={handleCheckboxChange}></input>
      )}
      <LazyLoad once>
        {/* start modal */}

        <Modal
          isOpen={passportDetailsOpened}
          onRequestClose={closeModal('passport-details')}
          ariaHideApp={false}
          className="passport-details-modal"
        >
          <div className="text-right">
            <button
              aria-label={`Close ${props.foodSampleProps.title} Details Modal`}
              onClick={closeModal('passport-details')}
              className="fas fa-times fa-2x close-modal"
            ></button>
          </div>

          <div className="text-left">
            <div className="passport-item-title">
              {props.foodSampleProps.title
                ? props.foodSampleProps.title
                : props.foodSampleProps.service_name
                ? props.foodSampleProps.service_name
                : props.foodSampleProps.experience_name
                ? props.foodSampleProps.experience_name
                : null}
            </div>
            <div className="mb-3 passport-image__full-width">
              <ImageSlider images={props.foodSampleProps.image_urls} />
            </div>

            <div>
              <div className="mb-3">
               { props.foodSampleProps.start_time !== null && props.foodSampleProps.end_time !== null ? (
                 <>
               <div className="passport-details-sub-title">Time</div>
                <div>{`${
                  props.foodSampleProps.start_time &&
                  formatMilitaryToStandardTime(props.foodSampleProps.start_time)
                } to ${
                  props.foodSampleProps.end_time &&
                  formatMilitaryToStandardTime(props.foodSampleProps.end_time)
                }`}</div>
                </>
                ) : null}
              {/* </div> */}
              {/* <div className="mb-3"> */}
                {props.foodSampleProps.experience_size_scope ? (
                  <div className="passport-details-sub-title">Size Scope</div>
                ) : (
                  <div className="passport-details-sub-title">Sample Size</div>
                )}
                <div>{`${
                  props.foodSampleProps.product_size
                    ? props.foodSampleProps.product_size
                    : props.foodSampleProps.experience_size_scope
                    ? props.foodSampleProps.experience_size_scope
                    :  props.foodSampleProps.service_size_scope
                    ? props.foodSampleProps.service_size_scope : null
                }`}</div>
              {/* </div> */}
              {/* <div className="mb-3"> */}
                {props.foodSampleProps.experience_size_scope ? (
                  <div className="passport-details-sub-title">Capacity</div>
                ) : (
                  <div className="passport-details-sub-title">Quantity left</div>
                )}
                <div>{`${
                  props.foodSampleProps.quantity
                    ? props.foodSampleProps.quantity
                    : props.foodSampleProps.experience_capacity
                    ? props.foodSampleProps.experience_capacity
                    : props.foodSampleProps.service_capacity
                    ? props.foodSampleProps.service_capacity
                    : 0
                }`}</div>
              {/* </div> */}
              </div>
              {props.foodSampleProps.experience_id ? (
                <div >
                  <div className="passport-details-sub-title">Additional Information</div>
                  {props.foodSampleProps.experience_type && (
                    <div>Available for {props.foodSampleProps.experience_type}</div>
                  )}
                  {props.foodSampleProps.additional_pricing_info && (
                    <div>Pricing Info: {props.foodSampleProps.additional_pricing_info}</div>
                  )}
                  {props.foodSampleProps.additional_information && (
                    <div>{props.foodSampleProps.additional_information}</div>
                  )}
                </div>
              ) : props.foodSampleProps.product_id ? (
                <div >
                  <div className="passport-details-sub-title">Days Available</div>
                  {props.foodSampleProps.is_available_on_monday && <div>Monday</div>}
                  {props.foodSampleProps.is_available_on_tuesday && <div>Tuesday</div>}
                  {props.foodSampleProps.is_available_on_tuesday && <div>Wednesday</div>}
                  {props.foodSampleProps.is_available_on_thursday && <div>Thursday</div>}
                  {props.foodSampleProps.is_available_on_friday && <div>Friday</div>}
                  {props.foodSampleProps.is_available_on_saturday && <div>Saturday</div>}
                  {props.foodSampleProps.is_available_on_sunday && <div>Sunday</div>}
                </div>
              ) : null}
              <div >
                <div className="passport-details-sub-title">Description</div>
                <div>
                  {props.foodSampleProps.description
                    ? props.foodSampleProps.description
                    : props.foodSampleProps.experience_description
                    ? props.foodSampleProps.experience_description
                    :  props.foodSampleProps.service_description
                    ? props.foodSampleProps.service_description
                    : null}
                </div>
              </div>
            </div>
          </div>
          <div>
         {!userRole ? (null): (
           <>
           <div>
                { claimReservationList.length && getClaimReservationstatus(claimReservationList) === true ? ( <button
                        onClick={
                          props.foodSampleProps.quantity > 0 &&
                          props.foodSampleProps.food_sample_creater_user_id !==
                            appContext.state.user.id
                            ? appContext.state.user.passport_id
                              ? productPaymentFirst
                              : closeModal('passport-details')
                            : props.foodSampleProps.experience_capacity > 0 
                            ? appContext.state.user.passport_id
                              ? experiencePaymentFirst
                              : closeModal('passport-details')
                            : props.foodSampleProps.service_capacity > 0 
                            ? appContext.state.user.passport_id
                              ? servicePaymentFirst
                              : closeModal('passport-details')
                              : null
                        }
                        disabled={submitAuthDisabled}
                        className={
                          'mt-3 btn btn-dark call-to-action-btn'
                        }
                      >
                        Buy Again
                      </button> ) : (
                <div  className="user-item-card-footer">
                  {props.foodSampleProps.quantity <= 0 ? (
                    <button className="mt-3 btn btn-dark call-to-action-btn">Sold Out!</button>
     
                    ):(
                      <div className="Quantity-selection">
                      <button
                        onClick={
                          props.foodSampleProps.quantity > 0 &&
                          props.foodSampleProps.food_sample_creater_user_id !==
                            appContext.state.user.id
                            ? appContext.state.user.passport_id
                              ? productPaymentFirst
                              : closeModal('passport-details')
                            : props.foodSampleProps.experience_capacity > 0 
                            ? appContext.state.user.passport_id
                              ? experiencePaymentFirst
                              : closeModal('passport-details')
                            : props.foodSampleProps.service_capacity > 0 
                            ? appContext.state.user.passport_id
                              ? servicePaymentFirst
                              : closeModal('passport-details')
                              : null
                        }
                        disabled={submitAuthDisabled}
                        className={
                          'mt-3 btn btn-dark call-to-action-btn'
                        }
                      >
                        Buy
                      </button>
                    </div>
                    // )}
                    // </>
                  )}
                  </div>
                  )}
                </div>
            </>
            )}
          </div>
          <div className="close-modal-bottom-content">
            <span onClick={closeModal('passport-details')} className="close-modal-bottom">
              Close
            </span>
          </div>
        </Modal>

        {/* end modal */}

        <div className="d-flex flex-column main-user-item-card">
          <div onClick={openModal('passport-details')}>
            <div className="food-sample-card-photo">
              <Link exact="true" to={`/festival/owner/${props.foodSampleProps.product_user_id ? props.foodSampleProps.product_user_id : props.foodSampleProps.service_user_id ? props.foodSampleProps.service_user_id : props.foodSampleProps.experience_user_id}`}>
              <span className="item-host-name">
                <i className="fas fa-map-marker-alt"></i>
                <strong>{props.foodSampleProps.business_name}</strong>
              </span>
              </Link>
              <span className="days-remaining">
                <strong>{props.foodSampleProps.nationality}</strong>
                {/* <Flag
                  code={props.foodSampleProps.service_nationality_id}
                  className="passport-card-flag"
                /> */}
              </span>

              <img
                src={props.foodSampleProps.image_urls[0]}
                alt={props.foodSampleProps.title}
                onLoad={() => setLoad(true)}
                className={load ? "passport-image" : "loading-image"}
              />
            </div>
          </div>
          {/* {valueOfTicketFromFestivalDetails === 'Free' || valueOfTicketFromFestivalDetails === 'Already Paid' ? (
            <>
              {props.foodSampleProps.price && props.foodSampleProps.price !== null || 
              props.foodSampleProps.service_price && props.foodSampleProps.service_price !== null || 
              props.foodSampleProps.experience_price && props.foodSampleProps.experience_price !== null ?
              (
               < div className="user-item-card-footer">
                Price: ${props.foodSampleProps.price ? props.foodSampleProps.price : 
          props.foodSampleProps.service_price ?  props.foodSampleProps.service_price : 
          props.foodSampleProps.experience_price ? props.foodSampleProps.experience_price : null }
                </div>
                 ):(<div className="user-item-card-footer">
                   <b>     Enjoy for Free!</b>
                   </div>)}
            </>
          ):
          ( 
          <div className="user-item-card-footer">
          Price: ${props.foodSampleProps.price ? props.foodSampleProps.price : 
          props.foodSampleProps.service_price ?  props.foodSampleProps.service_price : 
          props.foodSampleProps.experience_price ? props.foodSampleProps.experience_price : 2.00}
          </div>
          )} */}
          <div className="user-item-card-footer">
            <div className="user-item-card-footer-size">
              {`Size: ${
                  props.foodSampleProps.product_size
                    ? props.foodSampleProps.product_size
                    : props.foodSampleProps.experience_size_scope
                    ? props.foodSampleProps.experience_size_scope
                    :  props.foodSampleProps.service_size_scope
                    ? props.foodSampleProps.service_size_scope : null}`}
            </div>
            <div className="user-item-footer-title">
              {props.foodSampleProps.service_name
                ? props.foodSampleProps.service_name
                : props.foodSampleProps.title
                ? props.foodSampleProps.title
                : props.foodSampleProps.experience_name}
            </div>
          </div>

          { props.foodSampleProps.discounted_price ? 
          (
            <div className="user-item-card-footer">
             Discount Price: {props.foodSampleProps.discounted_price} ({props.foodSampleProps.promotion_name})
          </div>
          ) : null }
          <div className="user-item-card-footer">

            <div className="user-item-footer-buttons">

            {!userRole ? ( null ) :(
              <>
              {(userRole && userRole.includes('ADMIN')) ||
              props.foodSampleProps.business_details_user_id === appContext.state.user.id ? (
                <div className="edit-archive-festival-details">
                  <div disabled={submitAuthDisabled} onClick={props.foodSampleProps.title ? handleEditFoodSample : props.foodSampleProps.service_name ? handleEditServices : handleEditExperience }>
                    {(userRole && userRole.includes('ADMIN')) ||
                    (appContext.state.user.id &&
                      appContext.state.user.id === props.foodSampleProps.business_details_user_id) ? (
                      <button className="edit-button">Edit</button>
                    ) : null}
                  </div>

                </div>
              ) : (
             
                <div >
                { claimReservationList.length && getClaimReservationstatus(claimReservationList) === true ? ( <button
                        onClick={
                          props.foodSampleProps.quantity > 0 &&
                          props.foodSampleProps.food_sample_creater_user_id !==
                            appContext.state.user.id
                            ? appContext.state.user.passport_id
                              ? productPaymentFirst
                              : closeModal('passport-details')
                            : props.foodSampleProps.experience_capacity > 0 
                            ? appContext.state.user.passport_id
                              ? experiencePaymentFirst
                              : closeModal('passport-details')
                            : props.foodSampleProps.service_capacity > 0 
                            ? appContext.state.user.passport_id
                              ? servicePaymentFirst
                              : closeModal('passport-details')
                              : null
                        }
                        disabled={submitAuthDisabled}
                        className={
                          'add-to-cart-button'
                        }
                      >
                        Buy Again: 
                        {props.discount === 1 ? props.foodSampleProps.price ? '$' + props.foodSampleProps.price : props.foodSampleProps.service_price ? '$' + props.foodSampleProps.service_price : props.foodSampleProps.experience_price ? '$' + props.foodSampleProps.experience_price : 2.00 : null}
                        
                        <p className = "crossed-out">{props.discount < 1 ? props.foodSampleProps.price ? '$' + props.foodSampleProps.price : 
                        props.foodSampleProps.service_price ? '$' +  props.foodSampleProps.service_price: props.foodSampleProps.experience_price ? '$' + 
                        props.foodSampleProps.experience_price : 2.00 : null} </p>
                         
                          {props.discount < 1 ? props.foodSampleProps.price ? '$' + (props.foodSampleProps.price * props.discount).toFixed(2) : 
                         (props.foodSampleProps.service_price * props.discount) ?  '$' + (props.foodSampleProps.service_price * props.discount).toFixed(2) : 
                         (props.foodSampleProps.experience_price) ? '$' + (props.foodSampleProps.experience_price * props.discount).toFixed(2) : 2.00 : null}
                      </button> ) : (
                <div>
                  {props.foodSampleProps.quantity <= 0 ? (
                    <button className="add-to-cart-button">Sold Out!</button>
                
                    ):(
                      <div className="Quantity-selection">
                      <button
                        onClick={
                          props.foodSampleProps.quantity > 0 &&
                          props.foodSampleProps.food_sample_creater_user_id !==
                            appContext.state.user.id
                            ? appContext.state.user.passport_id
                              ? productPaymentFirst
                              : closeModal('passport-details')
                            : props.foodSampleProps.experience_capacity > 0 
                            ? appContext.state.user.passport_id
                              ? experiencePaymentFirst
                              : closeModal('passport-details')
                            : props.foodSampleProps.service_capacity > 0 
                            ? appContext.state.user.passport_id
                              ? servicePaymentFirst
                              : closeModal('passport-details')
                              : null
                        }
                        disabled={submitAuthDisabled}
                        className={
                          'add-to-cart-button'
                        }
                      >
                        Buy: 

                        {props.discount === 1 ? props.foodSampleProps.price ? '$' + props.foodSampleProps.price : props.foodSampleProps.service_price ? '$' + props.foodSampleProps.service_price : props.foodSampleProps.experience_price ? '$' + props.foodSampleProps.experience_price : 2.00 : null}
                        
                        <p className = "crossed-out">{props.discount < 1 ? props.foodSampleProps.price ? '$' + props.foodSampleProps.price : 
                        props.foodSampleProps.service_price ? '$' +  props.foodSampleProps.service_price: props.foodSampleProps.experience_price ? '$' + 
                        props.foodSampleProps.experience_price : 2.00 : null} </p>
                         
                          {props.discount < 1 ? props.foodSampleProps.price ? '$' + (props.foodSampleProps.price * props.discount).toFixed(2) : 
                         (props.foodSampleProps.service_price * props.discount) ?  '$' + (props.foodSampleProps.service_price * props.discount).toFixed(2) : 
                         (props.foodSampleProps.experience_price) ? '$' + (props.foodSampleProps.experience_price * props.discount).toFixed(2) : 2.00 : null}
                      </button>
                    </div>
                    // )}
                    // </>
                  )}
                  </div>
                  )}
                </div>
              )}
              </>
              )}
            </div>
          </div>
        </div>
      </LazyLoad>
    </div>
  );
};

export default PaidVendCard;
