
// Libraries
import React, { useState, useContext } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import LazyLoad from 'react-lazyload';
import moment from 'moment';
import { toast } from 'react-toastify';
import { AppContext } from '../../../ContextProvider/AppProvider';
import canadianCuisine from '../../../assets/images/canadian-cuisine.jpg';
import defaultRestaurantImage from '../../../assets/images/default-restaurant-image.png';

// Components
import ImageSlider from '../../ImageSlider/ImageSlider';

// Styling
import './RestaurantCard.scss';
import 'react-toastify/dist/ReactToastify.css';
import liveFestival from '../../../assets/images/live.png';

toast.configure();

const RestaurantCard = (props) => {
  // Set initial state
  const [load, setLoad] = useState(false);
  const [submitAuthDisabled, setSubmitAuthDisabled] = useState(false);

  // Calculate the number of days between start/end and current date
  const d1 = new Date(props.startDate);
  const d2 = new Date(props.endDate);
  const d3 = new Date();
  const millisecondsInDay = 86400000;
  const upcomingDays = Math.floor((d1 - d3) / millisecondsInDay) + 1;
  const endingDays = Math.ceil((d2 - d3) / millisecondsInDay) + 1;

  // Set date and time
  /*  const startDateTime = moment(
    new Date(props.startDate).toISOString().split("T")[0] +
    "T" +
    props.startTime +
    ".000Z"
  ).add(new Date().getTimezoneOffset(), "m");
  const endDateTime = moment(
    new Date(props.startDate).toISOString().split("T")[0] +
    "T" +
    props.endTime +
    ".000Z"
  ).add(new Date().getTimezoneOffset(), "m"); */

  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const userRole = appContext.state.user.role;

  // Host festival helper function
  const handleHostFestival = async (event) => {
    event.preventDefault();

    const url = '/host-festival';

    const acc_token = localStorage.getItem('access_token');

    const headers = { Authorization: `Bearer ${acc_token}` };

    const data = {
      festival_id: props.festivalId,
      festival_restaurant_host_id: appContext.state.user.id,
    };

    try {
      const response = await axios({ method: 'POST', url, headers, data });

      if (response && response.data && response.data.success) {
        setTimeout(() => {
          window.location.reload();
        }, 2000);

        toast(`Success! Thank you for hosting ${props.title}!`, {
          type: 'success',
          autoClose: 2000,
        });

        setSubmitAuthDisabled(true);
      }
    } catch (error) {
      toast('Error! Something went wrong!', {
        type: 'error',
        autoClose: 2000,
      });
    }
  };

  // Sponsor festival helper function
  const handleSponsorFestival = async (event) => {
    event.preventDefault();

    const url = '/sponsor-festival';

    const acc_token = localStorage.getItem('access_token');

    const headers = { Authorization: `Bearer ${acc_token}` };

    const data = {
      festival_id: props.festivalId,
      festival_business_sponsor_id: appContext.state.user.id,
    };

    try {
      const response = await axios({ method: 'POST', url, headers, data });

      if (response && response.data && response.data.success) {
        setTimeout(() => {
          window.location.reload();
        }, 2000);

        toast(`Success! Thank you for sponsoring ${props.title}!`, {
          type: 'success',
          autoClose: 2000,
        });

        setSubmitAuthDisabled(true);
      }
    } catch (error) {
      toast('Error! Something went wrong!', {
        type: 'error',
        autoClose: 2000,
      });
    }
  };

  /* const truncateDescription = (description) => {
    if (description.length > 130) {
      if (description[description.length - 1] === '.') {
        description = description.substr(0, 130) + '..';
      } else {
        description = description.substr(0, 130) + '...';
      }
    }
    return description;
  }; */

  // Render Festival Card
  return (
    <div className="col-md-8 col-xl-8 restaurant-card">
      <LazyLoad once>
        <div className="d-flex flex-column h-100 main-food-sample-card">
          <div className="food-sample-card-photo">
            <span className="days-remaining">
              {/* <i className="fas fa-map-marker-alt" /> */}
              <Link
                className="restaurant-name-link"
                exact="true"
                to={`/passport/${props.businessId}`}
              >
                <strong>{props.restaurant_name}</strong>
              </Link>
            </span>

            <img
              src={props.images[0] ? props.images[0] : defaultRestaurantImage}
              alt={props.title}
              onLoad={() => setLoad(true)}
              // className={load ? "passport-image" : "loading-image"}
            />
          </div>

          <div className="main-food-sample-info">
            <div className="food-sample-date">
              <i className="fas fa-map-marker-alt"></i>
              <strong>{props.address}</strong>
            </div>
            {/* <div className="food-sample-time">
              <i className="fas fa-clock"></i>
              <strong>
                {`${formatMilitaryToStandardTime(
                  props.foodSampleProps.start_time,
                )} to ${formatMilitaryToStandardTime(props.foodSampleProps.end_time)}`}
                &nbsp;EST
              </strong>
            </div> */}
            <div className="food-sample-name">{props.title}</div>
          </div>

          {/* <div className="edit-archive">
            {!appContext.state.user.id ? (
              <div className="col edit-button">
                <Link exact="true" to="/login">
                  Free Tasting
                </Link>
              </div>
            ) : userRole &&
              !userRole.includes('RESTAURANT') &&
              !userRole.includes('RESTAURANT_PENDING') &&
              !userRole.includes('ADMIN') ? null : (
              <div
                onClick={handleHostFestival}
                disabled={submitAuthDisabled}
                className="edit-button"
              >
                Free Tasting
              </div>
            )}
          </div> */}

          {/* <div className="view-details-button" disabled={submitAuthDisabled}>
            {!appContext.state.user.id ? (
              <Link exact="true" to="/login">
                Purchase
              </Link>
            ) : (
              <Link
                exact="true"
                to={`/payment/${
                  props.itemType === 'product'
                    ? `product/${props.productId}`
                    : props.itemType === 'service'
                    ? `service/${props.serviceId}`
                    : `experience/${props.experienceId}`
                }`}
              >
                Purchase
              </Link>
            )}
          </div> */}
        </div>
      </LazyLoad>
    </div>
  );
};

export default RestaurantCard;
