// Libraries
import React, { useState, useContext } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import LazyLoad from 'react-lazyload';
import moment from 'moment';
import { toast } from 'react-toastify';
import { AppContext } from '../../../ContextProvider/AppProvider';
import canadianCuisine from '../../../assets/images/canadian-cuisine.jpg';
import defaultRestaurantImage from '../../../assets/images/default-restaurant-image.png';

// Components
import ImageSlider from '../../ImageSlider/ImageSlider';

// Styling
import './RestaurantCard.scss';
import 'react-toastify/dist/ReactToastify.css';
import liveFestival from '../../../assets/images/live.png';

toast.configure();

const RestaurantCard = (props) => {
  // Set initial state
  const [load, setLoad] = useState(false);
  const [submitAuthDisabled, setSubmitAuthDisabled] = useState(false);

  // Calculate the number of days between start/end and current date
  const d1 = new Date(props.startDate);
  const d2 = new Date(props.endDate);
  const d3 = new Date();
  const millisecondsInDay = 86400000;
  const upcomingDays = Math.floor((d1 - d3) / millisecondsInDay) + 1;
  const endingDays = Math.ceil((d2 - d3) / millisecondsInDay) + 1;

  // Set date and time
  /*  const startDateTime = moment(
    new Date(props.startDate).toISOString().split("T")[0] +
    "T" +
    props.startTime +
    ".000Z"
  ).add(new Date().getTimezoneOffset(), "m");
  const endDateTime = moment(
    new Date(props.startDate).toISOString().split("T")[0] +
    "T" +
    props.endTime +
    ".000Z"
  ).add(new Date().getTimezoneOffset(), "m"); */

  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const userRole = appContext.state.user.role;
  
  // Host festival helper function
  const handleHostFestival = async (event) => {
    event.preventDefault();

    const url = '/host-festival';

    const acc_token = localStorage.getItem('access_token');

    const headers = { Authorization: `Bearer ${acc_token}` };

    const data = {
      festival_id: props.festivalId,
      festival_restaurant_host_id: appContext.state.user.id,
    };

    try {
      const response = await axios({ method: 'POST', url, headers, data });

      if (response && response.data && response.data.success) {
        setTimeout(() => {
          window.location.reload();
        }, 2000);

        toast(`Success! Thank you for hosting ${props.title}!`, {
          type: 'success',
          autoClose: 2000,
        });

        setSubmitAuthDisabled(true);
      }
    } catch (error) {
      toast('Error! Something went wrong!', {
        type: 'error',
        autoClose: 2000,
      });
    }
  };

  // Sponsor festival helper function
  const handleSponsorFestival = async (event) => {
    event.preventDefault();

    const url = '/sponsor-festival';

    const acc_token = localStorage.getItem('access_token');

    const headers = { Authorization: `Bearer ${acc_token}` };

    const data = {
      festival_id: props.festivalId,
      festival_business_sponsor_id: appContext.state.user.id,
    };

    try {
      const response = await axios({ method: 'POST', url, headers, data });

      if (response && response.data && response.data.success) {
        setTimeout(() => {
          window.location.reload();
        }, 2000);

        toast(`Success! Thank you for sponsoring ${props.title}!`, {
          type: 'success',
          autoClose: 2000,
        });

        setSubmitAuthDisabled(true);
      }
    } catch (error) {
      toast('Error! Something went wrong!', {
        type: 'error',
        autoClose: 2000,
      });
    }
  };

  // Render Festival Card
  return (
    <div className="col-lg-6 col-md-12">
   
    <LazyLoad once>
      <div className="d-flex flex-column main-user-item-card">
            <Link exact="true" className="restaurant-link-click" to={`/festival/owner/${props.businessId}`}>
                <div className="food-sample-card-photo">
                  <span className="item-host-name">
                    <i className="fas fa-map-marker-alt"></i>
                    <strong>{props.restaurant_name}</strong>
                  </span>
                  <span className="days-remaining">
                    {/* <strong>{props.foodSampleProps.nationality}</strong> */}
                  </span>
                  
                  <img
                    src={props.images[0]}
                    alt={props.title}
                    onLoad={() => setLoad(true)}
                    className={load ? "passport-image" : "loading-image"}
                  />
                </div>
            
              {/* <div className="user-item-card-footer">
                <div className="user-item-footer-title">
                  {props.title}
                </div>
              
              </div> */}
          </Link>
      </div>
    </LazyLoad>
  </div>
  );
};

export default RestaurantCard;