// Libraries
import React, { useState, useContext } from 'react';
import Modal from 'react-modal';
import { AppContext } from '../../ContextProvider/AppProvider';

// Components
import ServicesModal from './ServicesModal/ServicesModal';

// Styling
import './Services.scss';

const Services = () => {
  // Set initial state
  const [servicesOpened, setServicesOpened] = useState(false);

  // To use the JWT credentials
  const appContext = useContext(AppContext);

  // Open modal type helper function
  const openModal = (modalType) => () => {
    if (modalType === 'services') {
      setServicesOpened(true);
    }
  };

  // Close modal type helper function
  const closeModal = (modalType) => () => {
    if (modalType === 'services') {
      setServicesOpened(false);
    }
  };

  // Render Services Component
  return (
    <div>
      {/* Services modal */}
      <Modal
        isOpen={servicesOpened}
        onRequestClose={closeModal('services')}
        ariaHideApp={false}
        className="services-modal"
      >
        <div className="form-group">
          <span onClick={closeModal('services')} className="fas fa-times fa-2x close-modal"></span>
        </div>

        <div className="modal-title">Services on Tasttlig</div>

        <ServicesModal />
      </Modal>

      {appContext.state.signedInStatus && appContext.state.user.verified ? (
        <button
          type="button"
          data-toggle="tooltip"
          data-placement="left"
          title="Services"
          onClick={openModal('services')}
          className="fas fa-2x fa-concierge-bell services"
        ></button>
      ) : null}
    </div>
  );
};

export default Services;
