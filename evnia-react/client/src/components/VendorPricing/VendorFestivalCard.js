// // Libraries
// import React, { useState, useContext } from "react";
// import { Link } from "react-router-dom";
// import { useHistory } from "react-router-dom";
// import axios from "axios";
// import LazyLoad from "react-lazyload";
// import moment from "moment";
// import { toast } from "react-toastify";
// import { AppContext } from "../../ContextProvider/AppProvider";

// // Components
// import ImageSlider from "../ImageSlider/ImageSlider";

// // Styling
// import "../Festivals/FestivalCard/FestivalCard.scss";
// import "react-toastify/dist/ReactToastify.css";
// import liveFestival from "../../assets/images/live.png";

// toast.configure();

// const FestivalCard = (props) => {
//   // Set initial state
//   const [load, setLoad] = useState(false);
//   const [submitAuthDisabled, setSubmitAuthDisabled] = useState(false);

//   // Calculate the number of days between start/end and current date
//   const d1 = new Date(props.startDate);
//   const d2 = new Date(props.endDate);
//   const d3 = new Date();
//   const millisecondsInDay = 86400000;
//   const upcomingDays = Math.floor((d1 - d3) / millisecondsInDay) + 1;
//   const endingDays = Math.ceil((d2 - d3) / millisecondsInDay) + 1;
//   const history = useHistory();
//   // Set date and time
//   const startDateTime = moment(
//     new Date(props.startDate).toISOString().split("T")[0] +
//       "T" +
//       props.startTime +
//       ".000Z"
//   ).add(new Date().getTimezoneOffset(), "m");
//   const endDateTime = moment(
//     new Date(props.startDate).toISOString().split("T")[0] +
//       "T" +
//       props.endTime +
//       ".000Z"
//   ).add(new Date().getTimezoneOffset(), "m");

//   // To use the JWT credentials
//   const appContext = useContext(AppContext);
//   const userRole = appContext.state.user.role;

//   // Check festival helper function
//   const handleCheckFestival = () => {
//     props.hostFestivalList &&
//     props.hostFestivalList.includes(appContext.state.user.id)
//       ? toast(`You already selected for ${props.title} hosting!`, {
//           type: "info",
//           autoClose: 2000,
//         })
//       : localStorage.setItem("festival_id", JSON.stringify([props.festivalId]));

//     props.hostFestivalList.includes(props.festivalId)
//       ? props.setHostFestivalList((hostFestivalList) => [
//           ...hostFestivalList.filter((id) => id !== props.festivalId),
//         ])
//       : props.setHostFestivalList((hostFestivalList) => [
//           ...hostFestivalList,
//           props.festivalId,
//         ]);

//     // Passing selected festival id for members redirected to business form
//     props.hostFestivalList.includes(props.festivalId)
//       ? localStorage.setItem(
//           "festival_id",
//           JSON.stringify(props.hostFestivalList)
//         )
//       : localStorage.setItem(
//           "festival_id",
//           JSON.stringify([...props.hostFestivalList, props.festivalId])
//         );

//     // Passing festival title for toast display
//     localStorage.setItem("festival_title", props.title);
//   };

//   // When festival is already sponsored by user
//   const redundanceCheck = () => {
//     toast(`You already selected as a ${props.title} sponsor!`, {
//       type: "info",
//       autoClose: 2000,
//     });
//   };

//   // Host festival helper function
//   const handleHostFestival = async (event) => {
//     event.preventDefault();
//     if (
//       props.hostFestivalList &&
//       props.hostFestivalList.includes(appContext.state.user.id)
//     ) {
//       toast(`You already selected for ${props.title} hosting!`, {
//         type: "info",
//         autoClose: 2000,
//       });
//     } else {
//       const url = "/host-festival";

//       const acc_token = localStorage.getItem("access_token");

//       const headers = { Authorization: `Bearer ${acc_token}` };

//       const data = {
//         festival_id: [props.festivalId],
//         festival_restaurant_host_id: appContext.state.user.id,
//       };

//       try {
//         localStorage.setItem("festival_id", JSON.stringify([props.festivalId]));

//         const response = await axios({ method: "POST", url, headers, data });

//         if (response && response.data && response.data.success) {
//           setTimeout(() => {
//             window.location.reload();
//           }, 2000);

//           toast(`Success! Thank you for hosting ${props.title}!`, {
//             type: "success",
//             autoClose: 2000,
//           });

//           setSubmitAuthDisabled(true);
//         }
//       } catch (error) {
//         toast("Error! Something went wrong!", {
//           type: "error",
//           autoClose: 2000,
//         });
//       }
//     }
//   };

//   //Handle button for editing festival
//   const handleEditFestival = () => {
//     history.push({
//       pathname: "/edit-festival",
//       state: {
//         festivalId: props.festivalId,
//         images: props.images,
//         festival_name: props.title,
//         festival_type: props.type,
//         festival_price: props.price,
//         festival_city: props.city,
//         festival_start_date: props.startDate,
//         festival_end_date: props.endDate,
//         startTime: props.startTime,
//         endTime: props.endTime,
//         festival_description: props.description,

//         // festival_start_time: props.startTime,
//         // festival_end_time: props.endTime,
//       },
//     });
//   };

//   // Sponsor festival helper function
//   const handleSponsorFestival = async (event) => {
//     event.preventDefault();

//     //will be moved sponsorship package form submission
//     /* const url = "/sponsor-festival";

//     const acc_token = localStorage.getItem("access_token");

//     const headers = { Authorization: `Bearer ${acc_token}` };

//     const data = {
//       festival_id: props.festivalId,
//       festival_business_sponsor_id: appContext.state.user.id,
//     };

//     try {
//       const response = await axios({ method: "POST", url, headers, data });

//       if (response && response.data && response.data.success) {
//         setTimeout(() => {
//           window.location.reload();
//         }, 2000);

//         toast(`Success! Thank you for sponsoring ${props.title}!`, {
//           type: "success",
//           autoClose: 2000,
//         });

//         setSubmitAuthDisabled(true);
//       }
//     } catch (error) {
//       toast("Error! Something went wrong!", {
//         type: "error",
//         autoClose: 2000,
//       });
//     } */
//   };
//   // Render Festival Card
//   return (
//     <div className="col-md-6 col-xl-4 festival-card">
//       <LazyLoad once>
//         <div className="d-flex flex-column h-100 card-box">
//           {props.images.length > 1 ? (
//             <div>
//               <ImageSlider images={props.images} isCard={true} />
//             </div>
//           ) : (
//             <img
//               src={props.images[0]}
//               alt={props.title}
//               onLoad={() => setLoad(true)}
//               className={load ? "festival-card-image" : "loading-image"}
//             />
//           )}
//           {/*          <Link
//             exact="true"
//             to={`/festival/${props.festivalId}`}
//             className="festival-card-link"
//           > */}
//           {upcomingDays > 0 ? (
//             <div className="festival-card-date-upcoming">
//               <span className="festival-card-date-upcoming-text">Upcoming</span>
//               <span className="festival-card-date-upcoming-days">{`in ${upcomingDays} day${
//                 upcomingDays > 1 ? "s" : ""
//               }`}</span>
//             </div>
//           ) : upcomingDays === 0 ? (
//             <div className="festival-card-date-live">
//               <img src={liveFestival} alt="Live Festival" />
//             </div>
//           ) : endingDays > 0 ? (
//             <div className="festival-card-date-ending">
//               <span className="festival-card-date-ending-text">Ending</span>
//               <span className="festival-card-date-ending-days">{`in ${endingDays} day${
//                 endingDays > 1 ? "s" : ""
//               }`}</span>
//             </div>
//           ) : null}
//           <Link
//             exact="true"
//             to={`/festival/${props.festivalId}`}
//             className="festival-card-link"
//           >
//             <div className="festival-card-text">
//               <div className="festival-card-title">{props.title}</div>
//               <div className="festival-card-details">
//                 <div className="row">
//                   <div className="col-md-8 festival-card-time">
//                     <span className="fas fa-clock pr-1 festival-card-time-icon"></span>
//                     Starts At:{" "}
//                     {`${moment(startDateTime).format("h:mm a")} - ${moment(
//                       endDateTime
//                     ).format("h:mm a")}`}
//                   </div>
//                   <div className="col-md-4 pr-0 pl-1 festival-card-location">
//                     <span className="fas fa-map-marker-alt pr-1 festival-card-location-icon"></span>
//                     {props.city}
//                   </div>
//                 </div>
//               </div>
//               <div className="festival-card-details">{props.description}</div>
//             </div>
//           </Link>

//           <div className="row pb-2">

//             {/* {!appContext.state.user.id ? (
//               <div className="col-md-4 p-2">
//                 <Link
//                   exact="true"
//                   to="/login"
//                   className="festival-card-attend-btn"
//                 >
//                   Attend
//                 </Link>
//               </div>
//             ) : (
//               <div className="col-md-4 p-2">
//                 <Link
//                   exact="true"
//                   to={`/payment/festival/${props.festivalId}`}
//                   className="festival-card-attend-btn"
//                 >
//                   Attend
//                 </Link>
//               </div> */}

//           </div>
//           {userRole && userRole.includes("ADMIN") ? (
//             <div>
//               <div onClick={handleEditFestival}>
//                 <button disabled={submitAuthDisabled} className="edit-btn">
//                   Edit
//                 </button>
//               </div>
//             </div>
//           ) : (
//             <p></p>
//           )}

//           <div className="festival-card-footer">
//             <div className="row">
//               <div className="col-md-8 festival-card-category">
//                 {props.type}
//               </div>
//               <div className="col-md-4 festival-card-price">{`$${props.price}`}</div>
//             </div>
//           </div>
//           {/* </Link> */}
//         </div>
//       </LazyLoad>
//     </div>
//   );
// };

// export default FestivalCard;
