// Libraries
import React from 'react';

// Components
import SearchButton from './SearchButton';

// Styling
import './ExperienceTypeSearch.scss';

const buttonNames = [
  'Dining Experiences',
  'City Tours',
  'Boat Tours',
  'Music Experiences',
  'Arts Exhibitions',
  'Sports Games',
];

const ExperienceTypeSearch = (props) => (
  <div id="ExperienceTypeSearch">
    {buttonNames.map((buttonName) => (
      <SearchButton
        key={buttonName}
        handleFilterExpByType={props.handleFilterExpByType}
        name={buttonName}
      />
    ))}
  </div>
);

export default ExperienceTypeSearch;
