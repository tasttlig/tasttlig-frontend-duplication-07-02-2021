// Libraries
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

// Components
import Card from './Card';
// import ExperienceTypeSearch from "./ExperienceTypeSearch/ExperienceTypeSearch";

// Styling
import comingSoon from '../../assets/images/coming-soon.jpg';

// GET experiences from the database and render on the explore page
const fetchExperiences = async () => {
  try {
    const response = await axios({
      method: 'GET',
      url: '/experience/all',
    });

    return response;
  } catch (error) {
    return error.response;
  }
};

let renderExperienceCards = (arr) => {
  return arr.map((card, index) => (
    <Card
      key={index}
      publisher={card.user_id}
      publisherPicture={card.profile_img_url}
      firstName={card.first_name}
      lastName={card.last_name}
      history={this.props.history}
    />
  ));
};

const Explore = () => {
  // Set initial state
  const [load, setLoad] = useState(false);
  const [experiences, setExperiences] = useState([]);
  const [filteredExperiences, setFilteredExperiences] = useState([]);

  const handleFilterExpByType = (e) => {
    const filtered = experiences.filter(
      (experience) =>
        experience.experience_type && experience.experience_type.toLowerCase() === e.target.name,
    );
    setFilteredExperiences(filtered);
  };
  // Use effect
  useEffect(() => {
    fetchExperiences().then(({ data }) => {
      setExperiences(data.details);
      setFilteredExperiences(data.details);
    });
  }, []);

  return (
    <div className="experiences-page">
      {/* <div className="form-group experiences-page-title">Experiences</div> */}

      <div>
        <img
          src={comingSoon}
          alt="Coming Soon"
          onLoad={() => setLoad(true)}
          className={load ? 'coming-soon-image' : 'loading-image'}
        />
        <span className="coming-soon-section">
          <div className="coming-soon-title">Coming Soon</div>
          <div className="become-a-host-question">Interested in hosting with Tasttlig?</div>
          <Link to="/become-a-host" className="btn btn-primary become-a-host-btn">
            Become a Host
          </Link>
        </span>
      </div>

      {/* <ExperienceTypeSearch handleFilterExpByType={handleFilterExpByType} /> */}
      {/* <div className="row">
        {filteredExperiences.map(
          (experience, index) =>
            experience.status === "ACTIVE" && (
              <Card
                key={index}
                experienceId={experience.experience_id}
                publisher={experience.experience_creator_user_id}
                image={experience.image_urls[0]}
                title={experience.title}
                price={experience.price}
                capacity={experience.capacity}
                startDate={experience.start_date}
                endDate={experience.end_date}
                address={experience.address}
                city={experience.city}
                provinceTerritory={experience.state}
                postalCode={experience.postal_code}
                description={experience.description}
                phoneNumber={experience.phone_number}
                email={experience.email}
              />
            )
        )}
      </div> */}
    </div>
  );
};

export default Explore;
