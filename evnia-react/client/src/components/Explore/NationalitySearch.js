// Libraries
import React from 'react';
import { flagsUrl } from '../../data/nationality';

// Styling
import './NationalitySearch.scss';

export const NationalitySearch = (props) => {
  const { filterByFlag } = props;

  // Click flag helper function
  const handleClickFlag = (event) => {
    const country = event.target.dataset.countryName;

    filterByFlag(country);
  };

  // Render nationality search component
  return (
    <>
      <div className="flags scroll">
        {flagsUrl().map(({ url, country }, index) => (
          <img
            key={index}
            className="flag"
            src={url}
            data-country-name={country}
            onClick={handleClickFlag}
            alt="Nationality Flags"
          />
        ))}
      </div>
    </>
  );
};
