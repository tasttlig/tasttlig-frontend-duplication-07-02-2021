// Libraries
import React, { useState, useEffect, useContext } from 'react';
import axios from 'axios';
import { loadStripe } from '@stripe/stripe-js';
import { Elements, CardElement, useStripe, useElements } from '@stripe/react-stripe-js';
import { AppContext } from '../../ContextProvider/AppProvider';
import { toast } from 'react-toastify';
import { useHistory } from 'react-router-dom';

toast.configure();

const CheckoutForm = ({ props }) => {
  const appContext = useContext(AppContext);
  // To use the JWT credentials
  console.log('props on checkout form', props);
  const tasttligUserId = appContext.state.user.id;

  const history = useHistory();

  // Set initial state
  const [errorMessage, setErrorMessage] = useState('');
  const [userEmail, setUserEmail] = useState(
    appContext.state.user.email ? appContext.state.user.email : '',
  );
  const [userId, setUserId] = useState(
    appContext.state.user.id ? appContext.state.user.id : '',
  );
  const [anotherUserEmail, setAnotherUserEmail] = useState('');
  const [userEmailError, setUserEmailError] = useState('');
  const stripe = useStripe();
  const elements = useElements();

  let festivalPayments;
  if (localStorage.getItem('festivalPayments')) {
    festivalPayments = localStorage.getItem('festivalPayments').split(',');
  }
  console.log("37", userId, props.totalPrice, Number(festivalPayments[0]));
  const handleSubmit = async (event) => {
    event.preventDefault();

    if (!stripe || !elements) {
      return;
    }

    if (userEmail.length === 0) {
      setUserEmailError('Email is required.');
    } 
     if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(userEmail)) {
      setUserEmailError('Please enter a valid email.');
    } else {
      setUserEmailError('');
    }
    
    try {
      const approveApplication = async () => {
        const festivalId = Number(festivalPayments[0]);
        const ticketPrice = props.totalPrice;
        const url = `/vendor-application-timer/${festivalId}/${ticketPrice}/${userId}/approve`;
        const response = await axios({ method: 'POST', url });
    
        if (response.data.success) {
          console.log("Application pending");
        }
        else {
          console.log("pending error");
        }
      };
      const request_data = {
        item_id: props.props.match.params.item_id,
        item_type: props.props.match.params.item_type,
        email: userEmail,
        userId: userId,
        price: props.totalPrice,
        festivalDiscount: props.festivalDiscount,
        // if user submit multiple festivals to vendor
        vendor_festivals: festivalPayments,
      };

      const { data } = await axios.post('/payment/stripe', request_data);
      
      if (data.success) {
        
        const clientSecret = data.client_secret;
        const result = await stripe.confirmCardPayment(clientSecret, {
          payment_method: {
            card: elements.getElement(CardElement),
            billing_details: {
              email: userEmail,
            },
          },
        });

        if (result.error) {
          setErrorMessage(result.error.message);
        } else {
          // The payment has been processed!
          if (result.paymentIntent.status === 'succeeded') {
            request_data.payment_id = result.paymentIntent.id;
            request_data.email = userEmail;
            request_data.discount = props.discount;
            request_data.subscribed_festivals = props.props.location.festivalId
              ? props.props.location.festivalId
              : festivalPayments;
              request_data.additionalEmail = anotherUserEmail;
            
              const response = await axios.post('/payment/stripe/success', request_data);
              
            // add products and services helper function
            const redirectToAddSponsorInfoPage = async () => {
              //check if user have no product or service added
              //if(productList < 1 && serviceList < 1){
              // await history.push({
              //   pathname: '/',
              //   festivalId: props.props.location.festivalId,
              //   title: props.props.location.title,
              // });
              //} else{ 
                Window.location.href="/"
              // }
            };
            // Submit festival sponsorship helper function
            const handleSponsorFestival = async () => {
           
              const url = '/sponsor-festival';

              const acc_token = localStorage.getItem('access_token');

              const headers = { Authorization: `Bearer ${acc_token}` };

              const data = {
                festival_id: props.props.location.festivalId,
                festival_business_sponsor_id: appContext.state.user.id,
              };

              try {
                const response = await axios({
                  method: 'POST',
                  url,
                  headers,
                  data,
                });
                if (response && response.data && response.data.success) {
                  setTimeout(() => {
                    window.location = "/";
                  }, 2000);

                  toast(`Success! Thank you for sponsoring ${props.props.location.title}!`, {
                    type: 'success',
                    autoClose: 2000,
                  });
                }
              } catch (error) {
                toast('Error! Something went wrong!', {
                  type: 'error',
                  autoClose: 2000,
                });
              }
            };

            const handleVendorFestival = async () => {
              const url = '/vendor-festival';

              const acc_token = localStorage.getItem('access_token');

              const headers = { Authorization: `Bearer ${acc_token}` };

              const data = {
                festival_id: props.props.location.festivalId
                  ? props.props.location.festivalId
                  : festivalPayments,
              };

              try {
                const response = await axios({
                  method: 'POST',
                  url,
                  headers,
                  data,
                });
                if (response && response.data && response.data.success) {
                  localStorage.removeItem('festivalPayments');

                  toast(`Success! Thank you for vending!`, {
                    type: 'success',
                    autoClose: 2000,
                  });
                }
              } catch (error) {
                console.log(error);
                toast('Error! Something went wrong!', {
                  type: 'error',
                  autoClose: 2000,
                });
              }
            };

            const handleVendorApplication = async () => {
              const url = '/vendor-application';

              const acc_token = localStorage.getItem('access_token');

              const headers = { Authorization: `Bearer ${acc_token}` };

              const data = {
                festival_id: props.props.location.festivalId
                  ? props.props.location.festivalId
                  : festivalPayments,
              };

              try {
                const response = await axios({
                  method: 'POST',
                  url,
                  headers,
                  data,
                });
                if (response && response.data && response.data.success) {
                  localStorage.removeItem('festivalPayments');

                  toast(`Success! Thank you for vending!`, {
                    type: 'success',
                    autoClose: 2000,
                  });
                }
              } catch (error) {
                console.log(error);
                toast('Error! Something went wrong!', {
                  type: 'error',
                  autoClose: 2000,
                });
              }
            };

            console.log("request_data from payment page:", request_data)

            //To create a post request to the ticket page
            const handleTicketFestival = async (userType) => {
              event.preventDefault();

              localStorage.setItem('isAttendFestivalPaymentSuccessful', response.data.success);
              const url = '/ticket/add';

              const acc_token = localStorage.getItem('access_token');

              const headers = { Authorization: `Bearer ${acc_token}` };

              const bookingConfirm =
                tasttligUserId + props.props.match.params.item_id + result.paymentIntent.id;
                
              const data = {
                ticket_booking_confirmation_id: bookingConfirm,
                ticket_user_id: tasttligUserId,
                ticket_festival_id: userType === "Vendor" ? festivalPayments : props.props.match.params.item_id,
                no_of_admits: 1,
                ticket_price: props.totalPrice,
                ticket_type: userType === "Vendor" ? "Vendor" : null,
                stripe_receipt_id: result.paymentIntent.id,
              };

              try {
                const res = await axios({
                  method: 'POST',
                  url,
                  data,
                });
                if (res && res.data && res.data.success) {
                  setTimeout(() => {
                    window.location = '/';
                  }, 2000);

                  // toast(
                  //   `Success! Thank you for Purchasing ticket ${props.props.match.params.item_type}!`,
                  //   {
                  //     type: 'success',
                  //     autoClose: 2000,
                  //   },
                  // );
                }
              } catch (error) {
                console.log("error from tickets:", error)
                // toast('Error! Something went wrong!', {
                //   type: 'error',
                //   autoClose: 2000,
                // });
              }
            };

            if (response.data.success) {
              let message = '';
              if (
                props.props.match.params.item_type &&
                props.props.match.params.item_type === 'package' &&
                (props.props.match.params.item_id === 'S_KMIN' ||
                  props.props.match.params.item_id === 'S_KMOD' ||
                  props.props.match.params.item_id === 'S_KULTRA' ||
                  props.props.match.params.item_id === 'S_C1' ||
                  props.props.match.params.item_id === 'S_C2' ||
                  props.props.match.params.item_id === 'S_C3')
              ) {
                //message = `Success! Thank you for your ${props.props.match.params.item_type} purchase!`;
                handleSponsorFestival();
                // redirectToAddSponsorInfoPage();
              } else if (
                props.props.match.params.item_type &&
                props.props.match.params.item_type === 'package' &&
                (props.props.match.params.item_id === 'H_BASIC' ||
                  props.props.match.params.item_id === 'H_VEND' ||
                  props.props.match.params.item_id === 'H_AMB' ||
                  props.props.match.params.item_id === 'G_BASIC' ||
                  props.props.match.params.item_id === 'G_MSHIP' ||
                  props.props.match.params.item_id === 'G_MSHIP2' ||
                  props.props.match.params.item_id === 'KG_MSHIP2' ||
                  props.props.match.params.item_id === 'G_MSHIP3' ||
                  props.props.match.params.item_id === 'KG_MSHIP3' ||
                  props.props.match.params.item_id === 'G_AMB')
              ) {
                toast(`Your Subscription was successful! Thank you!`, {
                  type: 'success',
                  autoClose: 2000,
                });
              } else if (
                props.props.match.params.item_type &&
                props.props.match.params.item_type === 'package' &&
                (props.props.match.params.item_id === 'V_MIN' ||
                  props.props.match.params.item_id === 'V_MOD' ||
                  props.props.match.params.item_id === 'KV_MOD' ||
                  props.props.match.params.item_id === 'KV_ULTRA' ||
                  props.props.match.params.item_id === 'V_ULTRA')
              ) {
                //adding vendors to the ticket
                handleTicketFestival("Vendor");
                //handleVendorFestival();
                handleVendorApplication();
              } else {
                //if normal purchase
                handleTicketFestival();

                message= "Success! Thank you for your purchase!";
              }
              setTimeout(() => {
                window.location = '/';
              }, 2000);
            }
            approveApplication();
          }
        }
      }
      // }
    } catch (error) {
      console.log(error);
    }
  };

  // Mount Payment page
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  const appContextEmail = appContext.state.user.email;

  useEffect(() => {
    setUserEmail(appContextEmail);
  }, [appContextEmail]);

  const appContextId = appContext.state.user.id;

  useEffect(() => {
    setUserId(appContextId);
  }, [appContextId]);


  return (
    <form onSubmit={handleSubmit} noValidate>
      <div className="title mb-3">Where should we email you your receipt?</div>
      <div className="mb-3">
        <label htmlFor="inputEmail4" className="mb-1 inputLabel font-weight-bold">
          Email*
        </label>
        <input
          type="text"
          value={userEmail}
          onChange={(e) => setUserEmail(e.target.value)}
          disabled={appContext.state.user.email && true}
          className="form-control"
          required
        />
        {userEmailError && <div className="error-message">{userEmailError}</div>}
        <label htmlFor="inputEmail4" className="mb-1 inputLabel font-weight-bold">
         Add additional Email (optional)
        </label>
        <input
          type="text"
          value={anotherUserEmail}
          onChange={(e) => setAnotherUserEmail(e.target.value)}
          // disabled={appContext.state.user.email && true}
          className="form-control"
        />
      </div>
      <div className="mb-3">
        <CardElement />
        {errorMessage && <div className="error-message">{errorMessage}</div>}
      </div>
      <div className="mb-3">
        <button type="submit" disabled={!stripe} className="pay-btn">
          Pay
        </button>
      </div>
      <button
        type="button"
        data-toggle="tooltip"
        data-placement="bottom"
        title="Your payment and personal information are encrypted."
        className="secure-checkout"
      >
        <span className="fas fa-lock"></span>&nbsp;Secure Checkout
      </button>
    </form>
  );
};

const StripePaymentCard = (props) => {
  // Environment
  let stripePromise;

  if (process.env.REACT_APP_ENVIRONMENT == 'production') {
    stripePromise = loadStripe(process.env.REACT_APP_STRIPE_PROD_PUBLISHABLE_KEY);
  } else {
    stripePromise = loadStripe(process.env.REACT_APP_STRIPE_TEST_PUBLISHABLE_KEY);
  }

  return (
    <Elements stripe={stripePromise}>
      <CheckoutForm success={() => {}} props={props} />
    </Elements>
  );
};

export default StripePaymentCard;
