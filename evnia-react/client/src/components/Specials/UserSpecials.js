// Libraries
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import useInfiniteScroll from 'react-infinite-scroll-hook';

// Components
import Nav from '../Navbar/Nav';
// import SearchBar from "../Navbar/SearchBar";
// import FoodSamplesCreatedCard from "./FoodSamplesCreatedCard/FoodSamplesCreatedCard";

// Styling
import '../Dashboard/Dashboard.scss';

const UserSpecials = (props) => {
  // Set initial state
  const [userSpecials, setUserSpecials] = useState([]);
  const [hasNextPage, setHasNextPage] = useState(true);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(0);

  // Set search bar empty
  if (!props.location.state) {
    props.location.state = { keyword: '' };
  }

  // Fetch user specials from Kodidi
  const fetchUserSpecials = async () => {
    try {
      const url = '/external_api/kodidi/user_specials_list';
      const acc_token = localStorage.getItem('access_token');
      const headers = { Authorization: `Bearer ${acc_token}` };

      return await axios({ method: 'GET', url, headers });
    } catch (error) {
      return error.response;
    }
  };

  // Load next set of user food samples helper functions
  const loadNextPage = async (page) => {
    const url = '/external_api/kodidi/user_specials_list';
    const acc_token = localStorage.getItem('access_token');
    const headers = { Authorization: `Bearer ${acc_token}` };

    return axios({
      method: 'GET',
      url,
      headers,
      // params: {
      //   keyword: props.location.state.keyword,
      //   page: page + 1,
      // },
    });
  };

  const handleLoadMore = (page = currentPage, userSpecials = userSpecials) => {
    setLoading(true);
    loadNextPage(page).then((newPage) => {
      setLoading(false);
      // const pagination = newPage.data.details.pagination;
      // if (page < pagination.lastPage) {
      //   setCurrentPage(page + 1);
      // }
      // setHasNextPage(currentPage < pagination.lastPage);
      setUserSpecials(userSpecials.concat(newPage.data.details.data));
    });
  };

  const infiniteRef = useInfiniteScroll({
    loading,
    hasNextPage,
    onLoadMore: handleLoadMore,
    scrollContainer: 'window',
    threshold: 50,
  });

  // Mount Your Current Food Samples page
  useEffect(() => {
    window.scrollTo(0, 0);

    fetchUserSpecials().then(({ data }) => {
      setUserSpecials(data.details.data);
    });
  }, []);

  // useEffect(() => {
  //   handleLoadMore(0, []);
  // }, [props.location.state.keyword]);

  // Render empty page
  const Empty = () => {
    return <strong className="no-food-samples-found">No food samples found.</strong>;
  };

  // Render search bar
  const Filters = () => (
    <div className="page-filters">
      <section className="mb-4">
        {/*<SearchBar*/}
        {/*  keyword={props.location.state.keyword}*/}
        {/*  url="/my-current-food-samples"*/}
        {/*/>*/}
      </section>
      <hr />
    </div>
  );

  // Render Your Current Food Samples page
  return (
    <div>
      <Nav />

      <div className="your-current-food-samples">
        <div className="your-current-food-samples-title">Your Current Food Samples</div>
        <div className="row">
          <div className="col-md-3 p-0">
            <Filters />
          </div>
          <div className="col-md-9 p-0">
            <div className="food-sample-cards" ref={infiniteRef}>
              {userSpecials.length !== 0 ? (
                userSpecials.map((user_special) => {
                  console.log(user_special);
                })
              ) : (
                <Empty />
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default UserSpecials;
