// Libraries
import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';

// Styling
import './Navbar.scss';

const SearchBar = (props) => {
  const history = useHistory();

  // Set initial state
  let [foodAdCode, setFoodAdCode] = useState(props.keyword || '');
  let [extend, setExtend] = useState(false);
  let [isGlobal, setIsGlobal] = useState(true);

  // User input change helper function
  const handleInputChange = (event) => {
    setFoodAdCode(event.target.value);
  };

  useEffect(() => {
    if (props.keyword) {
      setFoodAdCode(props.keyword);
    }

    searchIcon();
  }, [props.keyword]);

  const changeSearchType = (event) => {
    setIsGlobal(event.target.value === '1');
  };

  const _handleKeyDown = (event) => {
    if (event.key === 'Enter') {
      if (isGlobal) {
        history.push({
          pathname: props.url,
          state: {
            keyword: foodAdCode,
          },
        });
      } else {
        history.push({
          pathname: props.url,
          state: {
            foodAdCode,
          },
        });
      }
    }
  };

  const _emptySearchText = () => {
    setFoodAdCode('');

    history.push({
      pathname: props.url,
      state: {
        keyword: '',
      },
    });
  };

  const _toggleSearchBar = () => {
    setExtend(!extend);
  };

  const searchIcon = () => {
    if (foodAdCode) {
      return <span onClick={_emptySearchText} className="fas fa-times search-icon" />;
    }

    return <span className="fa fa-search search-icon" />;
  };

  // Render search bar
  return (
    <div className="search">
      <div className={`search_content ${extend ? 'active' : ''}`}>
        <input
          type="text"
          placeholder="Search"
          value={foodAdCode}
          onChange={handleInputChange}
          className="main-search-bar"
          onKeyDown={_handleKeyDown}
          onFocus={_toggleSearchBar}
          onBlur={_toggleSearchBar}
        />
        {searchIcon()}
      </div>
      <form>
        {/* <div className="form-check">
          <label>
            <input
              type="radio"
              name="react-tips"
              value="1"
              checked={isGlobal}
              onChange={changeSearchType}
              className="form-check-input"
            />
            Global
          </label>
        </div> */}

        {/* <div className="form-check">
          <label>
            <input
              type="radio"
              name="react-tips"
              value="2"
              checked={!isGlobal}
              onChange={changeSearchType}
              className="form-check-input"
            />
            Food Ad Code
          </label>
        </div> */}
      </form>
    </div>
  );
};

export default SearchBar;
