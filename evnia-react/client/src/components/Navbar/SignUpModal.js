// Libraries
import React, { useContext } from 'react';
import Modal from 'react-modal';
import { AppContext } from '../../ContextProvider/AppProvider';
import { AuthModalContext } from '../../ContextProvider/AuthModalProvider';

// Components
import { formatPhoneNumber } from '../Functions/Functions';

// Styling
import './Navbar.scss';

const SignUpModal = () => {
  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const authModalContext = useContext(AuthModalContext);

  // Render Sign Up Modal
  return (
    <Modal
      isOpen={authModalContext.state.signUpOpened}
      onRequestClose={authModalContext.closeModal('sign-up')}
      ariaHideApp={false}
      className="sign-up-modal"
    >
      <div className="text-right">
        <button
          aria-label="Close Sign Up Modal"
          onClick={authModalContext.closeModal('sign-up')}
          disabled={authModalContext.state.submitAuthDisabled}
          className="fas fa-times fa-2x close-modal"
        ></button>
      </div>

      <div className="modal-title">Sign Up on Tasttlig</div>

      {appContext.state.errorMessage && (
        <div className="mb-3 invalid-message">{appContext.state.errorMessage}</div>
      )}

      <form onSubmit={authModalContext.handleSubmitSignUp} noValidate>
        <div className="mb-3">
          <input
            type="text"
            name="signUpFirstName"
            placeholder="First Name"
            value={authModalContext.state.signUpFirstName}
            onChange={authModalContext.handleChange}
            disabled={authModalContext.state.submitAuthDisabled}
            className="first-name"
            required
          />
          <span className="far fa-user input-icon"></span>
          {authModalContext.state.signUpFirstNameError && (
            <div className="error-message">{authModalContext.state.signUpFirstNameError}</div>
          )}
        </div>
        <div className="mb-3">
          <input
            type="text"
            name="signUpLastName"
            placeholder="Last Name"
            value={authModalContext.state.signUpLastName}
            onChange={authModalContext.handleChange}
            disabled={authModalContext.state.submitAuthDisabled}
            className="last-name"
            required
          />
          <span className="far fa-user input-icon"></span>
          {authModalContext.state.signUpLastNameError && (
            <div className="error-message">{authModalContext.state.signUpLastNameError}</div>
          )}
        </div>
        <div className="mb-3">
          <input
            type="email"
            name="signUpEmail"
            placeholder="Email Address"
            value={authModalContext.state.signUpEmail}
            onChange={authModalContext.handleChange}
            disabled={authModalContext.state.submitAuthDisabled}
            className="email"
            required
          />
          <span className="far fa-envelope input-icon"></span>
          {authModalContext.state.signUpEmailError && (
            <div className="error-message">{authModalContext.state.signUpEmailError}</div>
          )}
        </div>
        <div className="mb-3">
          <input
            type={authModalContext.state.signUpPasswordType}
            name="signUpPassword"
            placeholder="Password"
            value={authModalContext.state.signUpPassword}
            onChange={authModalContext.handleChange}
            disabled={authModalContext.state.submitAuthDisabled}
            className="password"
            required
          />
          <span onClick={authModalContext.handleClickSignUp} className="password-icon">
            {authModalContext.state.signUpPasswordType === 'password' ? (
              <i className="far fa-eye-slash"></i>
            ) : (
              <i className="far fa-eye"></i>
            )}
          </span>
          {authModalContext.state.signUpPasswordError && (
            <div className="error-message">{authModalContext.state.signUpPasswordError}</div>
          )}
          <div>
            <div className="password-less-char">
              {authModalContext.state.signUpPasswordLessChar}
            </div>
            <div className="password-min-char">{authModalContext.state.signUpPasswordMinChar}</div>
          </div>
        </div>
        <div className="mb-3">
          <input
            type="tel"
            name="signUpPhoneNumber"
            placeholder="Phone Number"
            value={formatPhoneNumber(authModalContext.state.signUpPhoneNumber)}
            onChange={authModalContext.handleChange}
            maxLength="14"
            disabled={authModalContext.state.submitAuthDisabled}
            className="phone-number"
            required
          />
          <span className="fas fa-phone-alt input-icon"></span>
          {authModalContext.state.signUpPhoneNumberError && (
            <div className="error-message">{authModalContext.state.signUpPhoneNumberError}</div>
          )}
        </div>

        <div className="mb-3">
          <button
            type="submit"
            disabled={authModalContext.state.submitAuthDisabled}
            className="sign-up-btn"
          >
            Sign Up
          </button>
        </div>
      </form>

      <div>
        <span>Already have a Tasttlig account?</span>&nbsp;
        <span onClick={authModalContext.openModal('log-in')} className="option">
          Login
        </span>
      </div>
    </Modal>
  );
};

export default SignUpModal;
