// Libraries
import React from 'react';
import { connect } from 'react-redux';
import * as shoppingCartActions from '../../redux/shoppingCart/actions';

const NavBarShoppingCart = (props) => {
  return (
    <div>
      <div
        className="btn btn-default nav-link row shadow-none"
        onClick={() => props.alternateCartVisibility()}
      >
        <span className={`d-inline fas fa-shopping-cart span-center ${props.shoppingCartColor}`} />
        <span className=" ml-2 d-inline badge">{props.totalQuantity}</span>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    totalQuantity: state.totalQuantity,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    alternateCartVisibility: () => {
      dispatch(shoppingCartActions.alternateCartVisibility());
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(NavBarShoppingCart);
