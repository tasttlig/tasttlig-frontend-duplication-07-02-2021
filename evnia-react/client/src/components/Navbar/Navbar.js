// Libraries
import React, { useState, useContext, useEffect } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import { AppContext } from '../../ContextProvider/AppProvider';
import { AuthModalContext } from '../../ContextProvider/AuthModalProvider';
import TermsAndConditions from '../LandingPage/TermsAndConditions/TermsAndConditions';
import VendingConditions from '../LandingPage/TermsAndConditions/VendingConditions';
import HostingConditions from '../LandingPage/TermsAndConditions/HostingConditions';
import BusinessConditions from '../LandingPage/TermsAndConditions/BusinessConditions';
import Modal from 'react-modal';

// Components
import SearchBar from './SearchBar';
import FilterBar from './FilterBar';
import SignUpModal from './SignUpModal';
import LogInModal from './LogInModal';
import ForgotPasswordModal from './ForgotPasswordModal';
import SearchModal from './SearchModal';
// import NavBarShoppingCart from "./NavBarShoppingCart";

// Styling
import './Navbar.scss';
import tasttligLogoWhite from '../../assets/images/tasttlig-logo-white.png';
import tasttligLogoBlack from '../../assets/images/tasttlig-logo-black.png';
import dropDownArrow from '../../assets/images/shapes/6.png';
import searchIcon from '../../assets/images/search.png';
import filterIcon from '../../assets/images/filter.png';
import 'react-toastify/dist/ReactToastify.css';


toast.configure();

const Navbar = (props) => {
  // Set initial state
  const [load, setLoad] = useState(false);
  const [openFilter, setOpenFilter] = useState(false);
  const [submitAuthDisabled, setSubmitAuthDisabled] = useState(false);
  let isHost = false;

  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const authModalContext = useContext(AuthModalContext);
  const userRole = appContext.state.user.role;
  const userBusinessType = appContext.state.user.business_type;
  const userBusinessCategory = appContext.state.user.food_business_type;

  const [viewTermsAndConditions, setViewTermsAndConditions] = useState(false);
  const [vendingConditions, setVendingConditions] = useState(false);
  const [hostingConditions, setHostingConditions] = useState(false);
  const [businessConditions, setBusinessConditions] = useState(false);
  // console.log('user role: ', userRole);
  // console.log('user business type: ', userBusinessType);
  // console.log('user business category: ', userBusinessCategory);
  // localStorage.removeItem('VendFromSignOut')
  // localStorage.removeItem('buyFestivalFromNotSignup')

  if (userRole && userRole.includes('RESTAURANT')) {
    isHost = true;
  }
  // console.log(userRole);

  // Host festival helper function
  const handleHostFestival = async (event) => {
    event.preventDefault();

    const url = '/host-festival';

    const acc_token = localStorage.getItem('access_token');

    const headers = { Authorization: `Bearer ${acc_token}` };

    const data = {
      festival_id: props.hostFestivalList,
      festival_restaurant_host_id: appContext.state.user.id,
    };

    localStorage.setItem('festival_id', JSON.stringify(props.hostFestivalList));

    try {
      const response = await axios({ method: 'POST', url, headers, data });

      if (response && response.data && response.data.success) {
        setTimeout(() => {
          window.location.reload();
        }, 2000);

        toast(`Success! Thank you for hosting!`, {
          type: 'success',
          autoClose: 2000,
        });

        setSubmitAuthDisabled(true);
      }
    } catch (error) {
      toast('Error! Something went wrong!', {
        type: 'error',
        autoClose: 2000,
      });
    }
  };


  const navbarLinks =
    appContext.state.signedInStatus &&
    window.location.href !== `${process.env.REACT_APP_PROD_BASE_URL}/complete-profile` &&
    window.location.href !== `${process.env.REACT_APP_TEST_BASE_URL}/complete-profile` &&
    window.location.href !== `${process.env.REACT_APP_DEV_BASE_URL}/complete-profile` &&
    window.location.href !== `${process.env.REACT_APP_PROD_BASE_URL}/complete-profile/business` &&
    window.location.href !== `${process.env.REACT_APP_TEST_BASE_URL}/complete-profile/business` &&
    window.location.href !== `${process.env.REACT_APP_DEV_BASE_URL}/complete-profile/business` ? (
      <div>
        <div className="image-text-formatting">
        {userRole &&
          !userRole.includes('BUSINESS_MEMBER_PENDING') && userRole.includes('BUSINESS_MEMBER') && !userRole.includes("HOST") && !userRole.includes("HOST_PENDING") ? (
            <div>
             <button
                type="button"
                className="main-navbar-item nav-host-btn"
                onClick={(e) => setHostingConditions(true)}
              >
                  Become a Host
              </button>
                {hostingConditions ? (
                  <Modal
                    className="terms-conditions-modal"
                    isOpen={hostingConditions}
                    onRequestClose={() => setHostingConditions(false)}
                  >
                    <div className="terms-conditions-background-image" />
                    <HostingConditions
                      user_id={props.user_id}
                      viewHostingConditionsState={hostingConditions}
                      changeHostingConditions={(e) => setHostingConditions(e)}
                    />
                  </Modal>
                ) : 
                null}
              </div>
          
          ) : null}
          
          {userRole &&
          userRole.includes('BUSINESS_MEMBER_PENDING') || userRole.includes('BUSINESS_MEMBER') && !userRole.includes("VENDOR") && !userRole.includes("HOST") && !userRole.includes("HOST_PENDING") ? (
            <div>
              <Link exact="true" to="/complete-profile/festival-vendor" className="modal-button2">
                View Vendor Packages
              </Link>
              </div>
          
          ) :
            userRole &&
            !userRole.includes('HOST_PENDING') &&
            (userRole.includes('VENDOR_PENDING') || userRole.includes('VENDOR')) &&
            !userRole.includes('HOST') ? (
              <div>
                  <div>
                    <Link exact="true" to="/complete-profile/festival-vendor" className="modal-button2">
                      View Vendor Packages
                    </Link>
                  </div>
                {hostingConditions ? (
                  <Modal
                    className="terms-conditions-modal"
                    isOpen={hostingConditions}
                    onRequestClose={() => setHostingConditions(false)}
                  >
                    <div className="terms-conditions-background-image" />
                    <HostingConditions
                      user_id={props.user_id}
                      viewHostingConditionsState={hostingConditions}
                      changeHostingConditions={(e) => setHostingConditions(e)}
                    />
                  </Modal>
                ) : null}

               
              </div>
            // ) : userRole && !userRole.includes('HOST_PENDING') && !userRole.includes('HOST') ? (
            //   <div>
            //     <button
            //       type="button"
            //       className="main-navbar-item nav-host-btn"
            //       onClick={(e) => setHostingConditions(true)}
            //     >
            //       Become a Host
            //     </button>
            //     {hostingConditions ? (
            //       <Modal
            //         className="terms-conditions-modal"
            //         isOpen={hostingConditions}
            //         onRequestClose={() => setHostingConditions(false)}
            //       >
            //         <div className="terms-conditions-background-image" />
            //         <HostingConditions
            //           user_id={props.user_id}
            //           viewHostingConditionsState={hostingConditions}
            //           changeHostingConditions={(e) => setHostingConditions(e)}
            //         />
            //       </Modal>
            //     ) : 
            //     null}
            //   </div>
            ) : userRole &&
              !userRole.includes('HOST_PENDING') &&
              !userRole.includes('HOST') &&
              !userRole.includes('VENDOR') &&
              !userRole.includes('VENDOR_PENDING') &&
              !userRole.includes('BUSINESS_MEMBER_PENDING') &&
              !userRole.includes('BUSINESS_MEMBER') &&
              userRole.includes('GUEST') ? (
              <div>
                {viewTermsAndConditions ? (
                  <Modal
                    className="terms-conditions-modal"
                    isOpen={viewTermsAndConditions}
                    onRequestClose={() => setViewTermsAndConditions(false)}
                  >
                    <div className="terms-conditions-background-image" />
                    <TermsAndConditions
                      user_id={props.user_id}
                      viewTermsAndConditionsState={viewTermsAndConditions}
                      changeViewTermsAndConditions={(e) => setViewTermsAndConditions(e)}
                    />
                  </Modal>
                ) : null}
              </div>
            ) : null
          }
           
          <div>
            {appContext.state.user.business_name ? (
              <div className="restaurant-user-name">{appContext.state.user.business_name}</div>
            ) : null}
            {appContext.state.user.first_name || appContext.state.user.last_name ? (
              <div
                className={
                  props.background === 'nav-transparent' ? 'user-full-name-white' : 'user-full-name'
                }
              >
                {appContext.state.user.first_name} {appContext.state.user.last_name}
              </div>
            ) : null}
          </div>
          {appContext.state.user.profile_image ? (
            <img
              src={appContext.state.user.profile_image}
              alt={`${appContext.state.user.first_name} ${appContext.state.user.last_name}`}
              onLoad={() => setLoad(true)}
              className={load ? 'main-navbar-account-profile-picture' : 'loading-image'}
            />
          ) : (
            <span className="fas fa-user-circle fa-3x main-navbar-account-default-picture"></span>
          )}
          <div className="dropdown">
            <button
              type="button"
              id="dropdownMenuButton"
              data-toggle="dropdown"
              aria-label="Navigation Menu"
              aria-haspopup="true"
              aria-expanded="false"
              className="main-navbar-account"
            >
              <img src={dropDownArrow} alt="Arrow" className="dropdown-image" />
            </button>
            <div
              className="dropdown-content dropdown-menu-right"
              aria-labelledby="dropdownMenuButton"
            >
              <div>
                <Link
                  exact="true"
                  to={`/passport/${appContext.state.user.id}`}
                  className="dropdown-item"
                >
                  Passport
                </Link>
              </div>
              <div>
                <Link exact="true" to="/dashboard" className="dropdown-item">
                  Dashboard
                </Link>
              </div>
              <div>
                <Link exact="true" to="/privacy" className="dropdown-item">
                Privacy Policy
                </Link>
              </div>
              <div>
                <span onClick={authModalContext.handleSignOut} className="dropdown-item">
                  Logout
                </span>
              </div>
            </div>
          </div>
          <div>
            <Link exact="true" to="/guest-vendor-plan" className="modal-button2">
              Upgrade Membership
            </Link>
          </div>
        </div>
      </div>
    ) : !appContext.state.signedInStatus ? (
      <div className="d-flex align-items-center">

        {!appContext.state.user.id ||
        (window.location.href !== `${process.env.REACT_APP_PROD_BASE_URL}/complete-profile` &&
          window.location.href !== `${process.env.REACT_APP_TEST_BASE_URL}/complete-profile` &&
          window.location.href !== `${process.env.REACT_APP_DEV_BASE_URL}/complete-profile` &&
          userRole &&
          !userRole.includes('RESTAURANT') &&
          !userRole.includes('RESTAURANT_PENDING')) ? (
          <div>
            {/* <Link
              exact="true"
              to="/host-landing"
              className="main-navbar-item nav-host-btn"
            >
              Become a Vendor
            </Link> */}

            {/*  <button
              type="button"
              className="main-navbar-item nav-host-btn"
              onClick={(e) => setViewTermsAndConditions(true)}
            >
              Join
            </button> */}
            {/* viewTermsAndConditions ? (
                <Modal className="terms-conditions-modal" 
                 isOpen ={viewTermsAndConditions} onRequestClose={() => setViewTermsAndConditions(false) }>
                  <div className = "terms-conditions-background-image"/>                  
                  <TermsAndConditions
                    user_id={props.user_id} 
                    viewTermsAndConditionsState={viewTermsAndConditions}
                    changeViewTermsAndConditions={(e) => setViewTermsAndConditions(e)}
                    />
                </Modal>
               ) : null} */}

            {/* <button
              type="button"
              className="main-navbar-item nav-host-btn"
              // onClick={(e) => setVendingConditions(true)}
              onClick={() =>  {localStorage.setItem('business-preference', 'Business');
            window.location.href='/sign-up'}}
            >
              Add your Restaurant
            </button> */}
             <div>
              <Link exact="true" to="/complete-profile/festival-vendor">
               <button
                type="button"
                className="main-navbar-item nav-host-btn"
                onClick={ () => localStorage.setItem('VendFromSignOut', 'true')}
              >
                  View Vendor Packages
              </button>
            </Link>
            </div>
            {vendingConditions ? (
              <Modal
                className="terms-conditions-modal"
                isOpen={vendingConditions}
                onRequestClose={() => setVendingConditions(false)}
              >
                <div className="terms-conditions-background-image" />
                <VendingConditions
                  user_id={props.user_id}
                  viewVendingConditionsState={vendingConditions}
                  changeVendingConditions={(e) => setVendingConditions(e)}
                  changeBusinessConditions={(e) => 
                    {setVendingConditions(false);
                    setBusinessConditions(e)}}
                />
              </Modal>
            ) : null}
             {businessConditions ? (
                  <Modal
                    className="terms-conditions-modal"
                    isOpen={businessConditions}
                    onRequestClose={() => setBusinessConditions(false)}
                  >
                    <div className="terms-conditions-background-image" />
                    <BusinessConditions
                      user_id={props.user_id}
                      viewHostingConditionsState={businessConditions}
                      changeBusinessConditions={(e) => setBusinessConditions(e)}
                    />
                  </Modal>
                ) : null}

            {/* <button type="button"  className="main-navbar-item nav-host-btn" onClick={(e) => setHostingConditions(true)}>
                Host
              </button>
              {hostingConditions ? (
                <Modal className="terms-conditions-modal" 
                 isOpen ={hostingConditions} onRequestClose={() => setHostingConditions(false) }>
                  <div className = "terms-conditions-background-image"/>                  
                  <HostingConditions
                    user_id={props.user_id}
                    viewHostingConditionsState={hostingConditions}
                    changeHostingConditions={(e) => setHostingConditions(e)}
                    />
                </Modal>
               ) : null} */}
          </div>
        ) : null}
        <div>
          <Link
            exact="true"
            to="/login"
            className={
              props.background === 'nav-transparent'
                ? 'main-navbar-item nav-link-landing-page'
                : 'main-navbar-item nav-link'
            }
          >
            <strong>Login</strong>
          </Link>
  
        </div>
        <Link exact="true" to="/sign-up">
            <button
              type="button"
              className="main-navbar-item nav-host-btn"
            >
                Join
            </button>
          </Link>
      </div>
    ) : (
      <div className="d-flex">
        <Link exact="true" to="/" className="skip-link">
          Skip
        </Link>
      </div>
    );

  // Render Navbar
  return (
    <nav
      className={
        props.navbarEffect
          ? `navbar-landing-page ${props.background}`
          : `navbar ${props.background}`
      }
    >
      <div className="container-xl navBar-container">
        {/* Toggle button */}
        {window.location.href !== `${process.env.REACT_APP_PROD_BASE_URL}/complete-profile` &&
          window.location.href !== `${process.env.REACT_APP_TEST_BASE_URL}/complete-profile` &&
          window.location.href !== `${process.env.REACT_APP_DEV_BASE_URL}/complete-profile` &&
          window.location.href !==
            `${process.env.REACT_APP_PROD_BASE_URL}/complete-profile/business` &&
          window.location.href !==
            `${process.env.REACT_APP_TEST_BASE_URL}/complete-profile/business` &&
          window.location.href !==
            `${process.env.REACT_APP_DEV_BASE_URL}/complete-profile/business` && (
            <div className="main-navbar-toggle-button">
              <button
                aria-label="Toggle Menu"
                onClick={props.drawerToggleClickHandler}
                className="toggle-button"
              >
                <div
                  className={
                    props.background === 'nav-transparent'
                      ? 'toggle-button-line'
                      : 'toggle-button-line-white-bg'
                  }
                />
                <div
                  className={
                    props.background === 'nav-transparent'
                      ? 'toggle-button-line'
                      : 'toggle-button-line-white-bg'
                  }
                />
                <div
                  className={
                    props.background === 'nav-transparent'
                      ? 'toggle-button-line'
                      : 'toggle-button-line-white-bg'
                  }
                />
              </button>
            </div>
          )}

        {/* Tasttlig Logo */}
        <div>
          {window.location.pathname === '/' ? (
            <Link exact="true" onClick={() => window.location.reload()}>
              <img
                src={props.background === 'nav-transparent' ? tasttligLogoWhite : tasttligLogoBlack}
                alt="Tasttlig"
                className="main-navbar-logo"
              />
            </Link>
          ) : (
            <Link exact="true" to="/">
              <img
                src={props.background === 'nav-transparent' ? tasttligLogoWhite : tasttligLogoBlack}
                alt="Tasttlig"
                className="main-navbar-logo"
              />
            </Link>
          )}
        </div>
        {/* {userRole ?
        (<button className="nav-upgrade-account-btn">
          Role: {userRole[0]} 
        </button>):null
        } */}

        {/*          {userRole &&
          !userRole.includes("HOST") &&
          !userRole.includes("HOST_PENDING") &&
          !userRole.includes("ADMIN") &&
          !userRole.includes("RESTAURANT") &&
          !userRole.includes("RESTAURANT_PENDING") && 
          !userRole.includes("SPONSOR") && 
          !userRole.includes("SPONSOR_PENDING") && 
          !userRole.includes("VENDOR") && 
          !userRole.includes("VENDOR_PENDING") && (
              <button className="nav-upgrade-account-btn">
              Role: {userRole[0]} 
             </button>
          )} */}

        {/* {userRole &&
          userRole.includes("ADMIN") &&
            (
              <button className="nav-upgrade-account-btn">
              Role: ADMIN 
             </button>
          )} */}

        {/* {userRole &&
          !userRole.includes("ADMIN") && 
          userRole.includes("HOST_PENDING") ||
          userRole.includes("HOST")
           && (
              <button className="nav-upgrade-account-btn">
              Role: {userRole[2]} 
             </button>
          )} */}

        <div />

        {/* {window.location.href ===
                `${process.env.REACT_APP_PROD_BASE_URL}/` ||
              window.location.href ===
                `${process.env.REACT_APP_TEST_BASE_URL}/` ||
              window.location.href ===
                `${process.env.REACT_APP_DEV_BASE_URL}/` ? (
                <>
                  <div className="main-navbar-item">
                    <SearchBar keyword={props.keyword} url={props.url} />
                  </div>
                  <div className="main-navbar-item">
                    <button
                      type="button"
                      aria-label="Filter"
                      data-toggle="tooltip"
                      data-placement="bottom"
                      title="Filter"
                      onClick={() => setOpenFilter(!openFilter)}
                      className="main-navbar-filter-button"
                    >
                      <img src={filterIcon} alt="Filter" />
                    </button>
                  </div>
                </>
              ) : null} */}

        {window.location.href !== `${process.env.REACT_APP_PROD_BASE_URL}/complete-profile` &&
          window.location.href !== `${process.env.REACT_APP_TEST_BASE_URL}/complete-profile` &&
          window.location.href !== `${process.env.REACT_APP_DEV_BASE_URL}/complete-profile` &&
          window.location.href !==
            `${process.env.REACT_APP_PROD_BASE_URL}/complete-profile/business` &&
          window.location.href !==
            `${process.env.REACT_APP_TEST_BASE_URL}/complete-profile/business` &&
          window.location.href !==
            `${process.env.REACT_APP_DEV_BASE_URL}/complete-profile/business` && (
            <>
              {/* <div>
                      <Link
                        exact="true"
                        to="/about"
                        className={
                          props.navbarEffect
                            ? "main-navbar-item nav-link-landing-page"
                            : "main-navbar-item nav-link"
                        }
                      >
                        About
                      </Link>
                    </div> */}
              {/* {appContext.state.signedInStatus &&
                    window.location.href !==
                      `${process.env.REACT_APP_PROD_BASE_URL}/` &&
                    window.location.href !==
                      `${process.env.REACT_APP_TEST_BASE_URL}/` &&
                    window.location.href !==
                      `${process.env.REACT_APP_DEV_BASE_URL}/` ? null : (
                      <div>
                        {!props.selectMultipleFestivals &&
                        appContext.state.user.id ? (
                          <div
                            onClick={props.handleHostMultipleFestivals}
                            className={
                              props.navbarEffect
                                ? "main-navbar-item nav-link-landing-page nav-host-btn"
                                : "main-navbar-item nav-host-btn"
                            }
                          >
                            Host
                          </div>
                        ) : props.selectMultipleFestivals &&
                          props.hostFestivalList.length === 0 &&
                          appContext.state.user.id ? (
                          <div
                            className={
                              props.navbarEffect
                                ? "main-navbar-item nav-link-landing-page nav-host-btn"
                                : "main-navbar-item nav-hosting-btn"
                            }
                          >
                            Hosting
                          </div>
                        ) : props.selectMultipleFestivals &&
                          props.hostFestivalList.length > 0 &&
                          userRole &&
                          !userRole.includes("RESTAURANT") &&
                          !userRole.includes("RESTAURANT_PENDING") &&
                          !userRole.includes("ADMIN") &&
                          appContext.state.user.id ? (
                          <div>
                            <Link
                              exact="true"
                              to="/complete-profile/business"
                              className={
                                props.navbarEffect
                                  ? "main-navbar-item nav-link-landing-page nav-host-btn"
                                  : "main-navbar-item nav-host-btn"
                              }
                            >
                              Host
                            </Link>
                          </div>
                        ) : props.selectMultipleFestivals &&
                          props.hostFestivalList.length > 0 &&
                          userRole &&
                          (userRole.includes("RESTAURANT") ||
                            userRole.includes("RESTAURANT_PENDING") ||
                            userRole.includes("ADMIN")) &&
                          appContext.state.user.id ? (
                          <div
                            onClick={handleHostFestival}
                            disabled={submitAuthDisabled}
                            className={
                              props.navbarEffect
                                ? "main-navbar-item nav-link-landing-page nav-host-btn"
                                : "main-navbar-item nav-host-btn"
                            }
                          >
                            Host
                          </div>
                        ) : null}
                      </div>
                    )} */}
              {userRole &&
              (userRole.includes('RESTAURANT') || userRole.includes('RESTAURANT_PENDING')) ? (
                <div>
                  <Link
                    exact="true"
                    to="/plans/business"
                    className={
                      props.navbarEffect
                        ? 'main-navbar-item nav-link-landing-page nav-upgrade-account-btn'
                        : 'main-navbar-item nav-upgrade-account-btn'
                    }
                  >
                    Upgrade Your Membership
                  </Link>
                </div>
              ) : null}

              {/*               {userRole &&
                (userRole.includes("MEMBER") && !userRole.includes("ADMIN") && !userRole.includes("HOST") && !userRole.includes("HOST_PENDING") && !userRole.includes("SPONSOR") && !userRole.includes("SPONSOR_PENDING") && !userRole.includes("VENDOR") && !userRole.includes("VENDOR_PENDING")) ? (
                  <div>
                    <Link
                      exact="true"
                      to="/become-a-host"
                      className="nav-upgrade-account-btn"
                    >
                      Apply to host
                    </Link>
                    <Link
                      exact="true"
                      to="/complete-profile/festival-vendor"
                      className="nav-upgrade-account-btn"
                    >
                      Apply to Vend
                    </Link>
                    <Link
                      exact="true"
                      to="/sponsor-plan"
                      className="nav-upgrade-account-btn"
                    >
                      Apply to Sponsor
                    </Link>
                  </div>
                  
                ) : null} */}
            </>
          )}

        {/* {!appContext.state.user.id ||
              (window.location.href !==
                `${process.env.REACT_APP_PROD_BASE_URL}/complete-profile` &&
                window.location.href !==
                  `${process.env.REACT_APP_TEST_BASE_URL}/complete-profile` &&
                window.location.href !==
                  `${process.env.REACT_APP_DEV_BASE_URL}/complete-profile` &&
                window.location.href !==
                  `${process.env.REACT_APP_PROD_BASE_URL}/complete-profile/business` &&
                window.location.href !==
                  `${process.env.REACT_APP_TEST_BASE_URL}/complete-profile/business` &&
                window.location.href !==
                  `${process.env.REACT_APP_DEV_BASE_URL}/complete-profile/business` &&
                userRole &&
                !userRole.includes("SPONSOR") &&
                !userRole.includes("SPONSOR_PENDING")) ? (
                <div>
                  <Link
                    exact="true"
                    to="/sponsor"
                    className="main-navbar-item sponsor-btn"
                  >
                    Sponsor
                  </Link>
                </div>
              ) : null} */}

        {/* Login Modal */}
        <LogInModal />

        {/* Forgot Password Modal */}
        <ForgotPasswordModal />

        {/* Sign Up Modal */}
        <SignUpModal />

        {/* Search Modal */}
        {/* <SearchModal keyword={props.keyword} url={props.url} /> */}

        {navbarLinks}

        {/* {!appContext.state.user.id ||
              (window.location.href !==
                `${process.env.REACT_APP_PROD_BASE_URL}/complete-profile` &&
                window.location.href !==
                  `${process.env.REACT_APP_TEST_BASE_URL}/complete-profile` &&
                window.location.href !==
                  `${process.env.REACT_APP_DEV_BASE_URL}/complete-profile` &&
                userRole &&
                !userRole.includes("RESTAURANT") &&
                !userRole.includes("RESTAURANT_PENDING")) ? (
                <div>
                  <Link
                    exact="true"
                    to="/host"
                    className="main-navbar-item host-btn"
                  >
                    Host
                  </Link>
                </div>
              ) : null} */}

        {/* {window.location.href ===
                `${process.env.REACT_APP_PROD_BASE_URL}/` ||
              window.location.href ===
                `${process.env.REACT_APP_TEST_BASE_URL}/` ||
              window.location.href ===
                `${process.env.REACT_APP_DEV_BASE_URL}/` ||
              window.location.href ===
                `${process.env.REACT_APP_PROD_BASE_URL}/09-2020-festival` ||
              window.location.href ===
                `${process.env.REACT_APP_TEST_BASE_URL}/09-2020-festival` ||
              window.location.href ===
                `${process.env.REACT_APP_DEV_BASE_URL}/09-2020-festival` ? (
                <>
                  <button
                    type="button"
                    aria-label="Open Search Modal"
                    data-toggle="tooltip"
                    data-placement="bottom"
                    title="Search"
                    onClick={authModalContext.openModal("search")}
                    className="main-navbar-search-button"
                  >
                    <img src={searchIcon} alt="Search" />
                  </button>
                  <button
                    type="button"
                    aria-label="Open Filter Modal"
                    data-toggle="tooltip"
                    data-placement="bottom"
                    title="Filter"
                    onClick={() => setOpenFilter(!openFilter)}
                    className="main-navbar-filter-button-responsive"
                  >
                    <img src={filterIcon} alt="Filter" />
                  </button>
                </>
              ) : null} */}

        {/* {!appContext.state.user.id &&
              (window.location.href ===
                `${process.env.REACT_APP_PROD_BASE_URL}/festival` ||
                window.location.href ===
                  `${process.env.REACT_APP_TEST_BASE_URL}/festival` ||
                window.location.href ===
                  `${process.env.REACT_APP_DEV_BASE_URL}/festival`) ? (
                <>
                  <span
                    onClick={authModalContext.openModal("log-in")}
                    className="main-navbar-item add-food-samples-btn"
                  >
                    Add Food Samples
                  </span>
                </>
              ) : userRole &&
                (userRole.includes("RESTAURANT") ||
                  userRole.includes("RESTAURANT_PENDING")) &&
                (window.location.href ===
                  `${process.env.REACT_APP_PROD_BASE_URL}/festival` ||
                  window.location.href ===
                    `${process.env.REACT_APP_TEST_BASE_URL}/festival` ||
                  window.location.href ===
                    `${process.env.REACT_APP_DEV_BASE_URL}/festival`) ? (
                <>
                  <Link
                    exact="true"
                    to="/create-food-samples"
                    className="main-navbar-item add-food-samples-btn"
                  >
                    Add Food Samples
                  </Link>
                </>
              ) : null} */}

        {/* {window.location.href !==
                `${process.env.REACT_APP_PROD_BASE_URL}/festival` &&
                window.location.href !==
                  `${process.env.REACT_APP_TEST_BASE_URL}/festival` &&
                window.location.href !==
                  `${process.env.REACT_APP_DEV_BASE_URL}/festival` && (
                  <div>
                    <Link
                      exact="true"
                      to="/host"
                      className="main-navbar-item host-btn"
                    >
                      Host
                    </Link>
                  </div>
                )} */}

        {/* Navbar Shopping Cart */}
        {/* <NavBarShoppingCart 
                shoppingCartColor={
                  props.navbarEffect
                    ? "text-white"
                    : ""
                }
              /> */}
      </div>
    </nav>
  );
};

export default Navbar;
