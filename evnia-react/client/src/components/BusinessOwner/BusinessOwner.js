// Libraries
import React, { useState, useEffect, useContext, Fragment, useRef } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { AppContext } from '../../ContextProvider/AppProvider';
import LoadingBar from 'react-top-loading-bar';
// Components
import Nav from '../Navbar/Nav';
import BusinessOwnerItems from './BusinessOwnerItems/BusinessOwnerItems';
import FoodSamplesCreatedCard from '../Dashboard/FoodSamplesCreated/FoodSamplesCreatedCard/FoodSamplesCreatedCard';

// Styling
import './BusinessOwner.scss';
import defaultProfilePicture from '../../assets/images/default-profile-picture.png';

const BusinessOwner = (props) => {
 
  // Set initial state
  const [load, setLoad] = useState(false);
  const [foodSampleOwner, setFoodSampleOwner] = useState([]);
  const [foodSampleOwnerItems, setFoodSampleOwnerItems] = useState([]);
  const [festivalHosts, setFestivalHosts] = useState([]);
  const [fetchedExperiences, setFetchedExperiences] = useState([]);
  const [fetchedServices, setFetchedServices] = useState([]);
  const [allProductsInFestival, setAllProductsInFestival] = useState([]);
  const [loading, setLoading] = useState(true);
  const [toggleState, setToggleState] = useState(1);
  const [selected, setSelected] = useState('all');
  const [keyword, setKeyword] = useState('');
  const [filterSearch, setFilterSearch] = useState('');
  const [filterPrice, setFilterPrice] = useState('none');
  const [filterQuantity, setFilterQuantity] = useState('none');
  const [festivalDetails, setFestivalDetails] = useState([]);
  const [filterSize, setFilterSize] = useState('none');
  
  
  const ref = useRef(null);
  const festivalIdFromStorage = localStorage.getItem('festivalId');

  const toggleTab = (index) => {
    setToggleState(index);
  };
  const handleInputChange = (event) => {
    //event.preventDefault();
    console.log(event.target.value);
    setKeyword(event.target.value);
  };
  const _handleKeyDown = (event) => {
    //event.preventDefault();
    if (event.key === 'Enter') {
      event.preventDefault();
      setFilterSearch(keyword);
    }
  };

  // To use the JWT credentials
  const appContext = useContext(AppContext);
  console.log("props coming from business owner page:", props)


  // Render Food Sample Owner Items helper function
  // const renderFoodSampleOwnerItems = (arr) => {
  //   return arr.map((card, index) => (
  //     <BusinessOwnerItems
  //       key={index}
  //       foodSampleId={card.product_id}
  //       images={card.image_urls}
  //       title={card.title}
  //       nationality={card.nationality}
  //       alpha_2_code={card.alpha_2_code}
  //       frequency={card.frequency}
  //       price={card.price}
  //       festival_selected={card.festival_selected}
  //       quantity={card.quantity}
  //       numOfClaims={card.claimed_total_quantity}
  //       address={`${card.business_unit} - ${card.business_street_number}, ${card.business_street_name}`}
  //       city={card.city}
  //       provinceTerritory={card.state}
  //       postalCode={card.zip_postal_code}
  //       // startDate={card.start_date}
  //       // endDate={card.end_date}
  //       startTime={card.start_time}
  //       endTime={card.end_time}
  //       description={card.description}
  //       sample_size={card.product_size}
  //       is_available_on_monday={card.is_available_on_monday}
  //       is_available_on_tuesday={card.is_available_on_tuesday}
  //       is_available_on_wednesday={card.is_available_on_wednesday}
  //       is_available_on_thursday={card.is_available_on_thursday}
  //       is_available_on_friday={card.is_available_on_friday}
  //       is_available_on_saturday={card.is_available_on_saturday}
  //       is_available_on_sunday={card.is_available_on_sunday}
  //       foodSampleOwnerId={card.product_user_id}
  //       // foodSampleOwnerPicture={foodSampleOwner.profile_image_link}
  //       isEmailVerified={foodSampleOwner.is_email_verified}
  //       firstName={foodSampleOwner.first_name}
  //       lastName={foodSampleOwner.last_name}
  //       email={foodSampleOwner.email}
  //       phoneNumber={foodSampleOwner.phone_number}
  //       facebookLink={foodSampleOwner.facebook_link}
  //       twitterLink={foodSampleOwner.twitter_link}
  //       instagramLink={foodSampleOwner.instagram_link}
  //       youtubeLink={foodSampleOwner.youtube_link}
  //       linkedinLink={foodSampleOwner.linkedin_link}
  //       websiteLink={foodSampleOwner.website_link}
  //       bioText={foodSampleOwner.bio_text}
  //       history={props.history}
  //       passportId={
  //         appContext.state && appContext.state.user && appContext.state.user.passport_id
  //           ? appContext.state.user.passport_id
  //           : ''
  //       }
  //     />
  //   ));
  // };

  const fetchFoodSamplesOwner = async () => {
    try {
      const response = await axios({
        method: 'GET',
        url: `/food-sample/owner/${props.match.params.id}`,
      });
      setFoodSampleOwner(response.data.owner_user);
      setFoodSampleOwnerItems(response.data.food_samples);
    } catch (error) {
      console.log(error);
      return error.response;
    }
  };

   // Fetch services in festival helper function
   const fetchServicesInFestival = async () => {
    try {
      const response = await axios({
        method: 'GET',
        url: `/services/festival/${festivalIdFromStorage}`,
        params: {
          price: filterPrice,
          quantity: filterQuantity,
          size: filterSize,
          keyword: filterSearch,
        },
      });
      setFetchedServices(response.data.details);
      return response;
    } catch (error) {
      console.log(error);
      return error.response;
    }
  };

  // Fetch experiences in festival helper function
  const fetchExperiencesInFestival = async () => {
    try {
      const response = await axios({
        method: 'GET',
        url: `/experiences/festival/${festivalIdFromStorage}`,
      });
      
      setFetchedExperiences(response.data.details);
      return response;
    } catch (error) {
      return error.response;
    }
  };

  const fetchallProductsInFestival = async () => {
    try {
      const response = await axios({
        method: 'GET',
        url: `/all-products/festival/${festivalIdFromStorage}`,
        params: {
          price: filterPrice,
          quantity: filterQuantity,
          size: filterSize,
          keyword: filterSearch,
          // dayOfWeek,
        },
      });

      setAllProductsInFestival(response.data.details);
      ref.current.complete();
    } catch (error) {
      console.log(error);
      return error.response;
    }
  };

  console.log("products from businessownerItems:", allProductsInFestival)
    console.log("services from businessownerItems:", fetchedServices)
    console.log("fetched Experiences from businessownerItems:", fetchedExperiences)

 
  // Get all fetches helper function
  const getAllFetches = async () => {
  
    await fetchFoodSamplesOwner();
    await fetchServicesInFestival();
    await fetchExperiencesInFestival();
    await fetchallProductsInFestival();
    setLoading(false);
  };

    // Mount Festival Details page
    useEffect(() => {
      window.scrollTo(0, 0);
      getAllFetches();
    }, []);


  useEffect(() => {
    getAllFetches();
    ref.current.continuousStart();
  }, [
    filterPrice,
    filterQuantity,
    filterSize,
    filterSearch,
    // dayOfWeek,
  ]);

  // Food Sample Owner Banner Component
  const Banner = () => (
    <div>
      <div className="food-sample-owner-profile-banner">
        <div className="row food-sample-owner-profile-banner-simple">
          <div className="col-md-5 banner-content">
            <div className="mb-4">
              {foodSampleOwner.business_image_urls ? (
                <img
                  src={foodSampleOwner.business_image_urls[0]}
                  alt={`${foodSampleOwner.first_name} ${foodSampleOwner.last_name}`}
                  onLoad={() => setLoad(true)}
                  className={load ? 'food-sample-owner-profile-picture' : 'loading-image'}
                />
              ) : (
                <img
                  src={defaultProfilePicture}
                  alt={`${foodSampleOwner.first_name} ${foodSampleOwner.last_name}`}
                  onLoad={() => setLoad(true)}
                  className={load ? 'food-sample-owner-default-picture' : 'loading-image'}
                />
              )}
            </div>
            <div className="food-sample-owner-profile-name">
              {`${foodSampleOwner.first_name} ${foodSampleOwner.last_name}`}
            </div>
            <div className="food-sample-owner-profile-promo">
              {foodSampleOwner.profile_tag_line && (
                <div className="pro mb-4 food-sample-owner-profile-tag-line">
                  {foodSampleOwner.profile_tag_line}
                </div>
              )}
              <address>
                {foodSampleOwner.business_name && (
                  <div>
                    <strong>{foodSampleOwner.business_name}</strong>
                    <br />
                  </div>
                )}
                {foodSampleOwner.business_address_1 && foodSampleOwner.business_address_1}
                {foodSampleOwner.business_address_2 && (
                  <span>, {foodSampleOwner.business_address_2}</span>
                )}
                <br />
                {foodSampleOwner.city && `${foodSampleOwner.city},`}{' '}
                {foodSampleOwner.state && foodSampleOwner.state}{' '}
                {foodSampleOwner.zip_postal_code && foodSampleOwner.zip_postal_code}
                <br />
                <div title="Email">Email: {foodSampleOwner.email}</div>
                <div title="Phone">Phone: {foodSampleOwner.phone_number}</div>
              </address>
            </div>
            {/* {appContext.state.user.id === foodSampleOwner.tasttlig_user_id && (
              <div>
                <Link to="/account" className="edit-profile-link">
                  Edit Profile
                </Link>
              </div>
            )} */}
          </div>
          <div className="col-md-7 banner-content">
            <div className="banner-images">
              <div className="banner-images-wrapper">
                <div className="banner-color-block-container">
                  <div className="banner-color-block"></div>
                </div>
                {foodSampleOwner.banner_image_link ? (
                  <img
                    src={foodSampleOwner.banner_image_link}
                    alt="Banner"
                    onLoad={() => setLoad(true)}
                    className={load ? 'main-banner-image' : 'loading-image'}
                  />
                ) : (
                  <div className="main-banner-image"></div>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="fade-rule" />
    </div>
  );

  // Render Food Sample Owner page
  return (
    <div>
      <Nav />
      <LoadingBar color="#88171A" ref={ref} shadow={true} />
      {/* Food Sample Owner section */}
      <div className="food-sample-owner">
        <Banner />
        {/* <div className="food-sample-owner-items"> */}
            <div className="row">
              <div className="tab-row">
                <div className="tab-switcher">
                 
                  <button
                    onClick={() => {
                      toggleTab(1);
                      setSelected('all');
                    }}
                    className={toggleState === 1 ? 'tab active-tab' : 'tab'}
                  >
                    All
                  </button>
                  <button
                    onClick={() => {
                      toggleTab(2);
                      setSelected('food-samples');
                    }}
                    className={toggleState === 2 ? 'tab active-tab' : 'tab'}
                  >
                    Products
                  </button>
                  <button
                    onClick={() => {
                      toggleTab(3);
                      setSelected('services');
                    }}
                    className={toggleState === 3 ? 'tab active-tab' : 'tab'}
                  >
                    Services
                  </button>
                  <button
                    onClick={() => {
                      toggleTab(4);
                      setSelected('experiences');
                    }}
                    className={toggleState === 4 ? 'tab active-tab' : 'tab'}
                  >
                    Experiences
                  </button>
                </div>
              </div>
            </div>
            <div className="row search-field">
              <input
                name="card_search"
                placeholder="Search..."
                type="text"
                value={keyword}
                onChange={handleInputChange}
                onKeyDown={_handleKeyDown}
              ></input>
            </div>
            <div className="row">
              <div className="col sticky-dash-nav">
                <div className="filters-container">
                  <select
                    name="price_shown"
                    label="Prices"
                    placeholder="Choose Order"
                    onChange={(event) => {
                      setFilterPrice(event.target.value);
                    }}
                  >
                    <option value="">Price</option>
                    <option value="free">Price (free)</option>
                    <option value="lowest_to_highest">Price (Lowest to Highest)</option>
                    <option value="highest_to_lowest">Price (Highest to Lowest)</option>
                  </select>
                  <select
                    name="quantity_shown"
                    label="Quantity/Capacity"
                    placeholder="Choose Order"
                    onChange={(event) => setFilterQuantity(event.target.value)}
                  >
                    <option value="">Quantity</option>
                    <option value="lowest_to_highest">Quantity (Lowest to Highest)</option>
                    <option value="highest_to_lowest">Quantity (Highest to Lowest)</option>
                  </select>
                  <select
                    name="size_shown"
                    label="Size/Scope"
                    placeholder="Choose"
                    onChange={(event) => setFilterSize(event.target.value)}
                  >
                    <option value="">Size</option>
                    <option value="bite_size">Bite size</option>
                    <option value="quarter">Quarter</option>
                    <option value="half">Half</option>
                    <option value="full">Full</option>
                    <option value="small">Small</option>
                    <option value="medium">Medium</option>
                    <option value="large">Large</option>
                  </select>
                </div>
              </div>
              <div className="col-md-10">
                {((selected && selected === 'all') || selected === 'food-samples') && (
                  <Fragment>
                    <div className="restaurant-cards-section">
                      {
                      
                        allProductsInFestival.length !== 0
                          ? allProductsInFestival.map((product) => (
                            <>
                              {product.product_user_id === Number(props.match.params.id) ? (
                              <FoodSamplesCreatedCard
                                key={product.product_id}
                                foodSampleProps={product}
                                festivalId={festivalIdFromStorage}
                                // festivalName={festivalDetails[0].festival_name}
                              />) : null}
                              </>
                            ))
                          : null
                      }
                    </div>
                  </Fragment>
                )}
                {((selected && selected === 'all') || selected === 'experiences') && (
                  <Fragment>
                    <div className="restaurant-cards-section">
                      {fetchedExperiences && fetchedExperiences.length !== 0
                        ? fetchedExperiences.map((exp) => (
                          <>
                          {exp.business_details_user_id === Number(props.match.params.id) ? (
                          <FoodSamplesCreatedCard
                            key={exp.product_id}
                            foodSampleProps={exp}
                            festivalId={festivalIdFromStorage}
                            // festivalName={festivalDetails[0].festival_name}
                          />) : null}
                          </>
                          ))
                        : null}
                    </div>
                  </Fragment>
                )}
                {((selected && selected === 'all') || selected === 'services') && (
                  <Fragment>
                    <div className="restaurant-cards-section">
                      {fetchedServices && fetchedServices.length > 0
                        ? fetchedServices.map((service) => (
                          <>
                          {service.service_user_id === Number(props.match.params.id) ? (
                          <FoodSamplesCreatedCard
                            key={service.product_id}
                            foodSampleProps={service}
                            festivalId={festivalIdFromStorage}
                            // festivalName={festivalDetails[0].festival_name}
                          />) : null}
                          </>
                          ))
                        //   <>
                        //   {console.log("services from service map:", service)}
                        //   </>
                        // ))
                        : null}
                    </div>
                  </Fragment>
                )}
              </div>
            </div>
          </div>
        {/* Food Sample Owner Items section */}
        {/* <div className="food-sample-owner-items">
          {renderFoodSampleOwnerItems(foodSampleOwnerItems)}
        </div> */}
      </div>
    // </div>
  );
};

export default BusinessOwner;
