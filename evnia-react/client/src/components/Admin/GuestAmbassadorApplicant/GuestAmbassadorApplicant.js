// Libraries
import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import Modal from 'react-bootstrap/Modal';

// Components
import Nav from '../../Navbar/Nav';
import FormReview from '../../Apply/ApplyForm/FormSteps/FormReview';

const GuestAmbassadorApplicant = (props) => {
  const [application, setApplication] = useState(null);
  const [declineModalOpen, setDeclineModalOpen] = useState(false);
  const [declineReason, setDeclineReason] = useState('');

  useEffect(() => {
    const fetchApplication = async () => {
      const appId = props.match.params.id;
      const url = `/hosts/guest-amb-application/${appId}`;

      const response = await axios({ method: 'GET', url });

      if (response.data.success) {
        console.log('app', response.data.application);
        let result = response.data.application[0];

        /* result = {
          linkedin_link: result.linkedin_link,
          facebook_link: result.facebook_link,
          youtube_link: result.youtube_link,
          instagram_link: result.instagram_link,
          twitter_link: result.twitter_link,
          ambassador_intent_description: result.ambassador_intent_description,
          is_influencer: result.is_influencer ? 'yes' : 'No',
        }; */
        result.is_influencer = result.is_influencer ? 'yes' : 'No';

        setApplication(result);
      }
    };

    fetchApplication();
  }, []);

  const closeDeclineModal = () => {
    setDeclineModalOpen(false);
  };

  const approveApplication = async () => {
    const url = `/hosts/applications/${application.tasttlig_user_id}/${application.application_id}/approve`;
    const data = { subscription_code: application.resume, email: application.email };
    const response = await axios({ method: 'POST', url, data });
    if (response.data.success) {
      window.location.href = '/admin/all-guest-ambassador-applicants';
    }
  };

  const declineApplication = async (event) => {
    event.preventDefault();

    if (!declineReason) {
      return;
    }

    const url = `/hosts/applications/${application.tasttlig_user_id}/${application.application_id}/decline`;
    const response = await axios({
      method: 'POST',
      url,
      data: {
        declineReason,
        email: application.email,
      },
    });

    if (response.data.success) {
      window.location.href = '/admin/all-guest-ambassador-applicants';
    }
  };

  return (
    <div>
      <Nav history={props.history} />

      <div className="all-host-applicants">
        <div className="all-host-applicants-navigation">
          <Link exact="true" to="/admin" className="all-host-applicants-navigation-content">
            Admin
          </Link>
          <span className="arrow">&nbsp;&gt;&nbsp;</span>
          <Link
            exact="true"
            to="/admin/all-guest-ambassador-applicants"
            className="all-host-applicants-navigation-content"
          >
            All Guest Ambassador Applicants
          </Link>
          <span className="arrow">&nbsp;&gt;&nbsp;</span>
          {application ? `${application.first_name} ${application.last_name}` : 'N/A'}
        </div>

        <div className="mt-5">
          {application ? (
            <>
              <div className="row">
                <div className="col-md-7 host-applicant-content-col-1">
                  <ul>
                    <li>Influencer: {application.is_influencer}</li>
                    <li>Linkedin Profile: {application.linkedin_link}</li>
                    <li>Facebook Profile: {application.facebook_link}</li>
                    <li>Youtube Channel: {application.youtube_link}</li>
                    <li>Instagram Handle: {application.instagram_link}</li>
                    <li>Twitter Handle: {application.twitter_link}</li>
                    <li>Intent Description: {application.ambassador_intent_description}</li>
                  </ul>
                </div>
              </div>

              <div className="mt-5 text-center">
                <button type="button" className="mr-2 btn btn-success" onClick={approveApplication}>
                  Approve
                </button>
                <button
                  type="button"
                  className="btn btn-primary"
                  onClick={() => setDeclineModalOpen(true)}
                >
                  Decline
                </button>
              </div>
            </>
          ) : (
            <strong>Application not found.</strong>
          )}
        </div>
      </div>

      <Modal show={declineModalOpen} onHide={() => setDeclineModalOpen(false)}>
        <form onSubmit={declineApplication}>
          <Modal.Header closeButton>Decline Application</Modal.Header>
          <Modal.Body>
            <div className="form-group">
              <label>Reason for decline</label>
              <textarea
                className="form-control"
                required={true}
                value={declineReason}
                onChange={(e) => setDeclineReason(e.target.value)}
              />
              <small className="form-text text-muted">
                This will be included in the email sent to the user
              </small>
            </div>
          </Modal.Body>
          <Modal.Footer>
            <button
              type="button"
              className="btn btn-dark"
              onClick={() => setDeclineModalOpen(false)}
            >
              Close
            </button>
            <button type="submit" className="btn btn-primary">
              Decline
            </button>
          </Modal.Footer>
        </form>
      </Modal>
    </div>
  );
};

export default GuestAmbassadorApplicant;
