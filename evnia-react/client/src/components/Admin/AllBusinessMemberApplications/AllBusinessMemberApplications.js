// Libraries
import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import moment from 'moment';

// Components
import Nav from '../../Navbar/Nav';
// import AllHostApplicantsCard from "./AllHostApplicantsCard/AllHostApplicantsCard";
// Styling
import '../AllHostApplicants/AllHostApplicants.scss';

export default class AllBusinessApplicants extends Component {
  // Set initial state
  state = {
    allBusinessApplicantItems: [],
  };

  // Render All Host Applicants helper function
  //   renderAllBusinessApplicants = (arr) => {
  //     return arr.map((card, index) => (
  //       <AllBusinessApplicantsCard
  //         // key={index}
  //         // id={card.id}
  //         // userId={card.user_id}
  //         // profileImage={card.profile_img_url}
  //         // firstName={card.first_name}
  //         // lastName={card.last_name}
  //         // email={card.email}
  //         // phoneNumber={card.phone_number}
  //         // businessName={card.business_name}
  //         // businessType={card.business_type}
  //         // foodHandlerCertificate={card.food_handler_certificate}
  //         // voidCheque={card.void_cheque}
  //         // onlineEmail={card.online_email}
  //         // paypalEmail={card.paypal_email}
  //         // stripeAccount={card.stripe_account}
  //         // hostSelectionVideo={card.host_selection_video}
  //         // verified={card.verified}
  //         // isHost={card.is_host}
  //         // createdAt={card.created_at}
  //       />
  //     ));
  //   };

  // Mount All Host Applicants page
  componentDidMount = () => {
    window.scrollTo(0, 0);

    const url = '/hosts/business-member-applications';

    axios({ method: 'GET', url })
      .then((response) => {
        this.setState({
          allBusinessApplicantItems: [
            ...this.state.allBusinessApplicantItems,
            ...response.data.applications.reverse(),
          ],
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };

  // Render All Host Applicants page
  render = () => {
    const { allBusinessApplicantItems } = this.state;

    return (
      <div>
        <Nav />

        <div className="all-host-applicants">
          <div className="all-host-applicants-navigation">
            <Link exact="true" to="/admin" className="all-host-applicants-navigation-content">
              Admin
            </Link>
            <span className="arrow">&nbsp;&gt;&nbsp;</span>All Business Applicants
          </div>

          {allBusinessApplicantItems && allBusinessApplicantItems.length ? (
            <div className="table-responsive mt-5">
              <table className="table table-bordered">
                <thead>
                  <tr>
                    <th>Email</th>
                    <th>Membership Type</th>
                    <th>Date Submitted</th>
                  </tr>
                </thead>
                <tbody>
                  {allBusinessApplicantItems.map((a) => (
                    <tr key={a.tasttlig_user_id}>
                      <td>
                        <Link
                          exact="true"
                          to={`/admin/all-business-member-applicants/${a.tasttlig_user_id}`}
                        >
                          {`${a.email}`}
                        </Link>
                      </td>
                      <td className="all-host-applicants-business-type">{a.type}</td>
                      <td>{moment(a.updated_at_datetime).toLocaleString()}</td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          ) : (
            <div className="mt-5">
              <strong>No applications available.</strong>
            </div>
          )}
        </div>
      </div>
    );
  };
}
