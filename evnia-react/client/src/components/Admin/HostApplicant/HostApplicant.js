// Libraries
import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import Modal from 'react-bootstrap/Modal';

// Components
import Nav from '../../Navbar/Nav';
import FormReview from '../../Apply/ApplyForm/FormSteps/FormReview';

const HostApplicant = (props) => {
  const type = 'Bank';

  const [application, setApplication] = useState(null);
  const [declineModalOpen, setDeclineModalOpen] = useState(false);
  const [declineReason, setDeclineReason] = useState('');

  useEffect(() => {
    const fetchApplication = async () => {
      const userId = props.match.params.id;
      const url = `/hosts/applications/${userId}`;

      const response = await axios({ method: 'GET', url });

      if (response.data.success) {
        let result = response.data.application;

        result = {
          ...result,
          residential_address_line_1: result.user_address_line_1,
          residential_address_line_2: result.user_address_line_2,
          residential_city: result.user_city,
          residential_state: result.user_state,
          residential_postal_code: result.user_postal_code,
          residential_country: result.user_country,
          has_business: !!result.business_name ? 'yes' : 'no',
          service_provider: result.business_type,
          address_line_1: result.business_address_1,
          address_line_2: result.business_address_2,
          business_city: result.city,
          banking: result.payment_type,
          menu_list: result.menu_items.map((m) => ({
            menuName: m.title,
            menuImages: m.image_urls,
          })),
        };

        setApplication(result);
      }
    };

    fetchApplication();
  }, []);

  const closeDeclineModal = () => {
    setDeclineModalOpen(false);
  };
console.log("application results from fetch", application)
  const approveApplication = async () => {
    const userId = props.match.params.id;
    const url = `/hosts/applications/${userId}/approve`;
    const response = await axios({ method: 'POST', url });

    if (response.data.success) {
      window.location.href = '/admin/all-host-applicants';
    }
  };

  const declineApplication = async (event) => {
    event.preventDefault();

    if (!declineReason) {
      return;
    }

    const userId = props.match.params.id;
    const url = `/hosts/applications/${userId}/decline`;
    const response = await axios({
      method: 'POST',
      url,
      data: {
        declineReason,
      },
    });

    if (response.data.success) {
      window.location.href = '/admin/all-host-applicants';
    }
  };

  return (
    <div>
      <Nav history={props.history} />

      <div className="all-host-applicants">
        <div className="all-host-applicants-navigation">
          <Link exact="true" to="/admin" className="all-host-applicants-navigation-content">
            Admin
          </Link>
          <span className="arrow">&nbsp;&gt;&nbsp;</span>
          <Link
            exact="true"
            to="/admin/all-host-applicants"
            className="all-host-applicants-navigation-content"
          >
            All Host Applicants
          </Link>
          <span className="arrow">&nbsp;&gt;&nbsp;</span>
          {application ? `${application.first_name} ${application.last_name}` : 'N/A'}
        </div>

        <div className="mt-5">
          {application ? (
            <>
              <div className="row">
                <div className="col-md-7 host-applicant-content-col-1">
                  <FormReview
                    values={application}
                    sponsor={application.sponsor_user_id && true}
                    readMode
                  />
                </div>

                <div className="col-md-5 host-applicant-content-col-2">
                  {application.videos.length ? (
                    <div className="mb-4">
                      <h6>Videos</h6>
                      <hr />
                      {application.videos.map((v, i) => (
                        <video key={i} loop controls={true} preload="auto" className="mt-3 w-100">
                          <source src={v} type="video/mp4" />
                        </video>
                      ))}
                    </div>
                  ) : null}
                  {application.documents.length ? (
                    <div className="mb-4">
                      <h6>Documents</h6>
                      <hr />
                      {application.documents.map((d) => (
                        <div className="pb-3" key={d.document_id}>
                          <a target="_blank" href={d.document_link}>
                            {d.document_type}
                          </a>
                        </div>
                      ))}
                    </div>
                  ) : null}
                </div>
              </div>

              <div className="mt-5 text-center">
                <button type="button" className="mr-2 btn btn-success" onClick={approveApplication}>
                  Approve
                </button>
                <button
                  type="button"
                  className="btn btn-primary"
                  onClick={() => setDeclineModalOpen(true)}
                >
                  Decline
                </button>
              </div>
            </>
          ) : (
            <strong>Application not found.</strong>
          )}
        </div>
      </div>

      <Modal show={declineModalOpen} onHide={() => setDeclineModalOpen(false)}>
        <form onSubmit={declineApplication}>
          <Modal.Header closeButton>Decline Application</Modal.Header>
          <Modal.Body>
            <div className="form-group">
              <label>Reason for decline</label>
              <textarea
                className="form-control"
                required={true}
                value={declineReason}
                onChange={(e) => setDeclineReason(e.target.value)}
              />
              <small className="form-text text-muted">
                This will be included in the email sent to the user
              </small>
            </div>
          </Modal.Body>
          <Modal.Footer>
            <button
              type="button"
              className="btn btn-dark"
              onClick={() => setDeclineModalOpen(false)}
            >
              Close
            </button>
            <button type="submit" className="btn btn-primary">
              Decline
            </button>
          </Modal.Footer>
        </form>
      </Modal>
    </div>
  );
};

export default HostApplicant;
