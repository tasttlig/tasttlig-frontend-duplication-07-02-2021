// Libraries
import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import Modal from 'react-bootstrap/Modal';

//styling
import '../AllBusinessMemberApplications/BusinessApplicant.scss';

// Components
import Nav from '../../Navbar/Nav';
// import FormReview from '../../Apply/ApplyForm/FormSteps/FormReview';

const VendorApplicantionInfo = (props) => {
  const type = 'Bank';

  const [application, setApplication] = useState(null);
  const [declineModalOpen, setDeclineModalOpen] = useState(false);
  const [declineReason, setDeclineReason] = useState('');
  const [personalButton, setPersonalButton] = useState('');
  const [businessButton, setBusinessButton] = useState('');

  useEffect(() => {
    // console.log("here")
    const fetchApplication = async () => {
      const userId = props.match.params.id;
      const url = `/sponsor-application/${userId}`;

      const response = await axios({ method: 'GET', url });

      if (response.data.success) {
        let result = response.data.application;
        console.log("response from one sponsor aplcant:", response);
        result = {
          first_name: result.first_name,
          last_name: result.last_name,
          dateOfBirth: result.dateOfBirth,

          sex: result.sex,
          regular_passport_id: result.passport_id,
          occupation: result.occupation,
          city: result.user_city,
          province: result.user_state,
          country: result.user_country,
          phone_number: result.phone_number,

          business_name: result.business_name,
          business_type: result.business_type,
          food_business_type: result.food_business_type,
          business_phone_number: result.business_phone_number,
          business_details_description: result.business_details_description,
          business_passport_id: result.business_passport_id,
          business_unit: result.business_unit,
          business_street_number: result.business_street_number,
          business_street_name: result.business_street_name,
          business_city: result.city,
          business_province: result.state,
          business_country: result.country,
          business_postal_code: result.zip_postal_code,
          business_registered: result.business_registered,
          business_retail: result.retail_business,
          business_registered_location: result.business_registered_location,
          business_logo: result.business_details_logo,
          food_handling_certificate: result.food_handling_certificate,
          business_preference: result.business_preference,
          CRA_business_number: result.CRA_business_number,

          // menu_list: result.menu_items.map((m) => ({
          //   menuName: m.title,
          //   menuImages: m.image_urls,
          // })),
        };

        setApplication(result);
      }
    };

    fetchApplication();
  }, []);

  const closeDeclineModal = () => {
    setDeclineModalOpen(false);
  };

  const approveApplication = async () => {
    const userId = props.match.params.id;
    const url = `/sponsor-applications/${userId}/approve`;
    const response = await axios({ method: 'POST', url });

    if (response.data.success) {
      window.location.href = '/admin/all-vendor-applicants';
    }
  };

  const declineApplication = async (event) => {
    event.preventDefault();

    if (!declineReason) {
      return;
    }

    const userId = props.match.params.id;
    const url = `/sponsor-upgrade-applications/${userId}/decline`;
    const response = await axios({
      method: 'POST',
      url,
      data: {
        declineReason,
      },
    });

    if (response.data.success) {
      window.location.href = '/admin/all-sponsor-applicants';
    }
  };

  return (
    <div>
      <Nav history={props.history} />

      <div className="all-host-applicants">
        <div className="all-host-applicants-navigation">
          <Link exact="true" to="/admin" className="all-host-applicants-navigation-content">
            Admin
          </Link>
          <span className="arrow">&nbsp;&gt;&nbsp;</span>
          <Link
            exact="true"
            to="/admin/all-sponsor-applicants"
            className="all-host-applicants-navigation-content"
          >
            All Sponsor Applicants
          </Link>
          <span className="arrow">&nbsp;&gt;&nbsp;</span>
          {application ? `${application.first_name} ${application.last_name}` : 'N/A'}
        </div>

        <div className="mt-5">
          {application ? (
            <>
              <div className="row">
                <div className="button1">
                  <button
                    onClick={() => {
                      setPersonalButton('clicked');
                      setBusinessButton('');
                    }}
                    className="do-you-have-a-restaurant-yes-btn1"
                  >
                    Click for applicant's personal information
                  </button>
                </div>

                <div>
                  <button
                    onClick={() => {
                      setBusinessButton('clicked');
                      setPersonalButton('');
                    }}
                    className="do-you-have-a-restaurant-yes-btn1"
                  >
                    Click for applicant's business information
                  </button>
                </div>
              </div>

              <div className="col text-center">
                <p>
                  {personalButton === 'clicked' && (
                    <div className="text-left">
                      <h4> First Name: {application.first_name} </h4>
                      <h4> Last Name: {application.last_name} </h4>
                      <h4> Date Of Birth: {application.dateOfBirth} </h4>
                      <h4> Sex: {application.sex} </h4>
                      <h4> Regular Passport ID: {application.regular_passport_id} </h4>
                      <h4> Occupation: {application.occupation} </h4>
                      <h4> City: {application.city} </h4>
                      <h4> Province: {application.province} </h4>
                      <h4> Country: {application.country} </h4>
                      <h4> Phone Number: {application.phone_number} </h4>
                    </div>
                  )}
                </p>
              </div>

              <div className="col text-center">
                <p>
                  {businessButton === 'clicked' && (
                    <div className="text-left">
                      <h5> Business Logo:</h5>
                      <div>
                        <img src={application.business_logo} />
                      </div>
                      <div>Business Name:</div> <h4> {application.business_name} </h4>
                      <h5> Street Number: {application.business_street_number} </h5>
                      <h5> Street Name: {application.business_street_name} </h5>
                      <h5> Unit/Floor no: {application.business_unit} </h5>
                      <h5> City: {application.business_city} </h5>
                      <h5> Province: {application.business_province} </h5>
                      <h5> Country: {application.business_country} </h5>
                      <h5> Zip/Postal Code: {application.business_postal_code} </h5>
                      <h5> Business Phone Number: {application.business_phone_number} </h5>
                      <h5>
                        {' '}
                        Business Registered or not?:{' '}
                        {application.business_registered === true ? 'Yes' : 'No'}{' '}
                      </h5>
                      <h5>
                        {' '}
                        Business Retail or not?:{' '}
                        {application.business_retail === true ? 'Yes' : 'No'}{' '}
                      </h5>
                      <h5>
                        {' '}
                        Business Registration Location: {
                          application.business_registered_location
                        }{' '}
                      </h5>
                      <h5> Business Type: {application.business_type} </h5>
                      <h5> Food Business Type: {application.food_business_type} </h5>
                      <h5> Food Handler Certificate: </h5>
                      <div>
                        <img src={application.food_handling_certificate} />
                      </div>
                      <h5> Business CRA number: {application.CRA_business_number?application.CRA_business_number:'None'} </h5>
                      {/* <h5> Business Preference: {application.business_preference} </h5> */}
                    </div>
                  )}
                </p>
              </div>

              <div className="mt-5 text-center">
                <button type="button" className="mr-2 btn btn-success" onClick={approveApplication}>
                  Approve
                </button>
                <button
                  type="button"
                  className="btn btn-primary"
                  onClick={() => setDeclineModalOpen(true)}
                >
                  Decline
                </button>
              </div>
            </>
          ) : (
            <strong>Application not found.</strong>
          )}
        </div>
      </div>

      <Modal show={declineModalOpen} onHide={() => setDeclineModalOpen(false)}>
        <form onSubmit={declineApplication}>
          <Modal.Header closeButton>Decline Application</Modal.Header>
          <Modal.Body>
            <div className="form-group">
              <label>Reason for decline</label>
              <textarea
                className="form-control"
                required={true}
                value={declineReason}
                onChange={(e) => setDeclineReason(e.target.value)}
              />
              <small className="form-text text-muted">
                This will be included in the email sent to the user
              </small>
            </div>
          </Modal.Body>
          <Modal.Footer>
            <button
              type="button"
              className="btn btn-dark"
              onClick={() => setDeclineModalOpen(false)}
            >
              Close
            </button>
            <button type="submit" className="btn btn-primary">
              Decline
            </button>
          </Modal.Footer>
        </form>
      </Modal>
    </div>
  );
};

export default VendorApplicantionInfo;
