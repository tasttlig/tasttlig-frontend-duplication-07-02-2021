// Libraries
import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';

// Components
import Nav from '../Navbar/Nav';

// Styling
import './Admin.scss';
import allHostApplicants from '../../assets/images/all-host-applicants.png';
import addApplication from '../../assets/images/add-application.png';
import createNewUser from '../../assets/images/create-new-user.png';
import createFoodSample from '../../assets/images/create-food-sample.png';
import festivalIcon from '../../assets/images/festival-icon.png';

const Admin = () => {
  // Mount Admin page
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  // Render Admin page
  return (
    <div>
      <Nav />

      <div className="admin">
        <div className="admin-title">Admin</div>

        <div className="row">
          <div className="col-lg-4 admin-section">
            <Link exact="true" to="/complete-profile/business" className="admin-content">
              <div>
                <div>
                  {/* <img src={addApplication} alt="Add Application" className="admin-icon" /> */}
                  <i alt="Add Application" className="fas fa-plus admin-icon"></i>
                </div>
                <div className="admin-sub-title">Add Application</div>
              </div>
            </Link>
          </div>
          {/* <div className="col-lg-4 admin-section">
            <Link exact="true" to="/admin/all-host-applicants" className="admin-content">
              <div>
                <div>
                  <img src={allHostApplicants} alt="All Host Applicants" className="admin-icon" />
                  <i alt="All Host Applications" className="far fa-address-card admin-icon"></i>
                </div>
                <div className="admin-sub-title">All Host/Vendor/Sponsor Applicants</div>
              </div>
            </Link>
          </div> */}
          <div className="col-lg-4 admin-section">
            <Link exact="true" to="/admin/all-host-applicants" className="admin-content">
              <div>
                <div>
                  <i alt="All Host Applications" className="far fa-address-card admin-icon"></i>
                </div>
                <div className="admin-sub-title">All Host Applicants</div>
              </div>
            </Link>
          </div>
          <div className="col-lg-4 admin-section">
            <Link exact="true" to="/admin/all-vendor-applicants" className="admin-content">
              <div>
                <div>
                  {/* <img src={allHostApplicants} alt="All Host Applicants" className="admin-icon" /> */}
                  <i alt="All Host Applications" className="far fa-address-card admin-icon"></i>
                </div>
                <div className="admin-sub-title">All Vendor Applicants</div>
              </div>
            </Link>
          </div>

          <div className="col-lg-4 admin-section">
            <Link exact="true" to="/admin/all-sponsor-applicants" className="admin-content">
              <div>
                <div>
                  {/* <img src={allHostApplicants} alt="All Host Applicants" className="admin-icon" /> */}
                  <i alt="All Host Applications" className="far fa-address-card admin-icon"></i>
                </div>
                <div className="admin-sub-title">All Sponsor Applicants</div>
              </div>
            </Link>
          </div>

          <div className="col-lg-4 admin-section">
            <Link exact="true" to="/admin/all-business-member-applicants" className="admin-content">
              <div>
                <div>
                  {/* <img src={allHostApplicants} alt="All Host Applicants" className="admin-icon" /> */}
                  <i alt="All Member Applications" className="far fa-address-card admin-icon"></i>
                </div>
                <div className="admin-sub-title">All Business Member Applicants</div>
              </div>
            </Link>
          </div>

          <div className="col-lg-4 admin-section">
            <Link
              exact="true"
              to="/admin/all-guest-ambassador-applicants"
              className="admin-content"
            >
              <div>
                <div>
                  {/* <img src={allHostApplicants} alt="All Host Applicants" className="admin-icon" /> */}
                  <i alt="All Member Applications" className="far fa-address-card admin-icon"></i>
                </div>
                <div className="admin-sub-title">All Guest Ambassador Applicants</div>
              </div>
            </Link>
          </div>

          <div className="col-lg-4 admin-section">
            <Link exact="true" to="/complete-profile/new-user" className="admin-content">
              <div className="dashboard-content-text">
                <img src={createNewUser} alt="Create New User" className="dashboard-icon" />
                <div className="admin-sub-title">Create New User</div>
              </div>
            </Link>
          </div>
          <div className="col-lg-4 admin-section">
            <Link exact="true" to="/create-food-samples" className="admin-content">
              <div className="dashboard-content-text">
                <img src={createFoodSample} alt="Create Food Sample" className="dashboard-icon" />
                <div className="admin-sub-title">Create Products</div>
              </div>
            </Link>
          </div>
          <div className="col-lg-4 admin-section">
            <Link exact="true" to="/create-festival" className="admin-content">
              <div className="dashboard-content-text">
                <img src={festivalIcon} alt="Create Festival" className="dashboard-icon" />
                <div>Create Festival</div>
              </div>
            </Link>
          </div>
          <div className="col-lg-4 admin-section">
            <Link exact="true" to="/restaurants" className="admin-content">
              <div className="dashboard-content-text">
                <img
                  src={festivalIcon}
                  alt="Add restaurants to festival"
                  className="dashboard-icon"
                />
                <div>Add Restaurants to Festival</div>
              </div>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Admin;
