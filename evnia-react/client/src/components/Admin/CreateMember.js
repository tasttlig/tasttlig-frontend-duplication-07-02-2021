// Libraries
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';

// Components
import Nav from '../Navbar/Nav';
import { formatPhoneNumber } from '../Functions/Functions';

// Styling
import './Admin.scss';
import 'react-datepicker/dist/react-datepicker.css';
import 'react-toastify/dist/ReactToastify.css';

toast.configure();

const CreateNewUser = () => {
  // Set initial state
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');
  const [is_participating_in_festival, setIs_participating_in_festival] = useState(false);
  const [firstNameError, setFirstNameError] = useState('');
  const [emailError, setEmailError] = useState('');
  const [phoneNumberError, setPhoneNumberError] = useState('');
  const [submitAuthDisabled, setSubmitAuthDisabled] = useState(false);

  // Validate user input for Creating a new User helper function
  const validateNewUser = () => {
    window.scrollTo(0, 0);
    let isError = false;

    // Render first name error message
    if (firstName.length === 0) {
      setFirstNameError('First Name is required.');
      isError = true;
    } else {
      setFirstNameError('');
    }

    // Render email error message
    if (email.length === 0) {
      setEmailError('Email is required.');
      isError = true;
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(email)) {
      setEmailError('Please enter a valid email.');
      isError = true;
    } else {
      setEmailError('');
    }

    // Render phone number error message
    if (phoneNumber.length === 0) {
      setPhoneNumberError('Phone Number is required.');
      isError = true;
    } else if (phoneNumber.length < 14) {
      setPhoneNumberError('Please enter a valid phone number.');
      isError = true;
    } else {
      setPhoneNumberError('');
    }

    return isError;
  };

  const handleSubmitNewUser = async (event) => {
    event.preventDefault();

    const isError = validateNewUser();

    if (!isError) {
      const url = '/admin/add-user';
      const acc_token = await localStorage.getItem('access_token');
      const headers = { Authorization: `Bearer ${acc_token}` };

      let data = {
        first_name: firstName,
        last_name: lastName || ' ',
        email,
        phone_number: phoneNumber,
        is_participating_in_festival: is_participating_in_festival,
      };

      try {
        const response = await axios({
          method: 'POST',
          url,
          headers,
          data,
        });

        if (response && response.data && response.data.success) {
          setTimeout(() => {
            localStorage.setItem('new_user_email', email);
            window.location.href = '/dashboard';
          }, 2000);

          toast(`Success! New User Created!`, {
            type: 'success',
            autoClose: 2000,
          });

          setSubmitAuthDisabled(true);
        }
      } catch (error) {
        toast('Error! Something went wrong!', {
          type: 'error',
          autoClose: 2000,
        });
      }
    }
  };

  // Mount Create User page
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  // Render Create User page
  return (
    <div>
      <Nav />

      <div className="create-member">
        <form onSubmit={handleSubmitNewUser} noValidate>
          <div className="text-center create-user-title">Create New User</div>

          <div className="mb-3">
            <div className="input-title">First Name*</div>
            <input
              type="text"
              value={firstName}
              onChange={(e) => setFirstName(e.target.value)}
              disabled={submitAuthDisabled}
              className="form-control"
              required
            />
            {firstNameError && <div className="error-message">{firstNameError}</div>}
          </div>

          <div className="mb-3">
            <div className="input-title">Last Name</div>
            <input
              type="text"
              value={lastName}
              onChange={(e) => setLastName(e.target.value)}
              disabled={submitAuthDisabled}
              className="form-control"
              required
            />
          </div>

          <div className="mb-3">
            <div className="input-title">Email*</div>
            <input
              type="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              disabled={submitAuthDisabled}
              className="form-control"
              required
            />
            {emailError && <div className="error-message">{emailError}</div>}
          </div>

          <div className="mb-3">
            <div className="input-title">Phone Number*</div>
            <input
              type="tel"
              name="phoneNumber"
              value={formatPhoneNumber(phoneNumber)}
              onChange={(e) => setPhoneNumber(e.target.value)}
              maxLength="14"
              disabled={submitAuthDisabled}
              className="phone-number"
              required
            />
            {phoneNumberError && <div className="error-message">{phoneNumberError}</div>}
          </div>

          <div className="mb-3">
            <label>
              <input
                type="checkbox"
                defaultChecked={is_participating_in_festival}
                onChange={() => setIs_participating_in_festival(!is_participating_in_festival)}
                disabled={submitAuthDisabled}
              />
              &nbsp;&nbsp;I agree to participate in Tasttlig Festival
            </label>
          </div>

          <div>
            <button type="submit" disabled={submitAuthDisabled} className="call-to-action-btn">
              Submit
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default CreateNewUser;
