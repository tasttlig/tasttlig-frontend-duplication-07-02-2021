// Libraries
import React, { useState, useEffect, useContext } from 'react';
import { AppContext } from '../../ContextProvider/AppProvider';
import { Link } from 'react-router-dom';

// Styling
import './ApplyForBusinessPassport.scss';

const ApplyForBusinessPassport = (props) => {
  // To use the JWT credentials
  const appContext = useContext(AppContext);

  return (
    <div className="container">
      <div className="row apply-for-business-passport">
        <h1>There is no Restaurant linked to this account.</h1>
        {/* <Link exact="true" to="/get-business-passport"> */}
       <Link exact="true" to="/business-passport"> 
          {/* <button className="apply-button">Apply for a Business Passport</button> */}
          <button className="apply-button">Add your Restaurant</button>
        </Link>
      </div>
    </div>
  );
};

export default ApplyForBusinessPassport;
