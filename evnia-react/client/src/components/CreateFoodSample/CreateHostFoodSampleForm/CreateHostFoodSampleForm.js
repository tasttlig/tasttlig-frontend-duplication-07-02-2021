// Libraries
import React, { useState, useEffect, useContext } from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import { AppContext } from '../../../ContextProvider/AppProvider';
import CreateSampleSaleDonation from '../../CreateSampleSaleDonation/CreateSampleSaleDonation';

// Components
import {
  Checkbox,
  CheckboxGroup,
  DateInput,
  Form,
  Input,
  MultiImageInput,
  Select,
  Textarea,
  UserSelector,
} from '../../EasyForm';
import { canadaProvincesTerritories } from '../../Functions/Functions';

// Styling
import './CreateHostFoodSampleForm.scss';
import 'react-datepicker/dist/react-datepicker.css';
import 'react-toastify/dist/ReactToastify.css';

toast.configure();

const CreateHostFoodSampleForm = (props) => {
  console.log(props);
  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const new_user_email = localStorage.getItem('new_user_email');
  const userRole = appContext.state.user.role;
  const update = props.location.state;
  const heading = props.heading;
  const data = props.update ? props.update : props.foodSample ? props.foodSample : {};

  data.userEmail = new_user_email;
  /*   if (!new_user_email) {
      console.log(localStorage, "local storage");
      data.userEmail = JSON.parse(localStorage.getItem("dataFromBecomeHost")).email
      console.log(data.userEmail, "userEmail")
    } */
  //console.log(localStorage);

  const onCreated = props.onCreated;

  // Set initial state
  const [nationalities, setNationalities] = useState([]);
  const [isAdmin, setIsAdmin] = useState(
    appContext.state.user &&
      appContext.state.user.role &&
      appContext.state.user.role.includes('ADMIN'),
  );
  const [successMessage, setSuccessMessage] = useState('');
  const [submitAuthDisabled, setSubmitAuthDisabled] = useState(false);
  const [festivalDetails, setFestivalDetails] = useState([]);
  const [festivalPreference, setFestivalPreference] = useState([]);
  const [festivalPreferenceName, setFestivalPreferenceName] = useState([]);

  // const [loading, setLoading] = useState(true);

  const fetchFestivalDetails = async () => {
    const url = '/festival/allFestival';

    try {
      const response = await axios({ method: 'GET', url });
      console.log(response);
      return response;
    } catch (error) {
      return error.response;
    }
  };
  // Mount Coming Soon Landing page
  useEffect(() => {
    window.scrollTo(0, 0);

    fetchFestivalDetails().then((response) => {
      setFestivalDetails(response.data.festival_list);
    });
  }, []);

  const onChangeFestival = function (event) {
    //takes the file and reads the file from buffer of array
    // console.log("value from festival: ", event.target.value.festival_name)
    //console.log(event.target.value);
    if (festivalPreference.includes(event.target.value)) {
      console.log('error');
    } else {
      const matchedFestival = festivalDetails.find((value) => {
        return value.festival_id === Number(event.target.value);
      });
      setFestivalPreference(festivalPreference.concat(matchedFestival));
      //setFestivalPreferenceName(festivalPreferenceName.concat(matchedFestival.festival_name))
    }
  };

  //function to render over the selected options from dropdown and print them
  const getOptions = function (festivals) {
    return festivals.map((festival) => (
      <button
        className="festival-card-festival-attend-btn"
        onClick={(event) => {
          console.log(event);
          console.log('click event firing');
          setFestivalPreference((stateFestivals) => {
            return stateFestivals.filter((stateFestival) => {
              console.log(stateFestival, festival);
              if (stateFestival.festival_id === festival.festival_id) {
                return false;
              } else {
                return true;
              }
            });
          });
        }}
      >
        {festival.festival_name}
      </button>
    ));
  };

  // Submit food sample helper function
  const submitFoodSample = async (data) => {
    window.scrollTo(0, 0);
    if (!new_user_email) {
      data.userEmail = JSON.parse(localStorage.getItem('dataFromBecomeHost')).email;
    }

    let userExist;
    const url = props.update
      ? `/food-sample/update/${props.update.foodSampleId}`
      : userRole
      ? '/food-sample/add'
      : '/food-sample/noUser/add';
    try {
      const response = await axios({
        method: props.update ? 'PUT' : 'POST',
        url,
        data: props.update ? data : [data],
      });

      if (userRole && userRole.includes('ADMIN')) {
        userExist = await axios({
          method: 'GET',
          url: `/user/${data.userEmail}`,
        });
      }
      if (
        (response &&
          response.data &&
          response.data.success &&
          ((userRole && (userRole.includes('HOST') || userRole.includes('HOST_PENDING'))) ||
            props.update ||
            (userExist && !userExist.data.message))) ||
        (response && response.data && response.data.success && !userRole)
      ) {
        onCreated ? onCreated() : defaultOnCreatedAction();
      }
    } catch (error) {
      console.log(error);
      toast('Error! Something went wrong!', {
        type: 'error',
        autoClose: 2000,
      });
    }
  };

  const onSubmit = async (values) => {
    console.log('values coming out of the on submit:', values);
    const {
      addressLine1,
      addressLine2,
      provinceTerritory,
      dietaryRestrictions,
      daysAvailable,

      ...rest
    } = values;

    let address = addressLine1;

    if (addressLine2 && addressLine2.length > 0) {
      address = `${address}, ${addressLine2}`;
    }

    let data = {
      ...rest,
      address,
      festivals: festivalPreference.map((festival) => {
        return festival.festival_id;
      }),
      is_vegetarian: dietaryRestrictions.includes('vegetarian'),
      is_vegan: dietaryRestrictions.includes('vegan'),
      is_gluten_free: dietaryRestrictions.includes('glutenFree'),
      is_halal: dietaryRestrictions.includes('halal'),
      is_available_on_monday: daysAvailable.includes('available_on_monday'),
      is_available_on_tuesday: daysAvailable.includes('available_on_tuesday'),
      is_available_on_wednesday: daysAvailable.includes('available_on_wednesday'),
      is_available_on_thursday: daysAvailable.includes('available_on_thursday'),
      is_available_on_friday: daysAvailable.includes('available_on_friday'),
      is_available_on_saturday: daysAvailable.includes('available_on_saturday'),
      is_available_on_sunday: daysAvailable.includes('available_on_sunday'),
      state: provinceTerritory,
      country: 'Canada',
    };

    data.start_time = values.start_time.toString().substring(16, 21);
    data.end_time = values.end_time.toString().substring(16, 21);

    if (props.update) {
      data = {
        food_sample_update_data: data,
      };
    }
    console.log('values', data);
    await submitFoodSample(data);
  };

  const defaultOnCreatedAction = () => {
    setTimeout(() => {
      // window.location.href = '/dashboard';
    }, 2000);

    toast(`Success! Thank you for ${props.update ? 'updating' : 'creating'} a food sample!`, {
      type: 'success',
      autoClose: 2000,
    });

    setSuccessMessage('Your food sample will be shown as soon as your application is approved.');
    setSubmitAuthDisabled(true);
  };

  // Mount Create Food Sample page
  useEffect(() => {
    window.scrollTo(0, 0);

    const fetchNationalities = async () => {
      const response = await axios({
        method: 'GET',
        url: '/nationalities',
      });

      setNationalities(response.data.nationalities);
    };

    fetchNationalities();
  }, []);

  useEffect(() => {
    setIsAdmin(
      appContext.state.user &&
        appContext.state.user.role &&
        appContext.state.user.role.includes('ADMIN'),
    );
  }, [appContext]);

  // Render Create Food Sample page
  return (
    <div className="create-food-sample">
      {successMessage && userRole && appContext.state.user.role.includes('HOST_PENDING') && (
        <div className="btn btn-success mb-3">{successMessage}</div>
      )}

      <div className="create-food-sample-title">
        {heading ? heading : 'Create Product or Service'}
      </div>
      <div classname="container-lg">
        <CreateSampleSaleDonation></CreateSampleSaleDonation>
      </div>
      {/* <Form data={data} onSubmit={onSubmit}>
        <MultiImageInput
          name="images"
          label="Images"
          dropbox_label="Click or drag-and-drop to upload one or more images"
          disabled={submitAuthDisabled}
          required
        />

        {isAdmin && !props.update && (
          <UserSelector
            name="userEmail"
            label="User Email"
            disabled={submitAuthDisabled}
            required
          />
        )}

        <div className="mb-3">
          <h5>Which festival(s) would you like to host?</h5>
          <div className="input-title">Select all that apply</div>
          <Select
            name="festival"
            label="List festival"
            onChange={onChangeFestival}
            disabled={submitAuthDisabled}
            // required
          >
            <option value="" name="">
              --Select--
            </option>
            {festivalDetails.map((n) => (
              <option key={n.festival_id} value={n.festival_id} name={n.festival_name}>
                {n.festival_name}
              </option>
            ))}
          </Select>
          {festivalPreference.length === 0 ? (
            <div className="col-md-4 p-2">
              <p></p>
            </div>
          ) : (
            <div className="col-md-4">{getOptions(festivalPreference)}</div>
          )}
        </div>

        <Input name="title" label="Food Name" disabled={submitAuthDisabled} required />

        {nationalities.length && (
          <Select name="nationality_id" label="Nationality" disabled={submitAuthDisabled} required>
            <option value="">--Select--</option>
            {nationalities.map((n) => (
              <option key={n.id} value={n.id}>
                {n.nationality}
              </option>
            ))}
          </Select>
        )}

        <div className="row">
          <div className="col-md-6 create-food-sample-start-date">
            <DateInput
              name="start_date"
              label="Start Date"
              dateFormat="yyyy/MM/dd"
              minDate={new Date()}
              peekNextMonth
              showMonthDropdown
              showYearDropdown
              dropdownMode="select"
              disabled={submitAuthDisabled}
              required
            />
          </div>
          <div className="col-md-6 create-food-sample-end-date">
            <DateInput
              name="end_date"
              label="End Date"
              dateFormat="yyyy/MM/dd"
              minDate={new Date()}
              peekNextMonth
              showMonthDropdown
              showYearDropdown
              dropdownMode="select"
              disabled={submitAuthDisabled}
              required
            />
          </div>
        </div>

        <div className="row">
          <div className="col-md-6 create-food-sample-start-time">
            <DateInput
              name="start_time"
              label="Start Time"
              showTimeSelect
              showTimeSelectOnly
              dateFormat="h:mm aa"
              disabled={submitAuthDisabled}
              required
            />
          </div>
          <div className="col-md-6 create-food-sample-end-time">
            <DateInput
              name="end_time"
              label="End Time"
              showTimeSelect
              showTimeSelectOnly
              dateFormat="h:mm aa"
              disabled={submitAuthDisabled}
              required
            />
          </div>
        </div>

        <Select name="sample_size" label="Sample Size" disabled={submitAuthDisabled} required>
          <option value="">--Select--</option>
          <option value="Bite Size">Bite Size</option>
          <option value="Quarter">Quarter</option>
          <option value="Half">Half</option>
          <option value="Full">Full</option>
        </Select>

        <Input
          name="quantity"
          label="Sample Quantity Available"
          type="number"
          min="1"
          disabled={submitAuthDisabled}
          required
        />

        <CheckboxGroup
          name="dietaryRestrictions"
          label="Dietary Restrictions (Optional)"
          options={[
            ['Vegetarian', 'vegetarian'],
            ['Vegan', 'vegan'],
            ['Gluten-Free', 'glutenFree'],
            ['Halal', 'halal'],
          ]}
          disabled={submitAuthDisabled}
        />

        <CheckboxGroup
          name="daysAvailable"
          label="Days Available"
          options={[
            ['Monday', 'available_on_monday'],
            ['Tuesday', 'available_on_tuesday'],
            ['Wednesday', 'available_on_wednesday'],
            ['Thursday', 'available_on_thursday'],
            ['Friday', 'available_on_friday'],
            ['Saturday', 'available_on_saturday'],
            ['Sunday', 'available_on_sunday'],
          ]}
          disabled={submitAuthDisabled}
          required
        />

        <Select name="spice_level" label="Spice Level (Optional)" disabled={submitAuthDisabled}>
          <option value="">--Select--</option>
          <option value="Mild">Mild</option>
          <option value="Medium">Medium</option>
          <option value="Hot">Hot</option>
        </Select>

        <Input name="addressLine1" label="Street Address" disabled={submitAuthDisabled} required />
        <Input name="addressLine2" label="Unit Address" disabled={submitAuthDisabled} />
        <Input name="city" label="City" disabled={submitAuthDisabled} required />
        <Select
          name="provinceTerritory"
          label="Province or Territory"
          disabled={submitAuthDisabled}
          required
        >
          {canadaProvincesTerritories()}
        </Select>
        <Input
          name="postal_code"
          label="Postal Code"
          maxLength="7"
          disabled={submitAuthDisabled}
          required
        />

        <Textarea name="description" label="Description" disabled={submitAuthDisabled} required />

        {!props.update && <Checkbox name="addToFestival" label="Add to festival?" />}

        <div>
          <button type="submit" disabled={submitAuthDisabled} className="call-to-action-btn">
            Submit
          </button>
        </div>
      </Form> */}
    </div>
  );
};

export default CreateHostFoodSampleForm;
