// Libraries
import React, { useState, useEffect } from 'react';
import axios from 'axios';

// Components
import Nav from '../Navbar/Nav';
import ReviewFoodSampleForm from './ReviewFoodSampleForm/ReviewFoodSampleForm';

const ReviewFoodSample = (props) => {
  // Set initial state
  const [reviewFoodSample, setReviewFoodSample] = useState([]);

  // Fetch food sample for review helper function
  const fetchReviewFoodSample = async () => {
    try {
      const food_sample_id = props.match.params.id;
      const response = await axios({
        method: 'GET',
        url: `/food-sample/${food_sample_id}`,
      });

      return response;
    } catch (error) {
      return error.response;
    }
  };

  // Mount Review Food Sample page
  useEffect(() => {
    window.scrollTo(0, 0);

    fetchReviewFoodSample().then(({ data }) => {
      data.details.map((item) => {
        if (parseInt(props.match.params.id) === item.food_sample_id) {
          setReviewFoodSample((info) => [...info, item]);
        }
      });
    });
  }, []);

  // Render Review Food Sample page
  return (
    <div>
      <Nav />

      {reviewFoodSample.map((food_sample) => (
        <ReviewFoodSampleForm
          key={food_sample.food_sample_id}
          reviewFoodSample={food_sample}
          token={props.match.params.token}
        />
      ))}
    </div>
  );
};

export default ReviewFoodSample;
