// Libraries
import React, { useEffect } from 'react';

// Components
import Nav from '../Navbar/Nav';
import CreateFestivalForm from './CreateFestivalForm/CreateFestivalForm';

const CreateFestival = (props) => {
  // Mount Create Festival page
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  // Render Create Festival page
  return (
    <div>
      <Nav />

      <CreateFestivalForm update={props.location.state} heading={props.heading} />
    </div>
  );
};

export default CreateFestival;
