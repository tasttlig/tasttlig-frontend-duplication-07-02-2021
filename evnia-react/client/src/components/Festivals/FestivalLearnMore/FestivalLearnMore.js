// Libraries
import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';

// Components
import Nav from '../../Navbar/Nav';
import BannerFooter from '../../Home/BannerFooter/BannerFooter';

// Styling
import './FestivalLearnMore.scss';
import festivalLearnMoreImage from '../../../assets/images/banner-main.jpg';

const FestivalLearnMore = (props) => {
  // Source tracking on new sign ups
  const source = new URLSearchParams(props.location.search).get('source');
  if (source) {
    if (!localStorage.getItem('source')) {
      localStorage.setItem('source', source);
    }
  }

  // Mount Learn More About Festival page
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  // Render Learn More About Festival page
  return (
    <div>
      <Nav />

      <div className="festival-learn-more">
        <div className="row">
          <div className="col-lg-6 festival-learn-more-content">
            <img
              src={festivalLearnMoreImage}
              alt="Experience the world"
              className="festival-learn-more-image"
            />
          </div>
          <div className="col-lg-6 festival-learn-more-content">
            <div className="festival-learn-more-text">
              <div className="mb-4">
                The Tasttlig Multi-National Food Festival provides guests with free food from around
                the world in one place. The festival platform provides guests a single place to
                discover, taste and experience food from around the world. It aims to celebrate
                diversity in food and culture in our cities by showcasing our multiple nationalities
                in the best light. The festival happens four times a year. December, March, June and
                September across major cities starting from Toronto. It runs from December 01 -
                31st, 2020. It’s your time to embark on a food adventure to celebrate hospitality,
                diversity and inclusion. For more information, follow us on our social media handles
                for frequent updates. Celebrate. Go and taste food from around the world.
              </div>
            </div>
            <div className="previous-festival-btn-content">
              <Link exact="true" to="/festival" className="previous-festival-btn">
                See Previous Festival
              </Link>
            </div>
          </div>
        </div>
      </div>

      <BannerFooter />
    </div>
  );
};

export default FestivalLearnMore;
