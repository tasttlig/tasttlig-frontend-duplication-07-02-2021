// Libraries
import React, { useState, useContext, useEffect, useRef } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import LazyLoad from 'react-lazyload';
import moment from 'moment';
import { toast } from 'react-toastify';
import { useHistory } from 'react-router-dom';
import { AppContext } from '../../../ContextProvider/AppProvider';
import Modal from 'react-modal';
import { formatDate } from '../../Functions/Functions';
import LoadingBar from 'react-top-loading-bar';
//import BusinessPassportStart from "./BusinessPassportModals/BusinessPassportStart";
import BusinessConditions from '../../LandingPage/TermsAndConditions/BusinessConditions';
// Components
import CreateSampleSaleDonation from '../../CreateSampleSaleDonation/CreateSampleSaleDonation';
import ViewProductsServicesExperiences from '../../ViewProductsServicesExperiences/ViewProductsServicesExperiences';
import ImageSlider from '../../ImageSlider/ImageSlider';
import BusinessPassportStart from './BusinessPassportStart';
import VendingConditions from '../../LandingPage/TermsAndConditions/VendingConditions';
// import {
//   Select,
// } from "../../EasyForm";

// Styling
import './FestivalCard.scss';
import 'react-toastify/dist/ReactToastify.css';
import liveFestival from '../../../assets/images/live.png';
import { stringify } from 'uuid';

toast.configure();

const ExpiredFestivalCard = (props) => {
  console.log('props from festival cards:', props);

  // Set initial state
  const [load, setLoad] = useState(false);
  const ref = useRef(null);
  const [submitAuthDisabled, setSubmitAuthDisabled] = useState(false);
  const [businessConditions, setBusinessConditions] = useState(false);
  const [passportDetailsOpened, setPassportDetailsOpened] = useState(false);
  const [foodSamples, setFoodSamples] = useState([]);
  const [foodSamplePreference, setFoodSamplePreference] = useState([]);
  const [isBusinessPassportStartOpen, setIsBusinessPassportStartOpen] = useState(false);
  const [isStartBusinessInterestOpen, setIsStartBusinessInterestOpen] = useState(false);
  const [isBusinessInterestConfirmationOpen, setIsBusinessInterestConfirmationOpen] = useState(
    false,
  );
  const [toggleState, setToggleState] = useState(2);
  const [vendingConditions, setVendingConditions] = useState(false);

  const toggleTab = (index) => {
    setToggleState(index);
  };

  // Calculate the number of days between start/end and current date
  const d1 = new Date(props.startDate);
  const d2 = new Date(props.endDate);
  const d3 = new Date();
  const millisecondsInDay = 86400000;
  const upcomingDays = Math.floor((d1 - d3) / millisecondsInDay) + 1;
  const endingDays = Math.ceil((d2 - d3) / millisecondsInDay) + 1;
  const history = useHistory();
  // Set date and time
  const startDateTime = moment(
    new Date(props.startDate).toISOString().split('T')[0] + 'T' + props.startTime + '.000Z',
  ).add(new Date().getTimezoneOffset(), 'm');

  const endDateTime = moment(
    new Date(props.startDate).toISOString().split('T')[0] + 'T' + props.endTime + '.000Z',
  ).add(new Date().getTimezoneOffset(), 'm');

  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const userRole = appContext.state.user.role;

  // Close modal type helper function
  const closeModal = (modalType) => () => {
    if (modalType === 'passport-details') {
      setPassportDetailsOpened(false);
    }
  };

  // When festival is already sponsored by user
  const redundanceCheck = () => {
    toast(`You already selected as a ${props.title} sponsor!`, {
      type: 'info',
      autoClose: 2000,
    });
  };

  // Host festival helper function
  const handleHostFestival = async (event) => {
    event.preventDefault();
    // if (props.hostFestivalList && props.hostFestivalList.includes(appContext.state.user.id))
    //  {
      
    //     if (userRole && !userRole.includes('HOST') && userRole.includes('VENDOR')) 
    //     {
    //         toast(`You already selected ${props.title} for vending!`, {
    //           type: 'info',
    //           autoClose: 2000,
    //         })
    //     } 
    //     else if( userRole && userRole.includes('HOST')) 
    //     {
    //       toast(`You already selected ${props.title} for hosting!`, {
    //         type: 'info',
    //         autoClose: 2000,
    //       })
    //     }
    
    // } else {
      //openModal("passport-details")
      toggleTab(2);
      setPassportDetailsOpened(true);
    // }
  };

  const handleFoodSampleFestival = async () => {
    const url = '/host-festival';
    const acc_token = localStorage.getItem('access_token');
    const headers = { Authorization: `Bearer ${acc_token}` };
    const data = {
      festival_id: [props.festivalId],
      festival_restaurant_host_id: appContext.state.user.id,
      foodSamplePreference: foodSamplePreference.map((sample) => {
        return sample.food_sample_id;
      }),
    };

    try {
      localStorage.setItem('festival_id', JSON.stringify([props.festivalId]));

      const response = await axios({ method: 'POST', url, headers, data });

      if (response && response.data && response.data.success) {
        setTimeout(() => {
          // window.location.reload();
          window.location.href = `/festival/${props.festivalId}`;
        }, 2000);

        toast(`Success! Thank you for hosting ${props.title}!`, {
          type: 'success',
          autoClose: 2000,
        });

        setSubmitAuthDisabled(true);
      }
    } catch (error) {
      toast('Error! Something went wrong!', {
        type: 'error',
        autoClose: 2000,
      });
    }
  };

  const fetchUserFoodSamples = async () => {
    const response = await axios({
      method: 'GET',
      url: `/food-sample/user/all`,
      data: {
        user_id: appContext.state.user.id,
      },
    });
    setFoodSamples(response.data.details.data);
    return response;
  };

  useEffect(() => {
    fetchUserFoodSamples();
  }, []);

  //Handle button for editing festival
  const handleEditFestival = () => {
    history.push({
      pathname: '/edit-festival',
      state: {
        festivalId: props.festivalId,
        images: props.images,
        festival_name: props.title,
        festival_type: props.type,
        festival_price: props.price,
        festival_city: props.city,
        festival_start_date: props.startDate,
        festival_end_date: props.endDate,
        festival_start_time: new Date(startDateTime),
        festival_end_time: new Date(endDateTime),
        festival_description: props.description,
      },
    });
  };

  // Sponsor festival helper function
  /* const handleSponsorFestival = async (event) => {
    event.preventDefault();

    //will be moved sponsorship package form submission
    const url = "/sponsor-festival";

    const acc_token = localStorage.getItem("access_token");

    const headers = { Authorization: `Bearer ${acc_token}` };

    const data = {
      festival_id: props.festivalId,
      festival_business_sponsor_id: appContext.state.user.id,
    };

    try {
      const response = await axios({ method: "POST", url, headers, data });

      if (response && response.data && response.data.success) {
        setTimeout(() => {
          window.location.reload();
        }, 2000);

        toast(`Success! Thank you for sponsoring ${props.title}!`, {
          type: "success",
          autoClose: 2000,
        });

        setSubmitAuthDisabled(true);
      }
    } catch (error) {
      toast("Error! Something went wrong!", {
        type: "error",
        autoClose: 2000,
      });
    } */

  let festivalFooterClassName;
  if (props.festivalPayment) {
    festivalFooterClassName = 'festival-card-footer-subscription';
  } else {
    festivalFooterClassName = 'festival-card-footer';
  }

  const gotoSignUpWithStoredFestival = () => {
    localStorage.setItem('initialFestivalSelection', props.festivalId);
    localStorage.setItem('initialFestivalTitle', props.title);

    history.push({
      pathname: '/sign-up',
      state: {},
    });
  };
  console.log('props.hostFestivalList from festival cards:', props.hostFestivalList)

  const BusinessPassportStart = () => {
    return (
      <Modal
        isOpen={isBusinessPassportStartOpen}
        onRequestClose={() => setIsBusinessPassportStartOpen(false)}
        ariaHideApp={true}
        class="modals"
      >
        <div className="bg-danger p-5 mb-4" closeButton>
          <div className="text-center text-white">Do you have a business?</div>
        </div>
        <div>
          <div className="btn btn-light text-dark ">
            <Link
              exact="true"
              to={{
                pathname: '/business-passport',
                is_sponsor: true,
              }}
              className="text-dark"
            >
              Yes
            </Link>
          </div>

          {/* <button
              type="button"
              className="ml-5 py-3 px-4 text-center btn btn-light text-dark"
              onClick={() => {
                setIsBusinessPassportStartOpen(false);
                setIsStartBusinessInterestOpen(true)
              }}
            >
            No
          </button> */}
          <div
            className="ml-5 py-3 px-4 text-center btn btn-light"
            onClick={() => {
              setIsBusinessPassportStartOpen(false);
            }}
          >
            <Link
              exact="true"
              to={{
                pathname: '/sponsor-plan',
              }}
              className="text-dark"
            >
              No
            </Link>
          </div>
        </div>
      </Modal>
    );
  };

  const StartBusinessInterest = () => {
    return (
      <Modal
        isOpen={isStartBusinessInterestOpen}
        onRequestClose={() => setIsStartBusinessInterestOpen(false)}
        ariaHideApp={false}
        className="modals"
      >
        <div className="bg-danger p-5 mb-4" closeButton>
          <div className="text-center text-white">Would you like to start a business?</div>
        </div>
        <div>
          <button
            type="button"
            className="btn btn-light text-dark "
            onClick={() => setIsBusinessInterestConfirmationOpen(true)}
          >
            Yes
          </button>

          <div className="ml-5 py-3 px-4 text-center btn btn-light">
            <Link
              exact="true"
              to={{
                pathname: '/sponsor-plan',
              }}
              className="text-dark"
            >
              No
            </Link>
          </div>
        </div>
      </Modal>
    );
  };

  const BusinessInterestConfirmation = () => {
    return (
      <Modal
        isOpen={isBusinessInterestConfirmationOpen}
        onRequestClose={() => setIsBusinessInterestConfirmationOpen(false)}
        ariaHideApp={false}
        className="modals"
      >
        <div className="row">
          <div className="col-sm-12 p-5 bg-danger" closeButton></div>

          <div col-sm-12 bg-white>
            <p className="text-dark">
              Thank you for your interest in starting a business. One of our team members will
              contact you to provide assistance.
            </p>
            <div
              className="p-2 text-center"
              onClick={() => {
                setIsStartBusinessInterestOpen(false);
                setIsBusinessInterestConfirmationOpen(false);
              }}
            >
              <Link
                exact="true"
                to={{
                  pathname: '/',
                }}
                className="btn btn-danger text-white"
              >
                Explore Tasttlig
              </Link>
            </div>

            <p className="text-danger text center pt-4 pb-2">
              Need help? connect with us at hello@tasttlig.com
            </p>
          </div>
        </div>
      </Modal>
    );
  };

  const vendorWaitApplication = () => {
    if (userRole.includes('BUSINESS_MEMBER_PENDING')) {
      toast('Waiting for admin approval for business member!', {
        type: 'error',
        autoClose: 2000,
      });
    } else if (userRole.includes('VENDOR_PENDING')) {
      toast(`Waiting for admin approval for vendor!`, {
        type: 'error',
        autoClose: 2000,
      });
    } else {
      toast('Need to apply for a business passport!', {
        type: 'error',
        autoClose: 2000,
      });
    }
  };

  const truncateDescription = (description) => {
    if (description.length > 130) {
      if (description[description.length - 1] === '.') {
        description = description.substr(0, 130) + '..';
      } else {
        description = description.substr(0, 130) + '...';
      }
    }
    return description;
  };

  const onChangeFoodSample = function (event) {
    //takes the file and reads the file from buffer of array
    if (foodSamplePreference.includes(event.target.value)) {
      console.log('error');
    } else {
      const matchedFestival = foodSamples.find((value) => {
        return value.food_sample_id === Number(event.target.value);
      });
      setFoodSamplePreference(foodSamplePreference.concat(matchedFestival));
      //setFestivalPreferenceName(festivalPreferenceName.concat(matchedFestival.festival_name))
    }
  };

  const renderVendorItems = function () {
    //takes the file and reads the file from buffer of array
    if(userRole && !userRole.includes('ADMIN') && userRole.includes('HOST')) {
      return <div className="host-sponsor-vend">
                <div className="host-button" onClick={handleHostFestival} disabled={submitAuthDisabled}>
                  Host
                </div>
            </div>
    } 
    if(userRole && !userRole.includes('ADMIN') && !userRole.includes('HOST') && userRole.includes('VENDOR') && (props.hostFestivalList && props.hostFestivalList.includes(appContext.state.user.id ))) {
      return <div className="host-sponsor-vend">
                <div className="host-button" onClick={handleHostFestival} disabled={submitAuthDisabled}>
                  Vend
                </div>
            </div>
    }
    if(userRole && !userRole.includes('ADMIN') && !userRole.includes('HOST') && (props.hostFestivalList && !props.hostFestivalList.includes(appContext.state.user.id )) && userRole.includes('SPONSOR') && (props.festivalSponsorList && props.festivalSponsorList.includes(appContext.state.user.id ))) {
      return   <div className="host-sponsor-vend">
                  <div className="host-button" onClick={handleHostFestival} disabled={submitAuthDisabled}>
                    Sponsor
                  </div>
              </div>
    }
    return  <div className="host-sponsor-vend">
                <div className="host-button" >
                  {''}
                </div>
            </div>
  };

  //function to render over the selected options from dropdown and print them
  const getOptions = function (samples) {
    return samples.map(
      (sample) =>
        sample && (
          <button
            className="festival-card-festival-attend-btn"
            key={sample.food_sample_id}
            onClick={(event) => {
              setFoodSamplePreference((stateFestivals) => {
                return stateFestivals.filter((stateFestival) => {
                  if (stateFestival && stateFestival.food_sample_id === sample.food_sample_id) {
                    return false;
                  } else {
                    return true;
                  }
                });
              });
            }}
          >
            {sample.title}
          </button>
        ),
    );
  };

  // Render Festival Card
  return (
     <div className="col-md-6 festival-card">
    <LazyLoad once>
        <div className="d-flex flex-column h-100 card-box">
        {BusinessPassportStart()}
        {StartBusinessInterest()}
        {BusinessInterestConfirmation()}

        <Modal
          isOpen={passportDetailsOpened}
          onRequestClose={closeModal('passport-details')}
          ariaHideApp={false}
          className={
            toggleState === 1 ? 'passport-details-modal' : 'passport-details-modal view-tab-open'
          }
        >
          <div className="container host-form-modal">
            <div className="row modal-content-top">
              <div className="col modal-heading">
                {toggleState === 2
                  ? 'Your Products, Services and Experiences'
                  : 'Create a Product, Service, or Experience'}
              </div>
              <div className="modal-actions-right">
                <div
                  onClick={toggleState === 1 ? () => toggleTab(2) : () => toggleTab(1)}
                  className="create-view-button"
                >
                  {toggleState === 1 ? (
                    <span>
                      <i className="fas fa-eye"></i> View
                    </span>
                  ) : (
                    <span>
                      <i className="fas fa-plus"></i> New
                    </span>
                  )}
                </div>
                <div onClick={closeModal('passport-details')} className="close-modal-button">
                  <i className="fas fa-times-circle"></i>
                </div>
              </div>
            </div>

            {toggleState && toggleState === 1 && userRole ? (
              <CreateSampleSaleDonation festivalId={props.festivalId} />
            ) : null}
            {toggleState && toggleState === 2 && userRole ? (
              <ViewProductsServicesExperiences
                festivalId={props.festivalId}
                festivalName={props.title}
              />
            ) : null}
          </div>
        </Modal>
        <Link exact="true" className="festival-card-links">
          {props.images.length > 1 ? (
            <div>
              <ImageSlider images={props.images} isCard={true} />
            </div>
          ) : (
            <img
              src={props.images[0]}
              alt={props.title}
              onLoad={() => setLoad(true)}
              className={load ? 'festival-card-image' : 'loading-image'}
            />
          )}
           {upcomingDays > 0 ? (
            <div className="festival-card-date-upcoming">
              <span className="festival-card-date-upcoming-text">Upcoming</span>
              <span className="festival-card-date-upcoming-days">{`in ${upcomingDays} day${
                upcomingDays > 1 ? 's' : ''
              }`}</span>
            </div>
          ) : upcomingDays === 0 ? (
            <div className="festival-card-date-live">
              <img src={liveFestival} alt="Live Festival" />
            </div>
          ) : endingDays > 0 ? (
            <div className="festival-card-date-ending">
              <span className="festival-card-date-ending-text">Ending</span>
              <span className="festival-card-date-ending-days">{`in ${endingDays} day${
                endingDays > 1 ? 's' : ''
              }`}</span>
            </div>
          ) : null}

        <div className="main-festival-info">
          <div className="festival-card-text">
            <div className="festival-card-title">{props.title}</div>
              <div className="festival-card-details">
                <div className="festival-location">
                  <i className="fas fa-map-marker-alt"></i> <strong>{props.city}</strong>
                </div>
                <div className="festival-date">
                  <i className="fas fa-calendar"></i>{' '}
                  <strong>{`${formatDate(props.startDate)} - ${formatDate(props.endDate)}`}</strong>
                </div>
                <div className="festival-time">
                  <i className="fas fa-clock"></i>
                  <strong>
                    {` ${moment(startDateTime).format('h:mm A')} - ${moment(endDateTime).format(
                      'h:mm A',
                    )}`}{' '}
                    EST
                  </strong>
                  </div>
                  <div className="festival-name">{props.title}</div>
                    <div className="festival-price">
                    <strong className="price-stronger">{`$${props.price}`} </strong>/ person
                  </div>
            </div>
            <div className="festival-card-details">{props.description}</div>
          </div>
        </div>
        </Link>
            {/* {!appContext.state.user.id ? (
                <div className = "align-vend-button">
                    <div>
                    <Link exact="true" to="/complete-profile/festival-vendor">
                      <button
                        type="button"
                        className="main-navbar-item nav-host-btn"
                        onClick={ () => localStorage.setItem('VendFromSignOut', 'true')}
                      >
                          View Vendor Packages
                      </button>
                    </Link>
                    </div>
                       
                    {vendingConditions ? (
                      <Modal
                        className="terms-conditions-modal"
                        isOpen={vendingConditions}
                        onRequestClose={() => setVendingConditions(false)}
                      >
                        <div className="terms-conditions-background-image" />
                        <VendingConditions
                          user_id={props.user_id}
                          viewVendingConditionsState={vendingConditions}
                          changeVendingConditions={(e) => setVendingConditions(e)}
                          changeBusinessConditions={(e) => 
                            {setVendingConditions(false);
                            setBusinessConditions(e)}}
                        />
                      </Modal>
                    ) : null}
                    {businessConditions ? (
                          <Modal
                            className="terms-conditions-modal"
                            isOpen={businessConditions}
                            onRequestClose={() => setBusinessConditions(false)}
                          >
                            <div className="terms-conditions-background-image" />
                            <BusinessConditions
                              user_id={props.user_id}
                              viewHostingConditionsState={businessConditions}
                              changeBusinessConditions={(e) => setBusinessConditions(e)}
                            />
                          </Modal>
                        ) : null}
                </div>
                    ) :
           renderVendorItems()}
            <div className="edit-festival-button">
              {userRole && userRole.includes('ADMIN') && !props.festivalPayment ? (
                <div>
                  <div onClick={handleEditFestival}>
                    <button disabled={submitAuthDisabled} className="festivals-button-primary">
                      Edit Festival
                    </button>
                  </div>
                </div>
              ) : (
                <p></p>
          )}
        </div>
        <div className="purchase-ticket-button">
          {!appContext.state.user.id ? (
             <div className="festival-card-footer">
             <Link exact="true" className="nav-vend-btn" to="/login">
              Join
            </Link>
           </div>
          
          ) : Number(props.price) === 0 ? (
          <div className="festival-card-footer">
            <Link exact="true" className="nav-vend-btn" to={`/festival/${props.festivalId}`}>
            Free
        </Link>
        </div>
        ) : (
      <>
       {props.guestFestivalList && props.guestFestivalList.includes(appContext.state.user.id) ? (
       <div className="festival-card-footer">
       <Link exact="true" className="nav-vend-btn" to={`/festival/${props.festivalId}`}>
            Attending
        </Link>
        </div>
        ) : (<div className="festival-card-footer">
          <Link className="nav-vend-btn" exact="true" to={`/payment/festival/${props.festivalId}`}>
            Buy Now
          </Link>
        </div>)  }
      </>
        )}
        </div> */}
        {/* <div className="festival-card-footer">
            <div className="row">
              <div className="col-md-8 festival-card-category">{props.type}</div>
            </div>
          </div> */}
        </div>
      </LazyLoad>
    </div>
  );
};

export default ExpiredFestivalCard;
