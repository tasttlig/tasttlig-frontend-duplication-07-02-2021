// Libraries
import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import axios from 'axios';
import LazyLoad from 'react-lazyload';
import Modal from 'react-modal';
import Flag from 'react-world-flags';
import { toast } from 'react-toastify';

// Components
import ImageSlider from '../../../ImageSlider/ImageSlider';
import {
  formatDate,
  formatMilitaryToStandardTime,
  formattedTimeToDateTime,
} from '../../../Functions/Functions';

// Styling
import '../../../Passport-Old/PassportCard/PassportCard.scss';
import 'react-toastify/dist/ReactToastify.css';

toast.configure();

const FoodSamplesCreatedCard = (props) => {
  // Set initial state
  const [load, setLoad] = useState(false);
  const [passportDetailsOpened, setPassportDetailsOpened] = useState(false);
  const [submitAuthDisabled, setSubmitAuthDisabled] = useState(false);
  const history = useHistory();

  // Set dietary restrictions
  let dietaryRestrictions = [];
  props.foodSampleProps.is_vegetarian && dietaryRestrictions.push('vegetarian');
  props.foodSampleProps.is_vegan && dietaryRestrictions.push('vegan');
  props.foodSampleProps.is_gluten_free && dietaryRestrictions.push('glutenFree');
  props.foodSampleProps.is_halal && dietaryRestrictions.push('halal');

  // Set days available
  let daysAvailable = [];
  props.foodSampleProps.is_available_on_monday && daysAvailable.push('available_on_monday');
  props.foodSampleProps.is_available_on_tuesday && daysAvailable.push('available_on_tuesday');
  props.foodSampleProps.is_available_on_wednesday && daysAvailable.push('available_on_wednesday');
  props.foodSampleProps.is_available_on_thursday && daysAvailable.push('available_on_thursday');
  props.foodSampleProps.is_available_on_friday && daysAvailable.push('available_on_friday');
  props.foodSampleProps.is_available_on_saturday && daysAvailable.push('available_on_saturday');
  props.foodSampleProps.is_available_on_sunday && daysAvailable.push('available_on_sunday');

  // Split address into address lines 1 and 2
  let addressParts = props.foodSampleProps.address.split(', ');
  let addressLine1 = addressParts[0];
  let addressLine2 = addressParts[1];

  // Open modal type helper function
  const openModal = (modalType) => () => {
    if (modalType === 'passport-details') {
      setPassportDetailsOpened(true);
    }
  };

  // Close modal type helper function
  const closeModal = (modalType) => () => {
    if (modalType === 'passport-details') {
      setPassportDetailsOpened(false);
    }
  };

  // Edit archived food sample helper function
  const handleEditFoodSample = () => {
    history.push({
      pathname: '/edit-food-sample',
      state: {
        foodSampleId: props.foodSampleProps.food_sample_id,
        images: props.foodSampleProps.image_urls,
        title: props.foodSampleProps.title,
        nationality_id: props.foodSampleProps.nationality_id,
        start_date: props.foodSampleProps.start_date,
        end_date: props.foodSampleProps.end_date,
        start_time: formattedTimeToDateTime(props.foodSampleProps.start_time),
        end_time: formattedTimeToDateTime(props.foodSampleProps.end_time),
        sample_size: props.foodSampleProps.sample_size,
        quantity: props.foodSampleProps.quantity,
        dietaryRestrictions,
        daysAvailable,
        spice_level: props.foodSampleProps.spice_level,
        addressLine1,
        addressLine2,
        city: props.foodSampleProps.city,
        provinceTerritory: props.foodSampleProps.state,
        postal_code: props.foodSampleProps.postal_code,
        description: props.foodSampleProps.description,
      },
    });
  };

  // Activate archived food sample helper function
  const handleSubmitActivateFoodSample = async (event) => {
    event.preventDefault();

    try {
      const url = `/food-sample/update/${props.foodSampleProps.food_sample_id}`;
      const acc_token = localStorage.getItem('access_token');
      const headers = { Authorization: `Bearer ${acc_token}` };
      const data = {
        food_sample_update_data: {
          status: 'ACTIVE',
        },
      };
      const response = await axios({ method: 'PUT', url, headers, data });

      if (response && response.data && response.data.success) {
        setTimeout(() => {
          window.location.reload();
        }, 2000);

        toast(`Success! Thank you for activating ${props.foodSampleProps.title}!`, {
          type: 'success',
          autoClose: 2000,
        });

        setSubmitAuthDisabled(true);
      }
    } catch (error) {
      toast('Error! Something went wrong!', {
        type: 'error',
        autoClose: 2000,
      });
    }
  };

  // Render Archived Food Samples Card
  return (
    <div className="col-xl-4 col-md-6 passport-card">
      <LazyLoad once>
        {/* Archived Food Sample Details Modal */}
        <Modal
          isOpen={passportDetailsOpened}
          onRequestClose={closeModal('passport-details')}
          ariaHideApp={false}
          className="passport-details-modal"
        >
          <div className="text-right">
            <button
              aria-label={`Close ${props.foodSampleProps.title} Details Modal`}
              onClick={closeModal('passport-details')}
              className="fas fa-times fa-2x close-modal"
            ></button>
          </div>

          <div className="text-left">
            <div className="passport-item-title">{props.foodSampleProps.title}</div>
            <div className="mb-3 passport-image__full-width">
              <ImageSlider images={props.foodSampleProps.image_urls} />
            </div>

            <div>
              <div className="mb-3">
                <div className="passport-details-sub-title">Address</div>
                <div>
                  {`${props.foodSampleProps.address}, ${props.foodSampleProps.city}, ${props.foodSampleProps.state} ${props.foodSampleProps.postal_code}`}
                </div>
              </div>
              <div className="mb-3">
                <div className="passport-details-sub-title">Date</div>
                <div>{`${formatDate(props.foodSampleProps.start_date)} to ${formatDate(
                  props.foodSampleProps.end_date,
                )}`}</div>
              </div>
              <div className="mb-3">
                <div className="passport-details-sub-title">Time</div>
                <div>{`${formatMilitaryToStandardTime(
                  props.foodSampleProps.start_time,
                )} to ${formatMilitaryToStandardTime(props.foodSampleProps.end_time)}`}</div>
              </div>
              <div className="mb-3">
                <div className="passport-details-sub-title">Sample Size</div>
                <div>{`${props.foodSampleProps.sample_size}`}</div>
              </div>
              <div className="mb-3">
                <div className="passport-details-sub-title">Days Available</div>
                {props.foodSampleProps.is_available_on_monday && <div>Monday</div>}
                {props.foodSampleProps.is_available_on_tuesday && <div>Tuesday</div>}
                {props.foodSampleProps.is_available_on_wednesday && <div>Wednesday</div>}
                {props.foodSampleProps.is_available_on_thursday && <div>Thursday</div>}
                {props.foodSampleProps.is_available_on_friday && <div>Friday</div>}
                {props.foodSampleProps.is_available_on_saturday && <div>Saturday</div>}
                {props.foodSampleProps.is_available_on_sunday && <div>Sunday</div>}
              </div>
              <div className="passport-details-description">
                {props.foodSampleProps.description}
              </div>
            </div>
          </div>

          <div className="close-modal-bottom-content">
            <span onClick={closeModal('passport-details')} className="close-modal-bottom">
              Close
            </span>
          </div>
        </Modal>

        <div className="d-flex flex-column h-100 card-box">
          <div
            onClick={openModal('passport-details')}
            className="passport-card-content mb-0 text-center"
          >
            <img
              src={props.foodSampleProps.image_urls[0]}
              alt={props.foodSampleProps.title}
              onLoad={() => setLoad(true)}
              className={load ? 'passport-image' : 'loading-image'}
            />
            <div className="passport-card-flag-container">
              <Flag code={props.foodSampleProps.alpha_2_code} className="passport-card-flag" />
              <div className="passport-card-flag-country">{props.foodSampleProps.nationality}</div>
            </div>
            <div className="passport-card-text">
              {props.foodSampleProps.status === 'ACTIVE' ? (
                <a className="fas fa-circle text-success" />
              ) : (
                <a className="fas fa-circle text-danger" />
              )}
              &nbsp;{props.foodSampleProps.title}
            </div>
          </div>
          <div className="mt-auto">
            <div>
              <div onClick={handleEditFoodSample}>
                <button disabled={submitAuthDisabled} className="edit-btn">
                  Edit
                </button>
              </div>
            </div>
            <div>
              <form onSubmit={handleSubmitActivateFoodSample} noValidate>
                <button type="submit" disabled={submitAuthDisabled} className="call-to-action-btn">
                  Activate
                </button>
              </form>
            </div>
          </div>
        </div>
      </LazyLoad>
    </div>
  );
};

export default FoodSamplesCreatedCard;
