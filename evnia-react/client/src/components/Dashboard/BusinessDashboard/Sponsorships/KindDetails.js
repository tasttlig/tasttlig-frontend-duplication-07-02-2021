// Libraries
import axios from 'axios';
import React, { useEffect, useContext, useState } from 'react';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router-dom';
import { AppContext } from '../../../../ContextProvider/AppProvider';
import moment from 'moment';
import { Checkbox, Form } from '../../../EasyForm';
import DatePicker from 'react-datepicker';
import useInfiniteScroll from 'react-infinite-scroll-hook';

// Styling
import './KindDetails.scss';
import 'react-datepicker/dist/react-datepicker.css';

const KindDetails = (props) => {
  // Set initial state
  const [festivalItems, setFestivalItems] = useState([]);
  const [selectedNationalities, setSelectedNationalities] = useState([]);
  const [startDate, setFilterStartDate] = useState('');
  const [startTime, setFilterStartTime] = useState('');
  const [cityLocation, setCityLocation] = useState('');
  const [filterRadius, setFilterRadius] = useState(25000000);
  const [hasNextPage, setHasNextPage] = useState(true);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(0);
  const [selectedItem, setSelectedItem] = useState('');
  const [selectMultipleFestivals, setSelectMultipleFestivals] = useState(false);
  const [hostFestivalList, setHostFestivalList] = useState([]);
  const [festivalSponsorList, setFestivalSponsorList] = useState([]);

  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const userId = appContext.state.user.id;
  //const { latitude, longitude, geoError } = usePosition();

  // Render festival cards helper function
  const renderFestivalCards = (arr) => {
    return arr.map((item, index) => (
      <tr>
        <th scope="row">
          {' '}
          <input type="checkbox" name={item.festival_name} value={item.festival_id}></input>
        </th>
        <td>{item.festival_id}</td>
        {/* <td>
            {item.festival_business_sponsor_id && item.festival_business_sponsor_id.includes(userId)
                ?item.festival_price
                :"-"}
            </td> */}
        <td>{item.festival_name}</td>
        <td>
          {item.festival_start_date} at {item.festival_start_time}
        </td>
        <td>
          {item.festival_end_date} at {item.festival_end_time}
        </td>
        <td>{item.festival_price}</td>
        <td>
          {item.festival_business_sponsor_id &&
          item.festival_business_sponsor_id.includes(userId) ? (
            <span className="bg-primary rounded-pill text-white py-1 px-2">sponsored</span>
          ) : (
            <span className="bg-danger rounded-pill text-white py-1 px-2">Not sponsoring</span>
          )}
        </td>
      </tr>
    ));
  };

  // Toggle nationality helper function
  const toggleNationality = (nationalities) => {
    let nationalities_list = [];

    nationalities.map((nationality) => {
      nationalities_list.push(nationality.value.nationality);
    });

    setSelectedNationalities(nationalities_list);
  };

  // Load next set of festivals helper functions
  const loadNextPage = async (page) => {
    const url = '/festival/all?';

    return axios({
      method: 'GET',
      url,
      params: {
        keyword: '',
        page: page + 1,
        nationalities: selectedNationalities,
        radius: filterRadius,
        startDate,
        startTime,
        cityLocation,
      },
    });
  };

  const handleLoadMore = (page = currentPage, festivals = festivalItems) => {
    setLoading(true);
    loadNextPage(page).then((newPage) => {
      setLoading(false);

      if (!newPage) {
        return false;
      }

      const pagination = newPage.data.details.pagination;

      if (page < pagination.lastPage) {
        setCurrentPage(page + 1);
      }

      setHasNextPage(currentPage < pagination.lastPage);
      setFestivalItems(festivals.concat(newPage.data.details.data));
    });
  };

  const infiniteRef = useInfiniteScroll({
    loading,
    hasNextPage,
    onLoadMore: handleLoadMore,
    scrollContainer: 'window',
    threshold: 50,
  });

  // Mount Festivals page
  useEffect(() => {
    window.scrollTo(0, 0);
    handleLoadMore();
  }, []);

  useEffect(() => {
    handleLoadMore(0, []);
  }, [selectedNationalities, startDate, startTime, cityLocation, filterRadius]);

  // Render empty festival page
  const FestivalEmpty = () => <strong className="">You have not sponsored yet</strong>;

  const renderFestivalItems = function () {
    return festivalItems.map((item) => {
      return item.festival_name;
    });
  };

  return (
    <form>
      <table className="table table-striped pt-5 mt-5">
        <thead>
          <tr>
            <th scope="col"></th>
            <th scope="col">Festival ID</th>
            {/* <th scope="col">Product Sponsored</th> */}
            <th scope="col">Festival Name</th>
            <th scope="col">Festival Start Date &amp; Time</th>
            <th scope="col">Festival End Date &amp; Time</th>
            <th scope="col">Festival Ticket</th>
            <th scope="col">Sponsor Status</th>
          </tr>
        </thead>
        <tbody>
          {festivalItems.length !== 0 ? renderFestivalCards(festivalItems) : <FestivalEmpty />}
        </tbody>
      </table>
    </form>
  );
};

export default KindDetails;
