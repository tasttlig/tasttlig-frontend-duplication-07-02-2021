// Libraries
import axios from 'axios';
import React, { useEffect, useContext, useState } from 'react';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router-dom';
import { AppContext } from '../../../../ContextProvider/AppProvider';
import moment from 'moment';
import { Checkbox, Form, CheckboxGroup } from '../../../EasyForm';
import DatePicker from 'react-datepicker';
import useInfiniteScroll from 'react-infinite-scroll-hook';
import EditProduct from './EditProduct';

// Styling
import './ProductDetails.scss';
import 'react-datepicker/dist/react-datepicker.css';

const ProductDetails = (props) => {
  // Set initial state
  const [products, setProducts] = useState([]);
  const [focus, setFocus] = useState('view');
  const [productToEdit, setProductToEdit] = useState({});
  const [selectedProduct, setSelectedProduct] = useState(false);
  const [addToFestivalSelected, setAddToFestivalSelected] = useState(false);
  const [festivalList, setFestivalList] = useState([]);
  let data = { product_festivals_id: [] };
  let count = 0;
  let ids = [];

  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const userId = appContext.state.user.id;
  const userRole = appContext.state.user.role;
  //const { latitude, longitude, geoError } = usePosition();

  // const fetchRowDetails = (e) => {
  //   const item = e.target.getAttribute('data-item');
  //   console.log('This is a test info for product >>>> :', item);
  // };

  // Render festival cards helper function
  const renderProducts = (arr) => {
    return arr.map((item, index) => (
      <tr
      // key={index}
      // data-item={item}
      // onClick={fetchRowDetails}
      /* onClick={() => {
          setSelectedProduct(item);
        }} */
      >
        <th scope="row">
          <input
            onClick={() => {
              addProductId(item.product_id);
            }}
            type="checkbox"
            name="product_id"
            value={item.product_id}
          ></input>
        </th>
        <td
          className="fa fa-edit"
          onClick={() => {
            setFocus('edit');
            setProductToEdit(item);
          }}
        ></td>
        <td data-title="product_id">{item.product_id}</td>
        <td data-title="product_name">{item.product_name}</td>
        <td data-title="product_price">{item.product_price}</td>
        <td data-title="country">{item.country}</td>
        <td data-title="product_size">{item.product_size}</td>
        <td data-title="product_quantity">{item.product_quantity}</td>
      </tr>
    ));
  };

  const addProductId = (id) => {
    const stringValue = '' + id;

    if (ids.length === 0) {
      ids[count] = stringValue;
      count++;
    } else if (ids.length > 0 && ids.includes(stringValue)) {
      ids = ids.filter((id) => id !== stringValue);
      count--;
    } else {
      ids[count] = stringValue;
      count++;
    }
    localStorage.setItem('productIds', JSON.stringify(ids));
  };
  const getSelectedProducts = () => {
    return ids.map((i) => Number(i));
  };

  const loadProducts = async () => {
    const url = `/products/details/${userId}`;

    const response = await axios({ method: 'GET', url });
    if (response.data.success) {
      setProducts(response.data.details);
    }
  };

  const removeItem = async () => {
    if (ids.length < 1) {
      toast('Select an item to remove!', {
        type: 'info',
        autoClose: 2000,
      });
      return;
    }
    const url = '/product/delete';
    data.product_id = ids;
    try {
      const response = await axios({ method: 'DELETE', url, data });
      if (response && response.data && response.data.success) {
        setTimeout(() => {
          loadProducts();
          setFocus('time-out');
        }, 2000);
        setTimeout(() => {
          setFocus('view');
        }, 4000);
        toast('Product removed successfully!', {
          type: 'success',
          autoClose: 2000,
        });
      }
    } catch (error) {
      toast('Error! Something went wrong!', {
        type: 'error',
        autoClose: 2000,
      });
    }
  };

  // Fetch festival list helper function
  const fetchFestivalList = async () => {
    const response = await axios({
      method: 'GET',
      url: '/festival-list',
    });
    setFestivalList(response.data.festival_list);
  };

  const addProductToFestival = async (data) => {
    if (data.product_festivals_id && data.product_festivals_id.length < 1) {
      toast('Please select a festival!', {
        type: 'info',
        autoClose: 2000,
      });
      return;
    }
    ids = JSON.parse(localStorage.getItem('productIds'));
    data.product_id = JSON.parse(localStorage.getItem('productIds'));
    let url = '/product/update';
    try {
      const response = await axios({
        method: 'PUT',
        url,
        data,
      });
      if (response && response.data && response.data.success && userRole) {
        setAddToFestivalSelected(false);
        ids = JSON.parse(localStorage.getItem('productIds'));
        setTimeout(() => {
          loadProducts();
          setFocus('time-out');
        }, 2000);
        setTimeout(() => {
          setFocus('view');
        }, 4000);
        toast('Product added to selected festival', {
          type: 'success',
          autoClose: 2000,
        });
      }
    } catch (error) {
      toast('Error! Something went wrong!', {
        type: 'error',
        autoClose: 2000,
      });
    }
  };

  // Mount Festivals page
  useEffect(() => {
    window.scrollTo(0, 0);
    loadProducts();
    fetchFestivalList();
  }, []);

  // Render empty festival page
  const ProductEmpty = () => <strong className="">You have not added products yet</strong>;

  return (
    <div>
      {focus === 'view' ? (
        <form className="bg-white">
          <div className="p-4">
            <span className="h2 border-bottom border-danger">My Products</span>
          </div>
          <div>
            <>
              <span
                onClick={removeItem}
                className="btn btn-danger text-center rounded-pill py-2 px-3 mr-2"
              >
                Remove
              </span>
              <span
                onClick={() => {
                  data.product_id = setAddToFestivalSelected(true);
                }}
                className="btn btn-danger text-center rounded-pill py-2 px-3"
              >
                Add to Festival
              </span>
              {addToFestivalSelected && (
                <Form data={data} onSubmit={addProductToFestival}>
                  <div className="col">
                    <CheckboxGroup
                      name="product_festivals_id"
                      options={festivalList.map((fest) => [fest.festival_name, fest.festival_id])}
                    />
                  </div>
                  <div className="col">
                    <button type="submit" className="remove-action-button">
                      Save
                    </button>
                  </div>
                </Form>
              )}
            </>
          </div>
          <table className="table table-striped pt-5 mt-5">
            <thead>
              <tr>
                <th scope="col"></th>
                <th scope="col"></th>
                <th scope="col">Product ID</th>
                <th scope="col">Product Name</th>
                <th scope="col">Product Price</th>
                <th scope="col">Made in</th>
                <th scope="col">Size</th>
                <th scope="col">Quantity</th>
              </tr>
            </thead>
            <tbody>{products.length !== 0 ? renderProducts(products) : <ProductEmpty />}</tbody>
          </table>
        </form>
      ) : focus === 'time-out' ? null : (
        <div>
          <div className="d-flex align-items-end flex-column">
            <span
              className=" btn btn-dark fas fa-times-circle mr-5 mt-3 p-3 rounded-circle"
              onClick={() => {
                setFocus('view');
              }}
            >
              <i class="fas fa-times"></i>
            </span>
          </div>
          <div className="row">
            <EditProduct product={productToEdit} />
          </div>
        </div>
      )}
    </div>
  );
};

export default ProductDetails;
