import axios from 'axios';
import React, { useEffect, useContext, useState, Fragment } from 'react';
import { AppContext } from '../../../../ContextProvider/AppProvider';
import DatePicker from 'react-datepicker';
import Modal from 'react-modal';
import { useForm, Controller } from 'react-hook-form';
import { DateInput, Form, MultiImageInput } from '../../../EasyForm';
import Radio from '../../../Shared/Radio';
import makeAnimated from 'react-select/animated';
import { AuthModalContext } from '../../../../ContextProvider/AuthModalProvider';
import { toast } from 'react-toastify';


// Styling
import './AddPromotion.scss';
import 'react-datepicker/dist/react-datepicker.css';


const animatedComponents = makeAnimated();




const AddPromotion = (props) => {
 
  const [modalOpen, setModalOpen] = useState(true);
  const [promotionName, setPromotionName] = useState('');
  const [promotionDescription, setPromotionDescription] = useState('');
  const [promotionDiscountPercentage, setPromotionDiscountPercentage] = useState('');
  const [promotionDiscountPrice, setPromotionDiscountPrice] = useState('');
  const [promotionStartDate, setPromotionStartDate] = useState('');
  const [promotionEndDate, setPromotionEndDate] = useState('');
  const [promotionStatus, setPromotionStatus] = useState('');
  const [discountRadioSelected, setDiscountRadioSelected] = useState('');
  const [percentageSelected, setPercentageSelected] = useState(null);
  const [priceSelected, setPriceSelected] =useState(null);
  const appContext = useContext(AppContext);
  const [addNewPromotion, setAddNewPromotion] = useState(true);

  // end form select component options

  const { register, handleSubmit, control, setValue, errors } = useForm({});

  const onSubmitPromotionForm = async (val) => {
    
     var result;
     console.log("submit from val", val)
     const url = `/promotions/add`;
     const data = {
      promotion_name: promotionName,
      promotion_description: promotionDescription,
      promotion_discount_percentage: percentageSelected,
      promotion_discount_price: priceSelected,
      promotion_start_date: promotionStartDate,
      promotion_end_date: promotionEndDate,
      user_id:appContext.state.user.id
     }
     
      var response;
      // console.log(data)
      try {
        response = await axios({ method: 'POST', url, data });
         console.log("response", response)
        setTimeout(3000);
        if (response && response.data && response.data.success) {
          toast('Success! Your new promotion details have been saved!', {
            type: 'success',
            autoClose: 4000,
          });
          setTimeout(() => {
            window.location.href = '/dashboard';
          }, 2000);
        } else {
          toast(response.data.message, {
            type: 'error',
            autoClose: 4000,
          });
        }
      } catch (error) {
        toast('Error! Something went wrong!'.concat(error.message), {
          type: 'error',
          autoClose: 4000,
        });
      }
    
  };
    // console.log(errors)
    // console.log("add promo props",props)
    const handleClose = () => {
      props.changeAddNewPromotion(false)
    }
    return(
            //  <Fragment>
                 <Form onSubmit={handleSubmit(onSubmitPromotionForm)}>
                  <div className="container host-form-modal">
                    <div className="row modal-content-top">
                        <div className="modal-heading1">
                            Add a new Promotional Offer
                        </div>
                    </div>
                  </div>
                  <div className="form-row">
                    <div className="col">
                        <input
                        name="promotionName"
                        type="text"
                        className={errors.promotionName? 'form-control invalid-input' : 'form-control'}
                        placeholder="Promotion Name"
                        onChange={(e)=>setPromotionName(e.target.value)}
                        value={promotionName}
                        ref={register({ required: true })}
                        />
                    </div>
                   </div>
                   <div className="form-row">
                    <div className="col">
                        <input
                        name="promotionDescription"
                        value={promotionDescription}
                        type="text"
                        className='form-control'
                        placeholder="Promotion Description"
                        ref={register({ required: true })}
                        onChange={(e)=>setPromotionDescription(e.target.value)}
                        />
                    </div>
                   </div>
                   <h6>Do you want to add a discount percentage on the price or flat discount on the price?</h6>
                   <div className="col radio-product-type">
                    <Radio
                        value="discount_percentage"
                        selected={discountRadioSelected}
                        text="Discount Percentage"
                        onChange={setDiscountRadioSelected}
                    />
                    <Radio
                        value="discount_price"
                        selected={discountRadioSelected}
                        text="Discount Price"
                        onChange={setDiscountRadioSelected}
                    />
                    </div>
                <div className="form-row">
                    <div className="col">
                        <input
                        name="percentageSelected"
                        disabled={discountRadioSelected!=="discount_percentage"}
                        type="text"
                        className={errors.percentageSelected && discountRadioSelected==="discount_percentage" ? 'form-control invalid-input' : 'form-control'}
                        placeholder="Discount Percentage"
                        value={percentageSelected}
                        onChange={(e)=>setPercentageSelected(e.target.value)}
                        ref={register({ required: true })}
                        />
                    </div>
                    <div className="col">                        
                        <input
                        name="priceSelected"
                        type="text"
                        disabled={discountRadioSelected!=="discount_price"}
                        className={errors.priceSelected && discountRadioSelected==="discount_price" ? 'form-control invalid-input' : 'form-control'}
                        placeholder='Promotion Price'
                        value={priceSelected}
                        onChange={(e)=>setPriceSelected(e.target.value)}
                        ref={register({ required: true })}
                        />
                    </div>
                </div>
                <div className="form-row">
                    {/* <div className="col">
                        <Controller
                        control={control}
                        name="start_date"
                        value={promotionStartDate}
                        ref={register({ required: true })}
                        render={(props) => (
                        <DatePicker 
                          className="form-control"
                          dateFormat="yyyy/MM/dd, h:mm:ss a"
                          placeholderText="Start Date"
                          minDate={new Date()}
                          showTimeSelect
                          onChange={(value)=>setPromotionStartDate(value)}
                          selected={props.value}

                          
                         />
                      )}
                  />  */}
                  <div className="form-row">
                    <div className="col">

                    <Controller
                      control={control}
                      name="test"
                      render={({onChange, value, ref}) => (
                        <DatePicker
                          className='form-control'
                          placeholderText="Enter Promotion Start Date"
                          dateFormat="yyyy/MM/dd, h:mm:ss a"
                          minDate={new Date()}
                          showTimeSelect
                          selected={promotionStartDate}
                          onChange={(date) => setPromotionStartDate(date)}
                          // ref={register({ required: true })}
                          // onChange={onChange}
                          // selected={value}
                          inputRef={ref}
                        />
                        )}
                      />
                    </div>

                    </div>

                    
                    <div className="col">
                    <Controller
                      control={control}
                      name="test"
                      render={({onChange, value, ref}) => (
                        <DatePicker
                          className='form-control'
                          placeholderText="Enter Promotion End Date"
                          dateFormat="yyyy/MM/dd, h:mm:ss a"
                          minDate={new Date()}
                          showTimeSelect
                          selected={promotionEndDate}
                          onChange={(date) => setPromotionEndDate(date)}
                          // ref={register({ required: true })}
                          // onChange={onChange}
                          // selected={value}
                          inputRef={ref}
                        />
                        )}
                      />
                        {/* <DatePicker
                          className='form-control'
                          placeholderText="Enter Promotion End Date"
                          dateFormat="yyyy/MM/dd, h:mm:ss a"
                          minDate={new Date()}
                          showTimeSelect
                          selected={promotionEndDate}
                          onChange={(date) => setPromotionEndDate(date)}
                          ref={register({ required: true })}
                        /> */}

                        {/* <Controller
                        control={control}
                        name="end_date"
                        value={promotionEndDate}
                        ref={register({ required: true })}
                        render={(props) => (
                            <DatePicker
                            className="form-control"
                            dateFormat="yyyy/MM/dd, h:mm:ss a"
                            placeholderText="End Date"
                            minDate={new Date()}
                            showTimeSelect
                            
                            onChange={(value)=>setPromotionEndDate(value)}
                            selected={props.value}
                            />
                          )}
                        /> */}
                    </div>
                </div>
                <div className="row save-changes-btn">
              <button
                type="submit"
                className="profile-button-secondary create-another-button"
                onClick={onSubmitPromotionForm}
              >
                Add Promotion
              </button>
              <button onClick={() => handleClose(false)} className="profile-button-primary">
                Close
              </button>
              </div>
              </Form>
              
            // </Fragment>

    );
};

export default AddPromotion;
