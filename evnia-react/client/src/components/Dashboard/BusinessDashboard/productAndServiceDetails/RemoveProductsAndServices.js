// Libraries
import axios from 'axios';
import React, { useEffect, useContext, useState } from 'react';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router-dom';
import { AppContext } from '../../../../ContextProvider/AppProvider';
import moment from 'moment';
import { Checkbox, Form } from '../../../EasyForm';
import DatePicker from 'react-datepicker';
import useInfiniteScroll from 'react-infinite-scroll-hook';
import RemoveProduct from './RemoveProduct';
import RemoveService from './RemoveService';
import SponsorPackageKindCard from '../../../Profile/Sponsor/SponsorPackageKindCard';

// Styling
import './RemoveProductsAndServices.scss';
import 'react-datepicker/dist/react-datepicker.css';

const RemoveProductsAndServices = (props) => {
  // Set initial state
  const [focus, setFocus] = useState('products');

  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const userId = appContext.state.user.id;

  // Mount Festivals page
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  // Render empty festival page
  const Empty = () => <strong className="">There is no content at the moment</strong>;

  return (
    <div className="bg-white">
      <div className="h4 p-3">Select your products or services</div>
      <div className="row pr-4 my-4 mr-2 d-flex align-items-end flex-column">
        <span
          onClick={() => {
            setFocus('add');
          }}
          className="btn btn-danger text-center rounded-pill py-2 px-3"
        >
          {focus === 'services' ? 'Add service' : 'Add product'}
        </span>
      </div>
      <div className="row my-2">
        <span
          onClick={() => {
            setFocus('products');
          }}
          className="mx-auto py-2 px-3 mr-1 btn btn-dark rounded-pill"
        >
          My Products
        </span>
        <span
          onClick={() => {
            setFocus('food-samples');
          }}
          className="mx-auto py-2 px-3 mr-1 btn btn-dark rounded-pill"
        >
          My Food Samples
        </span>
        <span
          onClick={() => {
            setFocus('services');
          }}
          className="mx-auto py-2 px-3 btn btn-dark rounded-pill"
        >
          My Services
        </span>
      </div>
      {focus === 'products' ? (
        <RemoveProduct />
      ) : focus === 'services' ? (
        <RemoveService />
      ) : focus === 'add' ? (
        <div className="row">
          <SponsorPackageKindCard />
        </div>
      ) : (
        <div></div>
      )}
    </div>
  );
};

export default RemoveProductsAndServices;
