// Libraries
import axios from 'axios';
import React, { useEffect, useContext, useState } from 'react';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router-dom';
import { AppContext } from '../../../ContextProvider/AppProvider';
import moment from 'moment';
import { Checkbox, Form } from '../../EasyForm';
import DatePicker from 'react-datepicker';
import useInfiniteScroll from 'react-infinite-scroll-hook';
import EditProduct from './productAndServiceDetails/EditProduct';

// Styling
import './GuestOrders.scss';
import 'react-datepicker/dist/react-datepicker.css';

const GuestOrders = (props) => {
  // Set initial state
  const [orders, setOrders] = useState([]);

  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const userId = appContext.state.user.id;
  //const { latitude, longitude, geoError } = usePosition();

  // Render festival cards helper function
  const renderOrders = (arr) => {
    return arr.map((item) => (
      <tr>
        <th scope="row">
          {' '}
          <input type="checkbox" name={item.product_id} value={item.product_name}></input>
        </th>
        <td>{item.product_type}</td>
        <td>{item.product_id}</td>
        <td>{item.product_name}</td>
        <td>{item.product_quantity}</td>
        <td>{item.product_price}</td>
      </tr>
    ));
  };

  const loadOrders = async () => {
    const url = '/user/orders';

    const response = await axios({ method: 'GET', url });
    console.log('ords', response);
    if (response.data.success) {
      //setOrders(response.data.details);
    }
  };

  // Mount Festivals page
  useEffect(() => {
    window.scrollTo(0, 0);
    loadOrders();
  }, []);

  // Render empty festival page
  const OrderEmpty = () => <strong className="">You have no orders</strong>;

  return (
    <form className="bg-white">
      <div className="p-4">
        <span className="h2 border-bottom border-danger">My Orders</span>
      </div>
      <table className="table table-striped pt-5 mt-5">
        <thead>
          <tr>
            <th scope="col"></th>
            <th scope="col">Date & time</th>
            <th scope="col">Order ID</th>
            <th scope="col">Order Name</th>
            <th scope="col">Quantity</th>
            <th scope="col">Price</th>
          </tr>
        </thead>
        <tbody>{orders.length !== 0 ? renderOrders(orders) : <OrderEmpty />}</tbody>
      </table>
    </form>
  );
};

export default GuestOrders;
