// Libraries
import React, { useState, useContext, useEffect } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import LazyLoad from 'react-lazyload';
import moment from 'moment';
import { toast } from 'react-toastify';
import { AppContext } from '../../../../ContextProvider/AppProvider';

// Components
import ImageSlider from '../../../ImageSlider/ImageSlider';
import { formatDate } from '../../../Functions/Functions';

// Styling
import 'react-toastify/dist/ReactToastify.css';
import liveFestival from '../../../../assets/images/live.png';

toast.configure();

const HostRedeemDetails = (props) => {
  console.log('props from host redem:', props);
  // Calculate the number of days between start/end and current date
  const d1 = new Date(props.startDate);
  const d2 = new Date(props.endDate);
  const [orderList, setOrderList] = useState([]);
  const [, setLoading] = useState(false);
  const d3 = new Date();
  const millisecondsInDay = 86400000;
  const upcomingDays = Math.floor((d1 - d3) / millisecondsInDay) + 1;
  const endingDays = Math.ceil((d2 - d3) / millisecondsInDay) + 1;
  const [detailsOpened, toggleDetailsOpened] = useState(false);

  //calculate remaining quantity of sample food
  const remainingQuantity = Number(props.totalQuantity) -Number(props.numberofClaims);
  const totalRedeemsQuantity = Number(props.numberofClaims) + Number(props.totalRedeemQuantity);
  console.log(props);
  console.log('total quanity',props.totalQuantity);
  console.log('total claims',props.numberofClaims);
  console.log('total redeem quantity: ', totalRedeemsQuantity);
  console.log('total quantity', remainingQuantity);

  // Set initial state
  const [confirmed, setConfirmed] = useState(false);
  const [error, setError] = useState(null);

  const handleRedeemingSubmit = async (event) => {
    event.preventDefault();

    // const token = localStorage.getItem("access_token");
    try {
      const response = await axios.post('/all-products-claim/confirm/', {
        claim_viewable_id: props.productId,
        quantity: remainingQuantity,
        redeemed_total_quantity: totalRedeemsQuantity,
      });

      if (
        response &&
        // responseFoodSamples &&
        response.data &&
        // responseFoodSamples.data &&
        response.data.success
        // responseFoodSamples.data.success
      ) {
        setConfirmed(response.data.success);
        setTimeout(() => {
          window.location.reload();
        }, 2000);

        toast(`Success! Thank you for Redeeming!`, {
          type: 'success',
          autoClose: 2000,
        });

        this.setState({ submitAuthDisabled: true });
      }
    } catch (error) {
      setError(error);
      // toast("Error! Something went wrong!", {
      //   type: "error",
      //   autoClose: 2000,
      // });
    }
  };
    console.log(remainingQuantity,'remainingQuantity');
  const handleServiceRedeemingSubmit = async (event) => {
    event.preventDefault();

    // const token = localStorage.getItem("access_token");
    try {
      const response = await axios.post('/all-services-claim/confirm/', {
        claim_viewable_id: props.productId,
        quantity: remainingQuantity,
        redeemed_total_quantity: totalRedeemsQuantity,
      });

      if (
        response &&
        // responseFoodSamples &&
        response.data &&
        // responseFoodSamples.data &&
        response.data.success
        // responseFoodSamples.data.success
      ) {
        setConfirmed(response.data.success);
        setTimeout(() => {
          window.location.reload();
        }, 2000);

        toast(`Success! Thank you for Redeeming!`, {
          type: 'success',
          autoClose: 2000,
        });

        this.setState({ submitAuthDisabled: true });
      }
    } catch (error) {
      setError(error);
      // toast("Error! Something went wrong!", {
      //   type: "error",
      //   autoClose: 2000,
      // });
    }
  };

  const handleExperienceRedeemingSubmit = async (event) => {
    event.preventDefault();

    // const token = localStorage.getItem("access_token");
    try {
      const response = await axios.post('/all-experience-claim/confirm/', {
        claim_viewable_id: props.productId,
        quantity: remainingQuantity,
        redeemed_total_quantity: totalRedeemsQuantity,
      });

      if (
        response &&
        // responseFoodSamples &&
        response.data &&
        // responseFoodSamples.data &&
        response.data.success
        // responseFoodSamples.data.success
      ) {
        setConfirmed(response.data.success);
        setTimeout(() => {
          window.location.reload();
        }, 2000);

        toast(`Success! Thank you for Redeeming!`, {
          type: 'success',
          autoClose: 2000,
        });

        this.setState({ submitAuthDisabled: true });
      }
    } catch (error) {
      setError(error);
      // toast("Error! Something went wrong!", {
      //   type: "error",
      //   autoClose: 2000,
      // });
    }
  };
  // Render claimed items Row
  return (
    <tr>
      <td scope="row">{props.productId}</td>

      <td>{props.claimantFullName}</td>
      <td>{props.festivalName}</td>
      <td>{props.foodName}</td>
      <td>{formatDate(props.dateTimeofClaim)}</td>
      <td>{props.numberofClaims}</td>
      <td>
        {props.claimStatus === 'Redeemed' ? (
          <button type="button" className="btn btn-primary view-Redeemed">
            {props.claimStatus}
          </button>
        ) : props.itemType && props.itemType === "Product" ? (
          <button
            type="button"
            className="btn btn-primary view-claimed"
            onClick={handleRedeemingSubmit}
          >
            {props.claimStatus}
          </button>
        ) :  props.itemType && props.itemType === "Experience" ? (
          <button
            type="button"
            className="btn btn-primary view-claimed"
            onClick={handleExperienceRedeemingSubmit}
          >
            {props.claimStatus}
          </button>
        ) : props.itemType && props.itemType === "Service" ? (
          <button
            type="button"
            className="btn btn-primary view-claimed"
            onClick={handleServiceRedeemingSubmit}
          >
            {props.claimStatus}
          </button>
        ) : null
        
        }
      </td>
    </tr>
  );
};

export default HostRedeemDetails;
