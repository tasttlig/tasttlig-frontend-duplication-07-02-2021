// Libraries
import React, { useState, useEffect, useContext } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import useInfiniteScroll from 'react-infinite-scroll-hook';
import { AppContext } from '../../../ContextProvider/AppProvider';
import moment, { now } from 'moment';
import { formatDate } from '../../Functions/Functions';
import autoTable from 'jspdf-autotable';
import dropDownArrow from '../../../assets/images/shapes/6.png';

// Components
import Nav from '../../Navbar/Nav';
import Footer from '../../Footer/Footer';
import ClaimsDetails from './ClaimsDetails/ClaimsDetails';
// Styling
import './MemberTicketClaimed.scss';
import 'react-datepicker/dist/react-datepicker.css';
import GoTop from '../../Shared/GoTop';

const MemberTicketClaimed = (props) => {
  // Set initial state
  const [passportItems, setPassportItems] = useState([]);
  const [selectedNationalities, setSelectedNationalities] = useState([]);
  const [startDate, setFilterStartDate] = useState('');
  const [startTime, setFilterStartTime] = useState('');
  const [cityLocation, setCityLocation] = useState('');
  const [filterRadius, setFilterRadius] = useState(25000000);
  const [hasNextPage, setHasNextPage] = useState(true);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(0);
  const [ticketItems, setTicketItems] = useState([]);
  const [reservationList, setReservationList] = useState([]);

  // To use the JWT credentials
  const appContext = useContext(AppContext);

  // let expiredFestivals = [];

  // // check if festival has passed, if it did add it to expiredFestivals
  // passportItems.forEach((row) => {
  //   if (moment(row.festival_end_date).isBefore()) {
  //     expiredFestivals.push(row);
  //   }
  // });

  // Render past festival table rows helper
  const renderClaimsRows = (arr) => {
    return arr.map((row, index) => (
      <ClaimsDetails
        key={index}
        foodSampleId={row.claim_viewable_id}
        claimantFullName={row.first_name + ' ' + row.last_name}
        dateTimeofClaim={row.reserved_on}
        numberofClaims={row.claimed_quantity}
        foodName={row.title}
        festivalName={row.foodsample_festival_name}
        claimStatus={row.current_status}
        history={props.history}
      />
    ));
  };

  // Fetch user festival reservations helper function
  const fetchUserFestivalReservations = async () => {
    try {
      const url = '/food-sample-claim/user/reservations';
      const acc_token = localStorage.getItem('access_token');
      const headers = { Authorization: `Bearer ${acc_token}` };

      return await axios({ method: 'GET', url, headers });
    } catch (error) {
      return error.response;
    }
  };

  // Mount Festival Reservations page
  useEffect(() => {
    window.scrollTo(0, 0);

    setLoading(true);
    fetchUserFestivalReservations().then(({ data }) => {
      setReservationList(data.details);
    });
    setLoading(false);
  }, []);

  // Render empty page
  const Empty = () => {
    return <strong className="no-food-samples-found">No Claims found.</strong>;
  };
  console.log('Reservaation list:', reservationList);

  // Render claims component
  return (
    <div>
      <Nav />
      <div className="dashboard">
        <Link exact="true" to="/dashboard">
          <h4 className="ticket-sub-image">My Tickets</h4>
        </Link>
        <span className="ticket-table-head">
          <h2 className="ticket-sub-title">My Claimed Samples</h2>
          <Link exact="true" to="/">
            <h4 className="ticket-sub-image">
              <i className="fas fa-running ticket-icon"></i>
              Go to Tasttlig
            </h4>
          </Link>
        </span>
        {/* <span className="table-caption">
          Your specialized preferences from past festivals and from
          registration.
        </span> */}
      </div>

      <div className="ticket-container">
        <div className="row ticket-row">
          <table className="table table-striped ticket-table">
            <thead>
              <tr>
                <th scope="col">Food Claim Id</th>
                <th scope="col">Claimant Full name</th>
                <th scope="col">Festival Name</th>
                <th scope="col">Name of Item</th>
                <th scope="col">Date of Claim</th>
                <th scope="col">Quantity of claims</th>
                <th scope="col">Claim Status</th>
              </tr>
            </thead>
            <tbody>
              {reservationList.length !== 0 ? (
                renderClaimsRows(reservationList)
              ) : (
                <tr>
                  <td>You haven't claimed any sample yet!</td>
                </tr>
              )}
            </tbody>
          </table>
        </div>
      </div>
      {/* <Footer/>
    <GoTop scrollStepInPx="50" delayInMs="16.66" /> */}
    </div>
  );
};

export default MemberTicketClaimed;
