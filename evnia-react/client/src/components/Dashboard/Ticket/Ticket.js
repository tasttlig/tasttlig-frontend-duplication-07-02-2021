// Libraries
import React, { useState, useEffect, useContext } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import useInfiniteScroll from 'react-infinite-scroll-hook';
import { AppContext } from '../../../ContextProvider/AppProvider';
import moment, { now } from 'moment';
import { formatDate } from '../../Functions/Functions';
import autoTable from 'jspdf-autotable';
import dropDownArrow from '../../../assets/images/shapes/6.png';

// Components
import Nav from '../../Navbar/Nav';
import Footer from '../../Footer/Footer';
import TicketDetails from './TicketDetails/TicketDetails';
// Styling
import './Ticket.scss';
import 'react-datepicker/dist/react-datepicker.css';
import GoTop from '../../Shared/GoTop';

const Ticket = (props) => {
  // Set initial state
  const [passportItems, setPassportItems] = useState([]);
  const [selectedNationalities, setSelectedNationalities] = useState([]);
  const [startDate, setFilterStartDate] = useState('');
  const [startTime, setFilterStartTime] = useState('');
  const [cityLocation, setCityLocation] = useState('');
  const [filterRadius, setFilterRadius] = useState(25000000);
  const [hasNextPage, setHasNextPage] = useState(true);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(0);
  const [ticketItems, setTicketItems] = useState([]);

  // To use the JWT credentials
  const appContext = useContext(AppContext);

  // let expiredFestivals = [];

  // // check if festival has passed, if it did add it to expiredFestivals
  // passportItems.forEach((row) => {
  //   if (moment(row.festival_end_date).isBefore()) {
  //     expiredFestivals.push(row);
  //   }
  // });

  // Render past festival table rows helper
  const renderTicketRows = (arr) => {
    return arr.map((row, index) => (
      <TicketDetails
        key={index}
        ticketId={row.ticket_id}
        ticketBookingId={row.ticket_booking_confirmation_id}
        festivalId={row.festival_id}
        images={row.image_urls}
        title={row.festival_name}
        type={row.festival_type}
        price={row.festival_price}
        city={row.festival_city}
        startDate={row.festival_start_date}
        endDate={row.festival_end_date}
        startTime={row.festival_start_time}
        endTime={row.festival_end_time}
        description={row.festival_description}
        history={props.history}
      />
    ));
  };

  console.log('ticketItems:', ticketItems);
  //To fetch tickets when entered into new page
  const loadNextPage = async (page) => {
    try {
      const response = await axios({
        method: 'GET',
        url: '/ticket/all',
        params: {
          ticket_user_id: appContext.state.user.id,
          page: page + 1,
        },
      });

      return response;
    } catch (error) {
      return error.response;
    }
  };

  const handleLoadMore = (page = currentPage, tickets = ticketItems) => {
    setLoading(true);
    loadNextPage(page).then((newPage) => {
      setLoading(false);

      if (!newPage) {
        return false;
      }

      const pagination = newPage.data.details.pagination;
      if (page < pagination.lastPage) {
        setCurrentPage(page + 1);
      }

      setHasNextPage(currentPage < pagination.lastPage);

      setTicketItems(tickets.concat(newPage.data.details.data));
    });
  };

  const infiniteRef = useInfiniteScroll({
    loading,
    hasNextPage,
    onLoadMore: handleLoadMore,
    scrollContainer: 'window',
    threshold: 50,
  });

  // Mount Festivals page
  useEffect(() => {
    window.scrollTo(0, 0);

    handleLoadMore();
  }, []);

  useEffect(() => {
    handleLoadMore(0, []);
  }, [selectedNationalities, startDate, startTime, cityLocation, filterRadius]);

  // Render Passport component
  return (
    <div>
      <Nav />
      <div className="dashboard">
        <Link exact="true" to="/dashboard/my-claimed-food">
          <h4 className="ticket-sub-image">My Claimed Food Samples</h4>
        </Link>
        <span className="ticket-table-head">
          <h2 className="ticket-sub-title">My Tickets</h2>
          <Link exact="true" to="/">
            <h4 className="ticket-sub-image">
              <i className="fas fa-running ticket-icon"></i>
              Go to Tasttlig
            </h4>
          </Link>
        </span>
        {/* <span className="table-caption">
          Your specialized preferences from past festivals and from
          registration.
        </span> */}
      </div>

      <div className="ticket-container">
        <div className="row ticket-row">
          {/* <h3 className="passport-sub-title">My Tickets</h3> */}
          {/* <button
              type="button"
              id="dropdownMenuButton"
              data-toggle="dropdown"
              aria-label="Navigation Menu"
              aria-haspopup="true"
              aria-expanded="false"
              className="main-navbar-account"
            >
              Filter <img src={dropDownArrow} alt="Arrow" className="dropdown-image" />
            </button> */}
          <table className="table table-striped ticket-table">
            <thead>
              <tr>
                <th scope="col">Ticket ID</th>
                <th scope="col">Festival Name</th>
                <th scope="col">Festival Type</th>
                <th scope="col">Ticket Start Date & Time</th>
                <th scope="col">Ticket End Date & Time</th>
                <th scope="col">Ticket Price</th>
                <th scope="col">Ticket Status</th>
              </tr>
            </thead>
            <tbody>
              {ticketItems.length !== 0 ? (
                renderTicketRows(ticketItems)
              ) : (
                <tr>
                  <td>You don't have any tickets yet!</td>
                </tr>
              )}
            </tbody>
          </table>
        </div>
      </div>
      {/* <Footer/>
    <GoTop scrollStepInPx="50" delayInMs="16.66" /> */}
    </div>
  );
};

export default Ticket;
