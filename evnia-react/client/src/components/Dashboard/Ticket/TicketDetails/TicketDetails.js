// Libraries
import React, { useState, useContext, useEffect } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import LazyLoad from 'react-lazyload';
import moment from 'moment';
import { toast } from 'react-toastify';
import { AppContext } from '../../../../ContextProvider/AppProvider';

// Components
import ImageSlider from '../../../ImageSlider/ImageSlider';
import { formatDate } from '../../../Functions/Functions';

// Styling
import './TicketDetails.scss';
import 'react-toastify/dist/ReactToastify.css';
import liveFestival from '../../../../assets/images/live.png';

toast.configure();

const TicketDetails = (props) => {
  console.log('props from tickets:', props);
  // Calculate the number of days between start/end and current date
  const d1 = new Date(props.startDate);
  const d2 = new Date(props.endDate);
  const [orderList, setOrderList] = useState([]);
  const [, setLoading] = useState(false);
  const d3 = new Date();
  const millisecondsInDay = 86400000;
  const upcomingDays = Math.floor((d1 - d3) / millisecondsInDay) + 1;
  const endingDays = Math.ceil((d2 - d3) / millisecondsInDay) + 1;
  const [detailsOpened, toggleDetailsOpened] = useState(false);
  // Set date and time
  // const startDateTime = moment(
  //   new Date(props.startDate).toISOString().split("T")[0] +
  //     "T" +
  //     props.startTime +
  //     ".000Z"
  // ).add(new Date().getTimezoneOffset(), "m");
  // const endDateTime = moment(
  //   new Date(props.startDate).toISOString().split("T")[0] +
  //     "T" +
  //     props.endTime +
  //     ".000Z"
  // ).add(new Date().getTimezoneOffset(), "m");

  const fetchUserOrders = async () => {
    try {
      const url = '/order/user';
      const acc_token = localStorage.getItem('access_token');
      const headers = { Authorization: `Bearer ${acc_token}` };

      return await axios({ method: 'GET', url, headers });
    } catch (error) {
      return error.response;
    }
  };

  // Mount Your Orders page
  useEffect(() => {
    window.scrollTo(0, 0);

    setLoading(true);
    fetchUserOrders().then(({ data }) => {
      setOrderList(data.details);
    });
    setLoading(false);
  }, []);

  console.log('orderlist', orderList);
  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const userRole = appContext.state.user.role;

  // Render Past Festival Table Row
  return (
    <tr>
      <td scope="row">
        {' '}
        <Link exact="true" to={`/ticket/${props.ticketId}`} className="ticket-card-link">
          {props.ticketBookingId}
        </Link>
      </td>

      <td>
        {' '}
        <Link
          exact="true"
          to={`/festival/${props.festivalId}`}
          onClick={localStorage.setItem('valueFromTicketDashboard', true)}
          className="ticket-card-link"
        >
          {props.title}
        </Link>
      </td>
      <td>{props.type}</td>
      <td>{`${formatDate(props.startDate)} at ${props.startTime}`}</td>
      <td>{`${formatDate(props.endDate)} at ${props.endTime}`}</td>
      <td>${props.price}</td>
      <td>
        {/* <button type="button" className="btn btn-primary view-paid"> */}
        <strong>Paid</strong>
        {/* </button> */}
      </td>
    </tr>
  );
};

export default TicketDetails;
