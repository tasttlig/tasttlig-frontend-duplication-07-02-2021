// Libraries
import React, { useState, useEffect } from 'react';
import axios from 'axios';

// Components
import Nav from '../../Navbar/Nav';
import PassportCard from '../../Passport-Old/PassportCard/PassportCard';

// Styling
import '../Dashboard.scss';

const FestivalReservation = () => {
  // Set initial state
  const [reservationList, setReservationList] = useState([]);
  const [, setLoading] = useState(false);

  // Fetch user festival reservations helper function
  const fetchUserFestivalReservations = async () => {
    try {
      const url = '/food-sample-claim/user/reservations';
      const acc_token = localStorage.getItem('access_token');
      const headers = { Authorization: `Bearer ${acc_token}` };

      return await axios({ method: 'GET', url, headers });
    } catch (error) {
      return error.response;
    }
  };

  // Mount Festival Reservations page
  useEffect(() => {
    window.scrollTo(0, 0);

    setLoading(true);
    fetchUserFestivalReservations().then(({ data }) => {
      setReservationList(data.details);
    });
    setLoading(false);
  }, []);

  // Render empty page
  const Empty = () => {
    return <strong className="no-food-samples-found">No Reservations found.</strong>;
  };

  // Render Festival Reservations page
  return (
    <div>
      <Nav />

      <div className="your-current-food-samples">
        <div className="your-current-food-samples-title">Your Reservations</div>
        <div className="row">
          <div className="col-xl-12 px-0">
            <div className="food-sample-cards">
              <div className="row col-sm-12 px-0">
                <div className="offset-xl-2 col-xl-8 p-0">
                  <div className="row">
                    {reservationList && reservationList.length !== 0 ? (
                      reservationList.map((card) => (
                        <PassportCard
                          business_name={card.business_name}
                          disableButton={true}
                          key={card.food_sample_id}
                          foodSampleId={card.food_sample_id}
                          images={card.image_urls}
                          title={card.title}
                          price={card.price}
                          address={card.address}
                          city={card.city}
                          provinceTerritory={card.state}
                          postalCode={card.postal_code}
                          startDate={card.start_date}
                          endDate={card.end_date}
                          startTime={card.start_time}
                          endTime={card.end_time}
                          description={card.description}
                          sample_size={card.sample_size}
                          is_available_on_monday={card.is_available_on_monday}
                          is_available_on_tuesday={card.is_available_on_tuesday}
                          is_available_on_wednesday={card.is_available_on_wednesday}
                          is_available_on_thursday={card.is_available_on_thursday}
                          is_available_on_friday={card.is_available_on_friday}
                          is_available_on_saturday={card.is_available_on_saturday}
                          is_available_on_sunday={card.is_available_on_sunday}
                          foodSampleOwnerId={card.food_sample_creater_user_id}
                          foodSampleOwnerPicture={card.profile_image_link}
                          isEmailVerified={card.is_email_verified}
                          firstName={card.first_name}
                          lastName={card.last_name}
                          email={card.email}
                          phoneNumber={card.phone_number}
                          facebookLink={card.facebook_link}
                          twitterLink={card.twitter_link}
                          instagramLink={card.instagram_link}
                          youtubeLink={card.youtube_link}
                          linkedinLink={card.linkedin_link}
                          websiteLink={card.website_link}
                          bioText={card.bio_text}
                          nationality={card.nationality}
                          alpha_2_code={card.alpha_2_code}
                          frequency={card.frequency}
                          foodSampleType={card.food_sample_type}
                          quantity={card.quantity}
                          numOfClaims={card.num_of_claims}
                        />
                      ))
                    ) : (
                      <Empty />
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FestivalReservation;
