import React from 'react';

import SearchButton from './SearchButton';

import './ExperienceTypeSearch.scss';

const buttonNames = [
  'Wedding Reception',
  'Anniversary Celebration',
  'Birthday Party',
  'Bachelor Party',
  'Kids Party',
  'Halloween',
  'Christmas Party',
  'Pot Luck',
];

const ExperienceTypeSearch = (props) => (
  <div id="ExperienceTypeSearch">
    {buttonNames.map((buttonName) => (
      <SearchButton
        key={buttonName}
        handleFilterExpByType={props.handleFilterExpByType}
        name={buttonName}
      />
    ))}
  </div>
);

export default ExperienceTypeSearch;
