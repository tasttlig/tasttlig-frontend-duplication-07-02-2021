// Libraries
import React from 'react';

const SearchButton = (props) => (
  <button name={props.name.toLowerCase()} onClick={props.handleFilterExpByType}>
    {props.name}
  </button>
);

export default SearchButton;
