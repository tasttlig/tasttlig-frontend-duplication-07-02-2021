// Libraries
import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import axios from 'axios';
import LazyLoad from 'react-lazyload';
import Modal from 'react-modal';
import Flag from 'react-world-flags';
import { toast } from 'react-toastify';

// Components
import ImageSlider from '../../../ImageSlider/ImageSlider';
import {
  formatDate,
  formatMilitaryToStandardTime,
  formattedTimeToDateTime,
} from '../../../Functions/Functions';

// Styling
import '../../../Experiences/ExperiencesCard/ExperiencesCard.scss';
import 'react-toastify/dist/ReactToastify.css';

toast.configure();

const ExperiencesArchived = (props) => {
  // Set initial state
  const [load, setLoad] = useState(false);
  const [experienceDetailsOpened, setExperienceDetailsOpened] = useState(false);
  const [submitAuthDisabled, setSubmitAuthDisabled] = useState(false);
  const history = useHistory();

  // Split address into address lines 1 and 2
  let addressParts = props.experienceProps.address.split(', ');
  let addressLine1 = addressParts[0];
  let addressLine2 = addressParts[1];

  // Open modal type helper function
  const openModal = (modalType) => () => {
    if (modalType === 'experience-details') {
      setExperienceDetailsOpened(true);
    }
  };

  // Close modal type helper function
  const closeModal = (modalType) => () => {
    if (modalType === 'experience-details') {
      setExperienceDetailsOpened(false);
    }
  };

  // Edit archived experience helper function
  const handleEditExperience = () => {
    history.push({
      pathname: '/edit-experience',
      state: {
        experienceId: props.experienceProps.experience_id,
        images: props.experienceProps.image_urls,
        title: props.experienceProps.title,
        type: props.experienceProps.type,
        nationality_id: props.experienceProps.nationality_id,
        price: props.experienceProps.price,
        style: props.experienceProps.style,
        start_date: props.experienceProps.start_date,
        end_date: props.experienceProps.end_date,
        start_time: formattedTimeToDateTime(props.experienceProps.start_time),
        end_time: formattedTimeToDateTime(props.experienceProps.end_time),
        capacity: props.experienceProps.capacity,
        dress_code: props.experienceProps.dress_code,
        is_restaurant_location: props.experienceProps.is_restaurant_location,
        addressLine1,
        addressLine2,
        city: props.experienceProps.city,
        provinceTerritory: props.experienceProps.state,
        postal_code: props.experienceProps.postal_code,
        food_description: props.experienceProps.food_description,
        game_description: props.experienceProps.game_description,
        entertainment_description: props.experienceProps.entertainment_description,
        feature_description: props.experienceProps.feature_description,
        transport_description: props.experienceProps.transport_description,
        parking_description: props.experienceProps.parking_description,
        accessibility_description: props.experienceProps.accessibility_description,
        environmental_consideration_description:
          props.experienceProps.environmental_consideration_description,
        value_description: props.experienceProps.value_description,
        other_description: props.experienceProps.other_description,
      },
    });
  };

  // Activate archived experience helper function
  const handleSubmitActivateExperience = async (event) => {
    event.preventDefault();

    try {
      const url = `/experience/update/${props.experienceProps.experience_id}`;
      const acc_token = localStorage.getItem('access_token');
      const headers = { Authorization: `Bearer ${acc_token}` };
      const data = {
        experience_update_data: {
          status: 'ACTIVE',
        },
      };
      const response = await axios({ method: 'PUT', url, headers, data });

      if (response && response.data && response.data.success) {
        setTimeout(() => {
          window.location.reload();
        }, 2000);

        toast(`Success! Thank you for activating ${props.experienceProps.title}!`, {
          type: 'success',
          autoClose: 2000,
        });

        setSubmitAuthDisabled(true);
      }
    } catch (error) {
      toast('Error! Something went wrong!', {
        type: 'error',
        autoClose: 2000,
      });
    }
  };

  // Render Your Current Experiences card
  return (
    <div className="col-xl-4 col-md-6 experience-card">
      <LazyLoad once>
        {/* Archived Experience Details Modal */}
        <Modal
          isOpen={experienceDetailsOpened}
          onRequestClose={closeModal('experience-details')}
          ariaHideApp={false}
          className="experience-details-modal"
        >
          <div className="text-right">
            <button
              aria-label={`Close ${props.experienceProps.title} Details Modal`}
              onClick={closeModal('experience-details')}
              className="fas fa-times fa-2x close-modal"
            ></button>
          </div>

          <div className="text-left">
            <div className="experience-details-title">{props.experienceProps.title}</div>
            <div className="mb-3">
              <ImageSlider images={props.experienceProps.image_urls} />
            </div>

            <div>
              <div className="mb-3">
                <div className="experience-details-sub-title">Address</div>
                <div>
                  {`${props.experienceProps.address}, ${props.experienceProps.city}, ${props.experienceProps.state} ${props.experienceProps.postal_code}`}
                </div>
              </div>
              <div className="mb-3">
                <div className="experience-details-sub-title">Date</div>
                <div>{`${formatDate(props.experienceProps.start_date)} to ${formatDate(
                  props.experienceProps.end_date,
                )}`}</div>
              </div>
              <div className="mb-3">
                <div className="experience-details-sub-title">Time</div>
                <div>{`${formatMilitaryToStandardTime(
                  props.experienceProps.start_time,
                )} to ${formatMilitaryToStandardTime(props.experienceProps.end_time)}`}</div>
              </div>
              <div className="mb-3">
                <div className="experience-details-sub-title">Price</div>
                <div>{`$${props.experienceProps.price}`}</div>
              </div>
              {props.experienceProps.dress_code && (
                <div className="mb-3">
                  <div className="experience-details-sub-title">Dress Code</div>
                  <div>{props.experienceProps.dress_code}</div>
                </div>
              )}
              <div className="mb-3">
                <div className="experience-details-sub-title">Capacity</div>
                <div>{props.experienceProps.capacity}</div>
              </div>
              <div className="mb-3">
                <div className="experience-details-sub-title">Experience Type</div>
                <div>{props.experienceProps.style}</div>
              </div>
              <div className="experience-details-description">
                <div className="mb-3">
                  <div className="experience-details-sub-title">
                    What you will be eating and drinking
                  </div>
                  <div>{props.experienceProps.food_description}</div>
                </div>
                <div className="mb-3">
                  <div className="experience-details-sub-title">What games will you be playing</div>
                  <div>{props.experienceProps.game_description}</div>
                </div>
                <div className="mb-3">
                  <div className="experience-details-sub-title">What entertainment is included</div>
                  <div>{props.experienceProps.entertainment_description}</div>
                </div>
                <div className="mb-3">
                  <div className="experience-details-sub-title">Special features</div>
                  <div>{props.experienceProps.feature_description}</div>
                </div>
                <div className="mb-3">
                  <div className="experience-details-sub-title">Transportation</div>
                  <div>{props.experienceProps.transport_description}</div>
                </div>
                <div className="mb-3">
                  <div className="experience-details-sub-title">Parking</div>
                  <div>{props.experienceProps.parking_description}</div>
                </div>
                <div className="mb-3">
                  <div className="experience-details-sub-title">Accessibility</div>
                  <div>{props.experienceProps.accessibility_description}</div>
                </div>
                <div className="mb-3">
                  <div className="experience-details-sub-title">Environmental consideration</div>
                  <div>{props.experienceProps.environmental_consideration_description}</div>
                </div>
                <div className="mb-3">
                  <div className="experience-details-sub-title">Value</div>
                  <div>{props.experienceProps.value_description}</div>
                </div>
                <div className="mb-3">
                  <div className="experience-details-sub-title">Other information</div>
                  <div>{props.experienceProps.other_description}</div>
                </div>
              </div>
            </div>
          </div>

          <div className="close-modal-bottom-content">
            <span onClick={closeModal('experience-details')} className="close-modal-bottom">
              Close
            </span>
          </div>
        </Modal>

        <div className="d-flex flex-column h-100 card-box">
          <div
            onClick={openModal('experience-details')}
            className="experience-card-content mb-0 text-center"
          >
            <img
              src={props.experienceProps.image_urls[0]}
              alt={props.experienceProps.title}
              onLoad={() => setLoad(true)}
              className={load ? 'experience-card-image' : 'loading-image'}
            />
            <div className="passport-card-flag-container">
              <Flag code={props.experienceProps.alpha_2_code} className="passport-card-flag" />
              <div className="passport-card-flag-country">{props.experienceProps.nationality}</div>
            </div>
            <div className="experience-card-text">
              {props.experienceProps.status === 'ACTIVE' ? (
                <a className="fas fa-circle text-success" />
              ) : (
                <a className="fas fa-circle text-danger" />
              )}
              &nbsp;{props.experienceProps.title}
            </div>
          </div>
          <div className="mt-auto">
            <div>
              <div onClick={handleEditExperience}>
                <button disabled={submitAuthDisabled} className="edit-btn">
                  Edit
                </button>
              </div>
            </div>
            <div>
              <form onSubmit={handleSubmitActivateExperience} noValidate>
                <button type="submit" disabled={submitAuthDisabled} className="call-to-action-btn">
                  Activate
                </button>
              </form>
            </div>
          </div>
        </div>
      </LazyLoad>
    </div>
  );
};

export default ExperiencesArchived;
