// Libraries
import React, { useState, useEffect } from 'react';
import axios from 'axios';

// Components
import Nav from '../../Navbar/Nav';
import OrderDetails from './OrderDetails';

// Styling
import '../Dashboard.scss';

const Order = () => {
  // Set initial state
  const [orderList, setOrderList] = useState([]);
  const [, setLoading] = useState(false);

  // Fetch user orders helper function
  const fetchUserOrders = async () => {
    try {
      const url = '/order/user';
      const acc_token = localStorage.getItem('access_token');
      const headers = { Authorization: `Bearer ${acc_token}` };

      return await axios({ method: 'GET', url, headers });
    } catch (error) {
      return error.response;
    }
  };

  // Mount Your Orders page
  useEffect(() => {
    window.scrollTo(0, 0);

    setLoading(true);
    fetchUserOrders().then(({ data }) => {
      setOrderList(data.details);
    });
    setLoading(false);
  }, []);

  // Render empty page
  const Empty = () => {
    return <strong>No orders found.</strong>;
  };

  // Render Your Orders page
  return (
    <div>
      <Nav />

      <div className="your-orders">
        <div className="your-orders-title">Your Experience Orders</div>
        <div className="row">
          <div className="col-xl-12 p-0">
            <div className="order-cards">
              <div className="row col-sm-12 p-0">
                <div className="offset-xl-2 col-xl-8 p-0">
                  <table className="table table-hover table-responsive-sm">
                    <thead className="thead-dark">
                      <tr>
                        <th scope="col" className="your-orders-column-name">
                          Order ID
                        </th>
                        <th scope="col" className="your-orders-column-name">
                          Date
                        </th>
                        <th scope="col" className="your-orders-column-name">
                          Status
                        </th>
                        <th scope="col" className="your-orders-column-name">
                          Total
                        </th>
                        <th scope="col" className="your-orders-column-name">
                          Stamps Collected
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      {orderList.length !== 0 ? (
                        orderList.map((order) => (
                          <OrderDetails order={order} key={order.order_id} />
                        ))
                      ) : (
                        <div className="mt-3">
                          <Empty />
                        </div>
                      )}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Order;
