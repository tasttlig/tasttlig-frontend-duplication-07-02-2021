// Libraries
import React, { useState } from 'react';
import moment from 'moment';

// Components
import { formatDate, formatMilitaryToStandardTime } from '../../Functions/Functions';

// Styling
import '../Dashboard.scss';

const OrderDetails = ({ order }) => {
  // Set initial state
  const [detailsOpened, toggleDetailsOpened] = useState(false);

  const OrderItem = ({ orderItem }) => {
    return (
      <div className="m-3">
        <div className="row">
          <div className="col-md-4 your-orders-item-content">
            <img
              src={orderItem.experience_images[0].image_url}
              alt={orderItem.title}
              className="mini-card-image"
            />
          </div>
          <div className="col-md-8 your-orders-item-content">
            <div>{`Title: ${orderItem.title}`}</div>
            <div>{`Address: ${orderItem.address}, ${orderItem.city}, ${orderItem.state} ${orderItem.postal_code}`}</div>
            <div>{`Date: ${formatDate(orderItem.start_date)}`}</div>
            <div>
              {`Time: ${formatMilitaryToStandardTime(
                orderItem.start_time,
              )} to ${formatMilitaryToStandardTime(orderItem.end_time)}`}
            </div>
            <div>{`Price: $${orderItem.price_before_tax}`}</div>
            <div>{`Quantity: ${orderItem.quantity}`}</div>
          </div>
        </div>
      </div>
    );
  };

  return (
    <>
      <tr
        onClick={() => toggleDetailsOpened(!detailsOpened)}
        data-toggle="collapse"
        data-target={`#detail_${order.order_id}`}
        className="your-orders-item"
      >
        <td className="your-orders-item-text">{`#${order.order_id}`}</td>
        <td className="your-orders-item-text">{moment(order.order_datetime).format('LLL')}</td>
        <td className="your-orders-item-text">{order.status}</td>
        <td className="your-orders-item-text">{`$${order.total_amount_after_tax}`}</td>
        <td className="your-orders-item-text">{order.total_amount_after_tax * 100}</td>
      </tr>
      {detailsOpened && (
        <tr>
          <td colSpan="5" className="p-0 your-orders-item-text">
            <div>
              {order.order_items &&
                order.order_items.length !== 0 &&
                order.order_items.map((orderItem) => (
                  <OrderItem orderItem={orderItem} key={orderItem.order_item_id} />
                ))}
            </div>
          </td>
        </tr>
      )}
    </>
  );
};

export default OrderDetails;
