// Libraries
import React, { Component } from 'react';
import axios from 'axios';

// Components
import ProfileFeedbackCard from './ProfileFeedbackCard/ProfileFeedbackCard';

// Styling
import './ProfileFeedback.scss';

export default class ProfileFeedback extends Component {
  // Profile Feedback constructor
  constructor(props) {
    super(props);

    this.state = {
      feedbackItems: [],
    };
  }

  url = '/recommendation/user';

  access_token = localStorage.getItem('access_token');

  headers = {
    Authorization: `Bearer ${this.access_token}`,
  };

  // Render profile feedback helper function
  renderFeedback = (arr) => {
    return arr.map((card, index) => (
      <ProfileFeedbackCard
        key={index}
        description={card.description}
        createdAt={card.created_at}
        response={card.reply}
      />
    ));
  };

  // Mount profile feedback
  componentDidMount = () => {
    axios({ method: 'GET', url: this.url, headers: this.headers })
      .then((response) => {
        this.setState({
          feedbackItems: [...this.state.feedbackItems, ...response.data.recommendations],
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };

  // Render Profile Feedback modal
  render = () => {
    const { feedbackItems } = this.state;

    return (
      <div>
        <div className="your-feedback">Your Feedback</div>
        {feedbackItems.length === 0 ? (
          <div className="your-feedback-list">None.</div>
        ) : (
          this.renderFeedback(feedbackItems.reverse())
        )}
      </div>
    );
  };
}
