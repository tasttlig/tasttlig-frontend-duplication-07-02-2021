import React, { useState, useEffect, useContext } from 'react';

import './PassportSideCard.scss';

const PassportSideCard = (props) => {
  const { isHost, location, jobTitle, phoneNumber, email, birthdate, profileImage, toggleTab } = props;
  console.log('passport INFO',props);

  return (
    <div className="passport-side-card">
      <div className="passport__profile-pic">
        <img
          src= {profileImage? profileImage :"/img/passport-profile-pic.png"}
          // src= {profileImage}
          className="profile-pic-image"
          alt="Profile Picture"
        />
      </div>
      {/* <div className="passport__user-name"></div> */}
      <div className="passport__user-role">{!isHost ? 'Member' : 'Business, Host'}</div>
      <div className="passport__user-info">
        <span>
          <i className="fas fa-map-marker-alt"></i>
          {location}
        </span>
        <span>
          <i className="fas fa-briefcase"></i>
          {jobTitle}
        </span>
        <span>
          <i className="fas fa-phone"></i>
          {phoneNumber}
        </span>
        <span>
          <i className="fas fa-envelope"></i>
          {email}
        </span>
        <span>
          <i className="fas fa-birthday-cake"></i>
          {birthdate}
        </span>
      </div>
      <button onClick={() => toggleTab(3)} className="passport__edit-button">
        Edit Passport <i className="fas fa-pencil-alt"></i>
      </button>
    </div>
  );
};

export default PassportSideCard;
