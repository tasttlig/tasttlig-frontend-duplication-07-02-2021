// Libraries
import React, { useState, useEffect, useContext, Fragment } from 'react';
import { useForm } from 'react-hook-form';
import { AppContext } from '../../../ContextProvider/AppProvider';
import { formatPhoneNumber } from '../../Functions/Functions';
import axios from 'axios';
import Swal from 'sweetalert2';

// Styling
import './BusinessPassport.scss';

import ApplyForBusinessPassport from '../../ApplyForBusinessPassport/ApplyForBusinessPassport';
import PendingBusinessPassport from '../../ApplyForBusinessPassport/PendingBusinessPassport';

const BusinessPassport = (props) => {
  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const user = appContext.state.user;
  const userRole = user.role;

  const [toggleStateRegistered, setToggleStateRegistered] = useState(user.business_registered);
  const [toggleStateRetail, setToggleStateRetail] = useState(user.retail_business);

  const [state, setState] = useState({
    businessName: user.business_name,
    businessPhoneNumber: user.business_phone_number,
    businessStreetNumber: user.business_street_number,
    businessStreetName: user.business_street_name,
    businessUnit: user.business_unit,
    businessCity: user.business_city,
    businessState: user.business_state,
    businessCountry: user.business_country,
    businessPostalCode: user.business_zip_postal_code,
    businessRegistered: user.business_registered,
    businessRetail: user.retail_business,
    businessType: user.business_type,
    foodBusinessType: user.food_business_type,
    businessRegisteredLocation: user.business_registered_location,
    foodHandlersCertificate: user.food_handlers_certificate,
  });

  const { register, handleSubmit, setValue, errors } = useForm({
    defaultValues: {
      businessName: state.businessName,
      businessPhoneNumber: state.businessPhoneNumber,
      businessStreetNumber: state.businessStreetNumber,
      businessStreetName: state.businessStreetName,
      businessUnit: state.businessUnit,
      businessCity: state.businessCity,
      businessState: state.businessState,
      businessCountry: state.businessCountry,
      businessPostalCode: state.businessPostalCode,
      businessRegistered: state.businessRegistered,
      businessRetail: state.businessRetail,
      businessType: state.businessType,
      foodBusinessType: state.foodBusinessType,
      businessRegisteredLocation: state.businessRegisteredLocation,
      foodHandlersCertificate: state.foodHandlersCertificate,
    },
  });

  const toggleTabRegistered = (index) => {
    setToggleStateRegistered(index);
  };

  const toggleTabRetail = (index) => {
    setToggleStateRetail(index);
  };

  const onSubmit = async (data) => {
    data.businessRegistered = toggleStateRegistered;
    data.businessRetail = toggleStateRetail;
      console.log("business data---", data);
    try {
      const url = `/user/update-business-profile/${user.id}`;

      const response = await axios({
        method: 'PUT',
        url,
        data,
      });

      if (response && response.data && response.data.success) {
        Swal.fire({
          title: 'Changes Saved',
          text: 'Your personal passport has been updated with your new information.',
          icon: 'success',
          confirmButtonColor: '#88171a ',
        });
        window.location.reload();
      }
    } catch (error) {
      Swal.fire({
        title: 'Error Saving Changes',
        text: 'Something went wrong, please try again.',
        icon: 'error',
        confirmButtonColor: '#88171a ',
      });
    }
  };

  return (
    <Fragment>
      {userRole &&
      !userRole.includes('HOST') &&
      !userRole.includes('RESTAURANT') &&
      !userRole.includes('SPONSOR') &&
      !userRole.includes('VENDOR') &&
      !userRole.includes('MEMBER_BUSINESS_BASIC') &&
      !userRole.includes('MEMBER_BUSINESS_INTERMEDIATE') &&
      !userRole.includes('MEMBER_BUSINESS_ADVANCED') &&
      !userRole.includes('MEMBER_BUSINESS_PRO') &&
      !userRole.includes('BUSINESS_MEMBER') &&
      !userRole.includes('BUSINESS_MEMBER_PENDING') &&
      !userRole.includes('HOST_PENDING') &&
      !userRole.includes('RESTAURANT_PENDING') &&
      !userRole.includes('SPONSOR_PENDING') &&
      !userRole.includes('VENDOR_PENDING') &&
      !userRole.includes('HOST_VEND') &&
      !userRole.includes('HOST_VEND_PENDING') &&
      !userRole.includes('HOST_AMBASSADOR') &&
      !userRole.includes('HOST_AMBASSADOR_PENDING') &&
      !userRole.includes('HOST_GUEST') ? (
        <ApplyForBusinessPassport />
      ) : userRole &&
        !userRole.includes('HOST') &&
        !userRole.includes('RESTAURANT') &&
        !userRole.includes('SPONSOR') &&
        !userRole.includes('VENDOR') &&
        !userRole.includes('MEMBER_BUSINESS_BASIC') &&
        !userRole.includes('MEMBER_BUSINESS_INTERMEDIATE') &&
        !userRole.includes('MEMBER_BUSINESS_ADVANCED') &&
        !userRole.includes('MEMBER_BUSINESS_PRO') &&
        !userRole.includes('BUSINESS_MEMBER') &&
        !userRole.includes('HOST_VEND') &&
        !userRole.includes('HOST_AMBASSADOR') &&
        !userRole.includes('HOST_GUEST') ? (
        <PendingBusinessPassport />
      ) : (
        <section className="passport-sections business-passport">
          {errors.businessName ||
          errors.businessPhoneNumber ||
          errors.businessStreetNumber ||
          errors.businessStreetName ||
          errors.businessCity ||
          errors.businessState ||
          errors.businessCountry ||
          errors.businessPostalCode ||
          errors.businessRegistered ||
          errors.businessRetail ||
          errors.businessType ||
          errors.foodBusinessType ||
          errors.businessRegisteredLocation ? (
            <span className="invalid-submit-info">Invalid Input</span>
          ) : null}

          <form className="container" onSubmit={handleSubmit(onSubmit)}>
            <div className="form-row">
              <div className="col">
                <input
                  name="businessName"
                  type="text"
                  className={errors.businessName ? 'form-control invalid-input' : 'form-control'}
                  placeholder="Business Name"
                  ref={register({ required: true })}
                />
              </div>
              <div className="col">
                <input
                  name="businessPhoneNumber"
                  type="tel"
                  className={
                    errors.businessPhoneNumber ? 'form-control invalid-input' : 'form-control'
                  }
                  onChange={(e) =>
                    setValue('businessPhoneNumber', formatPhoneNumber(e.target.value))
                  }
                  placeholder="Business Phone Number"
                  maxLength="14"
                  ref={register({ required: true, minLength: 14, maxLength: 14 })}
                />
              </div>
            </div>
            <div className="form-row">
              <div className="col">
                <input
                  name="businessStreetNumber"
                  type="text"
                  className={
                    errors.businessStreetNumber ? 'form-control invalid-input' : 'form-control'
                  }
                  placeholder="Business Street Number"
                  ref={register({ required: true })}
                />
              </div>
              <div className="col">
                <input
                  name="businessStreetName"
                  type="text"
                  className={
                    errors.businessStreetName ? 'form-control invalid-input' : 'form-control'
                  }
                  placeholder="Business Street Name"
                  ref={register({ required: true })}
                />
              </div>
              <div className="col">
                <input
                  name="businessUnit"
                  type="text"
                  className={errors.businessUnit ? 'form-control invalid-input' : 'form-control'}
                  placeholder="Unit Number"
                  ref={register}
                />
              </div>
            </div>
            <div className="form-row">
              <div className="col">
                <input
                  name="businessCity"
                  type="text"
                  className={errors.businessCity ? 'form-control invalid-input' : 'form-control'}
                  placeholder="City"
                  ref={register({ required: true })}
                />
              </div>
              <div className="col">
                <input
                  name="businessState"
                  type="text"
                  className={errors.businessState ? 'form-control invalid-input' : 'form-control'}
                  placeholder="Province"
                  ref={register({ required: true })}
                />
              </div>
            </div>
            <div className="form-row">
              <div className="col">
                <input
                  name="businessPostalCode"
                  type="text"
                  className={
                    errors.businessPostalCode ? 'form-control invalid-input' : 'form-control'
                  }
                  placeholder="Postal Code"
                  ref={register({ required: true })}
                />
              </div>
              <div className="col">
                <input
                  name="businessCountry"
                  type="text"
                  className={errors.businessCountry ? 'form-control invalid-input' : 'form-control'}
                  placeholder="Country"
                  ref={register({ required: true })}
                />
              </div>
            </div>
            <div className="row business-passport-form-questions">
              <div className="col passport-form-question-and-tab">
                <h1 className="passport-form-subheading">Is your business registered?</h1>
                <div className="tab-row">
                  <div className="tab-switcher">
                    <button
                      onClick={() => toggleTabRegistered(true)}
                      className={toggleStateRegistered === true ? 'tab active-tab' : 'tab'}
                    >
                      Yes
                    </button>
                    <button
                      onClick={() => toggleTabRegistered(false)}
                      className={toggleStateRegistered === false ? 'tab active-tab' : 'tab'}
                    >
                      No
                    </button>
                  </div>
                </div>
              </div>
              <div className="col passport-form-question-and-tab">
                <h1 className="passport-form-subheading">Is your business retail?</h1>

                <div className="tab-row">
                  <div className="tab-switcher">
                    <button
                      onClick={() => toggleTabRetail(true)}
                      className={toggleStateRetail === true ? 'tab active-tab' : 'tab'}
                    >
                      Yes
                    </button>
                    <button
                      onClick={() => toggleTabRetail(false)}
                      className={toggleStateRetail === false ? 'tab active-tab' : 'tab'}
                    >
                      No
                    </button>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col passport-form-question-and-tab">
                <h1 className="passport-form-subheading">What type of business is it?</h1>
                <input
                  name="businessType"
                  type="text"
                  className={errors.businessType ? 'form-control invalid-input' : 'form-control'}
                  placeholder="Type of Business"
                  ref={register({ required: true })}
                />
              </div>
              <div className="col passport-form-question-and-tab">
                <h1 className="passport-form-subheading">What type of food business?</h1>
                <input
                  name="foodBusinessType"
                  type="text"
                  className={
                    errors.foodBusinessType ? 'form-control invalid-input' : 'form-control'
                  }
                  placeholder="Type of Food Business"
                  ref={register({ required: true })}
                />
              </div>
            </div>
            <div className="row">
              <div className="col passport-form-question-and-tab">
                <h1 className="passport-form-subheading">
                  Which city, province, or state is your business registered in?
                </h1>
                <input
                  name="businessRegisteredLocation"
                  type="text"
                  className={
                    errors.businessRegisteredLocation
                      ? 'form-control invalid-input'
                      : 'form-control'
                  }
                  placeholder="City, Province, Country"
                  ref={register({ required: true })}
                />
              </div>
            </div>
            <div className="row">
              <div className="col passport-form-question-and-tab">
                <h1 className="passport-form-subheading">
                  Please upload your Food Handlers Certificate.
                </h1>
                <button className="profile-button-secondary">Upload File</button>
              </div>
            </div>
            <div className="row save-changes-btn">
              <button className="profile-button-primary">Save Changes</button>
            </div>
          </form>
        </section>
      )}
    </Fragment>
  );
};

export default BusinessPassport;
