//styles
import './SponsorPlan.scss';

// Libraries
import React, { useState, useContext, useRef, useEffect } from 'react';
import { AppContext } from '../../../ContextProvider/AppProvider';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { toast } from 'react-toastify';
import { useHistory } from 'react-router-dom';
import CreateSampleSaleDonation from '../../CreateSampleSaleDonation/CreateSampleSaleDonation';
import {
  Checkbox,
  CheckboxGroup,
  DateInput,
  Form,
  Input,
  MultiImageInput,
  PreFillInput,
  PreFillSelect,
  Select,
  Textarea,
  UserSelector,
} from './../../EasyForm';
import useInfiniteScroll from 'react-infinite-scroll-hook';

// Components
import Nav from '../../Navbar/Nav';
import Footer from '../../Home/BannerFooter/BannerFooter';
import GoTop from '../../Shared/GoTop';
import SponsorPackageCashCard from './SponsorPackageCashCard';
import SponsorPackageKindCard from './SponsorPackageKindCard';

toast.configure();

const SponsorPlan = (props) => {
  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const userRole = appContext.state.user.role;
  // Check that user is signed in to access user specific pages
  //if (!appContext.state.signedInStatus) window.location.href = "/login";
  // Get sponsor package details

  const history = useHistory();

  const [plan, setPlan] = useState('cash');
  const [festivalList, setFestivalList] = useState([]);
  const [nationalities, setNationalities] = useState([]);

  //cash card
  const [festivalItems, setFestivalItems] = useState([]);
  const [selectedFestivals, setSelectedFestivals] = useState([]);
  //let selectedFestivalIds = selectedFestivals.map(i=>Number(i));
  // Load next set of festivals helper functions
  const loadFestivals = async () => {
    const url = '/festival/allFestival';

    try {
      const response = await axios({
        method: 'GET',
        url,
      });

      return response;
    } catch (error) {
      return error.response;
    }
  };

  function sum(total, num) {
    return total + num;
  }

  const priceTotal = () => {
    return price.map((i) => Number(i)).reduce(sum);
  };
  const getSelectedFestivals = () => {
    return fest.map((i) => Number(i));
  };

  /* const pay= async (event)=>{
    event.preventDefault();

    if(count != 0){
    await history.push({
        pathname: `/payment/package/S_C1`,
        itemCount: count,
      });

    toast(`Total Price : $${priceTotal()}`, {
        type: "info",
        autoClose: 3000,
      });
    }
    else{
        toast(`No festival selected`, {
            type: "info",
            autoClose: 2000,
          });
    }
 
} */

  let count = 0;
  let fest = [];
  let price = [];

  const addFestival = (e) => {
    //e.preventDefault();
    // selectedFestivals.map(i=>Number(i)) convert to number before POST to backend
    const stringValue = '' + e.target.value;
    const priceVal = '' + e.target.name;
    /* fest[count] = stringValue;
    count++; */

    if (fest.length === 0) {
      fest[count] = stringValue;
      price[count] = priceVal;
      count++;
    } else if (fest.length > 0 && fest.includes(stringValue)) {
      fest = fest.filter((id) => id !== stringValue);
      price = price.filter((id) => id !== priceVal);
      count--;
    } else {
      fest[count] = stringValue;
      price[count] = priceVal;
      count++;
    }

    /* setSelectedFestivals([stringValue]); */

    /* if (selectedFestivals.length < 1) {
      setSelectedFestivals([stringValue]);
    } 
    else if (
      selectedFestivals.length > 0 &&
      selectedFestivals.includes(stringValue)
    ) {
      setSelectedFestivals(
        [selectedFestivals.filter((id) => id !== stringValue)]
      );
    } 
    else {
      setSelectedFestivals(...selectedFestivals, [stringValue]);
    } */
  };

  // Fetch nationalities helper function
  const fetchNationalities = async () => {
    const response = await axios({
      method: 'GET',
      url: '/nationalities',
    });

    setNationalities(response.data.nationalities);
  };

  // Fetch festival list helper function
  const fetchFestivalList = async () => {
    const response = await axios({
      method: 'GET',
      url: '/festival-list',
    });
    setFestivalList(response.data.festival_list);
  };

  useEffect(() => {
    window.scrollTo(0, 0);

    loadFestivals().then((response) => {
      setFestivalItems(response.data.festival_list);
    });
    fetchNationalities();
    fetchFestivalList();
  }, []);

  const handleBecomeSponsor = async () => {
    const url = '/become-in-kind-sponsor';
    const data = { is_sponsor: true };
    const response = await axios({ method: 'POST', url, data });
    if (response && response.data && response.data.success) {
      await history.push({
        pathname: '/sponsor-in-kind-confirmation',
      });
    } else {
      toast(`Something went wrong`, {
        type: 'error',
        autoClose: 2000,
      });
    }
  };

  /* const renderSponsorPackageCash = (arr) => {
    return arr.map((item, index) => (
      <SponsorPackageCashCard
        key={item.subscription_id}
        date_of_expiry={item.date_of_expiry}
        description={item.description}
        price={item.price}
        status={item.status}
        subscription_type={item.subscription_type}
        comission_to_pay_percentage={item.comission_to_pay_percentage}
        subscription_code={item.subscription_code}
        subscription_name={item.subscription_name}
        subscription_id={item.subscription_id}
        validity_in_months={item.validity_in_months}
        festivalId={props.location.festivalId}
        title={props.location.title}
      />
    ));
  }; */
  const onChangeValue = (e) => {
    setPlan(e.target.value);
  };

  return (
    <div className="row">
      <Nav />
      <div className="sponsor-cash-plan-image col-md-4"></div>
      <div className="col-md-6 text-center pb-5 sponsor-package-content">
        <h4 className="text-center mb-5">Sponsor Plan</h4>
        <form className="mb-3">
          <input
            className="mr-1"
            type="radio"
            value="cash"
            name="package"
            defaultChecked
            onClick={onChangeValue}
          />
          <span className="mr-4">In Cash</span>
          <span className="ml-4">In Kind</span>
          <input
            className="ml-1"
            type="radio"
            value="kind"
            name="package"
            onClick={onChangeValue}
          />
        </form>
        <div>
          {/* {(plan === 'kind' && userRole && userRole.includes('SPONSOR')) ||
          (plan === 'kind' && userRole && userRole.includes('SPONSOR_PENDING')) ? ( */}
            {(plan === 'kind' && userRole && userRole.includes('BUSINESS_MEMBER')) ? (
            // <SponsorPackageKindCard
            //   festivalId={props.location.festivalId}
            //   title={props.location.title}
            // />

            <CreateSampleSaleDonation sponsorType={true} />
          ) : plan === 'kind' &&
            userRole &&
            // !userRole.includes('SPONSOR') &&
            !userRole.includes('SPONSOR_PENDING') &&
            !userRole.includes('VENDOR') &&
            !userRole.includes('VENDOR_PENDING') &&
            !userRole.includes('BUSINESS_MEMBER') &&
            !userRole.includes('BUSINESS_MEMBER_PENDING') &&
            !userRole.includes('HOST') ? (
            <div className="px-5 btn btn-danger rounded-pill">
              <Link
                exact="true"
                to={{
                  pathname: '/business-passport',
                }}
                className="text-white "
              >
                Register your business
              </Link>
            </div> 
          // ) : 
          // (plan === 'kind'&& userRole && userRole.includes('VENDOR')) ||
          // userRole.includes('HOST') && !userRole.includes('SPONSOR') ? (
          // <div
          //   onClick={handleBecomeSponsor}
          //   className="px-5 btn btn-danger rounded-pill text-white"
          // >
          //   Become a Sponsor
          // </div>
        )  : plan === 'cash' ? (
            <div className="row">
              <div className="col-md-12 p-0">
                <div className="landing-page-cards">
                  {festivalItems.map((card, index) => (
                    <SponsorPackageCashCard
                      key={card.festival_id}
                      festivalId={card.festival_id}
                      images={card.image_urls}
                      title={card.festival_name}
                      type={card.festival_type}
                      price={card.festival_price}
                      city={card.festival_city}
                      startDate={card.festival_start_date}
                      endDate={card.festival_end_date}
                      startTime={card.festival_start_time}
                      endTime={card.festival_end_time}
                      description={card.festival_description}
                      //selectMultipleFestivals={selectMultipleFestivals}
                      hostFestivalList={card.festival_restaurant_host_id}
                      festivalSponsorList={card.festival_business_sponsor_id}
                      history={props.history}
                      addFestival={addFestival}
                    />
                  ))}
                </div>
              </div>
            </div>
          ) : null}
        </div>
        {/* <Link
          exact="true"
          to={{
            pathname: "/sponsor-products-and-services",
            festivalId: props.location.festivalId,
            title: props.location.title,
          }}
          className="continue-btn"
        >
          Continue
        </Link> 
        //{`/festival/${props.festivalId}`} 
        */}
      </div>
      <GoTop scrollStepInPx="50" delayInMs="16.66" />
    </div>
  );
};

export default SponsorPlan;
