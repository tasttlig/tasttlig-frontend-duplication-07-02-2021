// Libraries
import React, { useState, useContext } from 'react';
import { Link } from 'react-router-dom';
import { AppContext } from '../../../ContextProvider/AppProvider';
import { useHistory } from 'react-router-dom';

const SponsorshipPackageCard = (props) => {
  const history = useHistory();
  const appContext = useContext(AppContext);

  const submitPackage = async () => {
    //pass history in prop for use in sponsor page
    const pushHistory = history.push({
      pathname: '/sponsor-products-and-services',
    });
    /*
    !appContext.state.user.id ? window.location.href = "/business-passport"
    : await history.push({
      pathname: `/payment/${props.subscription_type.toLowerCase()}/${props.subscription_code}`,
      festivalId: props.festivalId,
      title: props.title
    });
    */
    if (appContext.state.signedInStatus === false) {
      //window.location.href = "/business-passport";
      await history.push({
        pathname: '/business-passport',
        history: history,
      });
      //localStorage.setItem('history',history.toString());
    } else {
      await history.push({
        pathname: `/payment/${props.subscription_type.toLowerCase()}/${props.subscription_code}`,
        festivalId: props.festivalId,
        title: props.title,
      });
    }
  };
  return (
    <div class={`card m-5 shadow rounded ${props.color}`}>
      <div class="card-header">
        <p class="font-weight-bold text-justify text-center text-body">{props.subscription_name}</p>
      </div>
      <div class="mx-auto card-body">
        <div className="py-2">{props.description}</div>
        <div className="col-12 text-center">
          <span className="text-center lead font-weight-bold"> &#36;{Math.trunc(props.price)}</span>
          <span className="small">.{props.price.split('.')[1]}</span>
        </div>
      </div>
      <div class="mx-auto card-footer">
        <button onClick={submitPackage} type="button" class="btn btn-primary btn-lg">
          Buy Package
        </button>
        {/* <Link
            exact="true"
            to={{
              pathname: "/sponsor-products-and-services",
              package: props.subscription_name,
            }}
            className="btn btn-dark"
          >
            Select
          </Link> */}
      </div>
    </div>
  );
};

export default SponsorshipPackageCard;
