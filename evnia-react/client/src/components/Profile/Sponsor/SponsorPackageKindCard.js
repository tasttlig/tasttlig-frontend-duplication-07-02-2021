// Libraries
import React, { useState, useEffect, useContext } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import Modal from 'react-modal';
import { AppContext } from './../../../ContextProvider/AppProvider';
import { toast } from 'react-toastify';

// Styling
import './SponsorPackageKindCard.scss';
import 'react-toastify/dist/ReactToastify.css';

// Components
import Footer from './../../Home/BannerFooter/BannerFooter';
import GoTop from './../../Shared/GoTop';
import Nav from './../../Navbar/Nav';
import {
  DateInput,
  Form,
  Input,
  MultiImageInput,
  Select,
  Textarea,
  Checkbox,
  CheckboxGroup,
} from './../../EasyForm';
import {
  formatDate,
  formattedTimeToDateTime,
  formatMilitaryToStandardTime,
} from './../../Functions/Functions';

toast.configure();

const SponsorPackageKindCard = (props) => {
  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const userRole = appContext.state.user.role;
  // Check that user is signed in to access user specific pages
  //if (!appContext.state.signedInStatus) window.location.href = "/login";

  // Set initial state
  const [loading, setLoading] = useState(true);
  const [nationalities, setNationalities] = useState([]);
  const [festivalList, setFestivalList] = useState([]);
  const [formDetails, setFormDetails] = useState({
    input_one: 'product_name',
    input_one_placeholder: 'Name Your Product',
    input_two: 'product_made_in_nationality_id',
    input_three: 'product_price',
    input_three_placeholder: 'Price',
    input_four: 'product_quantity',
    input_four_placeholder: 'Quantity',
    input_five: 'product_size',
    input_five_placeholder: 'Size',
    input_six: 'product_description',
    input_six_placeholder: 'Description',
    input_seven: 'product_expiry_date',
    input_seven_placeholder: 'Expiry Date',
    input_eight: 'product_expiry_time',
    input_eight_placeholder: 'Expiry Time',
    input_nine: 'product_images',
    input_ten: 'product_festival_id',
    input_eleven: 'product_creator_type',
  });
  const [selected, setSelected] = useState('products');
  const [submitAuthDisabled, setSubmitAuthDisabled] = useState(false);
  let data = {};

  // Get all fetches helper function
  const getAllFetches = async () => {
    setLoading(false);
  };

  // Select product form helper function
  const productSelected = () => {
    setFormDetails({
      input_one: 'product_name',
      input_one_placeholder: 'Name Your Product',
      input_two: 'product_made_in_nationality_id',
      input_three: 'product_price',
      input_three_placeholder: 'Price',
      input_four: 'product_quantity',
      input_four_placeholder: 'Quantity',
      input_five: 'product_size',
      input_five_placeholder: 'Size',
      input_six: 'product_description',
      input_six_placeholder: 'Description',
      input_seven: 'product_expiry_date',
      input_seven_placeholder: 'Expiry Date',
      input_eight: 'product_expiry_time',
      input_eight_placeholder: 'Expiry Time',
      input_nine: 'product_images',
      input_ten: 'product_festival_id',
      input_eleven: 'product_creator_type',
    });
  };

  // Select service form helper function
  const serviceSelected = () => {
    setFormDetails({
      input_one: 'service_name',
      input_one_placeholder: 'Name Your Service',
      input_two: 'service_nationality_id',
      input_three: 'service_price',
      input_three_placeholder: 'Price',
      input_four: 'service_capacity',
      input_four_placeholder: 'Capacity',
      input_five: 'service_size_scope',
      input_five_placeholder: 'Scope',
      input_six: 'service_description',
      input_six_placeholder: 'Description',
      input_seven: null,
      input_seven_placeholder: null,
      input_eight: null,
      input_eight_placeholder: null,
      input_nine: 'service_images',
      input_ten: 'service_festival_id',
    });
  };

  //select multiple festivals
  const selectFestivals = () => {
    return festivalList.map((fest) => (
      <Checkbox name={fest.festival_id} label={fest.festival_name} />
    ));
  };

  // Change form helper function
  const changeForm = (event) => {
    if (event.target.value === 'services') {
      setSelected('services');
      serviceSelected();
    } else {
      setSelected('products');
      productSelected();
    }
  };

  // Submit product, service helper functions
  const defaultOnCreatedAction = () => {
    setTimeout(() => {
      //window.location.href = `/festival/${props.location.festivalId}`;
      window.location.href = '/';
    }, 2000);

    toast(`Success! Thank you for creating ${selected}!`, {
      type: 'success',
      autoClose: 2000,
    });

    setSubmitAuthDisabled(true);
  };

  const submitProductOrService = async (data) => {
    window.scrollTo(0, 0);

    let url = '';
    if (selected === 'products') {
      if (userRole && userRole.includes('SPONSOR')) {
        data.product_creator_type = 'SPONSOR';
      } else if (userRole) data.product_creator_type = userRole[0];
      url = '/products/add';
    } else if (selected === 'services') {
      if (userRole && userRole.includes('SPONSOR')) {
        data.product_creator_type = 'SPONSOR';
      }
      if (userRole) data.service_creator_type = userRole[0];
      url = '/services/add';
    }
    try {
      const response = await axios({
        method: 'POST',
        url,
        data,
      });

      if (response && response.data && response.data.success && userRole) {
        defaultOnCreatedAction();
      }
    } catch (error) {
      toast('Error! Something went wrong!', {
        type: 'error',
        autoClose: 2000,
      });
    }
  };

  const onSubmit = async (values) => {
    const { ...rest } = values;

    let data = {
      ...rest,
    };

    if (selected === 'products') {
      //actions based on festival selection
      if (
        data.product_festival_id === 'all_by_subscription' &&
        data.selectedFestivals &&
        data.selectedFestivals.length >= 1
      ) {
        data.product_festival_id = data.selectedFestivals;
      } else if (
        data.product_festival_id === 'all_by_subscription' &&
        data.selectedFestivals &&
        data.selectedFestivals.length < 1
      ) {
        toast('Please select festival(s)!', {
          type: 'warn',
          autoClose: 4000,
        });
      } else if (
        data.product_festival_id !== 'all_by_subscription' &&
        data.selectedFestivals &&
        data.selectedFestivals.length >= 1
      ) {
        toast('Please select upcoming(created) festivals to select other festivals!', {
          type: 'warn',
          autoClose: 4000,
        });
        return;
      } else if (data.product_festival_id === 'uncreated') {
        data.product_festival_id = null;
      }

      data.product_expiry_time = formattedTimeToDateTime(
        values.product_expiry_time.toString().substring(16, 21),
      );
      data.product_expiry_date = formatDate(values.product_expiry_date);

      await submitProductOrService(data);
    } else {
      //actions based on festival selection
      if (
        data.service_festival_id === 'all_by_subscription' &&
        data.selectedFestivals &&
        data.selectedFestivals.length >= 1
      ) {
        data.service_festival_id = data.selectedFestivals;
      } else if (
        data.service_festival_id === 'all_by_subscription' &&
        data.selectedFestivals &&
        data.selectedFestivals.length < 1
      ) {
        toast('Please select festival(s)!', {
          type: 'warn',
          autoClose: 4000,
        });
      } else if (
        data.service_festival_id !== 'all_by_subscription' &&
        data.selectedFestivals &&
        data.selectedFestivals.length >= 1
      ) {
        toast('Please select upcoming(created) festivals to select other festivals!', {
          type: 'warn',
          autoClose: 4000,
        });
        return;
      } else if (data.service_festival_id === 'uncreated') {
        data.service_festival_id = null;
      }
      await submitProductOrService(data);
    }
  };

  // Fetch nationalities helper function
  const fetchNationalities = async () => {
    const response = await axios({
      method: 'GET',
      url: '/nationalities',
    });

    setNationalities(response.data.nationalities);
  };

  // Fetch festival list helper function
  const fetchFestivalList = async () => {
    const response = await axios({
      method: 'GET',
      url: '/festival-list',
    });
    setFestivalList(response.data.festival_list);
  };

  // Mount Dashboard page
  useEffect(() => {
    window.scrollTo(0, 0);

    fetchNationalities();
    fetchFestivalList();
    getAllFetches();
  }, []);

  return (
    <div className="container-fluid add-product-service-container">
      <div className="row">
        <div className="col">
          Upload your products and services you would like to sponsor to Tasttlig
        </div>
      </div>
      <div className="row">
        <div className="col">
          <select
            name="select-form"
            selected="products"
            onChange={changeForm}
            disabled={submitAuthDisabled}
            className="custom-select mb-3"
            required
          >
            <option value="products">Products</option>
            <option value="services">Services</option>
          </select>
        </div>
      </div>
      <Form data={data} onSubmit={onSubmit}>
        <div className="row image-row">
          {selected === 'products' && (
            <MultiImageInput
              name="product_images"
              dropbox_label="Click or drag-and-drop to upload one or more images"
              disabled={submitAuthDisabled}
              className="product-images"
              required
            />
          )}
          {selected === 'services' && (
            <MultiImageInput
              name="service_images"
              dropbox_label="Click or drag-and-drop to upload one or more images"
              disabled={submitAuthDisabled}
              className="product-images"
              required
            />
          )}
        </div>
        <div className="row text-cen">
          <div className="col-md-6 dashboard-forms-input-content-col-1">
            <Input
              name={formDetails.input_one}
              placeholder={formDetails.input_one_placeholder}
              disabled={submitAuthDisabled}
              className="product-name-input"
              required
              label={null}
            />
          </div>
          <div className="col-md-6 dashboard-forms-input-content-col-3">
            {nationalities.length && (
              <Select
                name={formDetails.input_two}
                placeholder={formDetails.input_two_placeholder}
                className="product-name-input"
                disabled={submitAuthDisabled}
                required
              >
                <option value="">--Nationality--</option>
                {nationalities.map((n) => (
                  <option key={n.id} value={n.id}>
                    {n.nationality}
                  </option>
                ))}
              </Select>
            )}
          </div>
        </div>
        <div className="row">
          <div className="col-md-4 dashboard-forms-input-content-col-1">
            <Input
              name={formDetails.input_three}
              step="0.01"
              placeholder={formDetails.input_three_placeholder}
              className="product-name-input"
              disabled={submitAuthDisabled}
              required
            />
          </div>
          <div className="col-md-4 dashboard-forms-input-content-col-2">
            <Input
              name={formDetails.input_four}
              type="number"
              placeholder={formDetails.input_four_placeholder}
              min="1"
              className="product-name-input"
              disabled={submitAuthDisabled}
              required
            />
          </div>
          <div className="col-md-4 dashboard-forms-input-content-col-3">
            <Select
              name={formDetails.input_five}
              placeholder={formDetails.input_five_placeholder}
              className="product-name-input"
              disabled={submitAuthDisabled}
              required
            >
              <option value="">{`--${
                formDetails.input_five === 'product_size' ? 'Size' : 'Scope'
              }--`}</option>
              <option value="Bite Size">Bite Size</option>
              <option value="Quarter">Quarter</option>
              <option value="Half">Half</option>
              <option value="Full">Full</option>
            </Select>
          </div>
        </div>
        {selected === 'products' && (
          <div className="row">
            <div className="col-md-6 dashboard-forms-input-content-col-1">
              <DateInput
                name={formDetails.input_seven}
                placeholderText={formDetails.input_seven_placeholder}
                dateFormat="yyyy/MM/dd"
                minDate={new Date()}
                peekNextMonth
                showMonthDropdown
                showYearDropdown
                dropdownMode="select"
                disabled={submitAuthDisabled}
                className="product-name-input"
                required
              />
            </div>
            <div className="col-md-6 dashboard-forms-input-content-col-3">
              <DateInput
                name={formDetails.input_eight}
                placeholderText={formDetails.input_eight_placeholder}
                showTimeSelect
                showTimeSelectOnly
                dateFormat="h:mm aa"
                disabled={submitAuthDisabled}
                className="last-date-input"
                required
              />
            </div>
          </div>
        )}
        <div className="first-row">
          <Textarea
            name={formDetails.input_six}
            placeholder={formDetails.input_six_placeholder}
            disabled={submitAuthDisabled}
            className="product-name-input"
            required
          />
        </div>

        <>
          <div>{`Which festival does this ${
            selected === 'products' ? 'product' : 'service'
          } belong to?`}</div>
          <div>
            {props.festivalId /* || localStorage.getItem("initialFestivalSelection") */ ? (
              <div>
                <Select
                  name={formDetails.input_ten}
                  className="product-name-input"
                  disabled={submitAuthDisabled}
                  required
                >
                  <option value="">--Select--</option>
                  <option
                    value={
                      props.festivalId /* || localStorage.getItem("initialFestivalSelection") */
                    }
                  >
                    Selected Festival &#40;
                    {props.title /* || localStorage.getItem("initialFestivalTitle") */}&#41;
                  </option>
                </Select>
              </div>
            ) : null}
          </div>
        </>

        <div className="first-row">
          <button type="submit" className="dashboard-forms-add-btn">
            Save
          </button>
        </div>
      </Form>
    </div>
  );
};

export default SponsorPackageKindCard;
