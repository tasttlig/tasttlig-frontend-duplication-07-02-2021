//styles
import './SponsorPackageForm.scss';

// Libraries
import React, { useState, useContext, useRef, useEffect } from 'react';
import { AppContext } from '../../../ContextProvider/AppProvider';
import { Link } from 'react-router-dom';
import axios from 'axios';
import SponsorshipPackageCard from './SponsorshipPackageCard';

// Components
import Nav from '../../Navbar/Nav';
import Footer from '../../Home/BannerFooter/BannerFooter';
import GoTop from '../../Shared/GoTop';

const SponsorshipPackageForm = (props) => {
  // To use the JWT credentials
  const appContext = useContext(AppContext);
  // Check that user is signed in to access user specific pages
  //if (!appContext.state.signedInStatus) window.location.href = "/login";

  const formRef = useRef(null);
  //const [sponsorshipPackage, setSponsorshipPackage] = useState("");

  // Set initial state
  const [item_type] = useState('package');
  const [item_id] = useState('all');
  const [sponsorshipPackages, setSponsorshipPackages] = useState([]);

  //change card color
  const [colors] = useState([
    'bg-primary',
    'bg-success',
    'bg-info',
    'bg-warning',
    'bg-danger',
    'bg-secondary',
    'bg-dark',
    'bg-light',
  ]);

  // Get sponsor package details
  useEffect(() => {
    (async function () {
      const data = {
        item_type,
        item_id,
      };

      const response = await axios.get('/subscription/details', {
        params: data,
      });

      //setSponsorshipPackages(response.data.item)
      //use only sponsorship packages
      setSponsorshipPackages(
        response.data.item.filter((pkg) =>
          pkg.subscription_name.startsWith('festival_sponsorship'),
        ),
      );
    })();
  }, []);

  const renderSponsorshipPackages = (arr) => {
    return arr.map((item, index) => (
      <SponsorshipPackageCard
        key={item.subscription_id}
        date_of_expiry={item.date_of_expiry}
        description={item.description}
        price={item.price}
        status={item.status}
        subscription_type={item.subscription_type}
        comission_to_pay_percentage={item.comission_to_pay_percentage}
        subscription_code={item.subscription_code}
        subscription_name={item.subscription_name}
        subscription_id={item.subscription_id}
        validity_in_months={item.validity_in_months}
        festivalId={props.location.festivalId}
        title={props.location.title}
        color={colors[index]}
      />
    ));
  };

  return (
    <div>
      <Nav />
      <div className="container pb-5 sponsor-package-content">
        <h4 className="text-center mb-5">Choose sponsorship package</h4>
        <div className="row">{renderSponsorshipPackages(sponsorshipPackages)}</div>
        {/* <Link
          exact="true"
          to={{
            pathname: "/sponsor-products-and-services",
            festivalId: props.location.festivalId,
            title: props.location.title,
          }}
          className="continue-btn"
        >
          Continue
        </Link> 
        //{`/festival/${props.festivalId}`} 
        */}
      </div>
      <Footer />
      <GoTop scrollStepInPx="50" delayInMs="16.66" />
    </div>
  );
};

export default SponsorshipPackageForm;
