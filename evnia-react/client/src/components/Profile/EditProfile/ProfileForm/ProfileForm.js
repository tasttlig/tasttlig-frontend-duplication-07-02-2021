// Libraries
import React, { useState } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { toast } from 'react-toastify';

// Components
import { canadaProvincesTerritories } from '../../../Functions/Functions';

// Styling
import '../../Profile.scss';
import 'react-toastify/dist/ReactToastify.css';

toast.configure();

const ProfileForm = (props) => {
  // Pre-fill current user profile information
  const profileData = {
    firstName: props.user.first_name,
    lastName: props.user.last_name,
    phoneNumber: props.user.phone_number,
    addressLine1: props.user.address_line_1,
    addressLine2: props.user.address_line_2,
    city: props.user.city,
    provinceTerritory: props.user.state,
    postalCode: props.user.postal_code,
    addressType: props.user.address_type,
    businessName: props.user.business_name,
    businessType: props.user.business_type,
  };

  const { update, handleSubmit } = useForm({ defaultValues: profileData });

  // Set initial state
  const [firstName, setFirstName] = useState(profileData.firstName);
  const [lastName, setLastName] = useState(profileData.lastName);
  const [phoneNumber, setPhoneNumber] = useState(profileData.phoneNumber);
  const [addressLine1, setAddressLine1] = useState(profileData.addressLine1);
  const [addressLine2, setAddressLine2] = useState(profileData.addressLine2);
  const [city, setCity] = useState(profileData.city);
  const [postalCode, setPostalCode] = useState(profileData.postalCode);
  const [provinceTerritory, setProvinceTerritory] = useState(profileData.provinceTerritory);
  const [addressType, setAddressType] = useState(profileData.addressType);
  const [businessName, setBusinessName] = useState(profileData.businessName);
  const [businessType, setBusinessType] = useState(profileData.businessType);
  const [addressLine1Error, setAddressLine1Error] = useState('');
  const [cityError, setCityError] = useState('');
  const [postalCodeError, setPostalCodeError] = useState('');
  const [provinceTerritoryError, setProvinceTerritoryError] = useState('');
  const [addressTypeError, setAddressTypeError] = useState('');
  const [businessNameError, setBusinessNameError] = useState('');
  const [businessTypeError, setBusinessTypeError] = useState('');
  const [openCompleteProfile, setOpenCompleteProfile] = useState(false);
  const [submitAuthDisabled, setSubmitAuthDisabled] = useState(false);

  // Validate user input for profile form helper function
  const validateProfileForm = () => {
    window.scrollTo(0, 0);

    // Render address error message
    if (!addressLine1) {
      setAddressLine1Error('Address is required.');
    } else {
      setAddressLine1Error('');
    }

    // Render city error message
    if (!city) {
      setCityError('City is required.');
    } else {
      setCityError('');
    }

    // Render Postal Code error message
    if (!postalCode) {
      setPostalCodeError('Postal Code is required.');
    } else if (postalCode.length < 6) {
      setPostalCodeError('Invalid Postal Code.');
    } else {
      setPostalCodeError('');
    }

    // Render Province or Territory error message
    if (!provinceTerritory) {
      setProvinceTerritoryError('Province or Territory is required.');
    } else {
      setProvinceTerritoryError('');
    }

    // Render address type error message
    if (!addressType) {
      setAddressTypeError('Address type is required.');
    } else {
      setAddressTypeError('');
    }

    // Render business name error message
    if (addressType === 'business' && !businessName) {
      setBusinessNameError('Business name is required.');
    } else {
      setBusinessNameError('');
    }

    // Render business type error message
    if (addressType === 'business' && !businessType) {
      setBusinessTypeError('Business type is required.');
    } else {
      setBusinessTypeError('');
    }

    // Set validation error state
    if (
      !addressLine1 ||
      !city ||
      !postalCode ||
      postalCode.length < 6 ||
      !provinceTerritory ||
      !addressType ||
      (addressType === 'business' && !businessName) ||
      (addressType === 'business' && !businessType)
    ) {
      return false;
    }

    return true;
  };

  // Submit profile form helper function
  const onSubmitProfileForm = async () => {
    const isValid = validateProfileForm();

    if (props.user.id || isValid) {
      const url = `/user/update-profile/${props.user.id}`;

      const data = {
        first_name: firstName,
        last_name: lastName,
        phone_number: phoneNumber,
        address_line_1: addressLine1,
        address_line_2: addressLine2,
        city,
        postal_code: postalCode,
        state: provinceTerritory,
        address_type: addressType,
        business_name: businessName,
        business_type: businessType,
        profile_status: 'accepted',
      };

      try {
        const response = await axios({ method: 'PUT', url, data });

        if (response && response.data && response.data.success) {
          setTimeout(() => {
            window.location.href = '/';
          }, 2000);

          toast('Success! Your profile has been updated!', {
            type: 'success',
            autoClose: 2000,
          });

          setSubmitAuthDisabled(true);
        }
      } catch (error) {
        toast('Error! Something went wrong!', {
          type: 'error',
          autoClose: 2000,
        });
      }
    }
  };

  // Render do you have a restaurant question
  const DoYouHaveARestaurant = () => (
    <div className="text-center">
      <div className="do-you-have-a-restaurant-title">Do you Have a Restaurant?</div>
      <div className="row">
        <div className="col-md-6 do-you-have-a-restaurant-yes">
          <Link
            exact="true"
            to="/complete-profile/business"
            className="do-you-have-a-restaurant-yes-btn"
          >
            Yes
          </Link>
        </div>
        <div className="col-md-6 do-you-have-a-restaurant-no">
          <div
            /* onClick={() => setOpenCompleteProfile(!openCompleteProfile)} */
            onClick={() => (window.location.href = '/')}
            className="do-you-have-a-restaurant-no-btn"
          >
            No
          </div>
        </div>
      </div>
    </div>
  );

  // Render Profile Form component
  return (
    <div>
      <DoYouHaveARestaurant />

      {openCompleteProfile && (
        <form onSubmit={handleSubmit(onSubmitProfileForm)} noValidate>
          <div className="py-3">
            <div className="complete-profile-sub-title">Personal Information</div>
            <div className="mb-3">
              <div className="input-title">First Name</div>
              <input
                type="text"
                ref={update}
                name="firstName"
                defaultValue={profileData.firstName}
                onChange={(e) => setFirstName(e.target.value)}
                disabled={submitAuthDisabled}
                className="form-control"
              />
            </div>
            <div className="mb-3">
              <div className="input-title">Last Name</div>
              <input
                type="text"
                ref={update}
                name="lastName"
                defaultValue={profileData.lastName}
                onChange={(e) => setLastName(e.target.value)}
                disabled={submitAuthDisabled}
                className="form-control"
              />
            </div>
            <div className="mb-3">
              <div className="input-title">Phone Number</div>
              <input
                type="tel"
                ref={update}
                name="phoneNumber"
                defaultValue={profileData.phoneNumber}
                onChange={(e) => setPhoneNumber(e.target.value)}
                maxLength="14"
                disabled={submitAuthDisabled}
                className="form-control"
              />
            </div>
          </div>
          <div className="pb-3">
            {/* <div className="complete-profile-sub-title">
              Enter Your Address For A Better Tasttlig Experience
            </div> */}
            {/* <div className="mb-3">
              <div className="input-title">Street Address</div>
              <input
                type="text"
                ref={update}
                name="addressLine1"
                defaultValue={profileData.addressLine1}
                onChange={(e) => setAddressLine1(e.target.value)}
                disabled={submitAuthDisabled}
                className="form-control"
              />
            </div> */}
            {/* <div className="mb-3">
              <div className="input-title">Unit Address</div>
              <input
                type="text"
                ref={update}
                name="addressLine2"
                defaultValue={profileData.addressLine2}
                onChange={(e) => setAddressLine2(e.target.value)}
                disabled={submitAuthDisabled}
                className="form-control"
              />
            </div> */}
            {/* <div className="mb-3">
              <div className="input-title">City</div>
              <input
                type="text"
                ref={update}
                name="city"
                defaultValue={profileData.city}
                onChange={(e) => setCity(e.target.value)}
                disabled={submitAuthDisabled}
                className="form-control"
              />
            </div> */}
            <div className="mb-3">
              <div className="input-title">Province or Territory</div>
              <select
                ref={update}
                name="provinceTerritory"
                defaultValue={profileData.provinceTerritory}
                onChange={(e) => setProvinceTerritory(e.target.value)}
                disabled={submitAuthDisabled}
                className="custom-select"
              >
                {canadaProvincesTerritories()}
              </select>
            </div>
            {/* <div className="mb-3">
              <div className="input-title">Postal Code</div>
              <input
                type="text"
                ref={update}
                name="postalCode"
                defaultValue={profileData.postalCode}
                onChange={(e) => setPostalCode(e.target.value)}
                maxLength="7"
                disabled={submitAuthDisabled}
                className="form-control"
              />
            </div> */}
          </div>
          {/* <fieldset>
          <legend>Location</legend>
          <hr />
          <div className="form-row">
            <div className="form-group col-md-6">
              <div className="input-title">Street Address*</div>
              <input
                type="text"
                ref={update}
                name="addressLine1"
                defaultValue={profileData.addressLine1}
                onChange={(e) => setAddressLine1(e.target.value)}
                disabled={submitAuthDisabled}
                className="form-control"
                required
              />
              {addressLine1Error && (
                <div className="error-message">{addressLine1Error}</div>
              )}
            </div>
            <div className="form-group col-md-6">
              <div className="input-title">Unit Address</div>
              <input
                type="text"
                ref={update}
                name="addressLine2"
                defaultValue={profileData.addressLine2}
                onChange={(e) => setAddressLine2(e.target.value)}
                disabled={submitAuthDisabled}
                className="form-control"
              />
            </div>
          </div>
          <div className="form-row">
            <div className="form-group col-md-6">
              <div className="input-title">City*</div>
              <input
                type="text"
                ref={update}
                name="city"
                defaultValue={profileData.city}
                onChange={(e) => setCity(e.target.value)}
                disabled={submitAuthDisabled}
                className="form-control"
                required
              />
              {cityError && <div className="error-message">{cityError}</div>}
            </div>
            <div className="form-group col-md-6">
              <div className="input-title">Postal Code*</div>
              <input
                type="text"
                ref={update}
                name="postalCode"
                defaultValue={profileData.postalCode}
                onChange={(e) => setPostalCode(e.target.value)}
                disabled={submitAuthDisabled}
                className="form-control"
                required
              />
              {postalCodeError && (
                <div className="error-message">{postalCodeError}</div>
              )}
            </div>
          </div>
          <div className="form-row">
            <div className="form-group col-md-12">
              <div className="input-title">Province or Territory*</div>
              <select
                ref={update}
                name="provinceTerritory"
                defaultValue={profileData.provinceTerritory}
                onChange={(e) => setProvinceTerritory(e.target.value)}
                disabled={submitAuthDisabled}
                className="custom-select"
                required
              >
                {canadaProvincesTerritories()}
              </select>
              {provinceTerritoryError && (
                <div className="error-message">{provinceTerritoryError}</div>
              )}
            </div>
          </div>
          <div className="form-group">
            <div>This is a:</div>
            <div className="form-check">
              <input
                type="radio"
                checked={addressType === "business"}
                onChange={() => setAddressType("business")}
                disabled={submitAuthDisabled}
                className="form-check-input"
              />
              <div>Business Address</div>
            </div>
            <div className="form-check">
              <input
                type="radio"
                checked={addressType === "home"}
                onChange={() => setAddressType("home")}
                disabled={submitAuthDisabled}
                className="form-check-input"
              />
              <div>Home Address</div>
            </div>
            <div className="form-check">
              <input
                type="radio"
                checked={addressType === "other"}
                onChange={() => setAddressType("other")}
                disabled={submitAuthDisabled}
                className="form-check-input"
              />
              <div>Other</div>
            </div>
            {addressTypeError && (
              <div className="error-message">{addressTypeError}</div>
            )}
          </div>
        </fieldset> */}

          {/* {addressType === "business" && (
          <fieldset>
            <legend>Business Information</legend>
            <hr />
            <div className="form-row">
              <div className="form-group col-md-12">
                <div className="input-title">Business Name*</div>
                <input
                  type="text"
                  ref={update}
                  name="businessName"
                  defaultValue={profileData.businessName}
                  onChange={(e) => setBusinessName(e.target.value)}
                  disabled={submitAuthDisabled}
                  className="form-control"
                  required
                />
                {businessNameError && (
                  <div className="error-message">{businessNameError}</div>
                )}
              </div>
            </div>
            <div className="form-group">
              <div>I am a:</div>
              <div className="form-check">
                <input
                  type="radio"
                  checked={businessType === "restaurant"}
                  onChange={() => setBusinessType("restaurant")}
                  disabled={submitAuthDisabled}
                  className="form-check-input"
                />
                <div>Restaurant</div>
              </div>
              <div className="form-check">
                <input
                  type="radio"
                  checked={businessType === "caterer"}
                  onChange={() => setBusinessType("caterer")}
                  disabled={submitAuthDisabled}
                  className="form-check-input"
                />
                <div>Caterer</div>
              </div>
              <div className="form-check">
                <input
                  type="radio"
                  checked={businessType === "chef"}
                  onChange={() => setBusinessType("chef")}
                  disabled={submitAuthDisabled}
                  className="form-check-input"
                />
                <div>Chef</div>
              </div>
              <div className="form-check">
                <input
                  type="radio"
                  checked={businessType === "food truck"}
                  onChange={() => setBusinessType("food truck")}
                  disabled={submitAuthDisabled}
                  className="form-check-input"
                />
                <div>Food Truck</div>
              </div>
              {businessTypeError && (
                <div className="error-message">{businessTypeError}</div>
              )}
            </div>
          </fieldset>
        )} */}
          <div className="submit-container">
            <Link exact="true" to="/complete-profile/preference">
              <button className="call-to-action-btn">Submit</button>
            </Link>
          </div>
        </form>
      )}
    </div>
  );
};

export default ProfileForm;
