import React, { useState, useEffect, useContext } from 'react';

import './Rewards.scss';

const Rewards = (props) => {
  return (
    <div
      onClick={() => props.toggleFullView('rewards')}
      className="passport-activities__card rewards-card"
    >
      <h3 className="passport-activities__card-title">Rewards</h3>
      <div className="passport-activites__card-count">10</div>
      <div className="passport-activities__card-festival-list"></div>
    </div>
  );
};

export default Rewards;
