import React, { useState, useEffect, useContext } from 'react';

import './Rewards.scss';

const FullRewards = (props) => {
  return (
    <div className="passport-activities__full-card rewards-card">
      <h3 className="passport-activities__card-title">Rewards</h3>
      <div onClick={() => props.toggleFullView('')} className="passport-activites__full-card-exit">
        <i className="fas fa-times-circle"></i>
      </div>
      <div className="passport-activities__full-card-list"></div>
    </div>
  );
};

export default FullRewards;
