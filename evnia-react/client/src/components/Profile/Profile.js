// Libraries
import React, { useState, useEffect, useContext, Fragment } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { AppContext } from '../../ContextProvider/AppProvider';
import Modal from 'react-modal';
import moment, { now } from 'moment';
import useInfiniteScroll from 'react-infinite-scroll-hook';
import { jsPDF } from 'jspdf';

import {
  formatDate,
  formatPhoneNumber,
  formatMilitaryToStandardTime,
} from '../Functions/Functions';

// Components
import Nav from '../Navbar/Nav';
import PassportProfile from './PassportProfile/PassportProfile';
import PassportActivities from './PassportActivities/PassportActivities';
import FullCurrentFestivals from './PassportActivities/CurrentFestivals/FullCurrentFestivals';
import FullPastFestivals from './PassportActivities/PastFestivals/FullPastFestivals';
import FullMyStamps from './PassportActivities/MyStamps/FullMyStamps';
import FullPreferences from './PassportActivities/Preferences/FullPreferences';
import FullRewards from './PassportActivities/Rewards/FullRewards';
import Footer from '../Footer/Footer';
import QRCode from 'qrcode.react';

// Styling
import './Profile.scss';
import PassportSideCard from './PassportSideCard/PassportSideCard';
import BusinessPassport from './BusinessPassport/BusinessPassport';
import EditPersonalPassport from './PersonalPassport/EditPersonalPassport';
import tasttligLogoWhite from '../../assets/images/tasttlig-logo-white.png';
import tasttligLogoBlack from '../../assets/images/tasttlig-logo-black.png';

const doc = new jsPDF();

const Profile = (props) => {
  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const userRoles = appContext.state.user.role;
  const userRolesString = userRoles && userRoles.join(', ').toLowerCase();
  const user = appContext.state.user;

  // Set initial state
  const [navBackground, setNavBackground] = useState('nav-white');
  const [toggleState, setToggleState] = useState(1);
  const [toggleEditView, setToggleEditView] = useState('personal');
  const [toggleFullCardView, setToggleFullCardView] = useState('');
  const [passportShortViewOpen, setPassportShortViewOpen] = useState(false);
  const [toggleShortView, setToggleShortView] = useState('guest');

  console.log('USER Data', appContext.state.user);

  let isHost = false;

  if (userRoles && userRoles.includes('HOST')) {
    isHost = true;
  }

  let hasBusinessPassport;

  if (
    userRoles &&
    (userRoles.includes('HOST') ||
      userRoles.includes('RESTAURANT') ||
      userRoles.includes('SPONSOR') ||
      userRoles.includes('VENDOR') ||
      userRoles.includes('MEMBER_BUSINESS_BASIC') ||
      userRoles.includes('MEMBER_BUSINESS_INTERMEDIATE') ||
      userRoles.includes('MEMBER_BUSINESS_ADVANCED') ||
      userRoles.includes('MEMBER_BUSINESS_PRO') ||
      userRoles.includes('BUSINESS_MEMBER') ||
      userRoles.includes('HOST_VEND') ||
      userRoles.includes('HOST_AMBASSADOR') ||
      userRoles.includes('HOST_GUEST'))
  ) {
    hasBusinessPassport = true;
  } else {
    hasBusinessPassport = false;
  }

  let fullName = (
    <Fragment>
      <strong>Name:</strong> {`${user.first_name} ${user.last_name}`}
    </Fragment>
  );

  const toggleTab = (index) => {
    setToggleState(index);
  };

  const toggleEditTab = (view) => {
    setToggleEditView(view);
  };

  const toggleFullView = (view) => {
    setToggleFullCardView(view);
  };

  const togglePassportModal = (view) => {
    setToggleShortView(view);
  };

  // Set initial state
  const [passportItems, setPassportItems] = useState([]);
  const [selectedNationalities, setSelectedNationalities] = useState([]);
  const [startDate, setFilterStartDate] = useState('');
  const [startTime, setFilterStartTime] = useState('');
  const [cityLocation, setCityLocation] = useState('');
  const [filterRadius, setFilterRadius] = useState(25000000);
  const [hasNextPage, setHasNextPage] = useState(true);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(0);
  const [stampList, setStampList] = useState([]);

  let expiredFestivals = [];
  let currentFestivals = [];

  // check if festival has passed, if it did add it to expiredFestivals
  // if not add it to current festivals
  passportItems.forEach((row) => {
    if (moment(row.festival_end_date).isBefore()) {
      expiredFestivals.push(row);
    } else {
      currentFestivals.push(row);
    }
  });

  // Fetch user product reservations helper function
  const fetchUserStampList = async () => {
    try {
      const url = '/all-product-claim/user/reservations';
      const acc_token = localStorage.getItem('access_token');
      const headers = { Authorization: `Bearer ${acc_token}` };
      return await axios({ method: 'GET', url, headers });
    } catch (error) {
      return error.response;
    }
  };

  useEffect(() => {
    if (localStorage.getItem('tab')==='2')
      {
        toggleTab(2);
        localStorage.setItem('tab', '1');
      }
    
    window.scrollTo(0, 0);

    setLoading(true);

    fetchUserStampList().then(({ data }) => {
      setStampList(data.details);
    });
    setLoading(false);
  }, []);

  const loadNextPage = async (page) => {
    try {
      const response = await axios({
        method: 'GET',
        url: '/passport',
        params: {
          userId: appContext.state.user.id,
          page: page + 1,
        },
      });
      console.log('user response',response);
      return response;
    } catch (error) {
      return error.response;
    }
  };

  const handleLoadMore = (page = currentPage, passport = passportItems) => {
    setLoading(true);
    loadNextPage(page).then((newPage) => {
      setLoading(false);
      if (!newPage) {
        return false;
      }

      const pagination = newPage.data.details.pagination;

      if (pagination && page < pagination.lastPage) {
        setCurrentPage(page + 1);
      }
      setHasNextPage(pagination && currentPage < pagination.lastPage);
      setPassportItems(passport.concat(newPage.data.details.data));
    });
  };

  const infiniteRef = useInfiniteScroll({
    loading,
    hasNextPage,
    onLoadMore: handleLoadMore,
    scrollContainer: 'window',
    threshold: 50,
  });

  const handleQRClick = async (event) => {
    event.preventDefault();
    setPassportShortViewOpen(true);
  };

  // Close modal type helper function
  const closeModal = (modalType) => () => {
    if (modalType === 'passportShortView') {
      setPassportShortViewOpen(false);
    }
  };

  // PRINT / SAVE PASSPORT SHORT VIEW
  const saveDiv = (divId, title) => {
    // if (document.getElementById(divId) !== null) {
    doc.html(
      `<html><head><title>${title}</title></head><body>` +
        document.getElementById(divId).innerHTML +
        `</body></html>`,
    );
    doc.save(`${user.first_name}_${user.last_name}'s-Tasttlig-Passport`);
    // }
  };

  const printDiv = (divId, title) => {
    let mywindow = window.open('', 'PRINT', 'height=650,width=900,top=100,left=150');

    mywindow.document.write(`<html><head><title>${title}</title>`);
    mywindow.document.write('</head><body >');
    mywindow.document.write(document.getElementById(divId).innerHTML);
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/

    mywindow.print();
    mywindow.close();

    return true;
  };

  return (
    <div>
      <Modal
        id="passport-short-form"
        isOpen={passportShortViewOpen}
        onRequestClose={closeModal('passportShortView')}
        ariaHideApp={false}
        className={
          toggleShortView === 'guest'
            ? 'passport-short-view-modal'
            : 'business-passport-short-view-modal'
        }
      >
        <div className="pass-short__heading">
          <div className="head-left-buttons">
            {userRoles && hasBusinessPassport ? (
              <div
                onClick={
                  toggleShortView === 'guest'
                    ? () => togglePassportModal('business')
                    : () => togglePassportModal('guest')
                }
              >
                <i title="Toggle Passports" className="fas fa-window-restore"></i>
              </div>
            ) : null}
            <i
              title="Save Your Passport"
              // onClick={() => saveDiv('passport-short-form', 'Title')}
              onClick={() => window.print()}
              className="fas fa-save"
            ></i>
          </div>
          <img
            className="pass-short__logo"
            src={toggleShortView === 'guest' ? tasttligLogoBlack : tasttligLogoWhite}
          ></img>
          <div className="head-right-buttons">
            <div onClick={closeModal('passportShortView')}>
              <i title="Exit" className="fas fa-times-circle"></i>
            </div>
          </div>
        </div>

        <div className="row pass-short__info">
          <div className="pass-short__title">PASSPORT</div>
          <div className="pass-short__passport-num">
            <strong>Passport No.:</strong> {user.passport_id}
          </div>
          <div className="pass-short__passport-name">
            {toggleShortView === 'guest' ? (
              fullName
            ) : (
              <strong className="business_name">{user.business_name}</strong>
            )}
          </div>
        </div>
        <div className="row">
          <QRCode
            value={`/passport/${appContext.state.user.id}`}
            // value="www.google.com"
            size={200}
            includeMargin={false}
            className="pass-short__qrcode"
          ></QRCode>
        </div>
        <div className="row pass-short__info">
          <div className="pass-short__passport-verified">
            <strong>Date Issued:</strong> {formatDate(user.created_at_datetime)}
          </div>
        </div>
      </Modal>
      <Nav background={navBackground} />
      {toggleState && toggleState !== 3 ? (
        <Fragment>
          <section className="passport-sections passport-tab-switcher passport-page-switcher">
            <div className="container">
              <div className="row">
                <div className="tab-row">
                  <div className="tab-switcher">
                    <button
                      onClick={() => toggleTab(1)}
                      className={toggleState === 1 ? 'tab active-tab' : 'tab'}
                    >
                      Profile
                    </button>
                    <button
                      onClick={() => toggleTab(2)}
                      className={toggleState === 2 ? 'tab active-tab' : 'tab'}
                    >
                      Activities
                    </button>
                  </div>
                </div>
              </div>
              <div className="row passport-titles">
                <div className="col-md-3 passport-heading">PASSPORT</div>
                <div className="col-md-8 passport-line-heading">
                  <span>TASTTLIG</span>
                </div>
                <div className="col-md-1 passport-qrcode">
                  <QRCode
                    value={`/passport/${appContext.state.user.id}`}
                    onClick={handleQRClick}
                    // value="www.google.com"
                    size={85}
                    includeMargin={false}
                    className="passport-qrcode-comp"
                  ></QRCode>
                </div>
              </div>
            </div>
          </section>
          <section className="container passport__main">
            <div className="row">
              <div className="col-md-3 passport__sidebar">
                <PassportSideCard
                  isHost={isHost}
                  roles={userRolesString}
                  location={appContext.state.user.city}
                  jobTitle={appContext.state.user.occupation}
                  phoneNumber={appContext.state.user.phone_number}
                  email={appContext.state.user.email}
                  birthdate={appContext.state.user.date_of_birth !== null ? (formatDate(appContext.state.user.date_of_birth)) : null}
                  profileImage = {appContext.state.user.profile_image}
                  toggleTab={toggleTab}
                />
              </div>
              <div className="col-md-9 passport__content">
                {toggleState && toggleState === 1 ? (
                  <PassportProfile
                    appContext={appContext.state.user}
                    hasBusinessPassport={hasBusinessPassport}
                    birthdate={appContext.state.user.date_of_birth !== null ? (formatDate(appContext.state.user.date_of_birth)) : null}
                  />
                ) : toggleState === 2 ? (
                  // {toggleFullCardView === '' ? < PassportActivities /> : toggleFullCardView === 'currentFestivals' ? <FullCurrentFestivals /> : toggleFullCardView === 'pastFestivals' ? <FullPastFestivals /> : toggleFullCardView === 'myStamps' ? <FullMyStamps /> : toggleFullCardView === 'preferences' ? <FullPreferences /> : toggleFullCardView === 'rewards' ? <FullRewards />}
                  toggleFullCardView === 'currentFestivals' ? (
                    <Fragment>
                      <FullCurrentFestivals
                        currentFestivals={currentFestivals}
                        infiniteRef={infiniteRef}
                        toggleFullView={toggleFullView}
                        formatDate={formatDate}
                      />
                    </Fragment>
                  ) : toggleFullCardView === 'pastFestivals' ? (
                    <Fragment>
                      <FullPastFestivals
                        expiredFestivals={expiredFestivals}
                        infiniteRef={infiniteRef}
                        toggleFullView={toggleFullView}
                        formatDate={formatDate}
                      />
                    </Fragment>
                  ) : toggleFullCardView === 'myStamps' ? (
                    <Fragment>
                      <FullMyStamps stampList={stampList} toggleFullView={toggleFullView} />
                    </Fragment>
                  ) : toggleFullCardView === 'preferences' ? (
                    <Fragment>
                      <FullPreferences
                        appContext={appContext.state.user}
                        toggleFullView={toggleFullView}
                      />
                    </Fragment>
                  ) : toggleFullCardView === 'rewards' ? (
                    <Fragment>
                      <FullRewards toggleFullView={toggleFullView} />
                    </Fragment>
                  ) : (
                    <Fragment>
                      <PassportActivities
                        currentFestivals={currentFestivals}
                        expiredFestivals={expiredFestivals}
                        stampList={stampList}
                        infiniteRef={infiniteRef}
                        toggleFullView={toggleFullView}
                      />
                    </Fragment>
                  )
                ) : null}
              </div>
            </div>
            <div className="row">
              <div className="col-md-12 passport-unique-string-gen">
                {`${hasBusinessPassport ? 'B' : 'P'}<${
                  user.country && user.country.substring(0, 3) ? user.country.substring(0, 3) : null
                }${user.last_name}<<${
                  user.first_name
                }<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                ${user.passport_id}<0${
                  user.country && user.country.substring(0, 3) ? user.country.substring(0, 3) : null
                }${moment(user.date_of_birth).format('YYMMDD')}0${
                  user.sex
                }2503303<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<02`}
              </div>
            </div>
          </section>
        </Fragment>
      ) : (
        <Fragment>
          <section className="passport-sections passport-tab-switcher passport-page-switcher">
            <div className="container">
              <div className="row edit-passport-buttons-ui">
                <div className="col">
                  <div onClick={() => toggleTab(1)} className="back-button">
                    <i className="fas fa-arrow-circle-left"></i>
                  </div>
                </div>
                <div className="col tab-row">
                  <div className="tab-switcher edit-passport-switcher">
                    <button
                      onClick={() => toggleEditTab('personal')}
                      className={toggleEditView === 'personal' ? 'tab active-tab' : 'tab'}
                    >
                      Personal
                    </button>
                    <button
                      onClick={() => toggleEditTab('business')}
                      className={toggleEditView === 'business' ? 'tab active-tab' : 'tab'}
                    >
                      Business
                    </button>
                  </div>
                </div>
                <div className="col"></div>
              </div>
            </div>
          </section>
          <section className="container passport__main">
            {toggleEditView === 'personal' ? <EditPersonalPassport /> : <BusinessPassport />}
          </section>
        </Fragment>
      )}

      <Footer />
    </div>
  );
};

export default Profile;
