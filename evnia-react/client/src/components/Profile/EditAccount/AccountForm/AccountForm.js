// Libraries
import React, { useState } from 'react';
import axios from 'axios';
import { v4 as uuidv4 } from 'uuid';
import { useForm } from 'react-hook-form';
import { toast } from 'react-toastify';

// Styling
import './AccountForm.scss';
import defaultProfilePicture from '../../../../assets/images/default-profile-picture.png';
import 'react-toastify/dist/ReactToastify.css';

toast.configure();

const AccountForm = (props) => {
  // Pre-fill current user profile information
  const profileData = {
    profileImage: props.user.profile_image_link,
    firstName: props.user.first_name,
    lastName: props.user.last_name,
    email: props.user.email,
    phoneNumber: props.user.phone_number,
    profileTagLine: props.user.profile_tag_line,
    bio: props.user.bio,
    bannerImage: props.user.banner_image_link,
  };

  const { update, handleSubmit } = useForm({
    defaultValues: profileData,
  });

  // Set initial state
  const [load, setLoad] = useState(false);
  const [profileImage, setProfileImage] = useState([]);
  const [firstName, setFirstName] = useState(profileData.firstName || '');
  const [lastName, setLastName] = useState(profileData.lastName || '');
  const [password, setPassword] = useState('');
  const [phoneNumber, setPhoneNumber] = useState(profileData.phoneNumber || '');
  const [profileTagLine, setProfileTagLine] = useState(profileData.profileTagLine || '');
  const [bio, setBio] = useState(profileData.bio || '');
  const [bannerImage, setBannerImage] = useState([]);
  const [firstNameError, setFirstNameError] = useState('');
  const [lastNameError, setLastNameError] = useState('');
  const [passwordError, setPasswordError] = useState('');
  const [phoneNumberError, setPhoneNumberError] = useState('');
  const [submitAuthDisabled, setSubmitAuthDisabled] = useState(false);

  // Profile image upload helper function
  const handleProfileImage = async (event) => {
    if (profileImage.length < 1) {
      let file = event.target.files[0];
      let dir_name = 'profile-images';
      let fileParts = file.name.split('.');
      let fileName = `${dir_name}/${uuidv4()} ${fileParts[0]}.${fileParts[1]}`;
      let fileType = fileParts[1];

      axios
        .post('/s3_signed_url', { fileName, fileType })
        .then((response) => {
          let signedRequest = response.data.signedRequest;
          let url = response.data.url;
          let options = {
            headers: {
              'Content-Type': fileType,
            },
          };

          axios
            .put(signedRequest, file, options)
            .then(() => {
              if (file.size <= 800000) {
                setProfileImage((image) => [...image, url]);
              } else {
                alert('File is bigger than 800K.');
              }
            })
            .catch((error) => {
              console.log(`ERROR ${JSON.stringify(error)}`);
            });
        })
        .catch((error) => {
          console.log(JSON.stringify(error));
        });
    }
  };

  // Banner image upload helper function
  const handleBannerImage = async (event) => {
    if (bannerImage.length < 1) {
      let file = event.target.files[0];
      let dir_name = 'banner-images';
      let fileParts = file.name.split('.');
      let fileName = `${dir_name}/${uuidv4()} ${fileParts[0]}.${fileParts[1]}`;
      let fileType = fileParts[1];

      axios
        .post('/s3_signed_url', { fileName, fileType })
        .then((response) => {
          let signedRequest = response.data.signedRequest;
          let url = response.data.url;
          let options = {
            headers: {
              'Content-Type': fileType,
            },
          };

          axios
            .put(signedRequest, file, options)
            .then(() => {
              setBannerImage((image) => [...image, url]);
            })
            .catch((error) => {
              console.log(`ERROR ${JSON.stringify(error)}`);
            });
        })
        .catch((error) => {
          console.log(JSON.stringify(error));
        });
    }
  };

  // Validate user input for account form helper function
  const validateAccountForm = () => {
    window.scrollTo(0, 0);

    // Render first name error message
    if (!firstName) {
      setFirstNameError('First name is required.');
    } else {
      setFirstNameError('');
    }

    // Render last name error message
    if (!lastName) {
      setLastNameError('Last name is required.');
    } else {
      setLastNameError('');
    }

    // Render password error message
    if (!password) {
      setPasswordError('Password is required.');
    } else if (password.length < 8) {
      setPasswordError('Your password must be at least 8 characters. Please try again.');
    } else {
      setPasswordError('');
    }

    // Render phone number error message
    if (!phoneNumber) {
      setPhoneNumberError('Phone number is required.');
    } else {
      setPhoneNumberError('');
    }

    // Set validation error state
    if (!firstName || !lastName || !password || password.length < 8 || !phoneNumber) {
      return false;
    }

    return true;
  };

  // Submit account form helper function
  const onSubmitAccountForm = async () => {
    const isValid = validateAccountForm();

    if (isValid) {
      const url = `/user/update-account/${props.user.id}`;

      const data = {
        profile_image_link: profileImage[0],
        first_name: firstName,
        last_name: lastName,
        password,
        phone_number: phoneNumber,
        profile_tag_line: profileTagLine,
        bio_text: bio,
        banner_image_link: bannerImage[0],
      };

      try {
        const response = await axios({ method: 'PUT', url, data });

        if (response && response.data && response.data.success) {
          setTimeout(() => {
            window.location.href = '/dashboard';
          }, 2000);

          toast('Success! Your profile has been updated!', {
            type: 'success',
            autoClose: 2000,
          });

          setSubmitAuthDisabled(true);
        }
      } catch (error) {
        toast('Error! Something went wrong!', {
          type: 'error',
          autoClose: 2000,
        });
      }
    }
  };

  // Render Profile Form component page
  return (
    <div>
      <form onSubmit={handleSubmit(onSubmitAccountForm)} noValidate>
        <fieldset>
          <legend>Basic Information</legend>
          <hr />

          <div className="profile-image-uploader">
            <div className="d-flex flex-row">
              <div className="banner-avatar">
                <img
                  src={
                    profileImage[0]
                      ? profileImage[0]
                      : profileData.profileImage
                      ? profileData.profileImage
                      : defaultProfilePicture
                  }
                  alt={`${profileData.firstName} ${profileData.lastName}`}
                  onLoad={() => setLoad(true)}
                  className={load ? '' : 'loading-image'}
                />
              </div>
              <fieldset>
                <input
                  type="file"
                  name="imageFile"
                  onChange={handleProfileImage}
                  accept="image/*"
                />
                <div className="info">JPG, GIF or PNG. Max size of 800K</div>
              </fieldset>
            </div>
          </div>

          <div className="mb-3">
            <div className="input-title">First Name*</div>
            <input
              type="text"
              ref={update}
              name="firstName"
              defaultValue={profileData.firstName}
              onChange={(e) => setFirstName(e.target.value)}
              disabled={submitAuthDisabled}
              className="form-control"
              required
            />
            {firstNameError && <div className="error-message">{firstNameError}</div>}
          </div>
          <div className="mb-3">
            <div className="input-title">Last Name*</div>
            <input
              type="text"
              ref={update}
              name="lastName"
              defaultValue={profileData.lastName}
              onChange={(e) => setLastName(e.target.value)}
              disabled={submitAuthDisabled}
              className="form-control"
              required
            />
            {lastNameError && <div className="error-message">{lastNameError}</div>}
          </div>
          <div className="mb-3">
            <div className="input-title">Email*</div>
            <input
              type="email"
              ref={update}
              name="email"
              defaultValue={profileData.email}
              disabled={true}
              className="form-control"
              required
            />
          </div>
          <div className="mb-3">
            <div className="input-title">Password (Min 8 Characters)*</div>
            <input
              type="password"
              name="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              disabled={submitAuthDisabled}
              className="form-control"
              required
            />
            {passwordError && <div className="error-message">{passwordError}</div>}
          </div>
          <div className="mb-3">
            <div className="input-title">Phone Number*</div>
            <input
              type="tel"
              ref={update}
              name="phoneNumber"
              defaultValue={profileData.phoneNumber}
              onChange={(e) => setPhoneNumber(e.target.value)}
              disabled={submitAuthDisabled}
              className="form-control"
              required
            />
            {phoneNumberError && <div className="error-message">{phoneNumberError}</div>}
          </div>
          <div className="mb-3">
            <div className="input-title">Profile Tag Line (Optional)</div>
            <input
              type="text"
              ref={update}
              name="profileTagLine"
              defaultValue={profileData.profileTagLine}
              onChange={(e) => setProfileTagLine(e.target.value)}
              disabled={submitAuthDisabled}
              className="form-control"
            />
          </div>
          <div className="mb-3">
            <div className="input-title">Bio (Optional)</div>
            <textarea
              ref={update}
              name="bio"
              defaultValue={profileData.bio}
              onChange={(e) => setBio(e.target.value)}
              disabled={submitAuthDisabled}
              className="bio-details"
              required
            />
          </div>
          <div className="form-row">
            <div className="mb-3 col-md-12">
              <div className="input-title">Banner Image (Optional)</div>
              <div className="dropbox">
                <input
                  type="file"
                  className="input-file"
                  onChange={handleBannerImage}
                  accept="image/*"
                />
                {bannerImage[0] ? (
                  <div className="img-preview">
                    <img
                      src={bannerImage[0]}
                      alt="Banner"
                      onLoad={() => setLoad(true)}
                      className={load ? '' : 'loading-image'}
                    />
                  </div>
                ) : (
                  <div className="message">Click here to add a banner image to your profile</div>
                )}
              </div>
            </div>
          </div>
        </fieldset>

        <div className="submit-container">
          <button type="submit" disabled={submitAuthDisabled} className="call-to-action-btn">
            Save
          </button>
        </div>
      </form>
    </div>
  );
};

export default AccountForm;
