import React, { useState, useEffect, useContext } from 'react';

const EditPassport = (props) => {
  return (
    <div className="container edit-passport">
      <div className="row">
        <div className="col">
          <button className="">Guest Passport</button>
        </div>
        <div className="col">
          <button className="">Business Passport</button>
        </div>
      </div>
    </div>
  );
};

export default EditPassport;
