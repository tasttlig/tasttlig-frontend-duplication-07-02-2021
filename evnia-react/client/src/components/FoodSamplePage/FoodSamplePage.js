// Libraries
import React, { useState, useEffect, useContext, useCallback } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import axios from 'axios';
import Modal from 'react-modal';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { toast } from 'react-toastify';
import { Spinner } from 'react-bootstrap';
import * as shoppingCartActions from '../../redux/shoppingCart/actions';
import {
  GoogleMap,
  DirectionsRenderer,
  DirectionsService,
  LoadScript,
} from '@react-google-maps/api';
import { AppContext } from '../../ContextProvider/AppProvider';
import { AuthModalContext } from '../../ContextProvider/AuthModalProvider';
import { usePosition } from '../../hooks';

// Components
import Nav from '../Navbar/Nav';
import ImageSlider from '../ImageSlider/ImageSlider';
import BannerFooter from '../Home/BannerFooter/BannerFooter';
import GoToTop from '../Shared/GoTop';
import { formatDate, formatMilitaryToStandardTime } from '../Functions/Functions';

// Styling
import './FoodSamplePage.scss';
import 'react-toastify/dist/ReactToastify.css';

toast.configure();

const FoodSampleDetailsPage = (props) => {
  // Set initial state
  const [foodSampleDetails, setFoodSampleDetails] = useState([]);
  const [foodId, setFoodId] = useState();
  const [contactOpened, setContactOpened] = useState(false);
  const [, setCopied] = useState(false);
  const [directions, setDirections] = useState(null);
  const [itemInCart, setItemInCart] = useState(false);
  const [reduxItem, setReduxItem] = useState(false);
  //const [passportIdOrEmail, setPassportIdOrEmail] = useState("")
  const [errorMessage, setErrorMessage] = useState('');
  const [canClaim, setCanClaim] = useState(true);
  const [submitAuthDisabled, setSubmitAuthDisabled] = useState(false);
  const [googleMapsApiKey, setGoogleMapsApiKey] = useState('');
  const [map, setMap] = useState(null);

  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const authModalContext = useContext(AuthModalContext);
  const userRole = appContext.state.user.role;
  const { latitude, longitude, geoError } = usePosition();

  // Derived state
  const isMapReady =
    googleMapsApiKey &&
    latitude &&
    longitude &&
    foodSampleDetails.latitude &&
    foodSampleDetails.longitude;
  const handleLoad = useCallback(function callback(map) {
    const bounds = new window.google.maps.LatLngBounds();
    map.fitBounds(bounds);
    setMap(map);
  }, []);
  const onUnmount = useCallback(function callback(map) {
    setMap(null);
  }, []);

  // Link copied to clipboard helper function
  const handleCopyToClipboard = () => {
    toast('Link copied to clipboard!', {
      type: 'success',
      autoClose: 2000,
    });
  };

  let passportIdOrEmail = null;

  const validatePassportIdOrEmail = () => {
    passportIdOrEmail = appContext.state.user.passport_id;

    if (!passportIdOrEmail) {
      setErrorMessage('Passport Id or Email is required.');

      return false;
    } else {
      setErrorMessage('');

      return true;
    }
  };

  // Claim food sample helper function
  const handleClaimFoodSample = async (event) => {
    if (!event) {
      event = window.event;
    }

    event.preventDefault();

    const isValid = validatePassportIdOrEmail();

    if (props.passportId || isValid) {
      const url = '/food-sample-claim';

      const acc_token = await localStorage.getItem('access_token');

      const headers = { Authorization: `Bearer ${acc_token}` };

      const data = {
        food_sample_claim_user: props.passportId ? props.passportId : passportIdOrEmail,
        food_sample_id: foodSampleDetails.food_sample_id,
      };
      try {
        const response = await axios({ method: 'POST', url, headers, data });

        if (response && response.data && response.data.success) {
          setTimeout(() => {
            window.location.reload();
          }, 2000);

          toast(`Success! Thank you for claiming ${foodSampleDetails.title}!`, {
            type: 'success',
            autoClose: 2000,
          });

          setSubmitAuthDisabled(true);
          setErrorMessage('');
        } else if (
          response &&
          response.data &&
          !response.data.success &&
          appContext.state.user.passport_id
        ) {
          toast(response.data.message, {
            type: 'error',
            autoClose: 2000,
          });
        } else {
          setErrorMessage(response.data.message);
          setCanClaim(response.data.canClaim);
        }
      } catch (error) {
        toast('Error! Something went wrong!', {
          type: 'error',
          autoClose: 2000,
        });
      }
    }
  };

  // Google Maps code
  const mapStyles = {
    width: '100%',
    height: '50vh',
    border: '128px 64px 48px',
  };
  const destination = `${foodSampleDetails.address} ${foodSampleDetails.city} ${foodSampleDetails.state}`;
  const origin = { lat: latitude, lng: longitude };
  const travelMode = 'WALKING'; /* Walking mode on Google Maps */
  const fetchRoute = (response) => {
    if (response !== null) {
      if (response.status == 'OK') {
        if (!directions) {
          setDirections(response);
        }
      }
    } else {
      console.log(response);
    }
  };

  // Get food sample details helper function
  const fetchFoodDetails = async () => {
    try {
      const response = await axios({
        method: 'GET',
        url: `/food-sample/${props.match.params.foodSampleId}`,
      });

      return response;
    } catch (error) {
      return error.response;
    }
  };

  // Get Google Maps API key helper function
  const fetchGoogleMapsApiKey = async () => {
    try {
      const response = await axios({
        method: 'GET',
        url: `/food-sample/googleMaps/api`,
      });

      return response;
    } catch (error) {
      return error.response;
    }
  };

  // Mount Food Sample Details page
  useEffect(() => {
    window.scrollTo(0, 0);

    fetchGoogleMapsApiKey().then((response) => {
      setGoogleMapsApiKey(response.data);
    });

    fetchFoodDetails().then(({ data }) => {
      setFoodSampleDetails(data.details[0]);
      setFoodId(data.details[0].food_sample_id);
    });
  }, []);

  // Render Food Sample Details page
  return (
    <div>
      <Nav />

      <div className="experience-details">
        <div>
          <div className="row">
            <div className="col-lg-7 experience-details-images-content">
              <div className="row product-details-owner-information">
                <div className="col-lg-1 px-0">
                  {foodSampleDetails.profile_image_link ? (
                    <img
                      src={foodSampleDetails.profile_image_link}
                      alt={`${foodSampleDetails.first_name} ${foodSampleDetails.last_name}`}
                      className="product-details-owner-profile-picture"
                    />
                  ) : (
                    <span className="fas fa-user-circle fa-3x product-details-owner-default-picture" />
                  )}
                </div>
                <div className="col-lg-8 product-details-title-content">
                  <div className="product-details-title">{foodSampleDetails.title}</div>
                  {foodSampleDetails.business_name && (
                    <div className="text-left">
                      <span className="product-details-owner-by">by </span>
                      <Link
                        exact="true"
                        to={`/business/${foodSampleDetails.business_name}`}
                        className="product-details-owner-name"
                      >
                        {foodSampleDetails.business_name}
                      </Link>
                    </div>
                  )}
                </div>
                <div className="col-lg-3 px-0">
                  <div>
                    <CopyToClipboard text={window.location.href} onCopy={() => setCopied(true)}>
                      <button onClick={handleCopyToClipboard} className="share-btn">
                        Share&nbsp;<span className="fas fa-share"></span>
                      </button>
                    </CopyToClipboard>
                  </div>
                </div>
              </div>
              <div className="experience-details-images-section">
                {[foodSampleDetails.image_urls].length > 1 ? (
                  <ImageSlider images={foodSampleDetails.image_urls} />
                ) : (
                  <img src={[foodSampleDetails.image_urls][0]} alt={foodSampleDetails.title} />
                )}
              </div>

              <div className="experience-description-content">
                <div className="experience-description-sub-title">Origin/Nationality</div>
                <div>{foodSampleDetails.nationality}</div>
              </div>

              {foodSampleDetails.sample_size && (
                <div className="experience-description-content">
                  <div className="experience-description-sub-title">Size of Sample</div>
                  <div>{foodSampleDetails.sample_size}</div>
                </div>
              )}

              {/* <div className="row experience-description-content">
                <div className="experience-description-sub-title">
                  Ingredients
                </div>
                <div>{foodSampleDetails.ingredients}</div>
              </div> */}

              {foodSampleDetails.spice_level && (
                <div className="experience-description-content">
                  <div className="experience-description-sub-title">Spice Level</div>
                  <div>{foodSampleDetails.spice_level}</div>
                </div>
              )}

              <div className="experience-description-content">
                <div className="experience-description-sub-title">Description</div>
                <div>{foodSampleDetails.description}</div>
              </div>
            </div>

            <div className="col-lg-5 experience-details-buy-content">
              <div className="experience-details-buy-section">
                {/* <div className="row product-details-owner-information">
                  <div className="col-25">
                    {foodSampleDetails.profile_image_link ? (
                      <img
                        src={foodSampleDetails.profile_image_link}
                        alt={`${foodSampleDetails.first_name} ${foodSampleDetails.last_name}`}
                        className="product-details-owner-profile-picture"
                      />
                    ) : (
                      <span className="fas fa-user-circle fa-3x product-details-owner-default-picture" />
                    )}
                  </div>
                  <div className="col-75">
                    <div className="product-details-title">
                      {foodSampleDetails.title}
                    </div>
                    {foodSampleDetails.business_name && (
                      <div className="text-left">
                        <span className="product-details-owner-by">by </span>
                        <Link
                          exact="true"
                          to={`/business/${foodSampleDetails.business_name}`}
                          className="product-details-owner-name"
                        >
                          {foodSampleDetails.business_name}
                        </Link>
                      </div>
                    )}
                  </div>
                </div> */}

                <div>
                  <div className="pb-2">
                    <div className="experience-details-sub-title">Address</div>
                    <div>{`${foodSampleDetails.address}, ${foodSampleDetails.city}, ${foodSampleDetails.state} ${foodSampleDetails.postal_code}`}</div>
                  </div>
                  <div className="pb-2">
                    <div className="experience-details-sub-title">Date</div>
                    <div>{`${formatDate(foodSampleDetails.start_date)} to ${formatDate(
                      foodSampleDetails.end_date,
                    )}`}</div>
                  </div>
                  {/* <div className="pb-2">
                    <div className="experience-details-sub-title">Time</div> */}
                  {/* {formatMilitaryToStandardTime(foodSampleDetails.start_time)} */}
                  {/* <div>
                      {`${formatMilitaryToStandardTime(
                        foodSampleDetails.start_time
                      )} to ${formatMilitaryToStandardTime(
                        foodSampleDetails.end_time
                      )}`}
                    </div> */}
                  {/* </div> */}
                  {foodSampleDetails.is_available_on_monday ||
                  foodSampleDetails.is_available_on_tuesday ||
                  foodSampleDetails.is_available_on_wednesday ||
                  foodSampleDetails.is_available_on_thursday ||
                  foodSampleDetails.is_available_on_friday ||
                  foodSampleDetails.is_available_on_saturday ||
                  foodSampleDetails.is_available_on_sunday ? (
                    <div className="pb-2">
                      <div className="passport-details-sub-title">Days Available</div>
                      {foodSampleDetails.is_available_on_monday && <div>Monday</div>}
                      {foodSampleDetails.is_available_on_tuesday && <div>Tuesday</div>}
                      {foodSampleDetails.is_available_on_wednesday && <div>Wednesday</div>}
                      {foodSampleDetails.is_available_on_thursday && <div>Thursday</div>}
                      {foodSampleDetails.is_available_on_friday && <div>Friday</div>}
                      {foodSampleDetails.is_available_on_saturday && <div>Saturday</div>}
                      {foodSampleDetails.is_available_on_sunday && <div>Sunday</div>}
                    </div>
                  ) : null}
                  <div className="pb-2">
                    <div className="experience-details-sub-title">Quantity Available</div>
                    <div>{foodSampleDetails.quantity}</div>
                  </div>
                  <div className="pb-4">
                    <div className="experience-details-sub-title">How to Reserve This Sample</div>
                    <div>
                      Click Reserve Button
                      <br />
                      Find code in your email
                      <br />
                      Show code at the restaurant
                    </div>
                  </div>

                  {foodSampleDetails.festival_id === 1 ||
                  (userRole &&
                    userRole.includes(
                      'ADMIN',
                    )) ? null : foodSampleDetails.food_sample_creater_user_id ===
                    appContext.state.user.id ? (
                    <div>You own this item</div>
                  ) : !appContext.state.user.id ? (
                    <Link exact="true" to="/login" className="buy-now-btn">
                      Reserve Free Tasting
                    </Link>
                  ) : (
                    <div
                      onClick={
                        foodSampleDetails.quantity > 0 &&
                        foodSampleDetails.food_sample_creater_user_id !== appContext.state.user.id
                          ? appContext.state.user.passport_id
                            ? handleClaimFoodSample
                            : authModalContext.closeModal()
                          : null
                      }
                      disabled={submitAuthDisabled}
                      className={
                        foodSampleDetails.quantity > 0 &&
                        foodSampleDetails.food_sample_creater_user_id !== appContext.state.user.id
                          ? 'mt-3 call-to-action-btn'
                          : 'mt-3 btn btn-dark call-to-action-btn'
                      }
                    >
                      Reserve Free Tasting
                    </div>
                  )}
                  <div>
                    <Modal
                      isOpen={contactOpened}
                      onRequestClose={() => setContactOpened(false)}
                      ariaHideApp={false}
                      className="contact-modal"
                    >
                      <div className="text-right">
                        <button
                          aria-label="Close Contact Information Modal"
                          onClick={() => setContactOpened(false)}
                          className="fas fa-times fa-2x close-modal"
                        ></button>
                      </div>

                      <div className="modal-title">
                        {`Contact Information on ${foodSampleDetails.title}`}
                      </div>

                      <div>
                        Phone:{' '}
                        <a href={`tel:${foodSampleDetails.phone_number}`} className="external-link">
                          {foodSampleDetails.phone_number}
                        </a>
                      </div>
                      <div>
                        Email:{' '}
                        <a href={`mailto:${foodSampleDetails.email}`} className="external-link">
                          {foodSampleDetails.email}
                        </a>
                      </div>
                    </Modal>
                  </div>

                  <div className="experience-description-content-responsive">
                    <div className="experience-description-sub-title">Origin/Nationality</div>
                    <div>{foodSampleDetails.nationality}</div>
                  </div>

                  {foodSampleDetails.sample_size && (
                    <div className="experience-description-content-responsive">
                      <div className="experience-description-sub-title">Size of Sample</div>
                      <div>{foodSampleDetails.sample_size}</div>
                    </div>
                  )}

                  {/* <div className="experience-description-content-responsive">
                    <div className="experience-description-sub-title">
                      Ingredients
                    </div>
                    <div>{foodSampleDetails.ingredients}</div>
                  </div> */}

                  {foodSampleDetails.spice_level && (
                    <div className="experience-description-content-responsive">
                      <div className="experience-description-sub-title">Spice Level</div>
                      <div>{foodSampleDetails.spice_level}</div>
                    </div>
                  )}

                  <div className="experience-description-content-responsive">
                    <div className="experience-description-sub-title">Description</div>
                    <div>{foodSampleDetails.description}</div>
                  </div>
                </div>
              </div>
            </div>

            {isMapReady ? (
              <div id="google-maps">
                Directions to {foodSampleDetails.business_name}
                {/* <LoadScript > */}
                <GoogleMap
                  googleMapsApiKey={googleMapsApiKey}
                  google={props.google}
                  zoom={10}
                  mapContainerStyle={mapStyles}
                  onLoad={handleLoad}
                  onUnmount={onUnmount}
                >
                  {destination !== '' && origin !== '' && (
                    <DirectionsService
                      options={{
                        destination: destination,
                        origin: origin,
                        travelMode: travelMode,
                      }}
                      callback={fetchRoute}
                    ></DirectionsService>
                  )}
                  {directions && (
                    <DirectionsRenderer options={{ directions: directions }}></DirectionsRenderer>
                  )}
                </GoogleMap>
                {/* </LoadScript> */}
              </div>
            ) : // <Spinner animation="border" role="status">
            //   <span className="sr-only">Loading...</span>
            // </Spinner>
            null}
          </div>
        </div>
      </div>

      <BannerFooter />

      <GoToTop />
    </div>
  );
};

export default FoodSampleDetailsPage;
