// Libraries
import React, { useState, useEffect, useContext } from 'react';
import axios from 'axios';
import useInfiniteScroll from 'react-infinite-scroll-hook';
import { AppContext } from '../../ContextProvider/AppProvider';
import moment, { now } from 'moment';
import { formatDate } from '../Functions/Functions';
// import jsPDF from 'jspdf';
// import autoTable from 'jspdf-autotable';

// Components
import Nav from '../Navbar/Nav';
// import PastFestivalsRow from './PastFestivalsRow/PastFestivalsRow';
// import CuisineRow from './CuisineRow/CuisineRow';
// import FoodTypeRow from './FoodTypeRow/FoodTypeRow';
// import AllergiesRow from './AllergiesRow/AllergiesRow';
import GoTop from '../Shared/GoTop';

// Styling
import './Passport.scss';
import 'react-datepicker/dist/react-datepicker.css';

const Passport = (props) => {
  // To use the JWT credentials
  const appContext = useContext(AppContext);

  return <div className="container"></div>;
};

export default Passport;
