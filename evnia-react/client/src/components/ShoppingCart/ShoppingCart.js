// Libraries
import React from 'react';
import { connect } from 'react-redux';
import * as shoppingCartActions from '../../redux/shoppingCart/actions';

const ShoppingCart = (props) => {
  const items = (items1) => {
    return items1.map((item) => (
      <div className="card" key={item.id}>
        <div className="card-image">
          <img src={item.img} alt={item.title} />
          <span className="card-title">{item.title}</span>
          <span
            to="/"
            onClick={() => {
              props.addToCart(item.type, item.id);
            }}
            className="btn-floating halfway-fab waves-effect waves-light red"
          >
            <i className="material-icons">add</i>
          </span>
        </div>

        <div className="card-content">
          <p>{item.desc}</p>
          <p>
            <b>Price: {item.price}$</b>
          </p>
        </div>
      </div>
    ));
  };

  return (
    <div className="container">
      <h3 className="center">Our items</h3>
      <div className="box">{items(props.items)}</div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    items: state.items,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    addToCart: (type, id) => {
      dispatch(shoppingCartActions.addToCart('', 'id'));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ShoppingCart);
