// Libraries
import React, { useEffect } from 'react';

// Components
import Nav from '../Navbar/Nav';
import HostButtonApplyForm from './ApplyForm/HostButtonApplyForm';

// Styling
import './Apply.scss';
import { Link } from 'react-router-dom';

const Apply = (props) => {
  // Source tracking on new sign ups
  const source = new URLSearchParams(props.location.search).get('source');
  if (source) {
    if (!localStorage.getItem('source')) {
      localStorage.setItem('source', source);
    }
  }

  // Mount Host page
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  // Render Host page
  return (
    <div>
      <div className="become-host-form">
        <Nav />
        <div className="welcome-paragraph">
          Tasttlig is the largest multicultural food tasting festival platform in the world. We have
          over 40,000 visitors during the months of our festivals. We send our guests to you to
          taste your food. For each food sample you give out, we pay you $2.00. Your total samples
          sold are deposited to your bank account at the end of each festival. Tasttlig festival
          happens four times a year in your city. To become a vendor in our festival, please
          complete the form below.{' '}
        </div>
        <div className="apply-to-host-navigation">
          <span className="apply-to-host-navigation-spacing"></span>
          <Link exact="true" to="/complete-profile/business" className="continue-btn" />
        </div>
      </div>
    </div>
  );
};

export default Apply;
