// Libraries
import React, { useEffect } from 'react';

// Components
import Nav from '../Navbar/Nav';
import HostButtonApplyForm from './ApplyForm/HostButtonApplyForm';
import VendButtonForm from './ApplyForm/VendButtonApplyForm';

// Styling
import './Apply.scss';

const Apply = (props) => {
  // Source tracking on new sign ups
  const source = new URLSearchParams(props.location.search).get('source');
  if (source) {
    if (!localStorage.getItem('source')) {
      localStorage.setItem('source', source);
    }
  }

  // Mount Host page
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  // Render Host page
  return (
    <div>
      <div>
        <Nav />
        <VendButtonForm history={props.history} />
      </div>
    </div>
  );
};

export default Apply;
