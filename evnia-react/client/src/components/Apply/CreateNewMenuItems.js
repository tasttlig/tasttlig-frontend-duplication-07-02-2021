// Libraries
import React, { useEffect } from 'react';

// Components
import Nav from '../Navbar/Nav';
import CreateMenuItemsForm from './ApplyForm/CreateMenuItemsForm';

// Styling
import './Apply.scss';

const CreateNewMenuItems = (props) => {
  // Source tracking on new sign ups
  const source = new URLSearchParams(props.location.search).get('source');
  if (source) {
    if (!localStorage.getItem('source')) {
      localStorage.setItem('source', source);
    }
  }

  // Mount Host Festival page
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  // Render Host Festival page
  return (
    <div>
      <div className="become-host-form">
        <Nav />
        <CreateMenuItemsForm history={props.history} />
      </div>
    </div>
  );
};

export default CreateNewMenuItems;
