// Libraries
import React, { useEffect } from 'react';

// Components
import Nav from '../Navbar/Nav';
import CreateNewUserForm from './ApplyForm/ApplyForm';

// Styling
import './Apply.scss';

const CreateNewUser = (props) => {
  // Mount Create New User page
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  // Render Create New User page
  return (
    <div>
      <Nav />
      <CreateNewUserForm history={props.history} />
    </div>
  );
};

export default CreateNewUser;
