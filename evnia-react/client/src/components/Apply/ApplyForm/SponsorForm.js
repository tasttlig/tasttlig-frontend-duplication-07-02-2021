// Libraries
import React, { Component } from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import { AppContext } from '../../../ContextProvider/AppProvider';
import { Link } from 'react-router-dom';
import Modal from 'react-modal';

// Components
import PersonalInfoForm from './FormSteps/PersonalInfoForm';
import BusinessInfoForm from './FormSteps/BusinessInfoForm';
import SponsorDetailsForm from './FormSteps/SponsorDetailsForm';
import FormReview from './FormSteps/FormReview';

// Styling
import 'react-toastify/dist/ReactToastify.css';
import { useHistory } from 'react-router-dom';
import './SponsorForm.scss';

toast.configure();

export default class SponsorForm extends Component {
  static contextType = AppContext;

  // Set initial state
  state = {
    step: 0,

    //business created modal
    isBusinessCreatedOpen: false,
    initialFestivalSelection: '',
    initialFestivalTitle: '',

    // Personal information form fields
    first_name: '',
    last_name: '',
    email: '',
    phone_number: '',
    is_sponsor: true,

    // Business information form fields
    business_name: '',
    address_line_1: '',
    address_line_2: '',
    business_city: '',
    state: '',
    postal_code: '',
    country: '',

    logo_link: '',
    business_street_number: '',
    business_street_name: '',

    //business details
    business_type: '',
    food_business_type: '',
    business_registered: '',
    retail_business: '',
    business_registered: '',
    business_unit: '',
    food_handlers_certificate: '',

    submitAuthDisabled: false,
  };

  // Set local storage for Sponsor multi-step form helper function
  UNSAFE_componentWillMount = () => {
    const sponsorFormData = JSON.parse(localStorage.getItem('sponsorFormData'));
    const initialFestival = JSON.parse(localStorage.getItem('initialFestivalSelection'));

    if (sponsorFormData) {
      Object.keys(this.state).forEach((key) => {
        const value = sponsorFormData[key];

        if (value) {
          this.setState({
            [key]: value,
          });
        }
      });
    }

    if (this.state.email === '') {
      this.setState({
        email: this.context.state.user.email,
      });
    }

    if (initialFestival) {
      this.setState({ initialFestivalSelection: initialFestival });
    }
  };

  // Proceed to next step helper function
  nextStep = () => {
    window.scrollTo(0, 0);

    this.setState({
      step: this.state.step + 1,
    });
  };

  // Go back to previous step helper function
  prevStep = () => {
    window.scrollTo(0, 0);

    this.setState({ step: this.state.step - 1 });
  };

  // Update Sponsor multi-step form data helper function
  update = (data) => {
    const sponsorFormData = JSON.parse(localStorage.getItem('sponsorFormData')) || {};

    localStorage.setItem('sponsorFormData', JSON.stringify({ ...sponsorFormData, ...data }));

    Object.keys(data).forEach((key) => {
      this.setState({ [key]: data[key] });
    });
  };

  // Submit Sponsor multi-step form helper function
  handleSubmitApplication = async (event) => {
    event.preventDefault();

    const url = '/user/sponsor';

    const { submitAuthDisabled, ...data } = this.state;

    try {
      const response = await axios({ method: 'POST', url, data });

      if (response && response.data && response.data.success) {
        toast(`Success! Thank you for submitting your application!`, {
          type: 'success',
          autoClose: 2000,
        });

        this.setState({ submitAuthDisabled: true });

        this.setState({ isBusinessCreatedOpen: true });
        //choose sponsorship package
        setTimeout(() => {
          //window.location.href = "/"; //sponsor-products-and-services";
          /* const historyString = localStorage.getItem('history');
          const history = eval('(' + historyString + ')');
          history.push("/sponsor-products-and-services"); */
          //localStorage.removeItem("history");
        }, 2000);
        //localStorage.removeItem("sponsorFormData");
      }
    } catch (error) {
      toast('Error! Something went wrong!', {
        type: 'error',
        autoClose: 2000,
      });
    }
  };

  // Get Sponsor multi-step form steps helper function
  getSteps = () => {
    let steps = [];

    if (
      !this.context.state.signedInStatus ||
      (this.context.state.user.role && this.context.state.user.role.includes('ADMIN'))
    ) {
      steps = [...steps, PersonalInfoForm];
    }

    steps = [...steps, BusinessInfoForm, SponsorDetailsForm, FormReview];

    return steps;
  };

  // Mount Sponsor multi-step form
  componentDidMount = () => {
    window.scrollTo(0, 0);
  };

  // Render Sponsor multi-step form
  render = () => {
    const { step, ...values } = this.state;
    const Step = this.getSteps()[step];

    return (
      <>
        <Step
          nextStep={this.nextStep}
          prevStep={this.prevStep}
          update={this.update}
          values={values}
          handleSubmitApplication={this.handleSubmitApplication}
        />
        <Modal
          isOpen={this.state.isBusinessCreatedOpen}
          onRequestClose={() => this.setState({ isBusinessCreatedOpen: false })}
          ariaHideApp={true}
          className="modals"
        >
          <div className="bg-danger p-5 mb-4" closeButton>
            <div className="text-center text-white">
              Your business passport has been created successfully
            </div>
          </div>
          <div>
            <div className="btn btn-danger text-dark">
              <Link
                exact="true"
                to={{
                  pathname: '/sponsor-plan',
                  festivalId: this.state.initialFestivalSelection
                    ? this.state.initialFestivalSelection
                    : null,
                }}
                className="text-dark"
              >
                Sponsor Tasttlig
              </Link>
            </div>

            <div className="btn btn-danger text-dark">
              <Link
                exact="true"
                to={{
                  pathname: '/',
                }}
                className="text-dark"
              >
                Explore Tasttlig
              </Link>
            </div>
          </div>
        </Modal>
      </>
    );
  };
}
