// Libraries
import React, { useState } from 'react';
// import { Progress } from "react-sweet-progress";

// Components
import { DateInput, FileUpload, Form } from '../../../EasyForm';
import axios from 'axios';

// Styling
// import "react-sweet-progress/lib/style.css";

const DocumentForm = (props) => {
  const { nextStep, prevStep, update, values, readMode } = props;

  const [foodHandlerCert, setFoodHandlerCert] = useState(values.food_handler_certificate);
  const [dineSafeCert, setDineSafeCert] = useState(values.dine_safe_certificate);
  const [healthSafetyCert, setHealthSafetyCert] = useState(values.health_safety_certificate);
  const [insurance, setInsurance] = useState(values.insurance);
  const [governmentId, setGovernmentId] = useState(values.government_id);

  const updateDocuments = async (data) => {
    const response = await axios({
      method: 'PUT',
      url: '/user/updateDocuments',
      data: {
        ...data,
        email: values.email,
      },
    });

    return response.data;
  };

  const onSubmit = async (data) => {
    update(data);
    await updateDocuments(data);
    nextStep();
  };

  let currentDate = () => {
    const date = new Date();

    return date.getFullYear() + '-' + ('0' + date.getMonth()).slice(-2) + '-' + date.getDate();
  };

  const FoodHandlerCert = () => (
    <>
      <FileUpload
        name="food_handler_certificate"
        label="Food Handler Certificate"
        accept="application/pdf"
        dir="food-handler-certificates"
        defaultValue={foodHandlerCert}
        onChange={(val) => {
          setFoodHandlerCert(val);
          update({
            food_handler_certificate: val,
          });
        }}
        required
      />

      {foodHandlerCert && (
        <>
          <DateInput
            name="food_handler_certificate_date_of_issue"
            label="Date of Issue"
            maxDate={currentDate()}
            required
          />
          <DateInput
            name="food_handler_certificate_date_of_expired"
            label="Expiry Date"
            maxDate={currentDate()}
            required
          />
        </>
      )}
    </>
  );

  const DineSafeCert = () => (
    <>
      <FileUpload
        name="dine_safe_certificate"
        label="DineSafe Certificate (Optional)"
        accept="application/pdf"
        dir="dine-safe-certificates"
        onChange={(val) => {
          setDineSafeCert(val);
          update({
            dine_safe_certificate: val,
          });
        }}
      />

      {dineSafeCert && (
        <>
          <DateInput
            name="dine_safe_certificate_date_of_issue"
            label="Date of Issue (Optional)"
            maxDate={currentDate()}
          />
          <DateInput
            name="dine_safe_certificate_date_of_expired"
            label="Expiry Date (Optional)"
            maxDate={currentDate()}
          />
        </>
      )}
    </>
  );

  const HealthSafetyCert = () => (
    <>
      <FileUpload
        name="health_safety_certificate"
        label="Health Safety Certificate"
        accept="application/pdf"
        dir="health-safety-certificates"
        onChange={(val) => {
          setHealthSafetyCert(val);
          update({
            health_safety_certificate: val,
          });
        }}
        required
      />

      {healthSafetyCert && (
        <>
          <DateInput
            name="health_safety_certificate_date_of_issue"
            label="Date of Issue"
            maxDate={currentDate()}
            required
          />
          <DateInput
            name="health_safety_certificate_date_of_expired"
            label="Expiry Date"
            maxDate={currentDate()}
            required
          />
        </>
      )}
    </>
  );

  const Insurance = () => (
    <>
      <FileUpload
        name="insurance"
        label="Insurance (Optional)"
        accept="application/pdf"
        dir="food-business-insurances"
        onChange={(val) => {
          setInsurance(val);
          update({
            insurance: val,
          });
        }}
      />

      {insurance && (
        <>
          <DateInput
            name="insurance_date_of_issue"
            label="Date of Issue"
            maxDate={currentDate()}
            required
          />
          <DateInput
            name="insurance_date_of_expired"
            label="Expiry Date"
            maxDate={currentDate()}
            required
          />
        </>
      )}
    </>
  );

  const GovernmentId = () => (
    <>
      <FileUpload
        name="government_id"
        label="Government ID"
        dir="government-id"
        onChange={(val) => {
          setGovernmentId(val);
          update({
            government_id: val,
          });
        }}
        required
      />

      {governmentId && (
        <>
          <DateInput
            name="government_id_date_of_issue"
            label="Date of Issue"
            maxDate={currentDate()}
            required
          />
          <DateInput
            name="government_id_date_of_expired"
            label="Expiry Date"
            maxDate={currentDate()}
            required
          />
        </>
      )}
    </>
  );

  return (
    <div>
      {/* {!readMode && <Progress />} */}
      <div className={!readMode ? 'apply-to-host' : ''}>
        {!readMode ? (
          <h1 className="apply-to-host-step-name">Documents</h1>
        ) : (
          <>
            <h6>Documents</h6>
            <hr />
          </>
        )}

        <Form data={values} onSubmit={onSubmit} readMode={readMode}>
          {values.business_category === 'Food' && (
            <>
              <FoodHandlerCert />
            </>
          )}

          {(values.service_provider === 'Restaurant' ||
            values.service_provider === 'Food Truck') && (
            <>
              <DineSafeCert />
              <HealthSafetyCert />
            </>
          )}

          {values.business_category === 'Transportation' && <GovernmentId />}

          <Insurance />

          {!readMode && (
            <div className="apply-to-host-navigation">
              <span onClick={prevStep} disabled={values.submitAuthDisabled} className="back-btn">
                Back
              </span>
              <input
                type="submit"
                value="Continue"
                disabled={values.submitAuthDisabled}
                className="continue-btn"
              />
            </div>
          )}
        </Form>
      </div>
    </div>
  );
};

export default DocumentForm;
