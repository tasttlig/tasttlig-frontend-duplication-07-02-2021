// Libraries
import React, { useState, useContext } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
// import { Progress } from "react-sweet-progress";
import { AppContext } from '../../../../ContextProvider/AppProvider';
import { toast } from 'react-toastify';

// Components
import { Checkbox, Form, Input, CheckboxGroup, Textarea } from '../../../EasyForm';

import BackgroundImage from '../../../../assets/images/background-vendor-image.png';
import BackgroundGirlImage from '../../../../assets/images/background-vendor-girl-image.png';
import tasttligLogoBlack from '../../../../assets/images/tasttlig-logo-black.png';
import CutoffImage from '../../../../assets/images/vendor-page-cutoff-image.png';
import SmallVendorImage from '../../../../assets/images/small-vendor-image.png';

// Styling
// import "react-sweet-progress/lib/style.css";

const VendorDetailsForm = (props) => {
  const { nextStep, update, values, readMode } = props;
  let data = {};
  // Set initial state
  const [existingUser, setExistingUser] = useState(null);
  const [submitAuthDisabled, setSubmitAuthDisabled] = useState(false);
  const [characters, setCharacters] = useState(250);
  const [other_info, setOther_info] = useState('');
  const [load, setLoad] = useState(false);

  // To use the JWT credentials
  const appContext = useContext(AppContext);
  console.log(appContext);

  const onSubmit = async (data) => {
    data.first_name = appContext.state.user.first_name;
    data.last_name = appContext.state.user.last_name;
    data.email = appContext.state.user.email;
    values.email = appContext.state.user.email;
    //console.log(values);
    try {
      const response = await axios({
        method: 'POST',
        url: `/vendor/${appContext.state.user.id}`,
        data,
      });

      console.log(response, 'vendor response');

      if (response && response.data && response.data.success) {
        setTimeout(() => {
          update(data);
          console.log('values', values);
          console.log('data', data);
          nextStep();
        }, 2000);
        toast(`Success! Thank you for submitting your vendor details!`, {
          type: 'success',
          autoClose: 2000,
        });
      } else {
        console.log('error');
      }
    } catch (error) {
      console.log(error);
      toast('Error! Something went wrong!', {
        type: 'error',
        autoClose: 2000,
      });
    }
  };
  const handleOnChange = (event) => {
    if (characters !== 0) {
      setOther_info(event.target.value);
      setCharacters(250 - event.target.value.length);
    }
  };

  const DescriptionInput = React.memo((props) => {
    return (
      <Textarea
        name="other_info"
        /* value={other_info} */
        type="text"
        label={`Any other information you want to share with us?`}
        /* onChange={handleOnChange} */
      />
    );
  });
  return (
    <div className="container-xxl">
      <div className="row">
        <div className="col-4 left-side-vendor">
          <div className="row vendor-tasttlig-container">
            {/* <button onClick={() => window.location.href = "/"}> */}
            <img
              className="col-md-6 tasttlig-logo-vendor"
              src={tasttligLogoBlack}
              onClick={() => (window.location.href = '/')}
            ></img>
            <div
              className="col-4 tasttlig-vendor-title"
              onClick={() => (window.location.href = '/')}
            >
              Vendor
            </div>
            {/* </button> */}
          </div>
        </div>
        <div className="col-8 right-side-vendor">
          <div className="container vendor-details-container">
            <div className="vendor-profile-name-picture-container">
              <div className="user-full-name">
                {appContext.state.user.first_name} {appContext.state.user.last_name}
              </div>
              {appContext.state.user.profile_image_link ? (
                <img
                  src={appContext.state.user.profile_image_link}
                  alt={`${appContext.state.user.first_name} ${appContext.state.user.last_name}`}
                  onLoad={() => setLoad(true)}
                  className={load ? 'main-navbar-account-profile-picture' : 'loading-image'}
                />
              ) : (
                <span className="fas fa-user-circle fa-3x main-navbar-account-default-picture"></span>
              )}
            </div>
            <div className="container title-subtitle-container">
              <div className="vendor-details-title">Vendor Details</div>
              <div className="sub-title-vendor">Make 50K per year by vending at Tasttlig</div>
            </div>
            <div className="container vendor-details-form">
              <Form data={data} onSubmit={onSubmit}>
                <div className="container question-answer-form">
                  <div className="row">
                    <CheckboxGroup
                      name="cooking_area"
                      label="Where do you cook your meals?"
                      options={[
                        ['Restaurant', 'restaurant'],
                        ['Truck', 'truck'],
                        ['Commerical Kitchen', 'commercial_kitchen'],
                        ['Other', 'other'],
                      ]}
                      disabled={submitAuthDisabled}
                      className="cooking-meals-checkboxes"
                      required
                    />
                  </div>
                </div>
                <div className="container question-answer-form">
                  <div className="row">
                    <Input
                      name="food_temperature_maintenance_plan"
                      label="How do you plan to keep your food at the right temperature?"
                      type="text"
                      required
                    />
                  </div>
                </div>
                <div className="container question-answer-form">
                  <div className="row">
                    <CheckboxGroup
                      name="equipment_required"
                      label="What other equipment do you require to vend?"
                      options={[
                        ['Tent', 'restaurant'],
                        ['Table', 'truck'],
                        ['Electricity', 'electricity'],
                        ['Commercial Kitchen', 'commercial_kitchen'],
                        ['Utensils', 'utensils'],
                        ['Transporation', 'transportation'],
                        ['Other', 'other'],
                      ]}
                      disabled={submitAuthDisabled}
                      required
                    />
                  </div>
                </div>
                <div className="container question-answer-form">
                  <div className="row">
                    {/*                     <Textarea
                      name="other_info"
                      value={other_info}
                      type="text"
                      label={`Any other information you want to share with us? ${characters}`}
                      onChange={handleOnChange}
                    /> */}
                    <DescriptionInput></DescriptionInput>
                  </div>
                </div>
                <div className="vendor-details-button-container">
                  <button type="submit" className="vendor-details-button">
                    {' '}
                    Proceed
                  </button>
                  <button onClick={() => nextStep()} className="vendor-details-button">
                    {' '}
                    Skip
                  </button>
                </div>
              </Form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default VendorDetailsForm;
