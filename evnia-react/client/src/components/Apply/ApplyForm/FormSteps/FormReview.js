// Libraries
import React, { useContext, useState } from 'react';
// import { Progress } from "react-sweet-progress";
import { AppContext } from '../../../../ContextProvider/AppProvider';

// Components
import PersonalInfoForm from './PersonalInfoForm';
import BusinessInfoForm from './BusinessInfoForm';
import ApplyToHostForm from './ApplyToHostForm';
import ProductForm from './ProductForm';
import SponsorDetailsForm from './SponsorDetailsForm';
import HostFoodSampleForm from './HostFoodSampleForm';
import AddProductsForm from './AddProductsForm';
import HostBusinessInfo from './HostBusinessInfo';
import HostPersonalInfo from './HostPersonalInfo';
// import ServicesForm from "./ServicesForm";
// import PaymentInfoForm from "./PaymentInfoForm";
// import DocumentForm from "./DocumentForm";
// import SocialProofForm from "./SocialProofForm";
// import ResidentialAddressForm from "./ResidentialAddressForm";
// import ApplyToCookForm from "./ApplyToCook";
// import AssetsForm from "./AssetsForm";
// import PromotionsForm from "./PromotionsForm";

// Styling
// import "react-sweet-progress/lib/style.css";

const FormReview = (props) => {
  console.log('props from form review: ', props);
  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const userRole = appContext.state.user.role;

  const { handleSubmitApplication, prevStep, values, readMode } = props;
  const standAloneForm = props.standalone;
  const [openCompleteHost, setOpenCompleteHost] = useState(false);
  const [openCompleteSample, setOpenCompleteSample] = useState(false);
  const [openHostBusinessInfo, setOpenHostBusinessInfo] = useState(false);
  const [openHostPersonalInfo, setOpenHostPersonalInfo] = useState(false);

  const HostAndSampleForms = () => (
    <div className="text-center">
      <div className="row">
        <div className="col-md-6 do-you-have-a-restaurant-yes">
          <div
            onClick={() => {
              setOpenCompleteSample(false);
              setOpenCompleteHost(!openCompleteHost);
            }}
            className="do-you-have-a-restaurant-yes-btn"
          >
            Click for Host Information
          </div>
        </div>
        <div className="col-md-6 do-you-have-a-restaurant-yes">
          <div
            onClick={() => {
              setOpenCompleteHost(false);
              setOpenCompleteSample(!openCompleteSample);
            }}
            className="do-you-have-a-restaurant-no-btn"
          >
            Click for Host food Sample
          </div>
        </div>
      </div>
    </div>
  );

  // Render Form Review part of multi-step form
  return (
    <div>
      {/* {!readMode && <Progress />} */}
      <div className={!readMode ? 'apply-to-host' : ''}>
        {!readMode && <h1 className="apply-to-host-step-name">Preview</h1>}

        <div className="mx-auto text-left">
          {(!appContext.state.signedInStatus ||
            (userRole &&
              userRole.includes('ADMIN') &&
              window.location.href !==
                `${process.env.REACT_APP_PROD_BASE_URL}/create-food-samples` &&
              window.location.href !==
                `${process.env.REACT_APP_TEST_BASE_URL}/create-food-samples` &&
              window.location.href !==
                `${process.env.REACT_APP_DEV_BASE_URL}/create-food-samples`) ||
            readMode) &&
          !standAloneForm ? (
            <div className="mb-5">
              {/* //   <PersonalInfoForm
            //     values={values}
            //     sponsor={props.sponsor}
            //     readMode
            //   /> */}
              <div className="row">
                <div className="col-md-6 do-you-have-a-restaurant-yes">
                  <div
                    onClick={() => {
                      setOpenHostBusinessInfo(false);
                      setOpenHostPersonalInfo(!openHostPersonalInfo);
                    }}
                    className="do-you-have-a-restaurant-yes-btn"
                  >
                    Click for Host's personal information
                  </div>
                </div>
                <div className="col-md-6 do-you-have-a-restaurant-yes">
                  <div
                    onClick={() => {
                      setOpenHostPersonalInfo(false);
                      setOpenHostBusinessInfo(!openHostBusinessInfo);
                    }}
                    className="do-you-have-a-restaurant-no-btn"
                  >
                    Click for Host's business personal information
                  </div>
                </div>
              </div>
            </div>
          ) : null}

          {/* <div className="mb-5">
            <ResidentialAddressForm values={values} readMode />
          </div> */}

          {/* {values.has_business && ( */}

          {values.isCreateFoodSamples ? (
            <div className="mb-5">
              <ProductForm values={values} readMode />
            </div>
          ) : (values.is_host === 'no' || !values.is_host || props.sponsor) &&
            !values.vend_button_festival_form &&
            !standAloneForm ? (
            <div className="mb-5">
              <BusinessInfoForm values={values} sponsor={props.sponsor} readMode />
            </div>
          ) : null}
          {/* )} */}

          {/* {!values.is_host || (values.has_business &&
            values.business_category !== "Party Suppliers" &&
            values.business_category !== "Transportation") ? (
              <div className="mb-5">
                <ProductForm values={values} readMode />
              </div>
            ) : null} */}

          {/* {values.menu_list && (
            <div className="mb-5">
              <PromotionsForm values={values} readMode />
            </div>
          )} */}

          {/* {values.service_provider === "Restaurant" && (
            <div className="mb-5">
              <AssetsForm values={values} readMode />
            </div>
          )} */}

          {/* {values.business_category !== "MC" && (
            <div className="mb-5">
              <ServicesForm values={values} readMode />
            </div>
          )} */}

          {values.is_host === 'yes' && <HostAndSampleForms />}
          {openCompleteHost ? (
            <div className="mb-5">
              <ApplyToHostForm values={values} readMode />
            </div>
          ) : (
            <div className="col-md-4 p-2">
              <p></p>
            </div>
          )}
          {openHostBusinessInfo ? (
            <div className="mb-5">
              <HostBusinessInfo values={values} readMode />
            </div>
          ) : (
            <div className="col-md-4 p-2">
              <p></p>
            </div>
          )}
          {openHostPersonalInfo ? (
            <div className="mb-5">
              <HostPersonalInfo values={values} readMode />
            </div>
          ) : (
            <div className="col-md-4 p-2">
              <p></p>
            </div>
          )}
          {openCompleteSample ? (
            <div className="mb-5">
              <HostFoodSampleForm values={values} readMode />
            </div>
          ) : (
            <div className="col-md-4 p-2">
              <p></p>
            </div>
          )}

          {values.isCreateFoodProducts && (
            <div className="mb-5">
              <AddProductsForm values={values} readMode />
            </div>
          )}

          {values.is_sponsor || props.sponsor ? (
            <div className="mb-5">
              <SponsorDetailsForm values={values} sponsor={props.sponsor} readMode />
            </div>
          ) : null}

          {/* <div className="mb-5">
            <ApplyToCookForm values={values} readMode />
          </div> */}

          {/* <div className="mb-5">
            <PaymentInfoForm values={values} readMode />
          </div> */}

          {/* {values.business_category === "Food" ||
          values.business_category === "Transportation" && (
            <div className="mb-5">
              <DocumentForm values={values} readMode />
            </div>
          )} */}

          {/* {values.yelp_review && (
            <div className="mb-5">
              <SocialProofForm values={values} readMode />
            </div>
          )} */}

          {!readMode && (
            <div className="apply-to-host-navigation">
              {/* <span
                onClick={prevStep}
                disabled={values.submitAuthDisabled}
                className="back-btn"
              >
                Previous
              </span> */}
              <input
                type="button"
                value="Previous"
                onClick={prevStep}
                disabled={values.submitAuthDisabled}
                className="px-3 py-2 btn-info rounded-pill"
              />

              <span
                onClick={handleSubmitApplication}
                disabled={values.submitAuthDisabled}
                className="px-4 py-2 btn-danger rounded-pill"
              >
                Create
              </span>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default FormReview;
