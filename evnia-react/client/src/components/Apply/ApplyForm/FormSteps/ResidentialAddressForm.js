// Libraries
import React, { Component } from 'react';
// import { Progress } from "react-sweet-progress";
import { AppContext } from '../../../../ContextProvider/AppProvider';

// Components
import { Form, Input, Select } from '../../../EasyForm';
import { nationalities } from '../../nationality';
import { canadaProvincesTerritories } from '../../../Functions/Functions';

// Styling
// import "react-sweet-progress/lib/style.css";

export default class ResidentialAddressForm extends Component {
  // Set initial state
  state = {
    formClasses: 'mx-auto text-left needs-validation',
  };

  // To use the JWT credentials
  static contextType = AppContext;

  // Render Personal Information Form Step page
  render = () => {
    const { nextStep, update, prevStep, values, readMode } = this.props;

    const onSubmit = (data) => {
      update(data);
      nextStep();
    };

    return (
      <div>
        {/* {!readMode && <Progress />} */}
        <div className={!readMode ? 'apply-to-host' : ''}>
          {!readMode ? (
            <h1 className="apply-to-host-step-name">Residential Address Information</h1>
          ) : (
            <>
              <h6>Residential Address Information</h6>
              <hr />
            </>
          )}
          <Form data={values} onSubmit={onSubmit} readMode={readMode}>
            <Input
              name="residential_address_line_1"
              label="Residential Street Address (Optional)"
            />
            <Input name="residential_address_line_2" label="Residential Unit Address (Optional)" />
            <Input name="residential_city" label="Residential City (Optional)" />
            <Select name="residential_state" label="Residential Province or Territory (Optional)">
              {canadaProvincesTerritories()}
            </Select>
            <Input
              name="residential_postal_code"
              label="Residential Postal Code (Optional)"
              maxLength="7"
            />
            <Select
              name="residential_country"
              options={nationalities.map((n) => [n.en_short_name, n.en_short_name])}
            >
              Residential Country (Optional)
            </Select>

            {!readMode && (
              <div className="apply-to-host-navigation">
                <span onClick={prevStep} disabled={values.submitAuthDisabled} className="back-btn">
                  Back
                </span>
                <input
                  type="submit"
                  value="Continue"
                  disabled={values.submitAuthDisabled}
                  className="continue-btn"
                />
              </div>
            )}
          </Form>
        </div>
      </div>
    );
  };
}
