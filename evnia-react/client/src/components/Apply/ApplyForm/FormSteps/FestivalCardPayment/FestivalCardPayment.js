// Libraries
import React, { useState, useContext, useEffect } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import LazyLoad from 'react-lazyload';
import moment from 'moment';
import { toast } from 'react-toastify';
import { useHistory } from 'react-router-dom';
import { AppContext } from '../../../../../ContextProvider/AppProvider';
import { formatDate } from '../../../../Functions/Functions';
//import BusinessPassportStart from "./BusinessPassportModals/BusinessPassportStart";

// Components
import ImageSlider from '../../../../ImageSlider/ImageSlider';

// Styling
// import '../../../../Festivals/FestivalCard/FestivalCard.scss';
import './FestivalCardPayment.scss'
import 'react-toastify/dist/ReactToastify.css';
import liveFestival from '../../../../../assets/images/live.png';

toast.configure();

const FestivalCard = (props) => {
  console.log("props coming from the festival cards:0", props)
  // Set initial state
  const [load, setLoad] = useState(false);
  const [isSelected, setIsSelected] = useState(false);

  // Calculate the number of days between start/end and current date
  const d1 = new Date(props.startDate);
  const d2 = new Date(props.endDate);
  const d3 = new Date();
  const millisecondsInDay = 86400000;
  const upcomingDays = Math.floor((d1 - d3) / millisecondsInDay) + 1;
  const endingDays = Math.ceil((d2 - d3) / millisecondsInDay) + 1;
  const history = useHistory();
  // Set date and time
  const startDateTime = moment(
    new Date(props.startDate).toISOString().split('T')[0] + 'T' + props.startTime + '.000Z',
  ).add(new Date().getTimezoneOffset(), 'm');
  const endDateTime = moment(
    new Date(props.startDate).toISOString().split('T')[0] + 'T' + props.endTime + '.000Z',
  ).add(new Date().getTimezoneOffset(), 'm');

  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const userRole = appContext.state.user.role;

  useEffect(() => {
    if (isSelected) {
      props.setSelected((prev) => [...prev, props.festivalId]);
    } else {
      props.setSelected((prev) => prev.filter((festival) => festival !== props.festivalId));
    }
  }, [isSelected, setIsSelected, props.setSelected, props.festivalId]);

  const truncateDescription = (description) => {
    if (description.length > 130) {
      if (description[description.length - 1] === '.') {
        description = description.substr(0, 130) + '..';
      } else {
        description = description.substr(0, 130) + '...';
      }
    }
    return description;
  };
  const handleCheckboxChange = (event) => {
    setIsSelected((prev) => !prev);
  };
  const handleOnClick = (event) => {
    const value = event.target.value;
    props.setSelected((prev) => [...prev, value]);
    console.log(props.selected);
  };

  // Render Festival Card
  return (
    <div className="festival-details-cards main-festival-card">
      <LazyLoad once>
        <div className="festival-card-photo">
          <form>
            <input
              type="checkbox"
              name="festival_payment_checkbox"
              className="checkbox-festival-payment"
              value={isSelected}
              onChange={handleCheckboxChange}

            ></input>
          </form>
          {upcomingDays > 0 ? (
            <span className="days-remaining">{`${upcomingDays} day${
              upcomingDays > 1 ? 's' : ''
            }`}</span>
          ) : (
            <span className="days-remaining">
              <strong>Happening Now</strong>
            </span>
          )}
          <img
            className={load ? 'festival-card-photo' : 'loading-image'}
            src={props.images[0]}
            alt={props.title}
            onLoad={() => setLoad(true)}
          />
        </div>
        <div className="main-festival-info">
          <div className="festival-location">
            <i className="fas fa-map-marker-alt"></i> <strong>{props.city}</strong>
          </div>
          <div className="festival-date">
            <i className="fas fa-calendar"></i> <strong>{formatDate(props.startDate)}</strong>
          </div>
          <div className="festival-time">
            <i className="fas fa-clock"></i>
            <strong>
              {` ${moment(startDateTime).format('h:mm A')} - ${moment(endDateTime).format(
                'h:mm A',
              )}`}{' '}
              EST
            </strong>
          </div>
          <div className="festival-name">{props.title}</div>
          <div className="festival-price">
            <strong className="price-stronger"> {props.price} / person </strong>
          </div>
          <div className="festival-info">
            <div className="info-restaurants">
              <i className="fas fa-utensils"></i>22 Restaurants
            </div>
            <div className="info-food-trucks">
              <i className="fas fa-truck"></i>10 Food Trucks
            </div>
          </div>
          {/* <div className="festival-description">
            <p className="description-paragraph">{truncateDescription(props.description)}</p>
          </div> */}
        </div>
        <div className="festival-price"> 
           <strong className="price-stronger">Vending Price: {props.discount === 1 ? '$' + props.vendPrice : null}</strong>
            <strong className = "crossed-out">{props.discount < 1 ? '$' + props.vendPrice : null} </strong>
            {props.discount < 1 ? '$' +( props.vendPrice * props.discount).toFixed(2) : null}
        </div>
        {/* <div className="purchase-ticket-button">{props.type}</div> */}
      </LazyLoad>
    </div>
  );
};

export default FestivalCard;
