// Libraries
import React, { useState, useEffect, useContext, useRef } from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
// import { Progress } from "react-sweet-progress";
import { AppContext } from '../../../../ContextProvider/AppProvider';

// Components
import {
  Checkbox,
  CheckboxGroup,
  DateInput,
  Form,
  Input,
  MultiImageInput,
  PreFillInput,
  PreFillSelect,
  Select,
  Textarea,
  UserSelector,
} from '../../../EasyForm';
import { canadaProvincesTerritories } from '../../../Functions/Functions';

// Styling
import '../../../CreateFoodSample/CreateFoodSampleForm/CreateFoodSampleForm.scss';
import 'react-toastify/dist/ReactToastify.css';
// import "react-sweet-progress/lib/style.css";

const AddProductsForm = (props) => {
  const { values, update, readMode, nextStep } = props;

  // To use the JWT credentials
  const appContext = useContext(AppContext);

  const formRef = useRef();

  // Set initial state
  const [nationalities, setNationalities] = useState([]);
  const [isAdmin, setIsAdmin] = useState(
    appContext.state.user &&
      appContext.state.user.role &&
      appContext.state.user.role.includes('ADMIN'),
  );
  const [foodSampleItems, setFoodSampleItems] = useState(values.foodSampleList || []);
  const [selectedFoodSampleItem, setSelectedFoodSampleItem] = useState({});
  const [preFillAddressLine1, setPreFillAddressLine1] = useState('');
  const [preFillAddressLine2, setPreFillAddressLine2] = useState('');
  const [preFillCity, setPreFillCity] = useState('');
  const [preFillProvinceTerritory, setPreFillProvinceTerritory] = useState('');
  const [preFillPostalCode, setPreFillPostalCode] = useState('');
  const userRole = appContext.state.user.role;
  console.log(appContext);
  const updateState = (newList) => {
    updateFoodSampleList(newList);
    setFoodSampleItems(newList);
    setSelectedFoodSampleItem({ newList });
  };

  const updateFoodSampleList = (foodSampleList) => {
    update({ foodSampleList });
  };

  // Add food sample helper function
  const onSubmit = async (data) => {
    window.scrollTo(0, 0);
    let newList = [...foodSampleItems];

    const response = await axios({
      method: 'GET',
      url: `/user/${values.email}`,
    });

    if (
      (userRole && (userRole.includes('RESTAURANT') || userRole.includes('RESTAURANT_PENDING'))) ||
      !response.data.message ||
      values.hostCreation
    ) {
      if (data.id) {
        newList = [...newList.filter((m) => parseInt(m.id) !== parseInt(data.id)), data];
      } else {
        data.id = foodSampleItems.length + 1;
        if (values.festivalId) {
          data.festivalId = values.festivalId;
        }
        newList.push(data);
      }
      console.log(newList);
      updateState(newList);
    }
  };

  // Edit food sample helper function
  const editFoodSampleItem = (foodSampleItem) => {
    const newList = foodSampleItems.filter((f) => f.id !== foodSampleItem.id);

    updateState(newList);
    setSelectedFoodSampleItem(foodSampleItem);
  };

  // Remove food sample helper function
  const removeFoodSampleItem = (foodSampleItem) => {
    const newList = foodSampleItems.filter((f) => f.id !== foodSampleItem.id);

    updateState(newList);
  };

  // Add food sample helper function
  const submitFoodSample = async () => {
    if (values.isCreateFoodSamples && !values.foodSampleList.length) {
      toast('Please add at least one food sample.', {
        type: 'error',
        autoClose: 2000,
      });

      return;
    }

    nextStep();
  };

  // Fetch nationalities helper function
  const fetchNationalities = async () => {
    const response = await axios({
      method: 'GET',
      url: '/nationalities',
    });

    setNationalities(response.data.nationalities);
  };

  // Fetch restaurant address helper function
  const fetchRestaurantAddress = async () => {
    const response = await axios({
      method: 'GET',
      url: `/business/${appContext.state.user.id}`,
    });

    response.data.details.data.map((item) => {
      if (appContext.state.user.id === item.user_id) {
        setPreFillAddressLine1(item.business_address_1);
        setPreFillAddressLine2(item.business_address_2);
        setPreFillCity(item.city);
        setPreFillProvinceTerritory(item.state);
        setPreFillPostalCode(item.postal_code);
      }
    });
  };

  // Mount nationalities and restaurants database
  useEffect(() => {
    window.scroll(0, 0);

    fetchNationalities();
    fetchRestaurantAddress();
  }, []);

  useEffect(() => {
    setIsAdmin(
      appContext.state.user &&
        appContext.state.user.role &&
        appContext.state.user.role.includes('ADMIN'),
    );
  }, [appContext]);

  return (
    <div className="apply-to-host">
      {/* {!readMode && <Progress />} */}
      <div>
        {!readMode ? (
          <h1 className="apply-to-host-step-name text-center">Add Products</h1>
        ) : (
          <>
            <h6>Products</h6>
            <hr />
          </>
        )}

        {!readMode ? (
          <div className="row">
            <div className="col-12 px-40">
              <Form
                data={selectedFoodSampleItem}
                onSubmit={onSubmit}
                readMode={readMode}
                formRef={formRef}
              >
                <MultiImageInput
                  name="product_images"
                  label="Images"
                  dropbox_label="Click or drag-and-drop to upload one or more images"
                  required
                />

                {/*                 {values.email ? (
                  <PreFillInput
                    name="email"
                    label="Restaurant Email"
                    preFill={values.email}
                    required
                  />
                ) : isAdmin ? (
                  <UserSelector name="userEmail" label="User Email" required />
                ) : null} */}

                <Input name="product_name" label="Product Name" required />

                {nationalities.length && (
                  <Select name="product_made_in_nationality_id" label="Nationality" required>
                    <option value="">--Select--</option>
                    {nationalities.map((n) => (
                      <option key={n.id} value={n.id}>
                        {n.nationality}
                      </option>
                    ))}
                  </Select>
                )}

                {/*                 <div className="row">
                  <div className="col-md-6 create-food-sample-start-date"> */}
                <DateInput
                  name="product_expiry_date"
                  label="Expiry Date"
                  dateFormat="yyyy/MM/dd"
                  minDate={new Date()}
                  peekNextMonth
                  showMonthDropdown
                  showYearDropdown
                  dropdownMode="select"
                  required
                />
                {/* </div> */}
                {/*                   <div className="col-md-6 create-food-sample-end-date">
                    <DateInput
                      name="end_date"
                      label="End Date"
                      dateFormat="yyyy/MM/dd"
                      minDate={new Date()}
                      peekNextMonth
                      showMonthDropdown
                      showYearDropdown
                      dropdownMode="select"
                      required
                    />
                  </div> */}
                {/* </div> */}

                {/*                <div className="row">
                  <div className="col-md-6 create-food-sample-start-time"> */}
                <DateInput
                  name="product_expiry_time"
                  label="Expiry Time"
                  showTimeSelect
                  showTimeSelectOnly
                  dateFormat="h:mm aa"
                  required
                />
                {/* </div> */}
                {/*                   <div className="col-md-6 create-food-sample-end-time">
                    <DateInput
                      name="end_time"
                      label="End Time"
                      showTimeSelect
                      showTimeSelectOnly
                      dateFormat="h:mm aa"
                      required
                    />
                  </div> */}
                {/* </div> */}

                <Select name="product_size" label="Product Size" required>
                  <option value="">--Select--</option>
                  <option value="Bite Size">Bite Size</option>
                  <option value="Quarter">Quarter</option>
                  <option value="Half">Half</option>
                  <option value="Full">Full</option>
                </Select>

                <Input
                  name="product_quantity"
                  label="Quantity Available"
                  type="number"
                  min="1"
                  required
                />
                <Input name="product_price" label="Price" type="number" required />

                {/*                 <CheckboxGroup
                  name="dietaryRestrictions"
                  label="Dietary Restrictions (Optional)"
                  options={[
                    ["Vegetarian", "vegetarian"],
                    ["Vegan", "vegan"],
                    ["Gluten-Free", "glutenFree"],
                    ["Halal", "halal"],
                  ]}
                /> */}

                {/*                 <CheckboxGroup
                  name="daysAvailable"
                  label="Days Available"
                  options={[
                    ["Monday", "available_on_monday"],
                    ["Tuesday", "available_on_tuesday"],
                    ["Wednesday", "available_on_wednesday"],
                    ["Thursday", "available_on_thursday"],
                    ["Friday", "available_on_friday"],
                    ["Saturday", "available_on_saturday"],
                    ["Sunday", "available_on_sunday"],
                  ]}
                  required
                /> */}

                {/*                 <Select name="spice_level" label="Spice Level (Optional)">
                  <option value="">--Select--</option>
                  <option value="Mild">Mild</option>
                  <option value="Medium">Medium</option>
                  <option value="Hot">Hot</option>
                </Select> */}
                {/* 
                {values.address_line_1 || preFillAddressLine1 ? (
                  <PreFillInput
                    name="addressLine1"
                    label="Street Address"
                    preFill={
                      values.address_line_1
                        ? values.address_line_1
                        : preFillAddressLine1
                        ? preFillAddressLine1
                        : ""
                    }
                    required
                  />
                ) : (
                  <Input name="addressLine1" label="Street Address" required />
                )}

                {values.address_line_2 || preFillAddressLine2 ? (
                  <PreFillInput
                    name="addressLine2"
                    label="Unit Address"
                    preFill={
                      values.address_line_2
                        ? values.address_line_2
                        : preFillAddressLine2
                        ? preFillAddressLine2
                        : ""
                    }
                  />
                ) : userRole && userRole.includes("ADMIN") ? (
                  <Input name="addressLine2" label="Unit Address" />
                ) : null}

                {values.business_city || preFillCity ? (
                  <PreFillInput
                    name="city"
                    label="City"
                    preFill={
                      values.business_city
                        ? values.business_city
                        : preFillCity
                        ? preFillCity
                        : ""
                    }
                    required
                  />
                ) : (
                  <Input name="city" label="City" required />
                )}

                {values.state || preFillProvinceTerritory ? (
                  <PreFillSelect
                    name="provinceTerritory"
                    label="Province or Territory"
                    preFill={
                      values.state
                        ? values.state
                        : preFillProvinceTerritory
                        ? preFillProvinceTerritory
                        : ""
                    }
                    required
                  >
                    {canadaProvincesTerritories()}
                  </PreFillSelect>
                ) : (
                  <Select
                    name="provinceTerritory"
                    label="Province or Territory"
                    required
                  >
                    {canadaProvincesTerritories()}
                  </Select>
                )}

                {values.postal_code || preFillPostalCode ? (
                  <PreFillInput
                    name="postal_code"
                    label="Postal Code"
                    preFill={
                      values.postal_code
                        ? values.postal_code
                        : preFillPostalCode
                        ? preFillPostalCode
                        : ""
                    }
                    maxLength="7"
                    required
                  />
                ) : (
                  <Input
                    name="postal_code"
                    label="Postal Code"
                    maxLength="7"
                    required
                  />
                )} */}

                <Textarea name="product_description" label="Description" required />

                {/*  <Checkbox name="addToFestival" label="Add to festival?" /> */}

                <button
                  type="submit"
                  disabled={values.submitAuthDisabled}
                  className="add-food-sample-btn"
                >
                  Add
                </button>
              </Form>
            </div>

            {foodSampleItems.length ? (
              <div className="col-12 px-0">
                {foodSampleItems.map((f) => (
                  <div key={f.title} className="menu-items-form__item">
                    <img src={f.product_images[0]} alt={f.title} className="menu-items-form__img" />
                    <div className="menu-items-form__ctrl">
                      <div className="menu-items-form__ctrl__title">{f.title}</div>
                      <div className="menu-items-form__ctrl__actions">
                        <i
                          onClick={() => editFoodSampleItem(f)}
                          title="edit"
                          className="fa fa-edit"
                        />
                        <span> | </span>
                        <i
                          onClick={() => removeFoodSampleItem(f)}
                          title="remove"
                          className="fa fa-times"
                        />
                      </div>
                    </div>
                  </div>
                ))}
              </div>
            ) : null}
          </div>
        ) : (
          <div className="row justify-content-between">
            {foodSampleItems.map((f, index) => (
              <div key={index} className="col-lg-6 col-xl-4 food-samples-preview-content">
                <img
                  src={f.product_images[0]}
                  alt={f.title}
                  className="food-samples-preview-image"
                />
                <div>{f.title}</div>
              </div>
            ))}
          </div>
        )}
        {!readMode && (
          <div className="apply-to-host-navigation">
            <span className="apply-to-host-navigation-spacing"></span>
            <span
              onClick={submitFoodSample}
              disabled={values.submitAuthDisabled}
              className={/* standAloneStep ? "continue-btn pull-right" : */ 'continue-btn'}
            >
              Continue
            </span>
          </div>
        )}
      </div>
    </div>
  );
};

export default AddProductsForm;
