// Libraries
import React, { useState } from 'react';
// import { Link } from "react-router-dom";
// import { Progress } from "react-sweet-progress";

// Components
import { FileUpload, Form, Input, Select, Textarea } from '../../../EasyForm';

// Styling
// import "react-sweet-progress/lib/style.css";

const ApplyToCookForm = (props) => {
  const { nextStep, prevStep, update, values, readMode } = props;

  const [showOptions, setShowOptions] = useState(values.is_cook === 'yes');

  const onSubmit = (data) => {
    update(data);
    nextStep();
  };

  return (
    <div>
      {/* {!readMode && <Progress />} */}
      <div className={!readMode ? 'apply-to-host' : ''}>
        {!readMode ? (
          <h1 className="apply-to-host-step-name">Apply To Cook</h1>
        ) : (
          <>
            <h6>Apply To Cook</h6>
            <hr />
          </>
        )}

        <Form data={values} onSubmit={onSubmit} readMode={readMode}>
          <Select
            name="is_cook"
            options={[
              ['Yes', 'yes'],
              ['No', 'no'],
            ]}
            onChange={(e) => setShowOptions(e.target.value === 'yes')}
          >
            Do you want to be a Tasttlig cook?
            {/* <Link className="ml-2" to="/apply-to-cook">
              <i className="fa fa-info-circle" />
            </Link> */}
          </Select>
          {showOptions && (
            <>
              <Textarea
                name="cook_selection"
                label="Why do you want to be a Tasttlig cook?"
                required
              />
              <FileUpload
                name="cook_selection_resume"
                dir="cook-selection-resume"
                accept="application/pdf"
                label="Upload your resume"
                required
              />
              <FileUpload
                name="cook_selection_video"
                dir="cook-selection-videos"
                accept="video/*"
                label="Upload 1 minute video on why you want to be selected (Optional)"
              />
              <Input name="youtube_link" label="YouTube Link (Optional)" />
            </>
          )}

          {!readMode && (
            <div className="apply-to-host-navigation">
              <span onClick={prevStep} disabled={values.submitAuthDisabled} className="back-btn">
                Back
              </span>
              <input
                type="submit"
                value="Continue"
                disabled={values.submitAuthDisabled}
                className=" continue-btn"
              />
            </div>
          )}
        </Form>
      </div>
    </div>
  );
};

export default ApplyToCookForm;
