// Libraries
import React from 'react';
import cookie from 'react-cookies';
// import { Progress } from "react-sweet-progress";

// Components
import { Form, Input, Textarea } from '../../../EasyForm';
import axios from 'axios';

// Styling
// import "react-sweet-progress/lib/style.css";

const SocialProofForm = (props) => {
  const { nextStep, prevStep, values, update, readMode } = props;

  let submitForm = () => {
    this.setState({
      formClasses: 'mx-auto text-left needs-validation was-validated',
    });
    setCookieForAllInput();
    if (document.getElementById('reviewForm').checkValidity()) {
      nextStep();
    }
  };

  let setCookieForAllInput = () => {
    const reviewInfoInput = [
      'yelpReview',
      'googleReview',
      'tripadvisorReview',
      'instagramReview',
      'youtubeReview',
      'mediaRecognition',
      'personalReview',
    ];
    reviewInfoInput.forEach((key) => {
      if (values[key]) {
        cookie.save(key, values[key]);
      }
    });
  };

  const updateSocialProof = async (data) => {
    const response = await axios({
      method: 'PUT',
      url: '/user/updateSocialProof',
      data: {
        ...data,
        email: values.email,
      },
    });

    return response.data;
  };

  const onSubmit = async (data) => {
    update(data);
    await updateSocialProof(data);
    nextStep();
  };

  return (
    <div>
      {/* {!readMode && <Progress />} */}
      <div className={!readMode ? 'apply-to-host' : ''}>
        {!readMode ? (
          <h1 className="apply-to-host-step-name">What are people saying about you</h1>
        ) : (
          <>
            <h6>What are people saying about you</h6>
            <hr />
          </>
        )}

        <Form data={values} onSubmit={onSubmit} readMode={readMode}>
          <Input name="yelp_review" label="Link to Yelp Reviews (Optional)" />
          <Input name="google_review" label="Link to Google Reviews (Optional)" />
          <Input name="tripadvisor_review" label="Link to TripAdvisor Reviews (Optional)" />
          <Input name="instagram_review" label="Link to Instagram Reviews (Optional)" />
          <Input name="facebook_review" label="Link to Facebook Reviews (Optional)" />
          <Input name="media_recognition" label="Link to Media Recognition (Optional)" />
          <Textarea
            name="personal_review"
            label="What other things are people saying about you? (Optional)"
          />

          {!readMode && (
            <div className="apply-to-host-navigation">
              <span onClick={prevStep} disabled={values.submitAuthDisabled} className="back-btn">
                Back
              </span>
              <input
                type="submit"
                value="Continue"
                disabled={values.submitAuthDisabled}
                className="continue-btn"
              />
            </div>
          )}
        </Form>
      </div>
    </div>
  );
};

export default SocialProofForm;
