// Libraries
import React, { useState, useContext, useEffect, Fragment } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
// import { Progress } from "react-sweet-progress";
import { AppContext } from '../../../../ContextProvider/AppProvider';
import { toast } from 'react-toastify';
import FestivalCardPayment from './FestivalCardPayment/FestivalCardPayment';
// Components
import { Checkbox, Form, Input, CheckboxGroup, Textarea } from '../../../EasyForm';
import Nav from '../../../Navbar/Nav';
import BackgroundImage from '../../../../assets/images/background-vendor-image.png';
import tasttligLogoBlack from '../../../../assets/images/tasttlig-logo-black.png';
import VendingConditions from '../../../LandingPage/TermsAndConditions/VendingConditions';

// Styling
// import "react-sweet-progress/lib/style.css";
import '../../../Apply/Vendor.scss';
import Modal from 'react-modal';


const VendingAndPlan = (props) => {
  const { handleSubmitApplication, nextStep, update, values, readMode } = props;
  let data = {};
  // Set initial state
  const [existingUser, setExistingUser] = useState(null);
  const [submitAuthDisabled, setSubmitAuthDisabled] = useState(false);
  const [form, setForm] = useState('VendingAndPlan');
  const [circlePriceName, setCirclePriceName] = useState('vend-circle');
  const [formTitle, setFormTitle] = useState('Vending & Plan');
  const [circleSubscriptionName, setCircleSubscriptionName] = useState('vend-empty-circle');
  const [hasNextPage, setHasNextPage] = useState(true);
  const [currentPage, setCurrentPage] = useState(0);
  const [festivalItems, setFestivalItems] = useState([]);
  const [loading, setLoading] = useState(false);
  const [selected, setSelected] = useState([]);
  const [load, setLoad] = useState(false);
  const [vendingConditions, setVendingConditions] = useState(false);
  const [businessConditions, setBusinessConditions] = useState(false);
  const [subscriptionType, setSubscriptionType] = useState('');
  const [discount, setDiscount] = useState(1);

  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const changeFormPrice = () => {
    setForm('VendingAndPlan');
    setFormTitle('Vending & Plan');
    setCirclePriceName('vend-circle');
    setCircleSubscriptionName('vend-empty-circle');
  };
  const changeFormSubscription = () => {
    setForm('VendorPlans');
    setFormTitle('Vendor Plans');
    setCirclePriceName('vend-empty-circle');
    setCircleSubscriptionName('vend-circle');
  };

  // Load next set of festivals helper functions
  const loadNextPage = async (page) => {
    const url = '/festival/all?';

    return axios({
      method: 'GET',
      url,
      params: {
        page: page + 1,
      },
    });
  };

  const handleLoadMore = (page = currentPage, festivals = festivalItems) => {
    setLoading(true);
    loadNextPage(page).then((newPage) => {
      setLoading(false);

      if (!newPage) {
        return false;
      }
      console.log(newPage);

      const pagination = newPage.data.details.pagination;

      if (page < pagination.lastPage) {
        setCurrentPage(page + 1);
      }

      setHasNextPage(currentPage < pagination.lastPage);
      setFestivalItems(festivals.concat(newPage.data.details.data));
    });
  };

  const onSubmit = async (data) => {
    if(selected.length === 0 ) {
      toast('Please Select one or more festivals to vend!', {
        type: 'error',
        autoClose: 3000,
      });
    } else {
      // handleSubmitApplication();
      if (data.subscription === 'sub-1') {
        setTimeout(() => {
          window.location.href = '/payment/package/V_MIN';
        }, 2000);
      }
      if (subscriptionType === "V_MOD" || data.subscription === 'sub-2') {
        console.log("subscriptionType from vend and plan", subscriptionType)

        setTimeout(() => {
          window.location.href = '/payment/package/V_MOD';
        }, 2000);
      }
      if (subscriptionType === "V_ULTRA" || data.subscription === 'sub-3') {
        setTimeout(() => {
          window.location.href = '/payment/package/V_ULTRA';
        }, 2000);
      }
    }
       
  };


  const handleFestivalPayment = (event) => {
    event.preventDefault();
    console.log(event);
    localStorage.removeItem('festivalPayments');
    localStorage.setItem('festivalPayments', selected);
    window.location.href = '/payment/package/V_MIN';
  };

  // Render festival cards helper function
  const renderFestivalCards = (arr) => {
    return arr.map((card, index) => (
      <FestivalCardPayment
        key={index}
        festivalId={card.festival_id}
        images={card.image_urls}
        title={card.festival_name}
        type={card.festival_type}
        price={card.festival_price}
        vendPrice={card.festival_vendor_price}
        city={card.festival_city}
        startDate={card.festival_start_date}
        endDate={card.festival_end_date}
        startTime={card.festival_start_time}
        endTime={card.festival_end_time}
        description={card.festival_description}
        festivalHost={card.festival_user_host_id}
        hostFestivalList={card.festival_restaurant_host_id}
        festivalSponsorList={card.festival_business_sponsor_id}
        history={props.history}
        selected={selected}
        setSelected={setSelected}
        discount= {discount}
      />
    ));
  };

   // fetch user's subscription list and filter them, if user has level 1 or 2 subsciption set discount accordingly
   const fetchSubscriptionList = async () => {
    try {
      const response = await axios({
        method: 'GET',
        url: `/valid-user-subscriptions/${appContext.state.user.id}`,
      });
      const sub1 = response.data.user.filter(subscription => subscription.subscription_code === 'V_MOD')
      const sub2 = response.data.user.filter(subscription => subscription.subscription_code === 'V_ULTRA')
      if (sub2.length > 0 && (sub2[0].suscribed_festivals === null || sub2[0].suscribed_festivals.length < 4) ){
        setDiscount(0.95);
        setSubscriptionType('V_ULTRA')
      }
      else if (sub1.length > 0 && sub1[0].suscribed_festivals === null){
        setDiscount(0.95);
        setSubscriptionType('V_MOD')
      }
      
      //setFestivalAttendants(response.data);
      return response;
    } catch (error) {
      return error.response;
    }
  };

  useEffect(() => {
    window.scrollTo(0, 0);

    handleLoadMore();
  }, []);

  useEffect(() => {
    handleLoadMore(0, []);
    fetchSubscriptionList();

  }, []);

  return (
    <Fragment>
    <Nav />
    <div className="container-xxl">
      <div className="row">
        <div className="col-4 left-side-vendor">
          <div className="row vendor-tasttlig-container">
            <img
              className="col-md-6 tasttlig-logo-vendor"
              onClick={() => (window.location.href = '/')}
              src={tasttligLogoBlack}
            ></img>
            <div
              className="col-4 tasttlig-vendor-title"
              onClick={() => (window.location.href = '/')}
            >
              Vendor
            </div>
          </div>
        </div>
        <div className="col-8 right-side-vendor">
          <div className="container-xl vendor-details-container">
            <div className="container">
              <div className="vendor-details-title">{formTitle}</div>
            </div>
            <div className="row vendor-plan-container">
              <div className="col-md-3">
                <div>Vending Price</div>
                <button
                  type="button"
                  className={circlePriceName}
                  onClick={() => changeFormPrice()}
                ></button>
              </div>
              {/* <div className="col-md-3">
                <div>Vending Subscription Plan</div>
                <button
                  type="button"
                  className={circleSubscriptionName}
                  onClick={() => changeFormSubscription()}
                ></button>
              </div> */}
              <div className="row justify-content-center">
                <div className="col-sm-6">
                  <hr className="vendor-plan-line"></hr>
                </div>
              </div>
            </div>
            {form === 'VendingAndPlan' && (
              <form className = "vending-plan-form" 
              // onSubmit={handleFestivalPayment}
              >
                <div className="row vending-and-plan-festivals">
                   {localStorage.getItem("festival_id") ? festivalItems.map((festivalItem) => (
                    <>
                      {festivalItem.festival_id === Number(localStorage.getItem("festival_id")) ? (
                         <FestivalCardPayment
                         festivalId={festivalItem.festival_id}
                         images={festivalItem.image_urls}
                         title={festivalItem.festival_name}
                         type={festivalItem.festival_type}
                         price={festivalItem.festival_price}
                         vendPrice={festivalItem.festival_vendor_price}
                         city={festivalItem.festival_city}
                         startDate={festivalItem.festival_start_date}
                         endDate={festivalItem.festival_end_date}
                         startTime={festivalItem.festival_start_time}
                         endTime={festivalItem.festival_end_time}
                         description={festivalItem.festival_description}
                         festivalHost={festivalItem.festival_host_admin_id}
                         hostFestivalList={festivalItem.festival_restaurant_host_id}
                         festivalSponsorList={festivalItem.festival_business_sponsor_id}
                         history={props.history}
                         selected={selected}
                         setSelected={setSelected}
                       />
                      ): null}
                    </>
                    // <>
                    // {console.log("localStorage.getItem item from the map function:", typeof localStorage.getItem("festival_id"))}
                    // </>
                  )) : renderFestivalCards(festivalItems)} 
                  {/* {renderFestivalCards(festivalItems)} */}

                </div>
              
                <button type="button" className="vendor-details-button" 
                    onClick={() => setVendingConditions(true)}>
                      {' '}
                      Pay
                    </button>
                    {vendingConditions ? (
                      <Modal
                        className="terms-conditions-modal"
                        isOpen={vendingConditions}
                        onRequestClose={() => setVendingConditions(false)}
                      >
                        <div className="terms-conditions-background-image" />
                        <VendingConditions
                          user_id={props.user_id}
                          viewVendingConditionsState={vendingConditions}
                          selected={selected}
                          subscriptionType={subscriptionType}
                          changeVendingConditions={(e) => setVendingConditions(e)}
                          changeBusinessConditions={(e) => 
                            {setVendingConditions(false);
                            setBusinessConditions(e)}}
                        />
                      </Modal>
                    ) : null}
              </form>
            )}
            {form === 'VendorPlans' && (
              <div className="container vendor-plans-page">
                <div className="row vendor-plans-subtitle">
                  {' '}
                  Select the subscription vending plan that fits your business
                </div>
                <Form data={data} onSubmit={onSubmit}>
                  <div className="row vendor-subscription-plan">
                    <div className="col-1 vendor-subscription-checkbox">
                      <Input type="radio" name="subscription" value="sub-1"></Input>
                    </div>
                    <div className="col-9 vendor-subscription-details-container">
                      <div className="vendor-subscription-details">
                        {' '}
                        Vend in One Festival: A Tasttlig Vendor with this package is able to list
                        their products and services in any one festival on the platform
                      </div>
                    </div>
                    <div className="col-2 vendor-price-container">
                      <div className="vendor-subscription-price">$30.00</div>
                    </div>
                  </div>
                  <div className="row dead-row">
                    <div className="col-10 dead-row-column"></div>
                  </div>
                  <div className="row vendor-subscription-plan">
                    <div className="col-1 vendor-subscription-checkbox">
                      <Input type="radio" name="subscription" value="sub-2"></Input>
                    </div>
                    <div className="col-9 vendor-subscription-details-container">
                      <div className="vendor-subscription-details">
                        {' '}
                        Vend in Four Festivals: A Tasttlig Vendor with this package is able to list
                        their products and services in any four festivals on the platform
                      </div>
                    </div>
                    <div className="col-2 vendor-price-container">
                      <div className="vendor-subscription-price">$100.00</div>
                    </div>
                  </div>
                  <div className="row dead-row">
                    <div className="col-10 dead-row-column"></div>
                  </div>
                  <div className="row vendor-subscription-plan">
                    <div className="col-1 vendor-subscription-checkbox">
                      <Input type="radio" name="subscription" value="sub-3"></Input>
                    </div>
                    <div className="col-9 vendor-subscription-details-container">
                      <div className="vendor-subscription-details">
                        {' '}
                        Vend in All Festivals in 2021: A Tasttlig Vendor with this package is able
                        to list their products and services in every festival created on the
                        platform for 2021
                      </div>
                    </div>
                    <div className="col-2 vendor-price-container">
                      <div className="vendor-subscription-price">$120.00</div>
                    </div>
                  </div>
                  {/* <div className="row dead-row">
                    <div className="col-10 dead-row-column"></div>
                  </div>
                  <div className="row vendor-subscription-plan">
                    <div className="col-1 vendor-subscription-checkbox">
                      <input type="checkbox"

                      ></input>
                    </div>
                    <div className="col-9 vendor-subscription-details-container">
                      <div className="vendor-subscription-details"> Vend in One Festival: A Tasttlig Vendor with this package is able to list their products and services in any one festival on the platform
                </div>
                    </div>
                    <div className="col-2 vendor-price-container">
                      <div className="vendor-subscription-price">
                        $30.00
                  </div>
                    </div>
                  </div> */}
                   
                  <div className="vendor-details-button-container">
                  <button type="submit" className="vendor-details-button">
                  Pay
                </button>
              
                  </div>
                </Form>
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
    </Fragment>
  );
};

export default VendingAndPlan;
