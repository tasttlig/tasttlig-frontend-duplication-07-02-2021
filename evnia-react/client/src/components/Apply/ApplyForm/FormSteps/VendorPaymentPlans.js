// Libraries
import React, { useState, useContext, useEffect } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
// import { Progress } from "react-sweet-progress";
import { AppContext } from '../../../../ContextProvider/AppProvider';
import { useHistory } from 'react-router-dom';

// Components
import { Checkbox, Form, Input } from '../../../EasyForm';
import GoTop from '../../../Shared/GoTop';

// Styling
// import "react-sweet-progress/lib/style.css";

const VendorPaymentPlans = (props) => {
  const { nextStep, update, values, readMode, festivalId } = props;
  console.log(props);
  // Set initial state
  const [existingUser, setExistingUser] = useState(null);
  const [item_type] = useState('subscription');
  const [item_id] = useState('V_SB');
  const [vendorPackages, setVendorPackages] = useState([]);
  const [colors] = useState([
    'bg-primary',
    'bg-success',
    'bg-info',
    'bg-warning',
    'bg-danger',
    'bg-secondary',
    'bg-dark',
    'bg-light',
  ]);

  // To use the JWT credentials
  const appContext = useContext(AppContext);

  useEffect(() => {
    (async function () {
      const data = {
        item_type,
        item_id,
      };

      const response = await axios.get('/subscription/details', {
        params: data,
      });
      console.log(response);
      //setSponsorshipPackages(response.data.item)
      //use only sponsorship packages
      /*       setVendorPackages(
        response.data.item.filter((pkg) =>
          pkg.subscription_name.startsWith("vendor_basic")
        )
      ); */
      setVendorPackages([response.data.item]);
    })();
  }, []);

  const renderVendorPackages = (arr) => {
    return arr.map((item, index) => (
      <SponsorshipPackageCard
        key={item.subscription_id}
        date_of_expiry={item.date_of_expiry}
        description={item.description}
        price={item.price}
        status={item.status}
        subscription_type={item.subscription_type}
        comission_to_pay_percentage={item.comission_to_pay_percentage}
        subscription_code={item.subscription_code}
        subscription_name={item.subscription_name}
        subscription_id={item.subscription_id}
        validity_in_months={item.validity_in_months}
        festivalId={values.festivalId}
        values={values}
        title={values.title ? values.title : null}
        color={colors[index]}
      />
    ));
  };

  const SponsorshipPackageCard = (props) => {
    const history = useHistory();

    const submitPackage = async () => {
      await history.push({
        pathname: `/payment/${props.subscription_type.toLowerCase()}/${props.subscription_code}`,
        festivalId: props.festivalId,
        title: props.title,
      });
    };
    return (
      <div className={`card m-5 shadow rounded ${props.color}`}>
        <div className="card-header">
          <p className="font-weight-bold text-justify text-center text-body">
            {props.subscription_name}
          </p>
        </div>
        <div className="mx-auto card-body">
          <div className="py-2">{props.description}</div>
          <div className="col-12 text-center">
            <span className="text-center lead font-weight-bold">
              {' '}
              &#36;{Math.trunc(props.price)}
            </span>
            <span className="small">.{props.price.split('.')[1]}</span>
          </div>
        </div>
        <div className="mx-auto card-footer">
          {values.is_vendor_payment && (
            <button onClick={submitPackage} type="button" className="btn btn-primary btn-lg">
              Buy Sponsorship Package
            </button>
          )}
          {/* <Link
              exact="true"
              to={{
                pathname: "/sponsor-products-and-services",
                package: props.subscription_name,
              }}
              className="btn btn-dark"
            >
              Select
            </Link> */}
        </div>
      </div>
    );
  };

  // Render Welcome Paragraph part of multi-step form
  return (
    <div>
      {/*      {!readMode &&
        <div className="apply-to-host">
          <div className="apply-to-host-step-name">
            Vendor Payment Plans
          </div>
          <div className="welcome-paragraph">
            <div className="row">
              <div className="col-4">
                Plan 1: 1 festival, $300
                {values.is_vendor_payment === "yes" && <div>
                  <Link
                    exact="true"
                    to={{

                    pathname: `/payment/subscription/V_SB`,
                    festivalId: values.festivalId
                    }}
                    className="festival-card-host-btn"
                  > Buy
                        </Link>
                </div>}

              </div>
              <div className="col-4">
                Plan 2: 4 festivals, $1000
                </div>
              <div className="col-4">
                Plan 3: Pay as you go
                </div>
            </div>
          </div>
          <Form
            data={values}
            onSubmit={() => nextStep()}
          >
            <div>
              {!readMode && (
                <div className="apply-to-host-navigation">
                  <span className="apply-to-host-navigation-spacing"></span>
                  <input
                    type="submit"
                    value="Continue"
                    disabled={values.submitAuthDisabled}
                    className="continue-btn"
                  />
                </div>
              )}
            </div>
          </Form>
        </div>} */}
      <div className="container pb-5 sponsor-package-content">
        {values.is_vendor_payment ? (
          <h4 className="text-center mb-5">Choose sponsorship package</h4>
        ) : (
          <h4> Vendor Payment Plans</h4>
        )}
        <div className="row">{renderVendorPackages(vendorPackages)}</div>
        {!values.is_vendor_payment && (
          <Form data={values} onSubmit={() => nextStep()}>
            <div>
              {!readMode && (
                <div className="apply-to-host-navigation">
                  <span className="apply-to-host-navigation-spacing"></span>
                  <input
                    type="submit"
                    value="Continue"
                    disabled={values.submitAuthDisabled}
                    className="continue-btn"
                  />
                </div>
              )}
            </div>
          </Form>
        )}
        {/* <Link
          exact="true"
          to={{
            pathname: "/sponsor-products-and-services",
            festivalId: props.location.festivalId,
            title: props.location.title,
          }}
          className="continue-btn"
        >
          Continue
        </Link> 
        //{`/festival/${props.festivalId}`} 
        */}
      </div>
      {/*  <Footer /> */}
      <GoTop scrollStepInPx="50" delayInMs="16.66" />
    </div>
  );
};

export default VendorPaymentPlans;
