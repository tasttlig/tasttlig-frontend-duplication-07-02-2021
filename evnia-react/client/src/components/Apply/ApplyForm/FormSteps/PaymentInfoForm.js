// Libraries
import React, { useState } from 'react';
// import { Progress } from "react-sweet-progress";

// Components
import { FileUpload, Form, Input, Select } from '../../../EasyForm';
import axios from 'axios';

// Styling
// import "react-sweet-progress/lib/style.css";

const PaymentInfoForm = (props) => {
  const { nextStep, prevStep, update, values, readMode } = props;

  const [paymentType, setPaymentType] = useState(values.banking);

  const updatePaymentInfo = async (data) => {
    const response = await axios({
      method: 'PUT',
      url: '/user/updatePaymentInfo',
      data: {
        ...data,
        email: values.email,
      },
    });

    return response.data;
  };

  const onSubmit = async (data) => {
    update(data);
    await updatePaymentInfo(data);
    nextStep();
  };

  return (
    <div>
      {/* {!readMode && <Progress />} */}
      <div className={!readMode ? 'apply-to-host' : ''}>
        {!readMode ? (
          <h1 className="apply-to-host-step-name">How would you like to be paid?</h1>
        ) : (
          <>
            <h6>How would you like to be paid?</h6>
            <hr />
          </>
        )}

        <Form data={values} onSubmit={onSubmit} readMode={readMode}>
          <Select
            name="banking"
            label="Payment Options"
            onChange={(e) => setPaymentType(e.target.value)}
          >
            <option value="">--Select--</option>
            <option value="Bank">Bank</option>
            <option value="Paypal">Paypal</option>
            <option value="Stripe">Stripe</option>
            <option value="Email Transfer">Email Transfer</option>
          </Select>

          {paymentType === 'Bank' && (
            <>
              <Input name="bank_number" label="Bank Number" required />
              <Input name="account_number" label="Account Number" required />
              <Input name="institution_number" label="Institution Number" required />
              <FileUpload
                name="void_cheque"
                label="Void Cheque"
                required
                disabled={values.submitAuthDisabled}
                dir="void-cheques"
                accept="image/x-png,image/gif,image/jpeg"
              />
            </>
          )}

          {paymentType === 'Paypal' && (
            <>
              <Input name="paypal_email" label="Email" required />
            </>
          )}

          {paymentType === 'Stripe' && (
            <>
              <Input name="stripe_account_number" label="Stripe Account Number" required />
            </>
          )}

          {paymentType === 'Email Transfer' && (
            <>
              <Input name="email" label="Email" required />
            </>
          )}

          {!readMode && (
            <div className="apply-to-host-navigation">
              <span onClick={prevStep} disabled={values.submitAuthDisabled} className="back-btn">
                Back
              </span>
              <input
                type="submit"
                value="Continue"
                disabled={values.submitAuthDisabled}
                className=" continue-btn"
              />
            </div>
          )}
        </Form>
      </div>
    </div>
  );
};

export default PaymentInfoForm;
