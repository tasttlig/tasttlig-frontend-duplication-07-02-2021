// Libraries
import React, { useState, useContext, useRef } from 'react';
import axios from 'axios';
// import { Progress } from "react-sweet-progress";
import { AppContext } from '../../../../ContextProvider/AppProvider';

// Components
import { Checkbox, Form, Input, PreFillInput, Select, MultiImageInput } from '../../../EasyForm';
import AddressLookup from '../../../AddressLookup/AddressLookup';
import { nationalities } from '../../nationality';
import { canadaProvincesTerritories } from '../../../Functions/Functions';

// Styling
// import "react-sweet-progress/lib/style.css";

const BusinessInfoForm = (props) => {
  const appContext = useContext(AppContext);
  const formRef = useRef(null);

  const { nextStep, update, values, readMode } = props;

  // Set initial state
  const [addressLookup, setAddressLookup] = useState('');

  // const [hasBusiness, setHasBusiness] = useState(values.has_business === "yes");
  const [businessType, setBusinessType] = useState('Food');
  // );
  // const [businessCategory, setBusinessCategory] = useState(
  //   values.business_category === "Food"
  // );
  // const [serviceProvider, setServiceProvider] = useState(
  //   values.service_provider || ""
  // );
  // const [serviceProvider, setServiceProvider] = useState(
  //   values.service_provider === "Restaurant"
  // );

  // Split address information into an array
  let addressDetails = [];

  addressDetails = addressLookup.split(', ');

  // Update Business Information part of multi-step form helper function
  const updateBusinessInfo = async (data) => {
    const url = values.is_sponsor ? '/user/update-sponsor-info' : '/user/update-business-info';
    //const url = "/user/update-business-info";

    const response = await axios({
      method: 'PUT',
      url,
      data: {
        ...data,
        email: data.email || values.email,
      },
    });

    return response.data;
  };

  const onSubmit = async (data) => {
    if (!values.email || values.email === '') {
      data.email = appContext.state.user.email;
    }
    data.business_type = businessType;

    console.log(data);
    update(data);
    await updateBusinessInfo(data);
    nextStep();
  };

  // const updateAddressInfo = (e) => {
  //   if (e.target.checked) {
  //     const currentValues = formRef.current.getValues();

  //     currentValues.address_line_1 = values.residential_address_line_1;
  //     currentValues.address_line_2 = values.residential_address_line_2;
  //     currentValues.business_city = values.residential_city;
  //     currentValues.state = values.residential_state;
  //     currentValues.postal_code = values.residential_postal_code;
  //     currentValues.country = values.residential_country;

  //     update(currentValues);
  //   }
  // };

  // Render Business Information part of multi-step form
  return (
    <div>
      {/* {!readMode && <Progress />} */}
      <div className={!readMode ? 'apply-to-host' : ''}>
        {!readMode ? (
          <h1 className="text-danger apply-to-host-step-name">{`${
            values.is_sponsor ? 'Business Passport' : 'Restaurant Information'
          }`}</h1>
        ) : (
          <>
            <h6 className="text-danger">{`${
              values.is_sponsor || props.sponsor ? 'Business Passport' : 'Restaurant  Information'
            }`}</h6>
            <hr />
          </>
        )}
        <p>
          Thank you for your interest in Tasttlig! Let's start by creating your Business Passport
        </p>

        <Form data={values} onSubmit={onSubmit} readMode={readMode} formRef={formRef}>
          <MultiImageInput
            name="logo"
            dropbox_label="Click or drag-and-drop to upload your logo"
            disabled={values.submitAuthDisabled}
            className=""
            required
          />

          {/* <Select
            name="has_business"
            label="Do you have a business?"
            onChange={(e) => setHasBusiness(e.target.value === "yes")}
            required
          >
            <option value="yes">Yes</option>
            <option value="no">No</option>
          </Select> */}

          {/* {hasBusiness && ( */}
          <>
            <Select
              name="business_type"
              label="Business Type"
              // onChange={(e) => setBusinessCategory(e.target.value)}
              onChange={(e) => setBusinessType(e.target.value)}
              required
            >
              <option value="">Select</option>
              <option value="Food">Food</option>
              <option value="Non-Food">Non-Food</option>
            </Select>

            {/* <Select
                name="service_provider"
                label="Type of Service Provider"
                // onChange={(e) => setServiceProvider(e.target.value)}
                onChange={(e) =>
                  setServiceProvider(e.target.value === "Restaurant")
                }
                required
              >
                <option value="">Select</option>
                {businessCategory && (
                  <>
                    <option value="Chef">Chef</option>
                    <option value="Caterer">Caterer</option>
                    <option value="Restaurant">Restaurant</option>
                    <option value="Food Truck">Food Truck</option>
                  </>
                )}

                {businessCategory === "Entertainment" && (
                  <>
                    <option value="DJ">DJ</option>
                    <option value="Musician">Musician</option>
                    <option value="Dancer">Dancer</option>
                    <option value="Clown">Clown</option>
                  </>
                )}

                {businessCategory === "Venues" && (
                  <>
                    <option value="Restaurant">Restaurant</option>
                    <option value="Banquet Hall">Banquet Hall</option>
                  </>
                )}

                {businessCategory === "Transportation" && (
                  <>
                    <option value="Taxi">Taxi</option>
                    <option value="Limo">Limo</option>
                    <option value="Bus">Bus</option>
                  </>
                )}

                {businessCategory === "Party Suppliers" && (
                  <>
                    <option value="Balloon Shop">Balloon Shop</option>
                    <option value="Decoration Shop">Decoration Shop</option>
                  </>
                )}

                {businessCategory === "MC" && (
                  <>
                    <option value="Hosting">Hosting</option>
                  </>
                )}
              </Select> */}

            <Input
              name={props.sponsor ? 'sponsor_name' : 'business_name'}
              label={`${values.is_sponsor || props.sponsor ? 'Business' : 'Restaurant'} Name`}
              required //={values.is_sponsor || props.sponsor ? false : true}
            />

            {/* {serviceProvider && (
                <Select
                  name="culture"
                  label="How do you identify your restaurant?"
                  required
                >
                  <option value="">--Select--</option>
                  {displayNationalitiesInSelect()}
                </Select>
              )} */}

            {/* {values.business_category === "Transportation" && (
                <Input
                  name="transportation_rate"
                  label="Rate (charge per km)"
                  required
                  type="number"
                  step=".01"
                />
              )} */}

            {/* {!context.state.signedInStatus && (
                <Checkbox name="use_residential" onClick={updateAddressInfo}>
                  Business address is same as residential address
                </Checkbox>
              )} */}

            {/*             <div className={!readMode ? "hide-input" : ""}>
              <div className="mb-3">
                <div className="input-title">{`${
                  values.is_sponsor || props.sponsor
                    ? "Sponsor Organization"
                    : "Restaurant"
                } Address ${
                  values.is_sponsor || props.sponsor ? "" : "*"
                }`}</div>
                <AddressLookup
                  value={addressLookup}
                  onSelect={(address) => {
                    setAddressLookup(address);
                  }}
                />
              </div>
            </div>

            <div className={!readMode ? "hide-input" : ""}>
              <PreFillInput
                name={props.sponsor ? "sponsor_address_1" : "address_line_1"}
                label={`${
                  values.is_sponsor || props.sponsor
                    ? "Sponsor Organization"
                    : "Restaurant"
                } Address`}
                preFill={addressDetails[0]}
                required={values.is_sponsor || props.sponsor ? false : true}
              />
              <PreFillInput
                name={props.sponsor ? "sponsor_city" : "business_city"}
                label={`${
                  values.is_sponsor || props.sponsor
                    ? "Sponsor Organization"
                    : "Restaurant"
                } City`}
                preFill={addressDetails[1]}
                required={values.is_sponsor || props.sponsor ? false : true}
              />
              <PreFillInput
                name={props.sponsor ? "sponsor_state" : "state"}
                label={`${
                  values.is_sponsor || props.sponsor
                    ? "Sponsor Organization"
                    : "Restaurant"
                } Province or Territory`}
                preFill={addressDetails[2]}
                required={values.is_sponsor || props.sponsor ? false : true}
              />
              <PreFillInput
                name={props.sponsor ? "sponsor_country" : "country"}
                label={`${
                  values.is_sponsor || props.sponsor
                    ? "Sponsor Organization"
                    : "Restaurant"
                } Country`}
                preFill={addressDetails[3]}
                required={values.is_sponsor || props.sponsor ? false : true}
              />
            </div> */}
            <Input
              name={props.sponsor ? 'business_street_number' : 'business_street_number'}
              label={`${
                values.is_sponsor || props.sponsor ? 'Business ' : 'Restaurant'
              } Street Number`}
              required //={values.is_sponsor || props.sponsor ? false : true}
            />
            <Input
              name={props.sponsor ? 'business_street_name' : 'business_street_name'}
              label={`${
                values.is_sponsor || props.sponsor ? 'Business ' : 'Restaurant'
              } Street Name`}
              required //={values.is_sponsor || props.sponsor ? false : true}
            />
            <Input
              name={props.sponsor ? 'business_unit' : 'business_unit'}
              label={`${values.is_sponsor || props.sponsor ? 'Business' : 'Restaurant'} Unit`}
            />
            <Input
              name={props.sponsor ? 'sponsor_city' : 'city'}
              label={`${values.is_sponsor || props.sponsor ? 'Business' : 'Restaurant'} City`}
              required //={values.is_sponsor || props.sponsor ? false : true}
            />
            <Select
              name={props.sponsor ? 'sponsor_country' : 'country'}
              label={`${values.is_sponsor || props.sponsor ? 'Business' : 'Restaurant'} Country`}
              options={nationalities.map((n) => [n.en_short_name, n.en_short_name])}
              required //={values.is_sponsor || props.sponsor ? false : true}
            ></Select>

            <Select
              name={props.sponsor ? 'sponsor_state' : 'state'}
              label={`${
                values.is_sponsor || props.sponsor ? 'Business' : 'Restaurant'
              } Province or Territory`}
              required //={values.is_sponsor || props.sponsor ? false : false}
            >
              {canadaProvincesTerritories()}
            </Select>
            <Input
              name={props.sponsor ? 'sponsor_postal_code' : 'zip_postal_code'}
              label={`${
                values.is_sponsor || props.sponsor ? 'Business' : 'Restaurant'
              } Postal Code`}
              maxLength="7"
              required //={values.is_sponsor || props.sponsor ? false : true}
            />
          </>
          {/* )} */}
          {/*           {!values.is_sponsor && !props.sponsor ? (
            <Checkbox name="in_current_festival" label="In Current Festival" />
          ) : null} */}

          {!readMode && (
            <div className="apply-to-host-navigation">
              <span className="apply-to-host-navigation-spacing"></span>
              <input
                type="submit"
                value="Next"
                disabled={values.submitAuthDisabled}
                className="px-3 py-2 btn-danger rounded-pill"
              />
            </div>
          )}
        </Form>
      </div>
    </div>
  );
};

export default BusinessInfoForm;
