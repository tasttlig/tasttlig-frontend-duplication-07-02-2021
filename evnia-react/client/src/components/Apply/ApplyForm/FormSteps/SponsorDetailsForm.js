// Libraries
import React, { useContext, useRef, useState } from 'react';
import axios from 'axios';
// import { Progress } from "react-sweet-progress";
import { AppContext } from '../../../../ContextProvider/AppProvider';

// Components
import { Form, Textarea, FileUpload, DateInput, Select } from '../../../EasyForm';

const SponsorDetailsForm = (props) => {
  const appContext = useContext(AppContext);
  const formRef = useRef(null);

  const { nextStep, prevStep, update, values, readMode } = props;

  const [hasBusiness, setHasBusiness] = useState();
  const [foodBusinessType, setFoodBusinessType] = useState();
  const [businessType, setBusinessType] = useState();
  const [isBusinessRegistered, setIsBusinessRegistered] = useState();
  const [isRetail, setIsRetail] = useState();

  const [foodHandlersCert, setFoodHandlersCert] = useState(values.food_handlers_certificate);

  let currentDate = () => {
    const date = new Date();
  };

  const FoodHandlersCert = () => (
    <>
      <FileUpload
        name="food_handlers_certificate"
        label="Food Handlers Certificate (PDF)"
        accept="application/pdf"
        dir="food_handlers_certificate"
        onChange={(val) => {
          setFoodHandlersCert(val);
          update({
            food_handlers_certificate: val,
          });
        }}
        required
      />

      {foodHandlersCert && (
        <>
          <DateInput
            name="food_handlers_certificate_date_of_issue"
            label="Date of Issue"
            maxDate={currentDate()}
            required
          />
          <DateInput
            name="food_handlers_certificate_date_of_expired"
            label="Expiry Date"
            maxDate={currentDate()}
            required
          />
        </>
      )}
    </>
  );

  // Update Sponsor Details part of multi-step form helper function
  const updateSponsorInfo = async (data) => {
    const response = await axios({
      method: 'PUT',
      url: '/user/update-business-info',
      data: {
        ...data,
        email: data.email || values.email,
      },
    });

    return response.data;
  };

  const onSubmit = async (data) => {
    if (!values.email || values.email === '') {
      data.email = appContext.state.user.email;
    }

    update(data);
    await updateSponsorInfo(data);
    nextStep();
  };

  // Render Sponsor Details part of multi-step form
  return (
    <div>
      {/* {!readMode && <Progress />} */}
      <div className={!readMode ? 'apply-to-host' : ''}>
        {!readMode ? (
          <h1 className="text-danger apply-to-host-step-name">Business Details</h1>
        ) : (
          <>
            <h6>Sponsor Details</h6>
            <hr />
          </>
        )}

        <Form data={values} onSubmit={onSubmit} readMode={readMode} formRef={formRef}>
          {/* <Textarea
            name={props.sponsor ? "sponsor_description" : "description"}
            label="How would you like to Sponsor?"
          /> */}

          {/* <Select
            name="has_business"
            label="Do you have a business?"
            onChange={(e) => setHasBusiness(e.target.value === "yes")}
            required
          >
            <option value="yes">Yes</option>
            <option value="no">No</option>
          </Select> */}

          {/* {hasBusiness && (  */}
          <>
            <Select
              name="business_registered"
              label="Is your business registered?"
              onChange={(e) => setIsBusinessRegistered(e.target.value === 'yes')}
              required
            >
              <option value="yes">Yes</option>
              <option value="no">No</option>
            </Select>

            <Select
              name="retail_business"
              label="Is your business retail?"
              onChange={(e) => setIsRetail(e.target.value === 'yes')}
              required
            >
              <option value="yes">Yes</option>
              <option value="no">No</option>
            </Select>

            <Select
              name="business_type"
              label="Business Type"
              // onChange={(e) => setBusinessCategory(e.target.value)}
              onChange={(e) => setBusinessType(e.target.value === 'Food')}
              required
            >
              <option value="">Select</option>
              <option value="Food">Food</option>
              <option value="Non for profit">Non for profit</option>
              <option value="Government">Government</option>
              <option value="Other business">Other business</option>
            </Select>

            <Select
              name="food_business_type"
              label="Food business type"
              // onChange={(e) => setServiceProvider(e.target.value)}
              onChange={(e) => setFoodBusinessType(e.target.value === 'Restaurant')}
              //required
            >
              <option value="">Select</option>
              {businessType && (
                <>
                  <option value="Restaurant">Restaurant</option>
                  <option value="Food Truck">Food Truck</option>
                  <option value="Caterer">Caterer</option>
                  <option value="Chef">Chef</option>
                </>
              )}
            </Select>

            <FoodHandlersCert />
          </>
          {/* )} */}

          {!readMode && (
            <div className="apply-to-host-navigation">
              {/* <span
                onClick={prevStep}
                disabled={values.submitAuthDisabled}
                className="back-btn"
              >
                Previous
              </span> */}
              <input
                type="button"
                value="Previous"
                onClick={prevStep}
                disabled={values.submitAuthDisabled}
                className="px-3 py-2 btn-info rounded-pill"
              />
              <span className="apply-to-host-navigation-spacing"></span>
              <input
                type="submit"
                value="Next"
                disabled={values.submitAuthDisabled}
                className="px-4 py-2 btn-danger rounded-pill"
              />
            </div>
          )}
        </Form>
      </div>
    </div>
  );
};

export default SponsorDetailsForm;
