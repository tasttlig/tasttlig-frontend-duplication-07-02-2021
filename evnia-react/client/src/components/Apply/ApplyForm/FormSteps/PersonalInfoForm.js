// Libraries
import React, { useState, useContext } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
// import { Progress } from "react-sweet-progress";
import { AppContext } from '../../../../ContextProvider/AppProvider';

// Components
import { Form, Input } from '../../../EasyForm';

// Styling
// import "react-sweet-progress/lib/style.css";

const PersonalInfoForm = (props) => {
  const { nextStep, update, values, readMode } = props;

  // Set initial state
  const [existingUser, setExistingUser] = useState(null);

  // To use the JWT credentials
  const appContext = useContext(AppContext);

  const userRole = appContext.state.user.role;

  // Check for existing user helper function
  const checkExistingUser = async (email) => {
    const response = await axios({
      method: 'GET',
      url: `/user/check-email/${email}`,
    });

    return response.data;
  };

  // Create new user from multi-step form helper function
  const createNewUser = async (data) => {
    const response = await axios({
      method: 'POST',
      url: '/user/create-new-multi-step-user',
      data,
    });

    return response.data;
  };

  const onSubmit = async (data) => {
    if (userRole && userRole.includes('ADMIN')) {
      data.created_by_admin = true;
    }
    const response = await checkExistingUser(data.email);

    if (response.success) {
      setExistingUser(response.user);
    } else {
      await createNewUser(data);
      update(data);
      nextStep();
    }
  };

  // Render user already exists page
  const UserExists = () => (
    <div className="account-already-exists">
      <p className="lead">
        An account already exists for <strong>{existingUser.email}</strong>.
        <br />
        {!appContext.state.user.id ? (
          <>Please login to access your dashboard.</>
        ) : (
          <>
            Back to{' '}
            <Link exact="true" to="/dashboard" className="dashboard-link">
              Dashboard
            </Link>
            .
          </>
        )}
      </p>

      {!appContext.state.user.id && (
        <Link exact="true" to="/login" className="log-in-btn">
          Login
        </Link>
      )}
    </div>
  );

  // Render Personal Information part of multi-step form
  return (
    <>
      {existingUser ? (
        <UserExists />
      ) : (
        <div>
          {/* {!readMode && <Progress />} */}
          <div className={!readMode ? 'apply-to-host' : ''}>
            {!readMode ? (
              <h1 className="apply-to-host-step-name">Personal Information</h1>
            ) : (
              <>
                <h6>Personal Information</h6>
                <hr />
              </>
            )}
            <Form data={values} onSubmit={onSubmit} readMode={readMode}>
              <Input
                name="first_name"
                required={values.is_host === 'no' || !values.is_host ? false : true}
                label={`First Name${
                  values.is_sponsor || props.sponsor
                    ? ' (Sponsor)'
                    : values.is_host === 'no' || !values.is_host
                    ? ' (Restaurant Staff)'
                    : ''
                }`}
              />
              <Input
                name="last_name"
                required={values.is_host === 'no' || !values.is_host ? false : true}
                label={`Last Name${
                  values.is_sponsor || props.sponsor
                    ? ' (Sponsor)'
                    : values.is_host === 'no' || !values.is_host
                    ? ' (Restaurant Staff)'
                    : ''
                }`}
              />
              <Input
                name="email"
                type="email"
                required={true}
                label={`${
                  values.is_sponsor || props.sponsor
                    ? 'Sponsor '
                    : values.is_host === 'no' || !values.is_host
                    ? 'Restaurant '
                    : ''
                } Email`}
              />
              <Input
                name="phone_number"
                type="tel"
                required={values.is_host === 'no' || !values.is_host ? false : true}
                label={`${
                  values.is_sponsor || props.sponsor
                    ? 'Sponsor '
                    : values.is_host === 'no' || !values.is_host
                    ? 'Restaurant '
                    : ''
                } Phone Number`}
              />

              {!readMode && (
                <div className="apply-to-host-navigation">
                  <span className="apply-to-host-navigation-spacing"></span>
                  <input
                    type="submit"
                    value="Continue"
                    disabled={values.submitAuthDisabled}
                    className="continue-btn"
                  />
                </div>
              )}
            </Form>
          </div>
        </div>
      )}
    </>
  );
};

export default PersonalInfoForm;
