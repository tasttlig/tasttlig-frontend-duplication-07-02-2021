// Libraries
import React, { Component } from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import { AppContext } from '../../../ContextProvider/AppProvider';

// Components
import PersonalInfoForm from './FormSteps/PersonalInfoForm';
import BusinessInfoForm from './FormSteps/BusinessInfoForm';
import FormReview from './FormSteps/FormReview';

// Styling
import 'react-toastify/dist/ReactToastify.css';

toast.configure();

export default class ApplyForm extends Component {
  static contextType = AppContext;

  // Set initial state
  state = {
    step: 0,

    // Personal information form fields
    first_name: '',
    last_name: '',
    email: '',
    phone_number: '',
    residential_address_line_1: '',
    residential_address_line_2: '',
    residential_city: '',
    residential_state: '',
    residential_postal_code: '',
    residential_country: 'CANADA',

    // Business information form fields
    // has_business: "no",
    // has_business: "yes",
    // business_category: "",
    // business_category: "Food",
    // service_provider: "",
    // service_provider: "Restaurant",
    use_residential: false,
    business_name: '',
    business_type: '',
    culture: '',
    address_line_1: '',
    address_line_2: '',
    business_city: '',
    state: '',
    postal_code: '',
    country: '',
    registration_number: '',
    facebook: '',
    instagram: '',
    transportation_rate: 0,

    // Food samples form fields
    foodSampleList: [],
    isFoodSamples: false,

    // Services form fields
    services: [],

    // Documents form fields
    food_handler_certificate: '',
    food_handler_certificate_date_of_issue: '',
    food_handler_certificate_date_of_expired: '',
    dine_safe_certificate: '',
    dine_safe_certificate_date_of_issue: '',
    dine_safe_certificate_date_of_expired: '',
    health_safety_certificate: '',
    health_safety_certificate_date_of_issue: '',
    health_safety_certificate_date_of_expired: '',
    insurance: '',
    insurance_date_of_issue: '',
    insurance_date_of_expired: '',
    government_id: '',
    government_id_date_of_issue: '',
    government_id_date_of_expired: '',

    // Assets form fields
    has_assets: 'no',
    assets: [],

    // Promotions form fields
    festival_participant: 'no',
    samples_available: 'yes',
    samples_per_day: 1,
    samples: [],

    // Payment information form fields
    banking: '',
    bank_number: '',
    account_number: '',
    institution_number: '',
    void_cheque: '',
    online_email: '',
    paypal_email: '',
    stripe_account: '',

    // Social proof form fields
    yelp_review: '',
    google_review: '',
    tripadvisor_review: '',
    instagram_review: '',
    facebook_review: '',
    youtube_review: '',
    personal_review: '',
    media_recognition: '',

    // Host form fields
    is_host: 'no',
    host_selection: '',
    host_selection_resume: '',
    host_selection_video: '',
    host_youtube_link: '',
    hostCreation: true,

    // Cook form fields
    is_cook: 'no',
    cook_selection: '',
    cook_selection_resume: '',
    cook_selection_video: '',
    cook_youtube_link: '',

    // Product form fields
    menu_list: [],
    sample_links: [],
    venue_name: '',
    venue_description: '',
    venue_photos: [],

    submitAuthDisabled: false,
  };

  // Set local storage for Host multi-step form helper function
  UNSAFE_componentWillMount = () => {
    const hostFormData = JSON.parse(localStorage.getItem('hostFormData'));

    if (hostFormData) {
      Object.keys(this.state).forEach((key) => {
        const value = hostFormData[key];

        if (value) {
          this.setState({
            [key]: value,
          });
        }
      });
    }

    if (this.state.email === '') {
      this.setState({
        email: this.context.state.user.email,
      });
    }
  };

  // Proceed to next step helper function
  nextStep = () => {
    window.scrollTo(0, 0);

    this.setState({
      step: this.state.step + 1,
    });
  };

  // Go back to previous step helper function
  prevStep = () => {
    window.scrollTo(0, 0);

    this.setState({ step: this.state.step - 1 });
  };

  // Update Host multi-step form data helper function
  update = (data) => {
    const hostFormData = JSON.parse(localStorage.getItem('hostFormData')) || {};

    localStorage.setItem('hostFormData', JSON.stringify({ ...hostFormData, ...data }));

    Object.keys(data).forEach((key) => {
      this.setState({ [key]: data[key] });
    });
  };

  // Submit Host form helper function
  handleSubmitApplication = async (event) => {
    event.preventDefault();

    const urlHost = '/user/host';
    // const urlFoodSamples = "/food-samples/add";

    const { submitAuthDisabled, use_residential, ...data } = this.state;

    try {
      const responseHost = await axios({ method: 'POST', url: urlHost, data });
      // const responseFoodSamples = await axios({
      //   method: "POST",
      //   url: urlFoodSamples,
      //   data: this.state.foodSampleList,
      // });

      if (
        responseHost &&
        // responseFoodSamples &&
        responseHost.data &&
        // responseFoodSamples.data &&
        responseHost.data.success
        // responseFoodSamples.data.success
      ) {
        localStorage.removeItem('hostFormData');

        setTimeout(() => {
          window.location.href = '/';
        }, 2000);

        toast(`Success! Thank you for submitting your application!`, {
          type: 'success',
          autoClose: 2000,
        });

        this.setState({ submitAuthDisabled: true });
      }
    } catch (error) {
      toast('Error! Something went wrong!', {
        type: 'error',
        autoClose: 2000,
      });
    }
  };

  // Get Host multi-step form steps helper function
  getSteps = () => {
    let steps = [];

    if (
      !this.context.state.signedInStatus ||
      (this.context.state.user.role && this.context.state.user.role.includes('ADMIN'))
    ) {
      steps = [...steps, PersonalInfoForm];
    }

    steps = [...steps, BusinessInfoForm, FormReview];

    return steps;
  };

  // Mount Host multi-step form
  componentDidMount = () => {
    window.scrollTo(0, 0);
  };

  // Render Host multi-step form
  render = () => {
    const { step, ...values } = this.state;
    const Step = this.getSteps()[step];

    return (
      <Step
        nextStep={this.nextStep}
        prevStep={this.prevStep}
        update={this.update}
        values={values}
        handleSubmitApplication={this.handleSubmitApplication}
      />
    );
  };
}
