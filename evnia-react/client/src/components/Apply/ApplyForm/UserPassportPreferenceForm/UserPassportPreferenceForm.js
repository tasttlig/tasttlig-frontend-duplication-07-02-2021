// Libraries
import React, { useEffect, useContext, useState } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { AppContext } from '../../../../ContextProvider/AppProvider';
import { AuthModalContext } from '../../../../ContextProvider/AuthModalProvider';
import {
  countriesOnPassport,
  foodsOnPassport,
  allergiesOnPassport,
  countriesOnPassportPreferences
} from '../../../Functions/Functions';
import { useForm, Controller } from 'react-hook-form';
import 'react-toastify/dist/ReactToastify.css';
import { formatPhoneNumber } from '../../../Functions/Functions';
import { toast } from 'react-toastify';
import { useHistory } from 'react-router-dom';

import Select from 'react-select';
import makeAnimated from 'react-select/animated';

// Components

// Styling
import './UserPassportPreferenceForm.scss';
import tasttligLogoBlack from '../../../../assets/images/tasttlig-logo-black.png';
import thumbsUpLogo from '../../../../assets/images/thumbsUp.png';
import { PassportConfirmationModal } from './PassportConfirmationModal.js';
import { Modal } from 'react-bootstrap';

toast.configure();

const UserPassportPreferenceForm = (props) => {

  const dataFromFestival = localStorage.getItem('dataFromFestival');

  console.log('props from passport: ', props);
  const profileData = {
    firstName: props.user.first_name,
    lastName: props.user.last_name,
    phoneNumber: props.user.phone_number,
    addressLine1: props.user.address_line_1,
    addressLine2: props.user.address_line_2,
    city: props.user.city,
    provinceTerritory: props.user.state,
    postalCode: props.user.postal_code,
    addressType: props.user.address_type,
    businessName: props.user.business_name,
    businessType: props.user.business_type,
  };
  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const authModalContext = useContext(AuthModalContext);
  const { update, handleSubmit, register, control, setValue, errors } = useForm({ defaultValues: profileData });
  const [provinceTerritory, setProvinceTerritory] = useState(profileData.provinceTerritory);
  const [countryCuisine, setcountryCuisine] = useState([]);
  const [foodPreference, setFoodPreference] = useState([]);
  const [allergyType, setAllergyType] = useState([]);
  const [submitAuthDisabled, setSubmitAuthDisabled] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [referenceTypeValue, setReferenceTypeValue] = useState([]);
  const [nationalities, setNationalities] = useState([]);


  
  const animatedComponents = makeAnimated();

  // option for selectors
  const cuisineOption = nationalities.map((n) => ({
    value: n.nationality,
    label: n.nationality,
  }));

  const preferenceOption = [
    { value: 'Rice', label: 'Rice' },
    { value: 'Bread', label: 'Bread' },
    { value: 'Vegetarian', label: 'Vegetarian' },
    { value: 'Vegan', label: 'Vegan' },
    { value: 'Meat', label: 'Meat' },
    { value: 'Salads', label: 'Salads' },
    { value: 'Desserts', label: 'Desserts' },
    { value: 'Halal', label: 'Halal' },
    { value: 'Kosher', label: 'Kosher' },
  ];

  const allergyOption = [
    { value: 'No allergies', label: 'No allergies' },
    { value: 'Milk', label: 'Milk' },
    { value: 'Eggs', label: 'Eggs' },
    { value: 'Peanuts', label: 'Peanuts' },
    { value: 'Tree Nuts', label: 'Tree Nuts' },
    { value: 'Soy', label: 'Soy' },
    { value: 'Wheat', label: 'Wheat' },
    { value: 'Fish', label: 'Fish' },
    { value: 'Shellfish', label: 'Shellfish' },
    { value: 'Gelatin', label: 'Gelatin' },
    { value: 'Gluten', label: 'Gluten' },
    { value: 'Meat', label: 'Meat' },
    { value: 'Seeds', label: 'Seeds' },
    { value: 'Spices', label: 'Spices' },
  ];

  const referenceOption = [
    { value: 'Facebook', label: 'Facebook' },
    { value: 'Instagram', label: 'Instagram' },
    { value: 'Google', label: 'Google' },
    { value: 'Linkedin', label: 'Linkedin' },
    { value: 'Flyer', label: 'Flyer' },
  ];



  // Submit profile form helper function
  const onSubmitPreferenceForm = async (values) => {
      const {
        preferred_cuisine,
        preferred_food,
        allergy,
        reference,
        ...rest
      } = values;
      console.log("values coming from the form:", values);

      // Saved the value sent by the selector into arrays
      if(preferred_cuisine) {
        for (let value of preferred_cuisine) {
          countryCuisine.push(value.value);
          //setcountryCuisine(countryCuisine.concat(value.value));
        }
      }

      if(preferred_food) {
        for (let value of preferred_food) {
          foodPreference.push(value.value);
        }
      } 

      if(allergy) {
        for (let value of allergy) {
          allergyType.push(value.value);
        }
      }

      if(reference) {
        for (let value of reference) {
          referenceTypeValue.push(value.value);
        }
      } 

      
    if (props.user.id) {
      const url = `/complete-profile/preference/${props.user.id}`;
      // Update the data ready to be uploaded with recently saved arrays 
      const data = {
        ...rest,
        preferred_country_cuisine: countryCuisine,
        food_preferences: foodPreference,
        food_allergies: allergyType,
        socialmedia_reference: referenceTypeValue,
        
      };
      console.log("data coming from the form:", data);
      try {
        const response = await axios({ method: 'POST', url, data });

        if (response && response.data && response.data.success) {
          if (dataFromFestival && dataFromFestival === 'Host') {
            setTimeout(() => {
              window.location.href = '/business-passport';
            }, 2000);
          } else {
            // remove selector from client screen
            setShowModal(true);
          }
          // display success message
          toast('Success! Your profile has been updated!', {
            type: 'success',
            autoClose: 2000,
          });

          setSubmitAuthDisabled(true);
        }
      } catch (error) {

        toast('Error! Something went wrong!', {
          type: 'error',
          autoClose: 2000,
        });
      }
      setTimeout(() => {
        localStorage.setItem('tab', '2');
        window.location.href = '/passport/' + appContext.state.user.id;
      }, 2000);
    }
  };


  
  console.log("countryCuisine coming from onChange country cuisine:", countryCuisine)


  const onChangeFoodPreference = function (event) {
    //takes the file and reads the file from buffer of array

    if (foodPreference.includes(event.target.value)) {
      console.log('error');
    } else {
      setFoodPreference(foodPreference.concat(event.target.value));
    }
  };

  const onChangeAllergy = function (event) {
    //takes the file and reads the file from buffer of array
    if (allergyType.includes(event.target.value)) {
      console.log('error');
    } else {
      setAllergyType(allergyType.concat(event.target.value));
    }
  };

  // update nationalities array with values fetched from back end
  const fetchNationalities = async () => {
    const response = await axios({
      method: 'GET',
      url: '/nationalities',
    });
    console.log('nationality', response.data);
    setNationalities(response.data.nationalities);
  };  


  const getOptionsFoodPreference= function (arr) {
    console.log("arr from get options:", arr)
    return arr.map((item) => (
      <button 
        className="festival-card-festival-attend-btn"
        onClick={(event) => {
          setFoodPreference((foodPreferences) => {
            return foodPreferences.filter((foodPreference) => {
              if(foodPreference === item) {
                return false;
              } else {
                return true;
              }
            })
          })
        }}
        >
          {item}
      </button>));
  };

  const getOptionsAllergies= function (arr) {
    console.log("arr from get options:", arr);
    return arr.map((item) => (
      <button 
        className="festival-card-festival-attend-btn"
        onClick={(event) => {
          setAllergyType((allergyTypes) => {
            return allergyTypes.filter((allergyType) => {
              if(allergyType === item) {
                return false;
              } else {
                return true;
              }
            })
          })
        }}
        >
          {item}
      </button>));
  };

  const openModal = () => {
    setShowModal((prev) => !prev);
  };
  console.log("nationalities from preferecnes:", nationalities)

  const productNationalities = (countries) => {
    
    countries.map((n) => ({
    value: n.nationality,
    label: n.nationality,
  }));
}
    // Mount Sign Up page
    useEffect(() => {
      window.scrollTo(0, 0);
      fetchNationalities();
    }, []);


   // Render Sign Up page
  return (
    <div className="row">
      <div className="col-lg-8 col-xl-6 px-0 sign-up-background-image-one" />
      <div className="col-lg-4 col-xl-4 px-0 sign-up-background">
        <div className="sign-up-content1">
          <div className=" mb-4 text-center">
            <Link exact="true" to="/">
              <img src={tasttligLogoBlack} alt="Tasttlig" className="main-navbar-logo" />
            </Link>
          </div>
          
          {appContext.state.errorMessage && (
            <div className="mb-3 text-center invalid-message">{appContext.state.errorMessage}</div>
          )}
          {showModal === false ? (
            <form onSubmit={handleSubmit(onSubmitPreferenceForm)} noValidate>

              <div className="mb-4">
                <h5>What Cuisine Would You Like To Taste At Tasttlig?</h5>
                <div className="input-title">Select all that apply</div>
                    <Controller
                      closeMenuOnSelect={false}
                      components={animatedComponents}
                      isMulti
                      as={Select}
                      options={cuisineOption}
                      name="preferred_cuisine"
                      type="text"
                      control={control}
                      // className={errors.dietaryRestrictions ? 'form-control invalid-input' : 'form-control'}
                      placeholder="Cuisine Available"
                      ref={register({ required: true })}
                    />
       
           
              </div>

              <div className="mb-4">
                <h5>What Is Your Food Preference?</h5>
                <div className="input-title">Select all that apply</div>
                 <Controller
                      closeMenuOnSelect={false}
                      components={animatedComponents}
                      isMulti
                      as={Select}
                      options={preferenceOption}
                      name="preferred_food"
                      type="text"
                      control={control}
                      // className={errors.dietaryRestrictions ? 'form-control invalid-input' : 'form-control'}
                      placeholder="Food Preference"
                      ref={register({ required: true })}
                    />
              </div>

              <div className="mb-4">
                <h5>Do You Have Any Allergies</h5>
                <div className="input-title">Select all that apply</div>
                 <Controller
                      closeMenuOnSelect={false}
                      components={animatedComponents}
                      isMulti
                      as={Select}
                      options={allergyOption}
                      name="allergy"
                      type="text"
                      control={control}
                      // className={errors.dietaryRestrictions ? 'form-control invalid-input' : 'form-control'}
                      placeholder="Allergy"
                      ref={register({ required: true })}
                    />
              </div>

            <div className="mb-4">
                <div className="business-registered-title">Where did you hear about us? (optional)</div>
                <Controller
                      closeMenuOnSelect={false}
                      components={animatedComponents}
                      isMulti
                      as={Select}
                      options={referenceOption}
                      name="reference"
                      type="text"
                      control={control}
                      // className={errors.dietaryRestrictions ? 'form-control invalid-input' : 'form-control'}
                      placeholder="Media"
                      ref={register({ required: false })}
                    />
            </div>

              <div className="create-button1">
                {/* <Link exact="true" to="/user-info" className="go-back">
                  <button className="continue-button mr-8">Back</button>
                </Link> */}

                <button
                  type="submit"
                  // disabled={authModalContext.state.submitAuthDisabled}
                  className="continue-button"
                >
                  Submit
                </button >
                {showModal ? (
                  <PassportConfirmationModal showModal={showModal} setShowModal={setShowModal} />
                ) : null}
              </div>
            </form>
          ) : (null
            // <div>
            //   <Modal show={true}>
            //     <Modal.Body className="modal-header-1">
            //       <div className="modal-div">
            //         <img className="modal-img1" src={tasttligLogoBlack} alt="Tasttlig" />
            //       </div>
            //       <div className="modal-div-2">
            //         <img className="modal-img" src={thumbsUpLogo} alt="Tasttlig" />
            //       </div>
            //       <div className="modal-div-7">Congratulations!</div>
            //       <div className="modal-div-4">You have successfully created your tasttlig passport !!</div>
            //       <div className="modal-div-5">
            //         <button
            //           type="button"
            //           className="modal-button2"
            //           onClick={() => {
            //             localStorage.removeItem('userInfoData');
            //             window.location.href = '/';
            //           }}
            //         >
            //           View Tasttlig{' '}
            //         </button>
            //       </div>
            //       <div className="modal-div-6">
            //         <button
            //           type="button"
            //           className="modal-button1"
            //           onClick={() => {
            //             localStorage.removeItem('userInfoData');
            //             window.location.href = '/business-passport';
            //           }}
            //         >
            //           Do Business With Tasttlig{' '}
            //         </button>
            //       </div>
            //     </Modal.Body>
            //   </Modal>
            // </div>
          )}
        </div>
      </div>
      <div className="col-xl-2 px-0 sign-up-background-image-two" />
    </div>
  );
};

export default UserPassportPreferenceForm;
