// Libraries
import React, { useEffect } from 'react';

// Components
import Nav from '../Navbar/Nav';
import ApplyForm from './ApplyForm/ApplyForm';

// Styling
import './Apply.scss';

const Apply = (props) => {
  // Source tracking on new sign ups
  const source = new URLSearchParams(props.location.search).get('source');
  if (source) {
    if (!localStorage.getItem('source')) {
      localStorage.setItem('source', source);
    }
  }

  // Mount Host Festival page
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  // Render Host Festival page
  return (
    <div>
      <div className="become-host-form">
        <Nav />
        <ApplyForm history={props.history} />
      </div>
    </div>
  );
};

export default Apply;
