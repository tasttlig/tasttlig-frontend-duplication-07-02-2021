// Libraries
import React, { useEffect } from 'react';

// Components
import Nav from '../Navbar/Nav';
import SponsorForm from './ApplyForm/SponsorForm';

// Styling
import './Apply.scss';

const Sponsor = (props) => {
  // Source tracking on new sign ups
  const source = new URLSearchParams(props.location.search).get('source');
  if (source) {
    if (!localStorage.getItem('source')) {
      localStorage.setItem('source', source);
    }
  }

  // Mount Sponsor page
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  // Render Sponsor page
  return (
    <div>
      <Nav />
      <SponsorForm />
    </div>
  );
};

export default Sponsor;
