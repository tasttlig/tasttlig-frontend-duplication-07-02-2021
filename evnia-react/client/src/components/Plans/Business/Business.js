// Libraries
import React, { useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { AppContext } from '../../../ContextProvider/AppProvider';

// Components
import Nav from '../../Navbar/Nav';
import Footer from '../../Home/BannerFooter/BannerFooter';

// Styling
import './Business.scss';

const Business = (props) => {
  // Source tracking on new sign ups
  const source = new URLSearchParams(props.location.search).get('source');
  if (source) {
    if (!localStorage.getItem('source')) {
      localStorage.setItem('source', source);
    }
  }

  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const userRole = appContext.state.user.role;

  // Mount Business Plans page
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  // Render Business Plans page
  return (
    <div>
      <Nav />

      <div className="plans-business">
        {/* <div className="jumbotron plans-business-banner">
          <h1 className="mb-0 plans-business-banner-statement">
            Business Membership Plans
          </h1>
        </div> */}

        <div className="plans-business-subscription-options">
          <div className="mb-5 border text-center">
            <div className="plans-business-pro">
              Business
              <br />
              Pro
            </div>
            <div className="card rounded-0">
              <div className="card-body p-3 text-left plans-business-subscription-text">
                <ul className="fa-ul mb-0 ml-4">
                  <li className="mb-2">
                    <span className="fa-li fas fa-check plans-business-check"></span>
                    Participate in festivals
                  </li>
                  <li className="mb-2">
                    <span className="fa-li fas fa-check plans-business-check"></span>
                    Marketing Services (promotional feature including discounts) on selling
                    products, services and experiences
                  </li>
                  <li className="mb-2">
                    <span className="fa-li fas fa-check plans-business-check"></span>
                    Technology Services
                  </li>
                  <li className="mb-2">
                    <span className="fa-li fas fa-check plans-business-check"></span>
                    Financial Services (having access to funding from investors)
                  </li>
                </ul>
              </div>
            </div>
            <div className="card rounded-0">
              <div className="card-body p-3 bg-light">
                <div className="plans-business-price">$5,000 CAD / year</div>
              </div>
            </div>
            {!appContext.state.user.id ? (
              <div>
                <Link
                  exact="true"
                  to="/payment/plan/business-pro"
                  className="plans-business-get-started-btn"
                >
                  Get Started
                </Link>
              </div>
            ) : userRole && userRole.includes('ADMIN') ? null : userRole &&
              !userRole.includes('MEMBER_BUSINESS_PRO') ? (
              <div>
                <Link
                  exact="true"
                  to="/payment/plan/business-pro"
                  className="plans-business-get-started-btn"
                >
                  Get Started
                </Link>
              </div>
            ) : (
              <div className="plans-business-pro-btn">You Are On This Plan</div>
            )}
          </div>

          {/* <div className="row text-center">
            <div className="col-xl-3 px-3">
              <div className="mb-5 border">
                <div className="plans-business-free">
                  Business
                  <br />
                  Free
                </div>
                <div className="card rounded-0">
                  <div className="card-body p-3 text-left plans-business-subscription-text">
                    <ul className="fa-ul mb-0 ml-4">
                      <li className="mb-2">
                        <span className="fa-li fas fa-check plans-business-check"></span>
                        Participate in festivals
                      </li>
                    </ul>
                  </div>
                </div>
                <div className="card rounded-0">
                  <div className="card-body p-3 bg-light">
                    <div className="plans-business-free-price">Free</div>
                  </div>
                </div>
                {!appContext.state.user.id ? (
                  <div>
                    <Link
                      exact="true"
                      to="/login"
                      className="plans-business-get-started-btn"
                    >
                      Get Started
                    </Link>
                  </div>
                ) : userRole && userRole.includes("ADMIN") ? null : (
                  <div>
                    <Link
                      exact="true"
                      to="/"
                      className="plans-business-get-started-btn"
                    >
                      Get Started
                    </Link>
                  </div>
                )}
              </div>
            </div>

            <div className="col-xl-3 px-3">
              <div className="mb-5 border">
                <div className="plans-business-basic">
                  Business
                  <br />
                  Basic
                </div>
                <div className="card rounded-0">
                  <div className="card-body p-3 text-left plans-business-subscription-text">
                    <ul className="fa-ul mb-0 ml-4">
                      <li className="mb-2">
                        <span className="fa-li fas fa-check plans-business-check"></span>
                        Participate in festivals
                      </li>
                      <li className="mb-2">
                        <span className="fa-li fas fa-check plans-business-check"></span>
                        Marketing Services (promotional feature including
                        discounts) on selling products, services and experiences
                      </li>
                    </ul>
                  </div>
                </div>
                <div className="card rounded-0">
                  <div className="card-body p-3 bg-light">
                    <div>
                      <span className="plans-business-original-price">
                        $720 CAD
                      </span>
                      &nbsp;&nbsp;
                      <span className="plans-business-discount-price">
                        $360 CAD
                      </span>
                    </div>
                  </div>
                </div>
                {!appContext.state.user.id ? (
                  <div>
                    <Link
                      exact="true"
                      to="/payment/plan/business-basic"
                      className="plans-business-get-started-btn"
                    >
                      Get Started
                    </Link>
                  </div>
                ) : userRole && userRole.includes("ADMIN") ? null : userRole &&
                  !userRole.includes("MEMBER_BUSINESS_BASIC") ? (
                  <div>
                    <Link
                      exact="true"
                      to="/payment/plan/business-basic"
                      className="plans-business-get-started-btn"
                    >
                      Get Started
                    </Link>
                  </div>
                ) : (
                  <div className="plans-business-basic-btn">
                    You Are On This Plan
                  </div>
                )}
              </div>
            </div>

            <div className="col-xl-3 px-3">
              <div className="mb-5 border">
                <div className="plans-business-intermediate">
                  Business
                  <br />
                  Intermediate
                </div>
                <div className="card rounded-0">
                  <div className="card-body p-3 text-left plans-business-subscription-text">
                    <ul className="fa-ul mb-0 ml-4">
                      <li className="mb-2">
                        <span className="fa-li fas fa-check plans-business-check"></span>
                        Participate in festivals
                      </li>
                      <li className="mb-2">
                        <span className="fa-li fas fa-check plans-business-check"></span>
                        Marketing Services (promotional feature including
                        discounts) on selling products, services and experiences
                      </li>
                      <li className="mb-2">
                        <span className="fa-li fas fa-check plans-business-check"></span>
                        Human Resources Services (helping to hire temporary
                        people in the hospitality industry)
                      </li>
                    </ul>
                  </div>
                </div>
                <div className="card rounded-0">
                  <div className="card-body p-3 bg-light">
                    <div>
                      <span className="plans-business-original-price">
                        $4,998 CAD
                      </span>
                      &nbsp;&nbsp;
                      <span className="plans-business-discount-price">
                        $2,499 CAD
                      </span>
                    </div>
                  </div>
                </div>
                {!appContext.state.user.id ? (
                  <div>
                    <Link
                      exact="true"
                      to="/payment/plan/business-intermediate"
                      className="plans-business-get-started-btn"
                    >
                      Get Started
                    </Link>
                  </div>
                ) : userRole && userRole.includes("ADMIN") ? null : userRole &&
                  !userRole.includes("MEMBER_BUSINESS_INTERMEDIATE") ? (
                  <div>
                    <Link
                      exact="true"
                      to="/payment/plan/business-intermediate"
                      className="plans-business-get-started-btn"
                    >
                      Get Started
                    </Link>
                  </div>
                ) : (
                  <div className="plans-business-intermediate-btn">
                    You Are On This Plan
                  </div>
                )}
              </div>
            </div>

            <div className="col-xl-3 px-3">
              <div className="mb-5 border">
                <div className="plans-business-advanced">
                  Business
                  <br />
                  Advanced
                </div>
                <div className="card rounded-0">
                  <div className="card-body p-3 text-left plans-business-subscription-text">
                    <ul className="fa-ul mb-0 ml-4">
                      <li className="mb-2">
                        <span className="fa-li fas fa-check plans-business-check"></span>
                        Participate in festivals
                      </li>
                      <li className="mb-2">
                        <span className="fa-li fas fa-check plans-business-check"></span>
                        Marketing Services (promotional feature including
                        discounts) on selling products, services and experiences
                      </li>
                      <li className="mb-2">
                        <span className="fa-li fas fa-check plans-business-check"></span>
                        Human Resources Services (helping to hire temporary
                        people in the hospitality industry)
                      </li>
                      <li className="mb-2">
                        <span className="fa-li fas fa-check plans-business-check"></span>
                        Financial Services (having access to funding from
                        investors)
                      </li>
                    </ul>
                  </div>
                </div>
                <div className="card rounded-0">
                  <div className="card-body p-3 bg-light">
                    <div>
                      <span className="plans-business-original-price">
                        $5,998 CAD
                      </span>
                      &nbsp;&nbsp;
                      <span className="plans-business-discount-price">
                        $2,999 CAD
                      </span>
                    </div>
                  </div>
                </div>
                {!appContext.state.user.id ? (
                  <div>
                    <Link
                      exact="true"
                      to="/payment/plan/business-advanced"
                      className="plans-business-get-started-btn"
                    >
                      Get Started
                    </Link>
                  </div>
                ) : userRole && userRole.includes("ADMIN") ? null : userRole &&
                  !userRole.includes("MEMBER_BUSINESS_ADVANCED") ? (
                  <div>
                    <Link
                      exact="true"
                      to="/payment/plan/business-advanced"
                      className="plans-business-get-started-btn"
                    >
                      Get Started
                    </Link>
                  </div>
                ) : (
                  <div className="plans-business-advanced-btn">
                    You Are On This Plan
                  </div>
                )}
              </div>
            </div>
          </div> */}
        </div>

        <div className="plans-business-footer">
          <div className="lead text-gray-700 text-center">
            <div className="plans-business-footer-text">7-Day Money Back Guarantee</div>
            <div>
              Contact <a href="mailto:support@tasttlig.com">support@tasttlig.com</a> for money back
            </div>
            <div>You will get your money back in 2 to 5 business days</div>
          </div>
        </div>
      </div>

      <Footer />
    </div>
  );
};

export default Business;
