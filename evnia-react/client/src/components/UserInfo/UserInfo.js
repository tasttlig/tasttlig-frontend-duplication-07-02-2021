// Libraries
import axios from 'axios';
import React, { useEffect, useContext, useState, Fragment } from 'react';
import { Link } from 'react-router-dom';
import { AppContext } from '../../ContextProvider/AppProvider';
import { AuthModalContext } from '../../ContextProvider/AuthModalProvider';
import { toast } from 'react-toastify';
import { useForm } from 'react-hook-form';
import { CountryDropdown, RegionDropdown, CountryRegionData } from 'react-country-region-selector';
import GooglePlacesAutocomplete from 'react-google-places-autocomplete';
import Form from 'react-bootstrap/Form';
import DatePicker from 'react-datepicker';
import Nav from '../Navbar/Nav';
import Footer from '../Footer/Footer';
import GoTop from '../Shared/GoTop';
import { fileUploadAsync } from '../../functions/FileUpload';

//components
// import NationalitySelector from "../Passport/NationalitySelector";
import AsyncSelect from 'react-select/async';
import Select from '../EasyForm/Controls/Select';
// import { canadaProvincesTerritories } from "../../../Functions/Functions";

// Styling
import './UserInfo.scss';
import tasttligLogoBlack from '../../assets/images/tasttlig-logo-black.png';
import UserBackgroundImage from '../../assets/images/free-food.jpg';
import { nationalities } from '../Apply/nationality';
import { Col, Row, ToastBody } from 'react-bootstrap';
// import { response } from "express";

// const NationalitySelector = (props) => {
//     const toggleNationality = props.toggleNationality;
//     const selectedItem = props.selectedItem;
//     const setSelectedItem = props.setSelectedItem;

//     const onChange = (selectedItem) => {
//       toggleNationality(selectedItem);
//       setSelectedItem(selectedItem);
//     };

//     const loadOptions = async (inputText, callback) => {
//       const response = await axios({
//         method: "GET",
//         url: `/food-sample/user-nationalities`,
//         params: {
//           keyword: inputText,
//           selectedNationality: selectedItem
//         }
//       });

//       callback(
//         response.data.details.map((item) => ({
//           label: `${item.nationality}`,
//           value: item,
//         }))
//       );
//     };

//     return (
//       <>
//         <AsyncSelect
//         //   isMulti
//           placeholder="Select Nationality"
//           loadOptions={loadOptions}
//           defaultOptions
//           value={selectedItem}
//           onChange={onChange}
//         />
//       </>
//     );
//   };

const UserInfo = (props) => {

  var prevData
  if(localStorage.getItem('userInfoData')) {
    var userInfoData = localStorage.getItem('userInfoData');
    prevData = JSON.parse(userInfoData);
  }
  console.log('prev data', prevData );
  const userData = {
    // age: "",
    dateOfBirth: prevData && prevData['user_business_country'] ? prevData['user_date_of_birth'] : '',
    gender: prevData && prevData['user_gender'] ? prevData['user_gender'] : '',
    occupation: prevData && prevData['user_occupation'] ? prevData['user_occupation'] : '',
    maritalStatus: prevData && prevData['user_marital_status'] ? prevData['user_marital_status'] : '',
    nationalities: prevData && prevData['user_country'] ? prevData['user_country'] : '',
    location: prevData && prevData['user_city'] ? prevData['user_city'] : '',
    province: prevData && prevData['user_state'] ? prevData['user_state'] : '',
    zipcode: prevData && prevData['user_zip_code'] ? prevData['user_zip_code'] : '',
    streetName: prevData && prevData['user_street_name'] ? prevData['user_street_name'] : '',
    streetNumber: prevData && prevData['user_street_number'] ? prevData['user_street_number'] : '',
    unit: prevData && prevData['user_apartment_number'] ? prevData['user_apartment_number'] : '',
  };

  

  //set initial state
  const { update, handleSubmit } = useForm({ defaultValues: userData });
  const [selectedNationalities, setSelectedNationalities] = useState([]);
  const [selectedItem, setSelectedItem] = useState('');
  const [cityLocation, setCityLocation] = useState('');
  // const [age, setAge] = useState(userData.age);
  const [dateOfBirth, setDateOfBirth] = useState(userData.dateOfBirth);
  const [occupation, setOccupation] = useState(userData.occupation);
  const [maritalStatus, setMaritalStatus] = useState(userData.maritalStatus);
  //const [options, setOptions] = useState(countryList().getData());
  const [nationalities, setNationalities] = useState(userData.nationalities);
  const [location, setLocation] = useState(userData.location);
  const [zipcode, setZipCode] = useState(userData.zipcode);
  const [streetName, setStreetName] = useState(userData.streetName);
  const [streetNumber, setStreetNumber] = useState(userData.streetNumber);
  const [unit, setUnit] = useState(userData.unit);
  const [province, setProvince] = useState(userData.province);
  const [submitAuthDisabled, setSubmitAuthDisabled] = useState(false);
  const [image, setImage] = useState({ preview: '', raw: '' });

  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const authModalContext = useContext(AuthModalContext);

  // Set search bar empty
  if (!props.location.state) {
    props.location.state = { keyword: '' };
  }

  // console.log(appContext.state.user.id)

  // Toggle nationality helper function
  // const toggleNationality = (nationalities) => {
  //     console.log(nationalities["label"])
  //     setNationalities(nationalities["label"])
  // };

  // const setNationalities = (value) => {
  //     console.log(nationalities);
  //     setNationalities(nationalities);
  //   }

  const [gender, setGender] = useState([
    { name: 'Male', isChecked: false },
    { name: 'Female', isChecked: false },
    { name: 'Other', isChecked: false },
  ]);

  const handleCheckChange = (checked, value) => {
    var genderList = gender.slice();
    genderList.map((gender) => {
      if (gender.name === value) {
        gender.isChecked = checked;
      } else {
        gender.isChecked = false;
      }
    });
    setGender(genderList);
  };

 
  const handleChange = (e) => {
    setImage({
      preview: URL.createObjectURL(e.target.files[0]),
      raw: e.target.files[0],
    });
      
  };

  const validateFields = () => {
    if (!occupation) {
      toast('Error! Please specify your occupation!', {
        type: 'error',
        autoClose: 6000,
      });
      return false;
    }
    if (!dateOfBirth) {
      toast('Error! Please specify your date of birth!', {
        type: 'error',
        autoClose: 6000,
      });
      return false;
    }
    //   var regex=/^[0-9]+$/;
    //   if (!age.match(regex)) {
    //     toast("Error! Age must be a number!", {
    //         type: "error",
    //         autoClose: 2000,
    //       });
    //       return false;
    //   }
    if (!maritalStatus) {
      toast('Error! Please specify your Marital Status!', {
        type: 'error',
        autoClose: 6000,
      });
      return false;
    }
    if (!nationalities) {
      toast('Error! Please specify your Country!', {
        type: 'error',
        autoClose: 6000,
      });
      return false;
    }
    if (!location) {
      toast('Error! Please specify your City!', {
        type: 'error',
        autoClose: 6000,
      });
      return false;
    }

    // if (!zipcode) {
    //   toast('Error! Please specify your postal code!', {
    //     type: 'error',
    //     autoClose: 2000,
    //   });
    //   return false;
    // }
    if (!streetName) {
      toast('Error! Please specify your street name', {
        type: 'error',
        autoClose: 6000,
      });
      return false;
    }

    return true;
  };

  const onSubmitProfileForm = async () => {
    if (validateFields()) {
      var result;
      try {
        if (image) {
          result = await fileUploadAsync(image.raw, 'dir');
        }
      } catch (error) {
        // toast('Error in Image Upload!', {
        //   type: 'error',
        //   autoClose: 2000,
        // });
        console.log(error);
      }
      const url = `/user/user-info/${appContext.state.user.id}`;
      const data = {
        // user_age: age,
        user_profile_image: !result ? '' : result.url, //if result is not null, then give result url
        user_date_of_birth: dateOfBirth,
        user_occupation: occupation,
        user_marital_status: maritalStatus,
        user_country: nationalities,
        user_city: location,
        user_zip_code: zipcode,
        user_street_name: streetName,
        user_street_number: streetNumber,
        user_apartment_number: unit,
        user_state: province,
        user_gender: '',
      };
      localStorage.setItem('userInfoData', JSON.stringify(data));
      console.log(data);
      gender.map((genderSelected) => {
        if (genderSelected.isChecked == true) {
          data['user_gender'] = genderSelected.name;
        }
      });

      // console.log(data);
      try {
        const response = await axios({ method: 'PUT', url, data });

        setTimeout(1000);

        if (response && response.data && response.data.success) {
          setTimeout(() => {
            window.location.href = '/complete-profile/preference';
          }, 2000);
          toast('Success! Your details have been updated!', {
            type: 'success',
            autoClose: 2000,
          });

          // setSubmitAuthDisabled(true);
        }
      } catch (error) {
        toast('Error! Something went wrong!', {
          type: 'error',
          autoClose: 2000,
        });
      }
    }
  };

  // Load next set of festivals helper functions
  const loadNextPage = async (page) => {
    const url = '/festival/all?';

    return axios({
      method: 'GET',
      url,
      params: {
        keyword: props.location.state.keyword,
        page: page + 1,
        nationalities: selectedNationalities,
        cityLocation,
      },
    });
  };

  const onPostalCodeSelected = (value) => {
    console.log(value);
    if (value.value.terms) {
      if (value.value.terms[0]) {
        console.log('here ', value.value.terms[0]);
        setLocation(value.value.terms[0].value);
      }
      if (value.value.terms[1]) {
        console.log('here ', value.value.terms[1]);
        setProvince(value.value.terms[1].value);
      }
      if (value.value.terms[2]) {
        setZipCode(value.value.terms[2].value);
      }
      if (value.value.terms[3]) {
        setNationalities(value.value.terms[3].value);
      }
    }
  };

  // Mount Login page
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  // Render User Info page
  return (
    <Fragment>
    <Nav/>

    <div className="row">
      <div className="col-lg-8 col-xl-6 px-0 sign-up-background-image-one"/>
      <div className="col-lg-4 col-xl-4 px-0 sign-up-background">
        <div className="sign-up-content">
          <div className="mb-4 text-center">
            <Link exact="true" to="/">
              <img src={tasttligLogoBlack} alt="Tasttlig" className="main-navbar-logo" />
            </Link>
          </div>

          {appContext.state.errorMessage && (
            <div className="mb-3 text-center invalid-message">{appContext.state.errorMessage}</div>
          )}

          <div className="heading-content">
            <span>Welcome! Let's start by creating your passport</span>&nbsp;
          </div>
          <div className="heading-caption">
            <span>Dont Worry !! You can always change the information later..... </span>&nbsp;
          </div>

          <div className="heading-moreInfo">
            <span>Tell us A Bit More About You </span>&nbsp;
          </div>
          <div className="row">
            <label htmlFor="upload-button">
              {image.preview ? (
                <img src={image.preview} alt="dummy" width="100" height="100" />
              ) : (
                <>
                  <span className="fa-stack fa-2x mt-3 mb-2">
                    <i className="fas fa-circle fa-stack-2x" />
                    <i className="fas fa-store fa-stack-1x fa-inverse" />
                  </span>
                  <h7 className="text-center">Upload your profile picture</h7>
                </>
              )}
            </label>
            <input
              type="file"
              id="upload-button"
              style={{ display: 'none' }}
              onChange={handleChange}
            />
            <br />
            {/* <button onClick={handleUpload}></button> */}
          </div>
          <div className="row">
            <div className="checkbox-title">Select your Gender</div>
            {gender.map((genderSelected) => {
              return (
                <div>
                  <label className="checkbox-preference">
                    <span className="checkbox-text">
                      {genderSelected.name}
                      <input
                        name="types"
                        type="checkbox"
                        checked={genderSelected.isChecked}
                        className="checkbox-text"
                        value={genderSelected.name}
                        onChange={(e) => handleCheckChange(e.target.checked, e.target.value)}
                      ></input>
                    </span>
                  </label>
                </div>
              );
            })}
          </div>
          <Form onSubmit={handleSubmit(onSubmitProfileForm)} noValidate>
            <Form.Row>
              {/* <Form.Group as={Col} controlId="formGridAge">
                        <Form.Control className="formGridStyle" type="email" placeholder="Enter Age" 
                        value={age}
                        onChange={(e) => setAge(e.target.value)} />
                        </Form.Group> */}

              <Form.Group as={Col} controlId="formGridDateofBirth">
                <DatePicker
                  className="custom-select1"
                  placeholderText="Enter you Date of Birth"
                  maxDate={new Date()}
                  selected={dateOfBirth}
                  onChange={(date) => setDateOfBirth(date)}
                  maxDate={new Date()}
                  showMonthDropdown
                  showYearDropdown
                  scrollableYearDropdown
                  yearDropdownItemNumber={50}
                />
              </Form.Group>

              <Form.Group as={Col} controlId="formGridOccupation">
                <Form.Control
                  className="formGridStyle"
                  type="text"
                  placeholder="What is your occupation?"
                  value={occupation}
                  onChange={(e) => setOccupation(e.target.value)}
                />
              </Form.Group>
            </Form.Row>
            <Form.Row>
              <Form.Group as={Col} controlId="formGridMarital">
                <Form.Control
                  as="select"
                  value={maritalStatus}
                  onChange={(e) => setMaritalStatus(e.target.value)}
                  custom
                >
                  <option key="blankChoice" hidden value="Marital Status">
                    {' '}
                    Marital Status
                  </option>
                  <option value="single">Single</option>
                  <option value="married">Married</option>
                  <option value="divorced">Divorced</option>
                  <option value="widowed">Widowed</option>
                </Form.Control>
              </Form.Group>

              <Form.Group as={Col} controlId="formGridUnit">
                <Form.Control
                  className="formGridStyle"
                  type="text"
                  placeholder="Unit Number"
                  value={unit}
                  onChange={(e) => setUnit(e.target.value)}
                />
              </Form.Group>
            </Form.Row>
            <Form.Row>
              <Form.Group as={Col} controlId="formGridStreetNumber">
                <Form.Control
                  className="formGridStyle"
                  type="text"
                  placeholder="Street Number"
                  value={streetNumber}
                  onChange={(e) => setStreetNumber(e.target.value)}
                />
              </Form.Group>

              <Form.Group as={Col} controlId="formGridStreetName">
                <Form.Control
                  className="formGridStyle"
                  type="text"
                  placeholder="Street Name"
                  value={streetName}
                  onChange={(e) => setStreetName(e.target.value)}
                />
              </Form.Group>
            </Form.Row>
            <Form.Row>
              <Form.Group className="mapsStyle">
              {/* <Form.Control
                  className="formGridStyle"
                  type="text"
                  placeholder="Postal Code"
                  value={zipcode}
                  onChange={(e) => setZipCode(e.target.value)}
                /> */}
                <Form.Label> Type Postal Code: </Form.Label> 
                <GooglePlacesAutocomplete
                  selectProps={{
                    styles: {
                      // input: (provided) => ({
                      //   ...provided,
                      //   color: 'blue',
                      // }),
                      // option: (provided) => ({
                      //   ...provided,
                      //   color: 'blue',
                      // }),
                      dropdownIndicator: (provided) => ({
                        ...provided,
                        visibility: 'hidden',
                      }),
                    },
                    placeholder: zipcode ? zipcode : 'Type in your postal code',
                    location,
                    onChange: onPostalCodeSelected,
                    value: 'location',
                    }}                     
                    
                ></GooglePlacesAutocomplete>
              </Form.Group>
            </Form.Row>
            <Form.Row>
              <Form.Group as={Col} controlId="formGridCity">
                <Form.Control
                  className="formGridStyle"
                  type="text"
                  placeholder="City"
                  value={location}
                  onChange={(e) => setLocation(e.target.value)}
                />
              </Form.Group>

              <Form.Group as={Col} controlId="formGridProvince">
                <Form.Control
                  className="formGridStyle"
                  type="text"
                  placeholder="Province"
                  value={province}
                  onChange={(e) => setProvince(e.target.value)}
                />
              </Form.Group>

              <Form.Group as={Col} controlId="formGridCountry">
                <Form.Control
                  className="formGridStyle"
                  type="text"
                  placeholder="Country"
                  value={nationalities}
                  onChange={(e) => setNationalities(e.target.value)}
                />
              </Form.Group>
            </Form.Row>
            <Form.Row></Form.Row>

            <div className="input-style-4">
              <button
                type="submit"
                disabled={authModalContext.state.submitAuthDisabled}
                className="log-in-btn"
              >
                Continue
              </button>
            </div>
          </Form>
        </div>
      </div>
      <div className="col-xl-2 px-0 login-background-image-two" />
    </div>
    <Footer />

    <GoTop scrollStepInPx="50" delayInMs="16.66" />
    </Fragment>
  );
};

export default UserInfo;
