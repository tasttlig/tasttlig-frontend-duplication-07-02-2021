// Libraries
import React, { useState } from 'react';
import LazyLoad from 'react-lazyload';
import Modal from 'react-modal';
import Flag from 'react-world-flags';

// Components
import MenuItemDetails from './MenuItemDetails';

const MenuItemCard = (props) => {
  // Set initial state
  const [load, setLoad] = useState(false);
  const [modalOpened, setModalOpened] = useState(false);
  const [isFullPageView] = useState(props.isFullPageView);

  // Menu item detail modal
  const MenuItemDetailModal = () => (
    <Modal
      isOpen={modalOpened}
      onRequestClose={() => setModalOpened(false)}
      ariaHideApp={false}
      className="product-details-modal"
    >
      <div className="text-right">
        <span onClick={() => setModalOpened(false)} className="fas fa-times fa-2x close-modal" />
      </div>

      <MenuItemDetails {...props} />

      <div className="close-modal-bottom-content">
        <span onClick={() => setModalOpened(false)} className="close-modal-bottom">
          Close
        </span>
      </div>
    </Modal>
  );

  // Render Menu Items Card
  return (
    <div
      className={
        isFullPageView
          ? 'col-sm-6 col-lg-4 col-xl-3 product-card'
          : 'col-md-6 col-xl-4 product-card'
      }
    >
      <LazyLoad once>
        <MenuItemDetailModal />

        <div className="d-flex flex-column h-100 card-box">
          <div className="mb-0 product-card-content">
            <div onClick={() => setModalOpened(true)} className="product-card-click">
              <img
                src={props.image_urls[0]}
                alt={props.title}
                onLoad={() => setLoad(true)}
                className={load ? 'product-image' : 'loading-image'}
              />
              <div className="passport-card-flag-container">
                <Flag code={props.alpha_2_code} className="passport-card-flag" />
                <div className="passport-card-flag-country">{props.nationality}</div>
              </div>
              <div className="product-card-text">
                <div className="pt-0 product-title">{props.title}</div>
                {/* <div className="product-business-name">
                  {props.business_name ? props.business_name : ""}
                </div> */}
              </div>
            </div>
            {/*<div>{`$${props.price}`}</div>*/}
            {/* <div className="product-card-details">
              {props.address}, {props.city}, {props.provinceTerritory}{" "}
              {props.postalCode}
            </div> */}
            {/* <div className="product-card-details">
              {moment(startDateTime).format("MMM Do")}{" "}
              {moment(startDateTime).format("hh:mm a")} -{" "}
              {moment(endDateTime).format("hh:mm a")}
              <br/>
              {props.frequency} until{" "}
              {endDate.format("MMM Do")}
            </div> */}
            {/* <div className="product-card-details">
              Quantity Available: {quantityAvailable}
            </div> */}
          </div>
          {props.children}
        </div>
      </LazyLoad>
    </div>
  );
};

export default MenuItemCard;
