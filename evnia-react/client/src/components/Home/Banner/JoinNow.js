// Libraries
import React, { useContext, useState } from 'react';
// import { Link } from "react-router-dom";
import { AppContext } from '../../../ContextProvider/AppProvider';
import { AuthModalContext } from '../../../ContextProvider/AuthModalProvider';

const JoinNow = (props) => {
  // To use the JWT credentials
  const appContext = useContext(AppContext);
  const authModalContext = useContext(AuthModalContext);
  const [btnColor] = useState(`btn-${props.color}`);

  return (
    <div>
      {/*{appContext.state.signedInStatus ? (*/}
      {/*  <div>*/}
      {/*    <div*/}
      {/*      onClick={authModalContext.handleSubmitTasttligFestivalGuest}*/}
      {/*      disabled={authModalContext.state.submitAuthDisabled}*/}
      {/*      className="btn btn-primary rsvp"*/}
      {/*    >*/}
      {/*      Join*/}
      {/*    </div>*/}
      {/*    /!* <Link to="/host" className="btn btn-secondary host-btn">*/}
      {/*      Host*/}
      {/*    </Link> *!/*/}
      {/*  </div>*/}
      {/*) : (*/}
      <div onClick={authModalContext.openModal('sign-up')} className={`btn join-now ${btnColor}`}>
        {props.text}
      </div>
      {/*)}*/}
    </div>
  );
};

export default JoinNow;
