// Libraries
import React from 'react';
import { Link } from 'react-router-dom';

// Styling
import kodidi from '../../../assets/images/kodidi.png';
import tasttligHost from '../../../assets/images/tasttlig-host.jpg';
import tasttligTalent from '../../../assets/images/tasttlig-talent.jpg';
import foodRounds from '../../../assets/images/food-rounds.jpg';

const OurProducts = () => {
  return (
    <div className="our-products">
      <div className="our-products-title">Our Products</div>

      <div className="kodidi-content">
        <div className="mb-3">
          <img src={kodidi} alt="Kodidi" />
        </div>
        <div className="kodidi-intro">Food From Around The World</div>
        <div className="kodidi-statement">Up To 100% Off All Day, Everyday</div>
        <div>
          <a
            href="https://kodidi.com"
            target="_blank"
            rel="noopener noreferrer"
            className="kodidi-go-eat"
          >
            Go Eat
          </a>
        </div>
      </div>

      <div className="our-products-section">
        <div className="row">
          <div className="col-lg-6 our-products-content">
            <div className="our-products-sub-title">Tasttlig Host</div>
          </div>
          <div className="col-lg-6 our-products-content"></div>
        </div>
        <div className="row">
          <div className="col-lg-6 our-products-content">
            <div className="text-center">
              <img src={tasttligHost} alt="Tasttlig Host" className="our-products-image" />
            </div>
          </div>
          <div className="col-lg-6 our-products-content">
            <div className="our-products-statement">
              Discover hosts who can create amazing experiences for you anywhere
            </div>
            <div className="our-products-learn-more-content">
              <Link exact="true" to="/host" className="our-products-learn-more">
                Learn More
              </Link>
            </div>
          </div>
        </div>
      </div>

      <div className="our-products-section">
        <div className="row">
          <div className="col-lg-6 our-products-content"></div>
          <div className="col-lg-6 our-products-content">
            <div className="our-products-sub-title">Tasttlig Talent</div>
          </div>
        </div>
        <div className="row flex-column-reverse flex-lg-row">
          <div className="col-lg-6 our-products-content-alternate">
            <div className="our-products-statement">
              Tasttlig helps you discover amazing talent and businesses in hospitality
            </div>
            <div className="our-products-learn-more-content">
              <a
                href="https://tasttligtalent.com"
                target="_blank"
                rel="noopener noreferrer"
                className="our-products-learn-more"
              >
                Learn More
              </a>
            </div>
          </div>
          <div className="col-lg-6 our-products-content">
            <div className="text-center">
              <img src={tasttligTalent} alt="Tasttlig Talent" className="our-products-image" />
            </div>
          </div>
        </div>
      </div>

      <div>
        <div className="row">
          <div className="col-lg-6 our-products-content">
            <div className="our-products-sub-title">Food Rounds</div>
          </div>
          <div className="col-lg-6 our-products-content"></div>
        </div>
        <div className="row">
          <div className="col-lg-6 our-products-content">
            <div className="text-center">
              <img src={foodRounds} alt="Food Rounds" className="our-products-image" />
            </div>
          </div>
          <div className="col-lg-6 our-products-content">
            <div className="our-products-statement">Discover emerging food businesses</div>
            <div className="our-products-learn-more-content">
              <a
                href="https://foodrounds.com"
                target="_blank"
                rel="noopener noreferrer"
                className="our-products-learn-more"
              >
                Learn More
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default OurProducts;
