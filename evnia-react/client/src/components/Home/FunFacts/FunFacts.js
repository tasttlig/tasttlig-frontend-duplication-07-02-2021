// Libraries
import React, { Component } from 'react';
import CountUp from 'react-countup';
import VisibilitySensor from 'react-visibility-sensor';

// Styling
import '../Home.scss';

export default class FunFacts extends Component {
  // Set initial state
  state = {
    didViewCountUp: false,
  };

  // Sponsors count helper function
  onVisibilityChange = (isVisible) => {
    if (isVisible) {
      this.setState({ didViewCountUp: true });
    }
  };

  // Render Fun Facts Component page
  render = () => {
    return (
      <section className="funfacts-area ptb-120">
        <div className="container">
          <div className="row">
            <div className="col-lg-9 col-30 col-sm-6">
              <div className="single-funfact">
                <div className="icon">
                  <i className="icofont-people"></i>
                </div>
                <h3 className="odometer">200</h3>
                <p>Total Hosts</p>
              </div>
            </div>

            <div className="col-lg-9 col-30 col-sm-6">
              <div className="single-funfact">
                <div className="icon">
                  <i className="icofont-ticket"></i>
                </div>
                <h3 className="odometer">200</h3>
                <p>Total Experience</p>
              </div>
            </div>

            <div className="col-lg-9 col-30 col-sm-6">
              <div className="single-funfact">
                <div className="icon">
                  <i className="icofont-microphone"></i>
                </div>
                <h3 className="odometer">200</h3>
                <p>Total Entertainment</p>
              </div>
            </div>

            <div className="col-lg-9 col-30 col-sm-6">
              <div className="single-funfact">
                <div className="icon">
                  <i className="icofont-businessman"></i>
                </div>
                <h3 className="odometer">
                  <VisibilitySensor
                    onChange={this.onVisibilityChange}
                    offset={{
                      top: 10,
                    }}
                    delayedCall
                  >
                    <CountUp start={0} end={this.state.didViewCountUp ? 100 : 0} duration={3} />
                  </VisibilitySensor>
                </h3>
                <p>Sponsors</p>
              </div>
            </div>

            <div className="col-lg-9 col-30 col-sm-6">
              <div className="single-funfact">
                <div className="icon">
                  <i className="icofont-users-social"></i>
                </div>
                <h3 className="guest">100000</h3>
                <p>Guests</p>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  };
}
