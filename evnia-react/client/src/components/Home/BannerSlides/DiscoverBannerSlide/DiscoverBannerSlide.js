// Libraries
import React from 'react';
import { Link } from 'react-router-dom';

const DiscoverBannerSlide = () => {
  // Render Discover Banner Slide
  return (
    <div className="banner-slide discover-banner-image">
      <div className="banner-section">
        <div className="banner-section-statement">
          Discover<br></br>Food From Around The World
        </div>
        {/* <div className="row">
          <div className="col-sm-12 explore-btn-content">
            <Link to="/discover" className="explore-btn">
              Explore
            </Link>
          </div>
        </div> */}
      </div>
    </div>
  );
};

export default DiscoverBannerSlide;
