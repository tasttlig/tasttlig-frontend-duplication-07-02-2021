// Libraries
import React from 'react';
import { Link } from 'react-router-dom';

const PassportBannerSlide = () => {
  // Render Passport Banner Slide
  return (
    <div className="banner-slide passport-banner-image">
      <div className="banner-section">
        <div className="banner-section-statement">
          Passport<br></br>Free Admissions To Experiences, Deals And Rewards
        </div>
        {/* <div className="row">
          <div className="col-sm-12 learn-more-btn-content">
            <Link to="#" className="learn-more-btn">
              Learn More
            </Link>
          </div>
        </div> */}
      </div>
    </div>
  );
};

export default PassportBannerSlide;
