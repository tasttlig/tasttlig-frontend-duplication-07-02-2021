// Libraries
import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

// Components
import Nav from '../Navbar/Nav';
import Introduction from './Introduction/Introduction';
import Create from './Celebrate/Create';
import Culture from './Celebrate/Culture';
import Customers from './Celebrate/Customers';
import Conclusion from './Conclusion/Conclusion';
import Footer from './BannerFooter/BannerFooter';

// Styling
import './Home.scss';

const Home = (props) => {
  // Source tracking on new sign ups
  const source = new URLSearchParams(props.location.search).get('source');
  if (source) {
    if (!localStorage.getItem('source')) {
      localStorage.setItem('source', source);
    }
  }

  // Set initial state
  const [navbarEffect, setNavbarEffect] = useState(true);

  // Mount Landing page
  useEffect(() => {
    window.scrollTo(0, 0);

    // Mount navbar styling effect
    window.addEventListener('scroll', () => {
      const isTop = window.scrollY < 1;

      if (isTop && <Link exact="true" to="/"></Link>) {
        setNavbarEffect(true);
      } else {
        setNavbarEffect(false);
      }
    });

    // Unmount navbar styling effect at scroll
    window.removeEventListener('scroll', () => {});
  }, []);

  // Render Landing page
  return (
    <div>
      <Nav navbarEffect={navbarEffect} />
      <Introduction />
      <Create />
      <Culture />
      <Customers />
      <Conclusion />
      <Footer />
    </div>
  );
};

export default Home;
