// Libraries
import React, { Component } from 'react';
// import { Link } from "react-router-dom";

// Styling
import '../Home.scss';

export default class WhyUs extends Component {
  // Render Why Us Component page
  render = () => {
    return (
      <section className="why-choose-us">
        <div className="row m-0">
          <div className="col-lg-3 col-sm-6 p-0">
            <div className="single-box">
              <div className="d-table">
                <div className="d-table-cell">
                  <div className="content">
                    <div className="icon">
                      <i className="icofont-food-cart"></i>
                    </div>
                    <h3>Food</h3>
                    <p>Multinational Food in Toronto.</p>

                    {/* <Link to="#" className="btn btn-primary">
                      Read More
                    </Link> */}
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="col-lg-3 col-sm-6 p-0">
            <div className="single-box">
              <div className="d-table">
                <div className="d-table-cell">
                  <div className="content">
                    <div className="icon">
                      <i className="icofont-headphone-alt-1"></i>
                    </div>
                    <h3>Music</h3>
                    <p>Multinational Music in Toronto.</p>

                    {/* <Link to="#" className="btn btn-primary">
                      Read More
                    </Link> */}
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="col-lg-3 col-sm-6 p-0">
            <div className="single-box">
              <div className="d-table">
                <div className="d-table-cell">
                  <div className="content">
                    <div className="icon">
                      <i className="icofont-paint"></i>
                    </div>
                    <h3>Art</h3>
                    <p>Multinational Art in Toronto.</p>

                    {/* <Link to="#" className="btn btn-primary">
                      Read More
                    </Link> */}
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="col-lg-3 col-sm-6 p-0">
            <div className="single-box">
              <div className="d-table">
                <div className="d-table-cell">
                  <div className="content">
                    <div className="icon">
                      <i className="icofont-game-controller"></i>
                    </div>
                    <h3>Games</h3>
                    <p>Multinational Trivial Games in Toronto.</p>

                    {/* <Link to="#" className="btn btn-primary">
                      Read More
                    </Link> */}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  };
}
