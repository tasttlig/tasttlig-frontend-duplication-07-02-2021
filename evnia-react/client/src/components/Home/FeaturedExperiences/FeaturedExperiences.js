// Libraries
import React, { useState, useEffect } from 'react';
import axios from 'axios';

// Components
import ExperiencesCard from '../../Experiences/ExperiencesCard/ExperiencesCard';

// Styling
import '../../Experiences/Experiences.scss';
import '../../Passport/Passport.scss';
import ShoppingCartHoverList from '../../ShoppingCart/ShoppingCartHoverList';

const FeaturedExperiences = (props) => {
  // Set initial state
  const [experienceItems, setExperienceItems] = useState([]);

  // Render Experience Cards helper function
  const renderExperienceCards = (arr) => {
    return arr
      .slice(0, 3)
      .map((card, index) => <ExperiencesCard key={card.experience_id} experiencesCard={card} />);
  };

  // GET experiences from the database and render on the page
  const fetchExperiences = async () => {
    try {
      const response = await axios({
        method: 'GET',
        url: '/experience/all',
      });

      return response;
    } catch (error) {
      return error.response;
    }
  };

  // Mount Experiences page
  useEffect(() => {
    window.scrollTo(0, 0);

    fetchExperiences().then(({ data }) => {
      setExperienceItems(data.details.data);
    });
  }, []);

  return (
    <div>
      <div className="experiences">
        <div className="mb-3 experiences-page-title">Featured Culinary Experiences</div>
        <div className="row">
          <div className="col-sm-12 p-0">
            <div className="experience-cards">{renderExperienceCards(experienceItems)}</div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FeaturedExperiences;
