// Libraries
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import lax from 'lax.js';

export default class LaxDiv extends Component {
  componentDidMount = () => {
    this.el = ReactDOM.findDOMNode(this);
    lax.addElement(this.el);
  };

  componentWillUnmount = () => {
    lax.removeElement(this.el);
  };

  render = () => {
    return (
      <div className="bg-title lax" data-lax-preset={this.props.dataPreset}>
        {this.props.text}
      </div>
    );
  };
}

LaxDiv.defaultProps = {
  text: 'Speakers',
  dataPreset: 'driftRight',
};
