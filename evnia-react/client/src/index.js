// Libraries
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { ShoppingReducer } from './redux/shoppingCart/reducer';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { PersistGate } from 'redux-persist/integration/react';
import { persistReducer, persistStore } from 'redux-persist';
import storage from 'redux-persist/lib/storage'; // defaults to localStorage

const persistConfig = { key: 'root', storage };
const persistedReducer = persistReducer(persistConfig, ShoppingReducer);
const store = createStore(persistedReducer);
const persistor = persistStore(store);

ReactDOM.render(
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <App />
    </PersistGate>
  </Provider>,
  document.getElementById('root'),
);
